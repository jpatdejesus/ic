<?php

$router->group(['prefix' => 'role', 'middleware' => ['jwt.auth.v1', 'permission.v1'], 'as' => 'module_role_management'], function () use ($router) {
    $router->post('', [
        'uses' => 'AdminRoleController@addRole',
        'name' => 'Admin Add Role',
        'description' => 'Adds new role',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->get('/get_role_groups', [
        'uses' => 'AdminRoleController@getRoleGroups',
        'name' => 'Admin Get Role Groups',
        'description' => 'Gets role groups',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN')
        ]
    ]);
    $router->get('', [
        'uses' => 'AdminRoleController@getRoles',
        'name' => 'Admin Get Roles',
        'description' => 'Gets list of roles',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->put('', [
        'uses' => 'AdminRoleController@updateRole',
        'name' => 'Admin Update Role',
        'description' => 'Updates role',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->delete('', [
        'uses' => 'AdminRoleController@deleteRole',
        'name' => 'Admin Delete Role',
        'description' => 'Deletes role',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->post('/duplicate', [
        'uses' => 'AdminRoleController@duplicateRoleAndPermissions',
        'name' => 'Admin Duplicate Role and Permissions',
        'description' => 'Duplicates role and permissions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->get('/role_and_permissions', [
        'uses' => 'AdminPermissionController@getRolePermissions',
        'name' => 'Admin Get User Role and Its Permissions',
        'description' => 'Gets role and permissions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->post('/add_or_update_role_permissions', [
        'uses' => 'AdminPermissionController@addOrUpdateRolePermissions',
        'name' => 'Add or Update Role Permissions',
        'description' => 'Adds or Updates Role Permissions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    $router->get('/get_user_role_and_permissions', [
        'uses' => 'AdminPermissionController@getUserRoleAndPermissions',
        'name' => 'Admin Get User Role and Its Permissions',
        'description' => 'Gets user role and permissions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->get('/get_roles_by_access_level', [
        'uses' => 'AdminPermissionController@getRolesByAccessLevel',
        'name' => 'Admin Roles By Access Level',
        'description' => 'Gets roles filtered by access level',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->get('/get_modules_and_permissions', [
        'uses' => 'AdminPermissionController@getModulesAndPermissions',
        'name' => 'Admin Get Modules And Permissions',
        'description' => 'Gets modules and permissions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
});