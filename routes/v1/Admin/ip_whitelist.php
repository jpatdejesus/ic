<?php
$router->group(['prefix' => 'ip', 'as' => 'module_ip_whitelist', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->get('is_whitelisted', [
        'uses' => 'IPWhitelistController',
        'name' => 'Is Whitelisted',
        'description' => 'Check if IP is whitelisted',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});
