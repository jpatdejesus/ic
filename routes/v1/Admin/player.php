<?php
$router->group(['middleware' => ['jwt.auth.v1', 'permission.v1'], 'prefix' => 'player', 'as' => 'module_player_inquiry'], function () use ($router) {
    $router->get('{userFilter}', [
        'uses' => 'PlayerInquiryController',
        'name' => 'Player Inquiry',
        'description' => 'Player Inquiry',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ],
    ]);
});

$router->group(['middleware' => ['jwt.auth.v1', 'permission.v1'], 'prefix' => 'game', 'as' => 'module_player_transactions'], function () use ($router) {
    $router->get('', [
        'uses' => 'GameController',
        'name' => 'Get Games',
        'description' => 'Gets Games',
        'linked_route' => 'Get Player Transactions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ],
    ]);
});

$router->group(['middleware' => ['jwt.auth.v1', 'permission.v1'], 'prefix' => 'player', 'as' => 'module_player_transactions'], function () use ($router) {
    $router->post('transaction[/{userFilter}]', [
        'uses' => 'PlayerTransactionController',
        'name' => 'Get Player Transactions',
        'description' => 'Gets Player Transactions',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ],
    ]);
});
