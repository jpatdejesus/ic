<?php
$router->group(['middleware' => ['jwt.auth.v1', 'permission.v1'], 'as' => 'module_audit_trails'], function () use ($router) {

    $router->post('/audits/logs', [
        'uses' => 'AuditTrailsController@index',
        'name' => 'Admin Tool Logs',
        'description' => 'Audit Trails of a user.',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->post('/ichips/logs', [
        'uses' => 'AuditTrailsController@iChipsApiLogs',
        'name' => 'Ichips Api Logs',
        'description' => 'IChips logs of a user.',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->get('/end_points', [
        'uses' => 'AuditTrailsController@getAllRoutes',
        'name' => 'Get Endpoints',
        'linked_route' => 'Ichips Api Logs',
        'description' => 'Get endpoints',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});
