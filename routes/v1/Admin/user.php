<?php
$router->group(['prefix' => 'user', 'as' => 'module_account_management', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->post('', [
        'uses' => 'AdminUserController@index',
        'name' => 'Get List Of Users',
        'description' => 'Get list of users',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->post('/create', [
        'uses' => 'AdminUserController@create',
        'name' => 'Create New User',
        'description' => 'Creates New User Record',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->get('/update/{id}', [
        'uses' => 'AdminUserController@show',
        'name' => 'Get Specific User Details',
        'linked_route' => 'Update User Details',
        'description' => 'Getting specific user record',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->put('/update/{id}', [
        'uses' => 'AdminUserController@update',
        'name' => 'Update User Details',
        'description' => 'Updating user record',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->delete('/disable/{id}', [
        'uses' => 'AdminUserController@disable',
        'name' => 'Delete User',
        'description' => 'Deleting user record',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);
    
});

$router->group(['prefix' => 'user', 'as' => 'module_profile_management', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->get('/profile', [
        'uses' => 'AdminUserController@showProfile',
        'name' => 'Get User Profile',
        'description' => 'Getting user profile',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->put('/profile', [
        'uses' => 'AdminUserController@updateProfile',
        'name' => 'Update User Profile',
        'description' => 'Updating user profile',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->get('/profile/image', [
        'uses' => 'AdminUserController@getProfilePictureUrl',
        'name' => 'Get User Profile Picture',
        'description' => 'Getting user profile picture',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->post('/profile/image', [
        'uses' => 'AdminUserController@uploadProfilePicture',
        'name' => 'Update User Profile Picture',
        'description' => 'Updating user profile picture',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});
