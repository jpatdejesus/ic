<?php
$router->group(['middleware' => ['jwt.auth.v1', 'ip_whitelist.v1', 'permission.v1'], 'prefix' => 'balance', 'as' => 'module_balance_adjustments'], function () use ($router) {
    $router->post('player/{username}', [
        'uses' => 'BalanceController@updateBalanceRequest',
        'name' => 'Update Balance',
        'description' => 'Update balance',
        'middleware' => ['validate_username_param.v1'],
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->post('batch/update', [
        'uses' => 'BalanceController@batchUpdateBalance',
        'name' => 'Batch Update Balance',
        'description' => 'Batch Update balance',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->get('player/{userFilter}', [
        'uses' => 'BalanceController@getPlayer',
        'name' => 'Search Player Update Balance',
        'linked_route' => 'Update Balance',
        'description' => 'Search player update balance',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->get('player/check/{username}', [
        'uses' => 'BalanceController@checkHoursIfValidToRequest',
        'name' => 'Check Valid Balance Request Hours',
        'linked_route' => 'Update Balance',
        'description' => 'Check valid balance request hours',
        'middleware' => ['validate_username_param.v1'],
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);

    $router->get('requests', [
        'uses' => 'BalanceController@getBalanceUpdateRequests',
        'name' => 'Update Balance Requests List',
        'description' => 'Update balance requests list',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});

$router->group(['middleware' => ['jwt.auth.v1', 'ip_whitelist.v1', 'permission.v1'], 'prefix' => 'balance', 'as' => 'module_update_balance'], function () use ($router) {
    $router->put('approve/{id}', [
        'uses' => 'BalanceController@approve',
        'name' => 'Approve Update Balance Request',
        'description' => 'Approve update balance request',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);

    $router->put('reject/{id}', [
        'uses' => 'BalanceController@reject',
        'name' => 'Reject Update Balance Request',
        'description' => 'Reject update balance request',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
        ]
    ]);

    $router->post('batch/update', [
        'uses' => 'BalanceController@batchUpdateBalance',
        'name' => 'Batch Update Balance',
        'description' => 'Batch update balance',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN')
        ]
    ]);
});

$router->group(['middleware' => ['jwt.auth.v1', 'ip_whitelist.v1'], 'prefix' => 'balance', 'as' => 'module_balance_adjustments'], function () use ($router) {

    $router->get('category', [
        'uses' => 'BalanceController@getAllCategory',
        'name' => 'Get All Balance Category',
        'description' => 'Get all balance category',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});