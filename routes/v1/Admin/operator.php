<?php

$router->group(['prefix' => 'operator', 'as' => 'module_operator_inquiry', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->post('', [
        'uses' => 'OperatorInquiryController',
        'name' => 'Operator Inquiry',
        'description' => 'Get specific operator details',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});