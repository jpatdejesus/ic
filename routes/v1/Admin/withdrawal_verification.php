<?php
$router->group(['prefix' => 'withdrawal_verification', 'as' => 'module_withdrawal_verification', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->post('black_list', [
        'uses' => 'WithdrawalBlackListController@getBlackList',
        'name' => 'Get Black List',
        'description' => 'Gets Withdrawal Black Lists',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->post('white_list', [
        'uses' => 'WithdrawalWhiteListController@getWhiteList',
        'name' => 'Get White List',
        'description' => 'Gets Withdrawal White Lists',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->post('add_user_to_white_list', [
        'uses' => 'WithdrawalBlackListController@addUserToWhitelist',
        'name' => 'Add User From Black List To White List',
        'description' => 'Adds User From Black List to White List',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
    $router->post('white_list_requests', [
        'uses' => 'WithdrawalWhiteListController@getWhiteListRequests',
        'name' => 'Get White List Requests',
        'description' => 'Gets All White List Requests',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->put('approve_or_reject_white_list_request/{request_id}/status/{status}', [
        'uses' => 'WithdrawalWhiteListController@approveOrRejectWhiteListRequest',
        'name' => 'Approve Or Reject White List Request',
        'description' => 'Approves or Rejects Pending White List Request',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
        ]
    ]);
    $router->post('get_rejected_logs', [
        'uses' => 'WithdrawalWhiteListController@getRejectedLogs',
        'name' => 'Get Rejected Requests Log',
        'description' => 'Gets All Rejected Requests Log',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});
