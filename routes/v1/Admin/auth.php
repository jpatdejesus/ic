<?php

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->put('/token', [
        'uses' => 'AdminAuthController@token',
    ]);
});

$router->group(['prefix' => 'auth', 'middleware' => 'jwt.auth.v1'], function () use ($router) {
    $router->post('/token', [
        'uses' => 'AdminAuthController@refreshToken',
    ]);

    $router->delete('/token', [
        'uses' => 'AdminAuthController@destroyToken',
    ]);

    $router->get('/token', [
        'uses' => 'AdminAuthController@validateToken',
    ]);
});