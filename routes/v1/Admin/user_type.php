<?php
$router->group(['prefix' => 'user_type', 'middleware' => ['jwt.auth.v1']], function () use ($router) {
    $router->get('', [
        'uses' => 'UserTypeController'
    ]);
});
