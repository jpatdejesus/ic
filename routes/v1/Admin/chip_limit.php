<?php
$router->group(['prefix' => 'chips', 'as' => 'module_chip_limit_inquiry', 'middleware' => ['jwt.auth.v1', 'permission.v1']], function () use ($router) {
    $router->post('', [
        'uses' => 'GetChipLimitInquiryController',
        'name' => 'Chip Limit Inquiry',
        'description' => 'Chip Limit Inquiry of a user.',
        'default' => [
            config('ichips.admin_default_roles_and_users.SUPER_ADMIN'),
            config('ichips.admin_default_roles_and_users.ADMIN'),
            config('ichips.admin_default_roles_and_users.AUDIT'),
        ]
    ]);
});
