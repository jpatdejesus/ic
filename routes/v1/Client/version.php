<?php

$router->get('version', function () {
    return response(config('ichips.version'));
});


$router->get('server-time-difference', function () {
    return [
        'server_timezone' => \Carbon\Carbon::now()->timezone,
        'config_server_timezone' => config('ichips.server_timezone'),
        'config_app_timezone' => config('app.timezone'),
        'server_time' => \Carbon\Carbon::now(),
        'st_to_manila_time' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now())->setTimezone('Asia/Manila'),
        'st_to_utc' => to_utc_tz(\Carbon\Carbon::now()),
        'minus_day_1' => to_utc_tz(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now())->setTimezone('Asia/Manila')->subDay()->toDateTimeString()),
    ];
});
