<?php

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->put('/token', [
        'uses' => 'AuthController@token',
    ]);
});

$router->group(['prefix' => 'auth', 'middleware' => 'jwt.auth.v1'], function () use ($router) {
    $router->post('/token', [
        'uses' => 'AuthController@refreshToken',
    ]);

    $router->delete('/token', [
        'uses' => 'AuthController@destroyToken',
    ]);

    $router->get('/token', [
        'uses' => 'AuthController@validateToken',
    ]);
});