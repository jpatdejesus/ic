<?php

$router->group(['prefix' => 'history', 'middleware' => ['jwt.auth.v1', 'permission.v1'], 'as' => 'module_history'], function () use ($router) {
    $router->group(['middleware' => ['validate_username_param.v1', 'validate_game_code_param.v1']], function () use ($router) {
        $router->post('/transaction/{username}', [
            'uses' => 'TransactionController@create',
            'name' => 'Insert Transaction',
            'description' => 'Insert a Transaction record.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.OTHERS'),
            ]
        ]);

        $router->get('/transaction/{username}', [
            'uses' => 'TransactionController@getTransaction',
            'name' => 'Get Transaction',
            'description' => 'Get a transaction information from a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->get('/transfer/{username}', [
            'uses' => 'TransferController@getTransfer',
            'name' => 'Get Transfer',
            'description' => 'Get a transfer information from a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);
    });

    $router->group(['middleware' => ['validate_game_code_param.v1']], function () use ($router) {
        $router->get('/gameresult', [
            'uses' => 'GameResultController@getGameResult',
            'name' => 'Get Game Result',
            'description' => 'Get a game result information of a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
            ]
        ]);
        $router->post('/gameresult', [
            'uses' => 'GameResultController@create',
            'name' => 'Insert Game Result',
            'description' => 'Insert a game result information of a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
                config('ichips.default_roles.OTHERS'),
            ]
        ]);
    });

    $router->group(['middleware' => ['validate_nullable_username_param.v1']], function () use ($router) {
        $router->get('/transaction', [
            'uses' => 'TransactionController@getAllTransaction',
            'name' => 'Get Transaction',
            'description' => 'Get a transaction information from a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->get('/transfer', [
            'uses' => 'TransferController@getAllTransfer',
            'name' => 'Get Transfer',
            'description' => 'Get a transfer information from a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->get('chipflow', [
            'uses' => 'ChipFlowController@getAllChipFlow',
            'name' => 'Get Chip Flow',
            'description' => 'Get a chip flow information of a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
                config('ichips.default_roles.GET'),
            ]
        ]);

        $router->get('chipflow/{username}', [
            'uses' => 'ChipFlowController@getChipFlow',
            'name' => 'Get Chip Flow',
            'description' => 'Get a chip flow information of a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
                config('ichips.default_roles.GET'),
            ]
        ]);
    });
    
    $router->group(['middleware' => ['validate_username_param.v1']], function () use ($router) {
        $router->get('/transactions/{username}', [
            'uses' => 'TransactionController@getAllTableTransactions',
            'name' => 'Get Transaction',
            'description' => 'Get all table transaction information from a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->post('/validate-transfer/{username}', [
            'uses' => 'TransferController@validateTransfer',
            'name' => 'Insert Transaction',
            'description' => 'Insert a Transaction record.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);
    });
});
