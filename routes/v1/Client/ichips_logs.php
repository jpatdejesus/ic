<?php
$router->group(['prefix' => 'ichips','middleware' => ['jwt.auth.v1']], function () use ($router) {

	$router->post('/logs/', [
        'uses' => 'IChipsLogsController',
        'name' => 'IChips Logs',
        'description' => 'IChips logs of a user.',
	]);
});