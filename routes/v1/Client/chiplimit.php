<?php

$router->group(['prefix' => 'chiplimit', 'middleware' => ['jwt.auth.v1', 'permission.v1', 'validate_username_param.v1'], 'as' => 'module_chip_limit'], function () use ($router) {
    $router->put('/{username}', [
        'uses' => 'UpdateUserChipLimitController',
        'name' => 'Update Chip Limit',
        'description' => 'Update chip limits of a player.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
        ]
    ]);

    $router->get('/{username}', [
        'uses' => 'GetChipLimitController',
        'name' => 'Get Chip Limit',
        'description' => 'Getting chip limit of a table.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
            config('ichips.default_roles.GET'),
            config('ichips.default_roles.OTHERS'),
        ]
    ]);
});
