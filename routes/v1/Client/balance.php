<?php
$router->group(['prefix' => 'balance', 'middleware' => ['jwt.auth.v1', 'permission.v1', 'validate_username_param.v1'], 'as' => 'module_balance'], function () use ($router) {

    $router->post('/payout/{username}', [
            'uses' => 'PayoutBulkBalanceController',
            'name' => 'Payout Balance',
            'description' => 'Payout to a user.',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
                config('ichips.default_roles.OTHERS'),
            ]
        ]);

    $router->post('/withdraw/{username}', [
        'uses' => 'WithdrawBalanceController',
        'name' => 'Withdraw Balance',
        'description' => 'Withdraw from a user.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
        ]
    ]);

    $router->post('/deposit/{username}', [
        'uses' => 'DepositBalanceController',
        'name' => 'Deposit Balance',
        'description' => 'Deposit to a user.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
        ]
    ]);

    $router->post('/bet/{username}', [
        // 'uses' => 'BetBalanceController',
        'uses' => 'BetBulkBalanceController',
        'name' => 'Bet Balance',
        'description' => 'Bet Balance of a user.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
            config('ichips.default_roles.OTHERS'),
        ]
    ]);


    $router->get('/{username}', [
        'uses' => 'GetBalanceController',
        'name' => 'Get Balance',
        'description' => 'Getting balance amount from a user.',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
            config('ichips.default_roles.GET'),
            config('ichips.default_roles.OTHERS'),
        ]
    ]);
});
