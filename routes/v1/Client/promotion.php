<?php

$router->group(['prefix' => 'promotion', 'middleware' => ['jwt.auth.v1', 'permission.v1'], 'as' => 'module_promotion'], function () use ($router) {
    $router->post('/filter', [
        'name' => 'Filter Transactions And Game Result',
        'uses' => 'PromotionController@filterExport',
        'description' => 'Filter Transactions and Game Result to be use for promotion',
         'default' => [
             config('ichips.default_roles.GMT'),
         ]
    ]);
});