<?php
$router->group(['prefix' => 'user', 'middleware' => ['jwt.auth.v1', 'permission.v1'], 'as' => 'module_user'], function () use ($router) {
    $router->post('', [
        'uses' => 'UserController@create',
        'name' => 'Create User',
        'description' => 'Creating a user',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
            config('ichips.default_roles.GENERAL'),
        ]
    ]);

    $router->get('search/{username}', [
        'uses' => 'UserController@search',
        'name' => 'User Search',
        'description' => 'Search a user',
        'default' => [
            config('ichips.default_roles.SUPER_ADMIN'),
            config('ichips.default_roles.ADMIN'),
        ]
    ]);

    $router->group(['middleware' => ['validate_username_param.v1']], function () use ($router) {
        $router->get('{username}', [
            'uses' => 'UserController@show',
            'name' => 'Get User Info',
            'description' => 'Getting information of a user',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
                config('ichips.default_roles.GET'),
                config('ichips.default_roles.OTHERS'),
            ]
        ]);

        $router->put('/{username}', [
            'uses' => 'UserController@update',
            'name' => 'Update User Info',
            'description' => 'Update user information',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->delete('/{username}', [
            'uses' => 'UserController@disable',
            'name' => 'Disable User',
            'description' => 'Disable status of a user',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
                config('ichips.default_roles.GENERAL'),
            ]
        ]);

        $router->put('/point/{username}', [
            'uses' => 'UpdateUserPointController',
            'name' => 'Update User Points',
            'description' => 'Update points of a user',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
            ]
        ]);

        $router->put('/exp/{username}', [
            'uses' => 'UpdateUserExpController',
            'name' => 'Update User Experience',
            'description' => 'Update experience of a user',
            'default' => [
                config('ichips.default_roles.SUPER_ADMIN'),
                config('ichips.default_roles.ADMIN'),
            ]
        ]);
    });
});
