<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    public function setUp()
    {
        parent::setUp();

        if (DB::connection()->getDriverName() === 'sqlite') {
            $this->setupDatabase();
        }
    }

    public function tearDown()
    {
        foreach (config('database.connections') as $key => $connection) {
            DB::connection($key)->setPdo(null);
        }

        parent::tearDown();
    }

    /**
     * @param string $requestAbstract
     * @param array $data
     */
    public function mockUserData(string $requestAbstract, $data = [])
    {
        $user = factory(\App\Models\v1\AdminUser::class)->create();

        $requestMock = $this->getMockBuilder($requestAbstract)
            ->setConstructorArgs([$data])
            ->setMethods(array('userData'))
            ->getMock();

        $requestMock->expects($this->any())
            ->method('userData')
            ->will($this->returnValue($user));

        $this->app->instance($requestAbstract, $requestMock);

        return $user;
    }

    /**
     * @param $userType
     */
    protected function createMockUsersPHPTableIfNotExist()
    {
        if (Schema::connection('ichips')->hasTable('users_php')) {
            return;
        }

        Schema::connection('ichips')->create('users_php', function (Blueprint $table) {
            $table->increments('userid');
            $table->string('UserName');
            $table->string('TrueName');
            $table->string('userpass');
            $table->string('pre_sequence');
            $table->string('winPoint');
            $table->string('moneysort');
            $table->dateTime('last_login')->nullable();
            $table->decimal('curMoney1', 50, 5)->default(0.00000);
        });
    }

    /**
     * @param $userType
     */
    protected function createMockTransferPHPTableIfNotExist()
    {
        if (Schema::connection('datarepo')->hasTable('transfers_php')) {
            return;
        }

        Schema::connection('datarepo')->create('transfers_php', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transfer_id', 50)->nullable();
            $table->string('op_transfer_id', 100)->unique();
            $table->string('username', 50);
            $table->char('currency_code', 3);
            $table->string('actions', 10);
            $table->decimal('amount', 50, 5);
            $table->decimal('balance', 50, 5);
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * @param $userType
     */
    protected function createMockCashflowPHPTableIfNotExist()
    {
        if (Schema::connection('datarepo')->hasTable('cashflow_php')) {
            return;
        }

        Schema::connection('datarepo')->create('cashflow_php', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("username", 32);
            $table->string("game_code", 32)->nullable();
            $table->string("trans_type", 45);
            $table->string("path", 300)->nullable();
            $table->string("betting_code", 100)->nullable()->comment('Handles betting_code if BET/PAYOUT or transfer_id if WITHDRAW/DEPOSIT or NULL');
            $table->decimal("trans_amount", 50, 5);
            $table->decimal("balance", 50, 5);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function userTypes()
    {
        $userTypes = config('ichips.user_types');
        $return = [];

        foreach ($userTypes as $key => $value) {
            $return[] = [
                $value['users']['model'],
                $key
            ];
        }
        return $return;
    }

    public function setupDatabase() {
        $testingPath = database_path('testing');
        $stubsPath = database_path('stubs');

        $file = new \Illuminate\Filesystem\Filesystem();
        $file->copyDirectory($stubsPath, $testingPath);
    }
}
