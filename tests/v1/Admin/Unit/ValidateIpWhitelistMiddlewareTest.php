<?php

use Illuminate\Support\Facades\Config;

class ValidateIpWhitelistMiddlewareTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $router = $this->app->router;
        $this->app->router->group(['middleware' => 'ip_whitelist.v1'], function () use ($router) {
            $router->get('/_test/testing_whitelist', function () {
                return response('OK');
            });
        });
    }

    public function tearDown()
    {
        parent::tearDown();

        unset($_SERVER['REMOTE_ADDR']);
    }

    /**
     * @test
     */
    public function it_should_not_access_ip_whitelist_routes()
    {
        $_SERVER['REMOTE_ADDR'] = '8.8.8.8';

        Config::set('ichips.ip_whitelist', [
            '172.21.0.1'
        ]);

        $response = $this->get('/_test/testing_whitelist');

        $response->assertResponseStatus(403);
    }

    /**
     * @test
     */
    public function it_should_access_ip_whitelist_routes()
    {
        $_SERVER['REMOTE_ADDR'] = '8.8.8.8';

        Config::set('ichips.ip_whitelist', [
            '8.8.8.*'
        ]);

        $response = $this->get('/_test/testing_whitelist');

        $response->assertResponseStatus(200);
    }
}
