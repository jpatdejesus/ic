<?php

use App\Traits\AuthToken;

class UpdateProfileFunctionTest extends TestCase
{
    use AuthToken;

    /**
     * @test
     */
    public function it_should_successfully_update_profile()
    {
        $response = $this->get('/v1/admin/user/profile', $this->getNewToken());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);

        $response = $this->put('/v1/admin/user/profile', $this->createValidParams(), $this->getNewToken());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_nickname()
    {
        $field = 'nickname';

        $validErrorResponse = [
            "The nickname field is required.",
            "The nickname may not be greater than 50 characters.",
            "The nickname field is invalid."
        ];

        /**
         * Test if nickname is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if nickname is valid format
         */
        $this->assertions($validErrorResponse, $field, 'nickname%^');

        /**
         * Test if nickname does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_current_password()
    {
        $field = 'current_password';

        $validErrorResponse = [
            "The current password field is invalid.",
        ];

        /**
         * Test if current password is different from the saved password
         */
        $this->assertions($validErrorResponse, $field, 'pass12345!');
    }

    /**
     * @test
     */
    public function it_should_test_invalid_password()
    {
        $field = 'password';

        $validErrorResponse = [
            "The password must be at least 8 characters.",
            "The password may not be greater than 50 characters.",
            "The password field is invalid.",
            "The password confirmation does not match.",
            "The password field cannot be the same with your current password."
        ];

        /**
         * Test if password is not the same with the currently saved password
         */
        $this->assertions($validErrorResponse, $field, 'password12345!');

        /**
         * Test if password is valid format
         */
        $this->assertions($validErrorResponse, $field, 'pass%^+');

        /**
         * Test if password is in minimum character length
         */
        $this->assertions($validErrorResponse, $field, 'pass1!');

        /**
         * Test if password does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));

        /**
         * Test if password confirmation is the same with parameter password
         */
        $this->assertions($validErrorResponse, $field, 'pass12345!');
    }

    public function createValidParams()
    {
        return [
            'nickname' => generate_random_string(8),
            'current_password' => 'password123!',
            'password' => 'password1234!',
            'password_confirmation' => 'password1234!',
        ];
    }

    public function createInvalidParam($testField, $value = null)
    {
        $return = [
            'nickname' => generate_random_string(8),
            'current_password' => 'password123!',
            'password' => 'password1234!',
            'password_confirmation' => 'password1234!',
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }

    public function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->put('/v1/admin/user/profile', $this->createInvalidParam($field), $this->getNewToken());
        } else {
            $response = $this->put('/v1/admin/user/profile', $this->createInvalidParam($field, $param), $this->getNewToken());
        }

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);

        $content = json_decode($response->response->getContent());

        $this->assertSame('Validation error!', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }

    public function getToken()
    {
        $credentials = [
            'email' => 'superadmin@nomail.com',
            'password' => 'password123!'
        ];

        $response = $this->put('/v1/admin/auth/token', $credentials);
        $content = json_decode($response->response->getContent());

        return [
            'Token' => $content->data->token
        ];
    }

    public function getNewToken()
    {
        $adminUser = factory(\App\Models\v1\AdminUser::class)->create();
        $adminUserRole = (new \App\Models\v1\AdminUserRole)->create([
            'user_id' => $adminUser->id,
            'role_id' => 1,
        ]);
        $adminUser->password = 'password123!';

        return ['token' => $this->loginAs($adminUser, strtoupper(config('ichips.app_types.admin')))->token];
    }
}
