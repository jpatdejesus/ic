<?php

class GetRoleAndPermissionsFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should get role and permissions.
     * @test
     * @return void
     */
    public function it_should_get_role_and_permissions_on_admin_app_type()
    {
        $response = $this->get('v1/admin/role/role_and_permissions?role_group=ADMIN&request_app_type=ADMIN', $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should get role and permissions.
     * @test
     * @return void
     */
    public function it_should_get_role_and_permissions_on_client_app_type()
    {
        $response = $this->get('v1/admin/role/role_and_permissions?role_group=ADMIN&request_app_type=CLIENT', $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should validate app type.
     * @test
     * @return void
     */
    public function it_should_validate_app_type()
    {
        $response = $this->get('v1/admin/role/role_and_permissions?role_group=ADMIN&request_app_type=NON_EXISTING_APP_TYPE', $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should validate role group.
     * @test
     * @return void
     */
    public function it_should_validate_role_group()
    {
        $response = $this->get('v1/admin/role/role_and_permissions?role_group=NON_EXISTING_ROLE_GROUP&request_app_type=ADMIN', $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }
}
