<?php

class WithdrawalVerificationFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;
    public $csToken;

    public function setUp()
    {
        parent::setUp();

        $csUser = \App\Models\v1\AdminUser::where('email', 'cs@nomail.com')->first();
        $csUser->password = 'password123!';

        $this->csToken = ['token' => $this->loginAs($csUser)->token];
        $this->token = $this->login();
    }

    /**
     * @group ignore
     * @test
     * @return void
     */
    public function it_should_get_withdrawal_rejected_logs()
    {
        $response = $this->post('v1/admin/withdrawal_verification/get_rejected_logs', [], $this->token);
        $response->assertResponseStatus(200);
    }

    /**
     * @group ignore
     * @test
     * @return void
     */
    public function it_should_get_black_listed_users()
    {
        $response = $this->post('v1/admin/withdrawal_verification/black_list', [], $this->token);
        $response->assertResponseStatus(200);
    }

    /**
     * @group ignore
     * @test
     * @return void
     */
    public function it_should_get_white_listed_users()
    {
        $response = $this->post('v1/admin/withdrawal_verification/white_list', [], $this->token);
        $response->assertResponseStatus(200);
    }

    /**
     * @group ignore
     * @test
     * @return void
     */
    public function it_should_get_white_list_requests()
    {
        $response = $this->post('v1/admin/withdrawal_verification/white_list_requests', [], $this->token);
        $response->assertResponseStatus(200);
    }

    /**
     * @test
     * @dataProvider userTypes
     * @return void
     */
    public function it_should_add_user_to_white_list($userModel, $userType)
    {
        $createParams = [
            'username' => 'test_' . str_random(),
            'user_type' => $userType,
            'remark' => 'Test Remark Added By Super Admin',
        ];

        $blackListUser = (new \App\Models\v1\WithdrawBlackList())->create($createParams);

        $response = $this->post('v1/admin/withdrawal_verification/add_user_to_white_list', ['id' => $blackListUser->id], $this->token);

        $response->assertResponseStatus(200);

        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * @test
     * @dataProvider userTypes
     * @return void
     */
    public function it_should_add_user_to_white_list_requests($userModel, $userType)
    {
        $createParams = [
            'username' => 'test_' . str_random(),
            'user_type' => $userType,
            'remark' => 'Test Remark Added By CS',
        ];

        $blackListUser = (new \App\Models\v1\WithdrawBlackList())->create($createParams);

        $response = $this->post('v1/admin/withdrawal_verification/add_user_to_white_list', ['id' => $blackListUser->id], $this->csToken);

        $response->assertResponseStatus(200);

        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * @test
     * @dataProvider userTypes
     * @return void
     */
    public function it_should_add_user_to_white_list_requests_and_approve_it($userModel, $userType)
    {
        $createParams = [
            'username' => 'test_' . str_random(),
            'user_type' => $userType,
            'remark' => 'Test Remark Added By CS and Approved By Super Admin',
        ];

        $blackListUser = (new \App\Models\v1\WithdrawBlackList())->create($createParams);

        $whiteListRequest = $this->post('v1/admin/withdrawal_verification/add_user_to_white_list', ['id' => $blackListUser->id], $this->csToken);

        $whiteListRequestData = json_decode($whiteListRequest->response->getContent())->data;

        $response = $this->put('v1/admin/withdrawal_verification/approve_or_reject_white_list_request/' . $whiteListRequestData->id . '/status/APPROVED', [], $this->token);

        $response->assertResponseStatus(200);

        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * @test
     * @dataProvider userTypes
     * @return void
     */
    public function it_should_add_user_to_white_list_requests_and_reject_it($userModel, $userType)
    {
        $createParams = [
            'username' => 'test_' . str_random(),
            'user_type' => $userType,
            'remark' => 'Test Remark Added By CS and Rejected By Super Admin',
        ];

        $blackListUser = (new \App\Models\v1\WithdrawBlackList())->create($createParams);

        $whiteListRequest = $this->post('v1/admin/withdrawal_verification/add_user_to_white_list', ['id' => $blackListUser->id], $this->csToken);

        $whiteListRequestData = json_decode($whiteListRequest->response->getContent())->data;

        $response = $this->put('v1/admin/withdrawal_verification/approve_or_reject_white_list_request/' . $whiteListRequestData->id . '/status/REJECTED', [], $this->token);

        $response->assertResponseStatus(200);

        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }
}
