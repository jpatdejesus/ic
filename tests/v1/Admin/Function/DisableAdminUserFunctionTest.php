<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class DisableAdminUserFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * If the user have permission to access the url and have valid parameters it will get a HTTP status code 200 (OK).
     *
     * @test
     */
    public function it_should_successfully_disable_the_admin_user()
    {
        $adminUser = factory(\App\Models\v1\AdminUser::class)->create();

        $response = $this->delete('/v1/admin/user/disable/' . $adminUser->id);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * Else if the user has permission but sent an invalid request they will get a
     * HTTP status code 422 (Unprocessable Entity) or HTTP status code 400(Bad Request)
     *
     * @test
     */
    public function it_should_have_response_422_or_400()
    {
        $response = $this->delete('/v1/admin/user/disable/0');
        if ($response->response->getStatusCode() === 400) {
            $response->assertResponseStatus(400);
        }

        if ($response->response->getStatusCode() === 422) {
            $response->assertResponseStatus(422);
        }

        $content = json_decode($response->response->getContent());
        $this->assertSame('Update user fail.', $content->error);
    }
}
