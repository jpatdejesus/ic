<?php

class GetRolesFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should get admin roles.
     * @test
     * @return void
     */
    public function it_should_get_admin_roles()
    {
        $response = $this->get('v1/admin/role?role_group=ADMIN&request_app_type=ADMIN', $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should get client roles.
     * @test
     * @return void
     */
    public function it_should_get_client_roles()
    {
        $response = $this->get('v1/admin/role?role_group=ADMIN&request_app_type=CLIENT', $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should return role not found error.
     * @test
     * @return void
     */
    public function it_should_return_role_not_found()
    {
        $response = $this->get('v1/admin/role?role_group=NOT_EXISTING&request_app_type=CLIENT', $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    /**
     * It should return admin role group not found error.
     * @test
     * @return void
     */
    public function it_should_return_admin_role_group_not_found()
    {
        $response = $this->get('v1/admin/role?role_group=OTHERS&request_app_type=ADMIN', $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    /**
     * It should return client role group not found error.
     * @test
     * @return void
     */
    public function it_should_return_client_role_group_not_found()
    {
        $response = $this->get('v1/admin/role?role_group=AUDIT&request_app_type=CLIENT', $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    /**
     * It should return request type error.
     * @test
     * @return void
     */
    public function it_should_return_request_type_error()
    {
        $response = $this->get('v1/admin/role?role_group=AUDIT&request_app_type=NON_EXISTING_REQUEST_TYPE', $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should only allow super_admin to get roles
     * @test
     * @return void
     */
    public function it_should_only_allow_super_admin_to_get_roles()
    {
        $user = \App\Models\v1\AdminUser::whereEmail('admin@nomail.com')->firstOrFail();
        $user->password = 'password123!';
        $this->token = [
            'token' => $this->loginAs($user)->token,
        ];
        $response = $this->get('v1/admin/role?role_group=ADMIN&request_app_type=ADMIN', $this->token);
        $response->assertResponseStatus(403);
        $response->seeJsonStructure([
            "message"
        ]);
    }


}
