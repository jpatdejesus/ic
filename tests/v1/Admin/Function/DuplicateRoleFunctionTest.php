<?php

class DuplicateRoleFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should duplicate role.
     * @test
     * @return void
     */
    public function it_should_duplicate_role_on_admin_app_type()
    {
        $bodyData = [
            'role_id' => 2,
            'role_name' => 'duplicated role name' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];

        $response = $this->post('v1/admin/role/duplicate', $bodyData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * It should duplicate role.
     * @test
     * @return void
     */
    public function it_should_duplicate_role_on_client_app_type()
    {
        $bodyData = [
            'role_id' => 3,
            'role_name' => 'duplicated role name' . str_random(10),
            'role_group' => 'GENERAL',
            'request_app_type' => 'CLIENT',
        ];

        $response = $this->post('v1/admin/role/duplicate', $bodyData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * It should validate app type.
     * @test
     * @return void
     */
    public function it_should_validate_app_type()
    {
        $bodyData = [
            'role_id' => 2,
            'role_name' => 'duplicated role name' . str_random(10),
            'role_group' => 'GENERAL',
            'request_app_type' => 'NON_EXISTING_APP_TYPE',
        ];

        $response = $this->post('v1/admin/role/duplicate', $bodyData, $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should validate role group.
     * @test
     * @return void
     */
    public function it_should_validate_role_group()
    {
        $bodyData = [
            'role_id' => 2,
            'role_name' => 'duplicated role name' . str_random(10),
            'role_group' => 'NON_EXISTING_ROLE_GROUP',
            'request_app_type' => 'CLIENT',
        ];

        $response = $this->post('v1/admin/role/duplicate', $bodyData, $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }

    /**
     * It should return error.
     * @test
     * @return void
     */
    public function it_should_return_error_if_role_not_exists()
    {
        $bodyData = [
            'role_id' => 100,
            'role_name' => 'duplicated role name' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];

        $response = $this->post('v1/admin/role/duplicate', $bodyData, $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }
}
