<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class OperatorInquiryFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @group ignore
     * @test
     */
    public function it_should_have_datatable_results_for_integer_parameter()
    {
        $operator = factory(\App\Models\v1\Operator::class)->create();

        $response = $this->post('/v1/admin/operator?search[param]='. $operator->UserId);
        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();
    }

    /**
     * @group ignore
     * @test
     */
    public function it_should_have_datatable_results_for_string_parameter()
    {
        $operator = factory(\App\Models\v1\Operator::class)
            ->create([
                'LoginName' => 'test' . rand(),
                'RealName' => 'test' . rand(),
            ]);

        $response = $this->post('/v1/admin/operator', ['search' => ['param' => $operator->UserId]]);
        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();

        $response = $this->post('/v1/admin/operator', ['search' => ['param' => $operator->LoginName]]);
        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();

        $response = $this->post('/v1/admin/operator', ['search' => ['param' => $operator->RealName]]);
        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();
    }
}
