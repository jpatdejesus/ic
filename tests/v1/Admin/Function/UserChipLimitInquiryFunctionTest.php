<?php

use App\Models\v1\Game;
use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Models\v1\UserType;
use Laravel\Lumen\Testing\WithoutMiddleware;

class UserChipLimitInquiryFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Function test for get user chip limit inquiry.
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_chip_limit_inquiry_by_username_and_user_type($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callUserChipLimitInquiryApi($user->getField('username'), $userType);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "username",
            "data",
        ]);
    }

    /**
     * Function test for get user chip limit inquiry.
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_chip_limit_inquiry_by_id_and_user_type($userModel, $userType)
    {
            $user = factory($userModel)->create();

            $response = $this->callUserChipLimitInquiryApi($user->getField('id'), $userType);
            $response->assertResponseStatus(200);
            $response->seeJsonStructure([
                "username",
                "data",
            ]);
    }

    /**
     * Function test for get user chip limit inquiry.
     * @dataProvider userTypes
     * @test
     */
    public function it_should_not_get_chip_limit_inquiry_if_invalid_username_or_id($userModel, $userType)
    {
        $response = $this->callUserChipLimitInquiryApi(null, $userType);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors" => [
                'param'
            ],
        ]);
    }

    /**
     * Function test for get user chip limit inquiry.
     * @dataProvider userTypes
     * @test
     */
    public function it_should_not_get_chip_limit_inquiry_if_invalid_user_type($userModel, $userType)
    {
            $user = factory($userModel)->create();

            $response = $this->callUserChipLimitInquiryApi($user->getField('id'), null);
            $response->assertResponseStatus(422);
            $response->seeJsonStructure([
                "message",
                "errors" => [
                    'user_type'
                ],
            ]);
    }

    /**
     * Call chip limit inquiry API
     * @param $param
     * @param $userType
     * @return mixed
     */
    private function callUserChipLimitInquiryApi($param, $userType='MARS')
    {
        return $this->post('/v1/admin/chips', ['param' => $param, 'user_type' => $userType]);
    }
}
