<?php

class AddRolesFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should add new role.
     * @test
     * @return void
     */
    public function it_should_add_new_role_on_admin_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];
        $response = $this->post('v1/admin/role', $formData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should add new role.
     * @test
     * @return void
     */
    public function it_should_add_new_role_on_client_app_type()
    {
        $formData = [
            'role_name' => 'test client role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'CLIENT',
        ];
        $response = $this->post('v1/admin/role', $formData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should validate app type.
     * @test
     * @return void
     */
    public function it_should_validate_app_type()
    {
        $formData = [
            'role_name' => 'test client role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'NON_EXISTING_APP_TYPE',
        ];
        $response = $this->post('v1/admin/role', $formData, $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should validate role group.
     * @test
     * @return void
     */
    public function it_should_validate_role_group()
    {
        $formData = [
            'role_name' => 'test client role' . str_random(10),
            'role_group' => 'NON_EXISTING_ROLE_GROUP',
            'request_app_type' => 'ADMIN',
        ];
        $response = $this->post('v1/admin/role', $formData, $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    /**
     * It should not add existing role name.
     * @test
     * @return void
     */
    public function it_should_not_add_existing_role_name()
    {
        $formData = [
            'role_name' => 'admin',
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];
        $response = $this->post('v1/admin/role', $formData, $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }
}
