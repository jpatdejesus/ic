<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class CreateAdminUserFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_should_successfully_create_an_admin_user()
    {
        $response = $this->post('/v1/admin/user/create', $this->createValidParams());
        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_first_name()
    {
        $field = 'first_name';

        $validErrorResponse = [
            "The first name field is required.",
            "The first name may not be greater than 50 characters.",
            "The first name field is invalid."
        ];

        /**
         * Test if first_name is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if first_name is valid format
         */
        $this->assertions($validErrorResponse, $field, 'firstname%');

        /**
         * Test if first_name does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_last_name()
    {
        $field = 'last_name';

        $validErrorResponse = [
            "The last name field is required.",
            "The last name may not be greater than 50 characters.",
            "The last name field is invalid."
        ];

        /**
         * Test if last_name is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if last_name is valid format
         */
        $this->assertions($validErrorResponse, $field, 'lastname%');

        /**
         * Test if last_name does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_nickname()
    {
        $field = 'nickname';

        $validErrorResponse = [
            "The nickname field is required.",
            "The nickname may not be greater than 50 characters.",
            "The nickname field is invalid."
        ];

        /**
         * Test if nickname is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if nickname is valid format
         */
        $this->assertions($validErrorResponse, $field, 'nickname%');

        /**
         * Test if nickname does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_email()
    {
        $field = 'email';

        $validErrorResponse = [
            "The email field is required.",
            "The email may not be greater than 50 characters.",
            "The email field is invalid.",
            "The email must be a valid email address."
        ];

        /**
         * Test if email is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if email is valid format
         */
        $this->assertions($validErrorResponse, $field, 'email%');

        /**
         * Test if email does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_password()
    {
        $field = 'password';

        $validErrorResponse = [
            "The password field is required.",
            "The password must be at least 8 characters.",
            "The password may not be greater than 50 characters.",
            "The password field is invalid.",
        ];

        /**
         * Test if password is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if password is valid format
         */
        $this->assertions($validErrorResponse, $field, 'pass1%^');

        /**
         * Test if password is in minimum character length
         */
        $this->assertions($validErrorResponse, $field, 'pass1!');

        /**
         * Test if password does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_status()
    {
        $field = 'status';

        $validErrorResponse = [
            "The status field is required.",
            "The selected status is invalid.",
        ];

        /**
         * Test if status is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if status is valid format
         */
        $this->assertions($validErrorResponse, $field, 'status%^');
    }

    /**
     * @test
     */
    public function it_should_test_invalid_role()
    {
        $field = 'role';

        $validErrorResponse = [
            "The role field is required.",
            "The selected role is invalid.",
        ];

        /**
         * Test if role is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if role is valid
         */
        $this->assertions($validErrorResponse, $field, 0);
    }

    public function createValidParams()
    {
        return [
            'first_name' => str_random(8),
            'last_name' => str_random(8),
            'nickname' => generate_random_string(8),
            'email' => strtolower(str_random(8)).'@testmail.com',
            'password' => 'password123!',
            'status' => 'ENABLED',
            'role' => 1,
        ];
    }

    public function createInvalidParam($testField, $value = null)
    {
        $return = [
            'first_name' => str_random(8),
            'last_name' => str_random(8),
            'nickname' => generate_random_string(8),
            'email' => strtolower(str_random(8)).'@testmail.com',
            'password' => 'password123!',
            'status' => 'ENABLED',
            'role' => 1,
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }

    public function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->post('/v1/admin/user/create', $this->createInvalidParam($field));
        } else {
            $response = $this->post('/v1/admin/user/create', $this->createInvalidParam($field, $param));
        }

        $this->assertResponseStatus(422);

        $response->seeJsonStructure([
            "message",
            "errors"
        ]);

        $content = json_decode($response->response->getContent());

        $this->assertSame('Validation error!', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
