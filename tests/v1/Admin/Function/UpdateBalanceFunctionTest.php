<?php

use App\Models\v1\User;
use App\Models\v1\UserPHP;
use Laravel\Lumen\Http\Request;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\AdminUpdateBalanceRequest;
use App\Http\v1\Admin\Controllers\BalanceController;
use App\Http\v1\Admin\Requests\UpdateBalanceRequest;
use App\Http\v1\Admin\Requests\GetBalanceUpdateRequests;
use App\Http\v1\Admin\Requests\ApproveBalanceRequest;
use App\Http\v1\Admin\Requests\RejectBalanceRequest;

class UpdateBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;

    const SQLITE_EXCLUDE_MESSAGE = 'Stored procedure is not allowed when running sqlite';

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_player($userModel, $userType)
    {
        $userModelInstance = new $userModel();
        $user = factory($userModel)->create([
            $userModelInstance->getFieldName('balance') => 100,
        ]);
        $repository = get_user_type_repository($userType, 'users');
        $userData = $repository->getPlayerForUpdateBalance($user->getField('username'))->first();

        $response = $this->callGetPlayerApi($user->getField('username'), $userType);
        $response->assertResponseStatus(200);

        $response->seeJsonContains([
            'message' => BalanceController::MESSAGE_GET_SUCCESS,
            'data' => [
                'balance' => number_format($user->getField('balance'), 5, '.', ''),
                'username' => $user->getField('username'),
                'id' => (int) $user->getField('id'),
                'currency' => $userData->currency,
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_succcessfully_add_balance_request($userModel, $userType)
    {
        $type = AdminUpdateBalanceRequest::TYPE_ADD;
        $amount = 100;
        $reason = 'test';

        $this->mockUserData(UpdateBalanceRequest::class,
            [
                'user_type' => $userType,
                'type' => $type,
                'amount' => $amount,
                'reason' => $reason,
            ]
        );

        $updateBalanceRequest = $this->app->make(UpdateBalanceRequest::class);

        $userModelInstance = new $userModel();

        $user = factory($userModel)->create([
            $userModelInstance->getFieldName('balance') => 100,
        ]);

        $response = $this->callUpdateBalanceApi($user->getField('username'), $amount, $reason, $userType);
        $response->assertResponseStatus(200);
    }

    /**
     * @dataProvider userTypes
     * test
     */
    public function it_should_validate_max_amount_on_add_balance_request($userModel, $userType)
    {
        $type = AdminUpdateBalanceRequest::TYPE_ADD;
        $amount = 100;
        $reason = 'test';

        $userModelInstance = new $userModel();

        $user = factory($userModel)->create([
            $userModelInstance->getFieldName('balance') => 100,
        ]);

        $response = $this->callUpdateBalanceApi($user->getField('username'), $amount, $reason, $userType);

        $response->assertResponseStatus(200);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_max_amount_on_deduct_balance_request($userModel, $userType)
    {
        $type = AdminUpdateBalanceRequest::TYPE_DEDUCT;
        $amount = 10000000000;
        $reason = 'test';

        $userModelInstance = new $userModel();

        $user = factory($userModel)->create([
            $userModelInstance->getFieldName('balance') => 100,
        ]);

        $response = $this->callUpdateBalanceApi($user->getField('username'), $amount, $reason, $userType, $type);

        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            'message' => 'Validation error!',
            'errors' => [
                'amount' => [
                    'The amount may not be greater than '. AdminUpdateBalanceRequest::MAX_DEDUCT_AMOUNT .'.'
                ],
            ]
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_succcessfully_deduct_balance_request($userModel, $userType)
    {
        $type = AdminUpdateBalanceRequest::TYPE_DEDUCT;
        $amount = 100;
        $reason = 'test';

        $this->mockUserData(UpdateBalanceRequest::class,
            [
                'user_type' => $userType,
                'type' => $type,
                'amount' => $amount,
                'reason' => $reason,
            ]
        );

        $userModelInstance = new $userModel();

        $user = factory($userModel)->create([
            $userModelInstance->getFieldName('balance') => 100,
        ]);

        $response = $this->callUpdateBalanceApi($user->getField('username'), 100, 'test', $userType, $type);

        $response->assertResponseStatus(200);
    }

    /**
     * @todo error on users_php table not found!
     * test
     */
    public function it_should_successfully_get_balance_request_by_user()
    {
        $user = $this->mockUserData(GetBalanceUpdateRequests::class);

        $adminUpdateBalanceRequests = factory(AdminUpdateBalanceRequest::class, 3)->create([
            'created_by' => $user->id
        ]);

        $response = $this->callGetBalanceRequestApi();

        $expectedResult = $adminUpdateBalanceRequests->toArray();

        // dd($expectedResult);

        $response->assertResponseStatus(200);

        $response->seeJsonContains([
            'count_records' => $adminUpdateBalanceRequests->count(),
            'count_results' => $adminUpdateBalanceRequests->count(),
            // 'data' => $expectedResult, //@todo! errors on datatype
        ]);
    }

    /**
     * @todo error on users_php table not found!
     * test
     */
    public function it_should_successfully_get_balance_request_if_role_is_super_admin()
    {
        $user = $this->mockUserData(GetBalanceUpdateRequests::class);

        factory(AdminUpdateBalanceRequest::class)->create();//create extra records for admin
        $adminUpdateBalanceRequests = factory(AdminUpdateBalanceRequest::class, 3)->create([
            'created_by' => $user->id
        ]);

        $response = $this->callGetBalanceRequestApi();

        $expectedResult = $adminUpdateBalanceRequests->toArray();

        $response->assertResponseStatus(200);

        $response->seeJsonContains([
            'count_records' => $adminUpdateBalanceRequests->count(),
            'count_results' => $adminUpdateBalanceRequests->count(),
            // 'data' => $expectedResult, //@todo! errors on datatype
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_approve_add_balance_request($userModel, $userType)
    {
        if (DB::connection()->getDriverName() === 'sqlite') {
            $this->markTestSkipped(self::SQLITE_EXCLUDE_MESSAGE);
        }

        $user = $this->mockUserData(ApproveBalanceRequest::class);

        $userPlayerModel = new $userModel;
        $player = factory($userModel)->create([
            $userPlayerModel->getFieldName('balance') => '100.00000'
        ]);

        $adminUpdateBalanceRequest = factory(AdminUpdateBalanceRequest::class)->create([
            'created_by' => $user->id,
            'type' => AdminUpdateBalanceRequest::TYPE_ADD,
            'username' => $player->getField('username'),
            'user_type' => $userType,
            'amount' => '100.00000'
        ]);

        $response = $this->callApproveBalanceRequestApi($adminUpdateBalanceRequest->id);
        $response->assertResponseStatus(200);

        $response->seeInDatabase($adminUpdateBalanceRequest->getTable(), [
            'id' => $adminUpdateBalanceRequest->id,
            'status' => AdminUpdateBalanceRequest::STATUS_APPROVED,
            'updated_by' => $user->id,
        ], 'ichips-admin');

        $response->seeInDatabase($player->getTable(), [
            $player->getFieldName('username') => $player->getField('username'),
            $player->getFieldName('balance') => '200.00000',
        ], 'ichips');
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_approve_deduct_balance_request($userModel, $userType)
    {
        if (DB::connection()->getDriverName() === 'sqlite') {
            $this->markTestSkipped(self::SQLITE_EXCLUDE_MESSAGE);
        }

        $user = $this->mockUserData(ApproveBalanceRequest::class);

        $userPlayerModel = new $userModel;
        $player = factory($userModel)->create([
            $userPlayerModel->getFieldName('balance') => '100.00000'
        ]);

        $adminUpdateBalanceRequest = factory(AdminUpdateBalanceRequest::class)->create([
            'created_by' => $user->id,
            'type' => AdminUpdateBalanceRequest::TYPE_DEDUCT,
            'username' => $player->getField('username'),
            'user_type' => $userType,
            'amount' => '200.00000'
        ]);

        $response = $this->callApproveBalanceRequestApi($adminUpdateBalanceRequest->id);
        $response->assertResponseStatus(200);

        $response->seeInDatabase($adminUpdateBalanceRequest->getTable(), [
            'id' => $adminUpdateBalanceRequest->id,
            'status' => AdminUpdateBalanceRequest::STATUS_APPROVED,
            'updated_by' => $user->id,
        ], 'ichips-admin');

        $response->seeInDatabase($player->getTable(), [
            $player->getFieldName('username') => $player->getField('username'),
            $player->getFieldName('balance') => '-100.00000',
        ], 'ichips');
    }

    /**
     * @test
     */
    public function it_should_reject_update_balance_request()
    {
        $user = $this->mockUserData(RejectBalanceRequest::class);

        $adminUpdateBalanceRequest = factory(AdminUpdateBalanceRequest::class)->create([
            'created_by' => $user->id
        ]);

        $response = $this->callRejectBalanceRequestApi($adminUpdateBalanceRequest->id);

        $response->assertResponseStatus(200);

        $response->seeInDatabase($adminUpdateBalanceRequest->getTable(), [
            'id' => $adminUpdateBalanceRequest->id,
            'status' => AdminUpdateBalanceRequest::STATUS_REJECTED,
            'updated_by' => $user->id,
        ], 'ichips-admin');
    }

    /**
     * Call player inquiry API
     * @param $username
     * @param $userType
     *
     * @return mixed
     */
    private function callGetPlayerApi($username, $userType = 'MARS')
    {
        $response = $this->get(url("v1/admin/balance/player/$username?user_type=$userType"));

        return $response;
    }

    /**
     * Call player inquiry API
     * @param $username
     * @param $userType
     *
     * @return mixed
     */
    private function callUpdateBalanceApi($username, $amount, $reason, $userType = 'MARS', $type = AdminUpdateBalanceRequest::TYPE_ADD)
    {
        $response = $this->post(url("v1/admin/balance/player/$username?user_type=$userType"), [
            'type' => $type,
            'amount' => $amount,
            'reason' => $reason,
            'category' => 'promo',
        ]);

        return $response;
    }

    /**
     * Call player inquiry API
     * @param $username
     * @param $userType
     *
     * @return mixed
     */
    private function callGetBalanceRequestApi()
    {
        $response = $this->get(url("v1/admin/balance/requests"));

        return $response;
    }

    /**
     * Call player approve balance API
     * @param $id
     *
     * @return mixed
     */
    private function callApproveBalanceRequestApi($id)
    {
        $response = $this->put(url("v1/admin/balance/approve/".$id));

        return $response;
    }

    /**
     * Call player reject balance API
     * @param $id
     *
     * @return mixed
     */
    private function callRejectBalanceRequestApi($id)
    {
        $response = $this->put(url("v1/admin/balance/reject/".$id));

        return $response;
    }
}
