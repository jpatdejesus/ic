<?php

use Carbon\Carbon;
use App\Models\v1\User;
use Laravel\Lumen\Testing\WithoutMiddleware;

class AdminIChipsLogsFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Function test for get all audit logs.
     * @group ignore
     * @test
     */
    public function it_should_successfully_get_ichips_audit_logs()
    {
        $response = $this->callGetAdminAuditApi();
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "data",
        ]);
    }

    /**
     * Function test for get audit logs with username and date.
     * @group ignore
     * @test
     */
    public function it_should_successfully_get_ichips_audit_logs_with_username_and_date()
    {
        $user = factory(User::class)->create();
        $end_point = 'bet_balance';
        $date_from = Carbon::today()->toDateString();
        $date_to = Carbon::tomorrow()->toDateString();
        $response = $this->callGetAdminAuditApi(
            $user->username,
            $end_point,
            $date_from,
            $date_to
        );
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "data",
        ]);
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callGetAdminAuditApi()
    {
        $response = $this->post(url('/v1/admin/audits/logs/'));
        return $response;
    }
}
