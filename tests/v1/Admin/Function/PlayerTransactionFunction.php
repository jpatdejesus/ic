<?php

use Carbon\Carbon;
use App\Models\v1\User;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Http\v1\Admin\Controllers\PlayerTransactionController;

class PlayerTransactionFunction extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_all_player_transaction($userModel, $userType)
    {
        $user = factory($userModel)->create(); 
        $date_start = Carbon::today()->toDateString();
        $date_end = Carbon::tomorrow()->toDateString();
        $response = $this->callPlayerTransactionApi($user->getField('username'), $userType, $date_start, $date_end);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'count_records',
            'count_results',
            'data'=>[
            'id' => 'id',
            'bet_code'=> 'bet_code',
            'bet_amount'=> 'bet_amount',
            'username'=> 'username',
            'user_type'=> 'user_type',
            'bet_place'=> 'bet_place',
            'effective_bet_amount'=> 'effective_bet_amount',
            'shoehandnumber'=> 'shoehandnumber',
            'gameset_id'=> 'gameset_id',
            'gamename'=> 'gamename',
            'tablenumber' => 'tablenumber',
            'balance'=> 'balance',
            'win_loss'=> 'win_loss',
            'result_id' => 'result_id',
            'result' => 'result',
            'bet_date' => 'bet_date',
            'super_six' => 'super_six',
            'is_sidebet' => 'super_six',
            'currency_code' => 'currency_code',
            ]
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_required_date()
    {
        $response = $this->callPlayerTransactionApi('user_not_found', 'MARS');

        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            'message' => 'Validation error!',
            'errors' => [
                'date_start' => [
                    'The date start field is required.',
                ],
                'date_end' => [
                    'The date end field is required.',
                ],
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_date_end_must_be_after_date_start()
    {
        $response = $this->callPlayerTransactionApi('user_not_found', 'MARS', Carbon::today()->toDateString(), Carbon::yesterday()->toDateString());

        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            'message' => 'Validation error!',
            'errors' => [
                'date_end' => [
                    'The date end must be a date after or equal to date start.',
                ],
            ],
        ]);
    }

    /**
     * Call player transaction API
     * @param $username
     * @param $userType
     *
     * @return mixed
     */
    private function callPlayerTransactionApi($username, $userType = 'MARS', $date_start = '', $date_end = '', $gameName = 'baccarat')
    {
        $response = $this->post(url("v1/admin/player/transaction/$username?user_type=$userType&date_start=$date_start&date_end=$date_end&game_name=$gameName"));
        return $response;
    }
}
