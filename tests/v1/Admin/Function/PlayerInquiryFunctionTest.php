<?php

use Carbon\Carbon;
use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Models\v1\Transfer;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Http\v1\Admin\Controllers\PlayerInquiryController;

class PlayerInquiryFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @group ignore
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_player_inquiry_details($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $point = "200";
        $exp = "300";
        $user->setField('point', $point); //set desired balance
        $user->setField('exp', $exp); //set desired balance
        $user->save();

        $date_start = Carbon::today()->toDateString();
        $date_end = Carbon::tomorrow()->toDateString();

        $cashflowModel = get_user_type_model($userType, 'cashflow');
        $cashflow = $cashflowModel
            ->whereBetween('created_at', [$date_start, $date_end])
            ->where('username', $user->getField('username'));

        $response = $this->callPlayerInquiryApi($user->getField('username'), $userType, $date_start, $date_end);
        $response->assertResponseStatus(200);

        // $response->seeJsonContains([
        //     'message' => PlayerInquiryController::MESSAGE_GET_SUCCESS,
        //     // 'data' => [ //TODO!
        //     //     'balance' => $depositResponseData->data->current_balance,
        //     //     'point' => $point,
        //     //     'exp' => $exp,
        //     //     'username' => $user->getField('username'),
        //     //     'id' => (int) $user->getField('id'),
        //     //     'cashflow' => [
        //     //         'count_records' => $cashflow->count(),
        //     //         'count_results' => $cashflow->count(),
        //     //         'data' => $cashflow->get()->toArray(),
        //     //     ]
        //     // ],
        // ]);
    }

    /**
     * @test
     */
    public function it_should_return_user_not_found()
    {
        $response = $this->callPlayerInquiryApi('user_not_found', 'MARS', Carbon::today()->toDateString(), Carbon::tomorrow()->toDateString());

        $response->assertResponseStatus(404);
        $response->seeJsonContains(['error' => PlayerInquiryController::MESSAGE_PLAYER_NOT_FOUND]);
    }

    /**
     * @test
     */
    public function it_should_validate_required_date()
    {
        $response = $this->callPlayerInquiryApi('user_not_found', 'MARS');

        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            'message' => 'Validation error!',
            'errors' => [
                'date_start' => [
                    'The date start field is required.'
                ],
                'date_end' => [
                    'The date end field is required.'
                ],
            ]
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_date_end_must_be_after_date_start()
    {
        $response = $this->callPlayerInquiryApi('user_not_found', 'MARS', Carbon::today()->toDateString(), Carbon::yesterday()->toDateString());

        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            'message' => 'Validation error!',
            'errors' => [
                'date_end' => [
                    'The date end must be a date after or equal to date start.'
                ],
            ]
        ]);
    }

    /**
     * Call player inquiry API
     * @param $username
     * @param $userType
     *
     * @return mixed
     */
    private function callPlayerInquiryApi($username,  $userType = 'MARS', $date_start = '', $date_end = '')
    {
        $response = $this->get(url("v1/admin/player/$username?user_type=$userType&date_start=$date_start&date_end=$date_end"));

        return $response;
    }
}
