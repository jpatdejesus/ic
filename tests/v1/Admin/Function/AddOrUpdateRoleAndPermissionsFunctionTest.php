<?php

class AddOrUpdateRoleAndPermissionsFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should update role and permissions.
     * @test
     * @return void
     */
    public function it_should_add_or_update_role_and_permissions_on_admin_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];

        $newRoleData = $this->post('v1/admin/role', $formData, $this->token);

        $roleId = json_decode($newRoleData->response->getContent())->data[0]->id;

        $bodyData = [
            'role_id' => $roleId,
            'request_app_type' => 'ADMIN',
            'permissions' => [9,13]
        ];

        $response = $this->post('v1/admin/role/add_or_update_role_permissions', $bodyData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * It should ger role and permissions.
     * @test
     * @return void
     */
    public function it_should_add_or_update_role_and_permissions_on_client_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'CLIENT',
        ];

        $newRoleData = $this->post('v1/admin/role', $formData, $this->token);

        $roleId = json_decode($newRoleData->response->getContent())->data[0]->id;

        $bodyData = [
            'role_id' => $roleId,
            'request_app_type' => 'CLIENT',
            'permissions' => [9,13]
        ];

        $response = $this->post('v1/admin/role/add_or_update_role_permissions', $bodyData, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * It should validate app type.
     * @test
     * @return void
     */
    public function it_should_validate_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'CLIENT',
        ];

        $newRoleData = $this->post('v1/admin/role', $formData, $this->token);

        $roleId = json_decode($newRoleData->response->getContent())->data[0]->id;

        $bodyData = [
            'role_id' => $roleId,
            'request_app_type' => 'NON_EXISTING_APP_TYPE',
            'permissions' => '[{"permission_ids":[9,13]}]'
        ];

        $response = $this->post('v1/admin/role/add_or_update_role_permissions', $bodyData, $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should not update non existing role.
     * @test
     * @return void
     */
    public function it_should_not_update_non_existing_role()
    {
        $bodyData = [
            'role_id' => 100,
            'request_app_type' => 'CLIENT',
            'permissions' => '[{"permission_ids":[9,13]}]'
        ];

        $response = $this->post('v1/admin/role/add_or_update_role_permissions', $bodyData, $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }
}
