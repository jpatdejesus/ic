<?php

use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\AdminUser;

class AdminAuditLogsFunctionTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * Function test for get audit logs.
     * @group ignore
     * @test
     */
    public function it_should_successfully_get_audit_logs()
    {
        $user = factory(AdminUser::class)->create();
        $response = $this->callGetAdminAuditApi($user->email);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "data",
        ]);
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callGetAdminAuditApi($email)
    {
        $params = [
            'email' => $email,
        ];

        $response = $this->post(url('/v1/admin/audits/logs/'), $params);
        return $response;
    }
}
