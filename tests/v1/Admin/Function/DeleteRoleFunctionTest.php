<?php

class DeleteRoleFunctionTest extends TestCase
{
    use \App\Traits\AdminAuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should delete role.
     * @test
     * @return void
     */
    public function it_should_delete_role_on_admin_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];
        $role = $this->post('v1/admin/role', $formData, $this->token);
        $roleId = json_decode($role->response->getContent())->data[0]->id;

        $response = $this->delete('v1/admin/role?role_id=' . $roleId . '&role_group=' . $formData['role_group'] . '&request_app_type=' . $formData['request_app_type'], [], $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * It should delete role.
     * @test
     * @return void
     */
    public function it_should_delete_role_on_client_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'CLIENT',
        ];
        $role = $this->post('v1/admin/role', $formData, $this->token);
        $roleId = json_decode($role->response->getContent())->data[0]->id;

        $response = $this->delete('v1/admin/role?role_id=' . $roleId . '&role_group=' . $formData['role_group'] . '&request_app_type=' . $formData['request_app_type'], [], $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /**
     * It should validate app type.
     * @test
     * @return void
     */
    public function it_should_validate_app_type()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'ADMIN',
        ];
        $role = $this->post('v1/admin/role', $formData, $this->token);
        $roleId = json_decode($role->response->getContent())->data[0]->id;

        $response = $this->delete('v1/admin/role?role_id=' . $roleId . '&role_group=' . $formData['role_group'] . '&request_app_type=NON_EXISTING_APP_TYPE', [], $this->token);
        $response->assertResponseStatus(500);
    }

    /**
     * It should validate role group.
     * @test
     * @return void
     */
    public function it_should_validate_role_group()
    {
        $formData = [
            'role_name' => 'test admin role' . str_random(10),
            'role_group' => 'ADMIN',
            'request_app_type' => 'CLIENT',
        ];
        $role = $this->post('v1/admin/role', $formData, $this->token);
        $roleId = json_decode($role->response->getContent())->data[0]->id;

        $response = $this->delete('v1/admin/role?role_id=' . $roleId . '&role_group=NON_EXISTING_ROLE_GROUP&request_app_type=' . $formData['request_app_type'], [], $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }

    /**
     * It should not delete default role.
     * @test
     * @return void
     */
    public function it_should_not_delete_default_role()
    {
        $response = $this->delete('v1/admin/role?role_id=1&role_group=ADMIN&request_app_type=ADMIN', [], $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }

    /**
     * It should return error.
     * @test
     * @return void
     */
    public function it_should_return_error_if_role_not_exists()
    {
        $response = $this->delete('v1/admin/role?role_id=100&role_group=ADMIN&request_app_type=ADMIN', [], $this->token);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors",
        ]);
    }
}
