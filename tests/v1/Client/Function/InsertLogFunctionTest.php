<?php
use App\Traits\AuthToken;
use App\Services\Log\TableNameBuilder;
use Illuminate\Support\Facades\Schema;

class InsertLogFunctionTest extends TestCase
{
    use AuthToken;
    
    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_insert_log($userModel, $userType)
    {
        $token = $this->login();
        $user = factory($userModel)->create();
        $url = "/v1/user/point/{$user->getField('username')}" ;
        $logTableName = TableNameBuilder::build('Update User Points');

        $paramList = [
            'user_points_method' => '+',
            'user_points_value' => 50.00,
            'user_type' => $userType
        ];
        
        $response = $this->put(
            $url,
            $paramList,
            $token
        );

        $responseContent = [
            "message" => "Success",
            "data" => [
                "point" => "50.00000"
            ]
        ];

        $seeData = [
            'api_consumer_username' => 'ichips',
            'username' => $user->getField('username'),
            'request_content' => json_encode($paramList),
            'response_content' => json_encode($responseContent),
            'remark' => ''
        ];
        $response->seeInDatabase($logTableName, $seeData, 'logs');
    }

    /**
     * @test
     */
    public function it_should_not_create_log_table()
    {
        $url = "/v1/auth/token" ;
        $logTableName = TableNameBuilder::build('');

        $paramList = [
            'username' => 'ichips',
            'password' => 'password123!',
        ];
        
        $response = $this->put(
            $url,
            $paramList
        );

        $tableExist = Schema::connection('logs')->hasTable($logTableName);
        $this->assertFalse($tableExist);
    }
}
