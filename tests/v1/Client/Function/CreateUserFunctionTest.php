<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class CreateUserFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_create_a_user($userModel, $userType)
    {
        if ($userModel === \App\Models\v1\User::class) {
            $response = $this->post('/v1/user', $this->createValidParams($userType));
            $response->assertResponseStatus(201);
            $response->seeJsonStructure([
                "message",
                "data" => ['user_id']
            ]);
        } else {
            $this->assertTrue(true);
        }
    }

    /**
     * @test
     */
    public function it_should_test_invalid_currency()
    {
        $field = 'currency';

        $validErrorResponse = [
            "The currency field is required.",
            "The selected currency is invalid."
        ];

        /**
         * Test if currency is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if currency does exist
         */
        $this->assertions($validErrorResponse, $field, 'test');
    }

    /**
     * @test
     */
    public function it_should_test_invalid_username()
    {
        $field = 'username';

        $validErrorResponse = [
            "The username field is required.",
            "The username must be at least 6 characters.",
            "The username may not be greater than 50 characters.",
            "The username field is invalid."
        ];

        /**
         * Test if username is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if username have minimum character length
         */
        $this->assertions($validErrorResponse, $field, 'test');

        /**
         * Test if username have valid format
         */
        $this->assertions($validErrorResponse, $field, 'test a');

        /**
         * Test if username does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_nickname()
    {
        $field = 'nickname';

        $validErrorResponse = [
            "The nickname field is required.",
            "The nickname may not be greater than 50 characters.",
            "The nickname field is invalid."
        ];

        /**
         * Test if nickname is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if nickname is valid format
         */
        $this->assertions($validErrorResponse, $field, 'nickname%^');

        /**
         * Test if nickname does not exceed maximum character length
         */
        $this->assertions($validErrorResponse, $field, str_random(51));
    }

    /**
     * @test
     */
    public function it_should_test_invalid_status()
    {
        $field = 'status';

        $validErrorResponse = [
            "The status field is required.",
            "The selected status is invalid."
        ];
        /**
         * Test if status is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if status does exist in the list of valid values
         */
        $this->assertions($validErrorResponse, $field, 'test');
    }

    /**
     * @test
     */
    public function it_should_test_invalid_user_type()
    {
        $field = 'user_type';

        $validErrorResponse = [
            "The user type field is required.",
            "The selected user type is invalid."
        ];
        /**
         * Test if user_type is not null
         */
        $this->assertions($validErrorResponse, $field);

        /**
         * Test if user_type does exist in the list of valid values
         */
        $this->assertions($validErrorResponse, $field, 0);
    }

    /**
     * @test
     */
    public function it_should_test_invalid_chip_limits()
    {
        $field = 'chip_limits';

        $validErrorResponse = [
            "The chip limits field is invalid.",
            "The selected game code 'test1' is invalid.",
            "The selected game code 'test2' is invalid.",
            "The selected game code 'test2' min amount should be less than the max amount."
        ];

        /**
         * Test if chip_limits does exist
         */
        $invalidChipLimits = '[{"test1":{"min":1, "max":100}}, {"test2":{"min":1000, "max":200}}]';

        $this->assertions($validErrorResponse, $field, $invalidChipLimits);
    }

    public function createValidParams($userType)
    {
        return [
            'currency' => factory(\App\Models\v1\Currency::class)->create()->currency_code,
            'username' => strtolower(str_random(8)),
            'nickname' => generate_random_string(8),
            'status' => 'ENABLED',
            'user_type' => $userType,
            'chip_limits' => '[{"baccarat":{"min":1,"max":100}},{"dragontiger":{"min":10,"max":200}}]',
        ];
    }

    public function createInvalidParam($testField, $value = null)
    {
        $return = [
            'currency' => factory(\App\Models\v1\Currency::class)->create()->currency_code,
            'username' => strtolower(str_random(8)),
            'nickname' => generate_random_string(8),
            'status' => 'ENABLED',
            'user_type' => 'MARS',
            'chip_limits' => '[{"baccarat":{"min":1,"max":100}},{"dragontiger":{"min":10,"max":200}}]',
        ];

        if($value !== null){
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }

    public function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->post('/v1/user', $this->createInvalidParam($field));
        } else {
            $response = $this->post('/v1/user', $this->createInvalidParam($field, $param));
        }

        $this->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);

        $content = json_decode($response->response->getContent());

        $this->assertSame('Validation error!', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
