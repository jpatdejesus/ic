<?php

use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\User;
use App\Models\v1\UserPHP;

class GetBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * Function test for get balance.
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_balance($userModel, $userType)
    {
        $this->createMockUsersPHPTableIfNotExist();
        $user = factory($userModel)->create();
        $user->setField('balance', 100); //set desired balance
        $user->save();
            
        $response = $this->callGetBalanceApi($user->getField('username'), $userType);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
        ]);
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callGetBalanceApi($username, $user_type = 'MARS')
    {
        $response = $this->get(url('/v1/balance/' . $username.'?user_type='.$user_type));
        return $response;
    }
}
