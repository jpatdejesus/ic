<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class DisableUserFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * If the user have permission to access the url and have valid parameters it will get a HTTP status code 200 (OK).
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_disable_user($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->delete('v1/user/'. $user->getField('username') . '?user_type='.$userType);
        $response->assertResponseStatus(200);
    }

    /**
     * Else if the user has permission but sent an invalid request they will get a
     * HTTP status code 422 (Unprocessable Entity) or HTTP status code 400(Bad Request)
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_response_422_or_400($userModel, $userType)
    {
        $response = $this->delete('v1/user/0?user_type='.$userType);
        if ($response->response->getStatusCode() === 400) {
            $response->assertResponseStatus(400);
        }

        if ($response->response->getStatusCode() === 422) {
            $response->assertResponseStatus(422);
        }

        $content = json_decode($response->response->getContent());
        $this->assertSame('Update user fail.', $content->error);
    }
}
