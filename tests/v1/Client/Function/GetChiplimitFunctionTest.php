<?php

use App\Models\v1\Game;
use App\Models\v1\UserType;
use App\Models\v1\UserChipLimit;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Http\v1\Client\Controllers\GetChipLimitController;
use Illuminate\Support\Facades\DB;

class GetChipLimitFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_user_chiplimit($userModel, $userType)
    {
        $game = Game::first();

        $user = factory($userModel)->create();

        $userType = UserType::where('user_type_name', $userType)->first();

        //to do! Decimal issue sqlite
        $min = DB::connection()->getDriverName() === 'sqlite' ? '1' : '1.00000';

        $max = DB::connection()->getDriverName() === 'sqlite' ? '10' : '10.00000';

         UserChipLimit::insert([
            'user_id' => $user->getField('id'),
            'user_type_id' => $userType->id,
            'game_type_id' => $game->game_type_id,
            'min' => $min,
            'max' => $max,
        ]);

        $response = $this->callGetChipLimitApi($user->getField('username'), $game->game_code, $userType->user_type_name);
        $response->assertResponseStatus(200);
        $response->seeJsonContains([
            'message' => GetChipLimitController::MESSAGE_SUCCESS,
            'data' => [
                'min' => $min,
                'max' => $max,
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_default_chiplimit($userModel, $userType)
    {
        $game = Game::first();
        $userType = UserType::where('user_type_name', $userType)->first();
        $user = factory($userModel)->create();

        $response = $this->callGetChipLimitApi($user->getField('username'), $game->game_code, $userType->user_type_name);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data' => [
                'min',
                'max',
            ]
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_get_default_chiplimit_without_currency_filter($userModel, $userType)
    {
        $game = Game::first();
        $userType = UserType::where('user_type_name', $userType)->first();

        $user = factory($userModel)->create();

        $response = $this->callGetChipLimitApi($user->getField('username'), $game->game_code, $userType->user_type_name);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data' => [
                'min',
                'max',
            ],
        ]);
        //do assertion here on get chiplimit!!
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_missing_game_code($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callGetChipLimitApi($user->getField('username'), null, 'dummy');
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'game_code',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_non_existing_game_code($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callGetChipLimitApi($user->getField('username'), 'non_existing_game', 'dummy');
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'game_code',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_missing_user_type($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $game = Game::first();

        $response = $this->callGetChipLimitApi($user->getField('username'), $game->game_code);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'user_type',
            ],
        ]);
    }

    /**
     * test
     */
    public function it_should_validate_non_existing_user_type()
    {

    }

    /**
     * Call withdraw API
     *
     * @param $username
     * @param $gameCode
     * @param $userType
     *
     * @return mixed
     */
    private function callGetChipLimitApi($username, $gameCode = null, $userType = null)
    {
        $response = $this->get(url('v1/chiplimit/' . $username . '?game_code=' . $gameCode . '&user_type=' . $userType . ''));

        return $response;
    }
}
