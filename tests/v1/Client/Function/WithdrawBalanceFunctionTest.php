<?php

use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Models\v1\Transfer;
use App\Repositories\v1\CashFlowRepository;
use App\Repositories\v1\TransferRepository;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Http\v1\Client\Controllers\WithdrawBalanceController;

class WithdrawBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_withdraw_balance($userModel, $userType)
    {
        $this->createMockUsersPHPTableIfNotExist();
        $this->createMockTransferPHPTableIfNotExist();
        $this->createMockCashflowPHPTableIfNotExist();

        $user = factory($userModel)->create();

        $user->setField('balance', 100); //set desired balance
        $user->save();

        $amount = 1;
        $balance = number_format(($user->getField('balance') - $amount), 5, '.', '');
        $transferAction = TransferRepository::WITHDRAW_ACTION;
        $cashFlowAction = CashFlowRepository::WITHDRAW_TRANS_TYPE;

        $response = $this->callWithdrawApi($user->getField('username'), $amount, time() . $userType, $userType);

        $transfer = get_user_type_model($userType, 'transfer')
            ->whereUsername($user->getField('username'))
            ->latest()
            ->first();
        $cashflow = get_user_type_model($userType, 'cashflow');

        $response->assertResponseStatus(200);
        $response->seeJsonContains([
            'message' => WithdrawBalanceController::MESSAGE_UPDATE_SUCCESS,
            'data' => [
                'transfer_id' => $transfer->getField('transfer_id'),
                'current_balance' => $balance,
            ],
        ]);
        $response->seeInDatabase($transfer->getTable(), [
            'username' => $user->getField('username'),
            'amount' => $amount,
            'balance' => $balance,
            'actions' => TransferRepository::WITHDRAW_ACTION,
        ], 'datarepo');

        $response->seeInDatabase($user->getTable(), [
            $user->getKeyName() => $user->getField('id'),
            $user->getFieldName('balance') => $balance,
        ], 'ichips');
        $response->seeInDatabase($cashflow->getTable(), [
            'username' => $user->getField('username'),
            'trans_amount' => $amount,
            'balance' => $balance,
            'trans_type' => CashFlowRepository::WITHDRAW_TRANS_TYPE,
            'betting_code' => $transfer->getField('transfer_id'),
            'game_code' => null,
        ], 'datarepo');
    }

    /**
     * @test
     */
    public function it_should_validate_minimium_balance_widthrawal()
    {
        $user = factory(User::class)->create();

        $response = $this->callWithdrawApi($user->getField('username'), 0);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_maximum_balance_widthrawal()
    {
        $user = factory(User::class)->create([
            'balance' => 100,
        ]);

        $response = $this->callWithdrawApi($user->getField('username'), 100.1);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_missing_amount_parameter()
    {
        $user = factory(User::class)->create();

        $response = $this->callWithdrawApi($user->getField('username'), null);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_missing_op_transfer_id_parameter()
    {
        $user = factory(User::class)->create([
            'balance' => 100,
        ]);

        $response = $this->callWithdrawApi($user->getField('username'), 1, null);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'op_transfer_id',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_non_integer_amount_parameter()
    {
        $user = factory(User::class)->create();

        $response = $this->callWithdrawApi($user->getField('username'), 'test');

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_unique_op_transfer_id()
    {
        $user = factory(User::class)->create([
            'balance' => 100,
        ]);

        $transfer = factory(Transfer::class)->create([
            'balance' => 100,
        ]);

        $response = $this->callWithdrawApi($user->getField('username'), 1, $transfer->op_transfer_id);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'op_transfer_id',
            ],
        ]);
    }

    /**
     * Call withdraw API
     * @param $username
     * @param $amount
     * @param $op_transfer_id
     * @param $game_code
     *
     * @return mixed
     */
    private function callWithdrawApi($username, $amount, $op_transfer_id = '0000', $user_type = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => $op_transfer_id,
            'user_type' => $user_type,
        ];

        return $this->post(url('v1/balance/withdraw/' . $username), $data);
    }
}
