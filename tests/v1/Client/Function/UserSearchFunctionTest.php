<?php
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\User as MARS;
use App\Models\v1\UserPHP as OG_CASH;

class UserSearchFunctionTest extends TestCase
{
    use WithoutMiddleware;

    const SEARCH_BY_ID = 1;
    const SEARCH_BY_NAME = 2;
    
    public function setup()
    {
        parent::setup();
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_search_the_user_by_userid($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $this->call_search_api(self::SEARCH_BY_ID, $user->getField('id'), 200, $userType);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_search_the_user_by_username($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $this->call_search_api(self::SEARCH_BY_NAME, $user->getField('username'), 200, $userType);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_return_invalid_search($userModel, $userType)
    {
        $this->call_search_api(self::SEARCH_BY_ID, "invalid_user", 404, $userType);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_return_an_error_search($userModel, $userType)
    {
        $this->call_search_api(null, "invalid_user", 422, $userType);
    }

    /**
     * @param $search_type
     * @param $query
     * @param $code
     * @param $user_type
     */
    private function call_search_api($search_type, $query, $code, $user_type)
    {
        $this->get('v1/user/search/' . trim($query)."/?user_type=".$user_type."&search=".$search_type);
        $this->seeStatusCode($code);
    }

}
