<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class InsertTransactionFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @var \Faker\Generator
     */
    public $faker;

    public function setup()
    {
        parent::setUp();

        $this->faker = Faker\Factory::create();
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_create_transaction($userModel, $userType)
    {
        $gameCode = $this->getGameCode();
        $user = $this->getUser($userModel);
        $data = $this->makeTransaction()->toArray();
        $data['username'] = $user->getField('username');
        $url = $this->getUrl($user->getField('username'), $gameCode);
        $response = $this->post($url, $data);

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
        ]);
        $response->seeInDatabase("{$gameCode}_transactions", $data, 'datarepo');
    }

    /**
     * @test
     */
    public function it_should_validate_game_code_parameter()
    {
        $gameCode = 'NON_EXISTING_GAME_CODE';
        $seeErrorFields = ['game_code'];
        $this->assertErrorCreateTransaction($seeErrorFields, [], $gameCode);
    }

    /**
     * @test
     */
    public function it_should_validate_bet_code_parameter()
    {
        $data = ['bet_code' => 100.00];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['bet_code' => $this->faker->words(20, true)];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_bet_amount_parameter()
    {
        $data = ['bet_amount' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_bet_place_parameter()
    {
        $data = ['bet_place' => 1000.00];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_effective_bet_amount_parameter()
    {
        $data = ['effective_bet_amount' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['effective_bet_amount' => -1];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['effective_bet_amount' => 9999999999999999.99];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_shoehandnumber_parameter()
    {
        $data = ['shoehandnumber' => false];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['shoehandnumber' => $this->faker->words(20, true)];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_gameset_id_parameter()
    {
        $data = ['gameset_id' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['gameset_id' => 9999999999999];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_gamename_parameter()
    {
        $data = ['gamename' => false];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['gamename' => $this->faker->words(100, true)];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_tablenumber_parameter()
    {
        $data = ['tablenumber' => false];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['tablenumber' => $this->faker->words(100, true)];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_balance_parameter()
    {
        $data = ['balance' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['balance' => 9999999999999999.99];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['balance' => -1];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_win_loss_parameter()
    {
        $data = ['win_loss' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['win_loss' => 9999999999999999.99];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['win_loss' => -9999999999999999.99];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_result_id_parameter()
    {
        $data = ['result_id' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['result_id' => 999999999999];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_result_parameter()
    {
        $data = ['result' => false];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_bet_date_parameter()
    {
        $data = ['bet_date' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['bet_date' => $this->faker->words(100, true)];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_super_six_parameter()
    {
        $data = ['super_six' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['super_six' => 'true' ];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['super_six' => 'false' ];
        $this->executeAssertErrorCreateTransaction($data);
    }

    /**
     * @test
     */
    public function it_should_validate_is_sidebet_parameter()
    {
        $data = ['is_sidebet' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['is_sidebet' => 'true' ];
        $this->executeAssertErrorCreateTransaction($data);
        $data = ['is_sidebet' => 'false' ];
        $this->executeAssertErrorCreateTransaction($data);
    }


    /** ============================================================================================== */
    /**
     * Code below are just helper functions
     * Code below are just helper functions
     * Code below are just helper functions
     */
    /** ============================================================================================== */

    /**
     * @param string $model
     * @param array $data
     * @return mixed
     */
    public function getUser($model = \App\Models\v1\User::class, $data = [])
    {
        return factory($model)->create($data);
    }

    public function makeTransaction($data = [])
    {
        return factory(App\Models\v1\Transaction::class)->make($data);
    }

    public function getUrl($username, $gameCode)
    {
        return '/v1/history/transaction/' . $username . '?game_code=' . $gameCode;
    }

    public function getGameCode()
    {
        if (!$game = App\Models\v1\Game::first()) {
            throw \Exception("Cannot continue testing! Game is empty!");
        }
        return $game->game_code;
    }

    public function createTransaction($gameCode = '', $data = [])
    {
        $gameCode = $gameCode ?: $this->getGameCode();
        $user = $this->getUser();
        $data = $this->makeTransaction($data)->toArray();
        $url = $this->getUrl($user->username, $gameCode);
        return $this->post($url, $data);
    }

    public function assertErrorCreateTransaction($seeErrorFields = [], $data = [], $gameCode = '')
    {
        $user = $this->getUser();
        $url = $this->getUrl($user->username, $gameCode);
        $response = $this->post($url, $data);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => $seeErrorFields,
        ]);
        return $response;
    }

    public function executeAssertErrorCreateTransaction($data)
    {
        $this->assertErrorCreateTransaction(array_keys($data), $data);
        $this->assertErrorCreateTransaction(array_keys($data), []);
    }
}
