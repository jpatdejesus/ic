<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class GetTransferFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Function test for valid get transfer.
     *
     * @group ignore
     * @dataProvider userTypes
     * @test
     */
    public function it_should_get_all_transfer_history_data($userModel, $userType)
    {
        $response = $this->get('v1/history/transfer?user_type=' . $userType . '&start=1&length=50');
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "count_records",
            "count_results",
            "data"
        ]);
    }

    /**
     * Function test for valid get transfer.
     *
     * @dataProvider userTypes
     * @group ignore
     * @test
     */
    public function it_should_have_valid_response($userModel, $userType)
    {
        $transfer = factory($userModel)->create([
            'username' => 'test_user'
        ]);

        $response = $this->get('v1/history/transfer/' . $transfer->getField('username') . '?user_type=' . $userType . '&start=1&length=50');
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "count_records",
            "count_results",
            "data"
        ]);
    }

    /**
     * Function test for user not found get transfer.
     *
     * @dataProvider userTypes
     * @group ignore
     * @test
     */
    public function it_should_have_empty_data_array($userModel, $userType)
    {
        $unauthorized_user = 'unauthorized_user';
        
        $response = $this->get('v1/history/transfer/'.$unauthorized_user.'?user_type=' . $userType . '&start=1&length=50');

        $data_array = json_decode($this->response->getContent('data'))->data;

        $response->assertResponseStatus(200);
        $response->assertTrue(empty($data_array));
        $response->seeJsonStructure([
            "message",
            "count_records",
            "count_results",
            "data"
        ]);
    }

    /**
     * Function test for unprocessable entity get transfer.
     *
     * @dataProvider userTypes('transfer')
     * @test
     */
    public function it_should_have_unprocessable_entity_response($userModel, $userType)
    {
        $transfer = factory($userModel)->create([
            'username' => 'test_user'
        ]);

        $response = $this->get('v1/history/transfer/' . $transfer->getField('username') . '?user_type=' . $userType . '&start=abc1&length=123');
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    /**
     * Function test for unprocessable entity get transfer.
     *
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_invalid_date_format_all_transfer($userModel, $userType)
    {
        $response = $this->get('v1/history/transfer?user_type=' . $userType . '&start=1&length=25&start_date=test&end_date=test');
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
    }

    public function userTypes()
    {
        $userTypes = config('ichips.user_types');
        $return = [];

        foreach ($userTypes as $key => $value) {
            $return[] = [
                $value['transfer']['model'],
                $key
            ];
        }
        return $return;
    }
}
