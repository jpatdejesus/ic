<?php

use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\User;

class UpdateUserInfoFunctionTest extends TestCase
{
    use WithoutMiddleware;

    protected $username = null;

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_update_user_info($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->put('/v1/user/' . $user->getField('username'), $this->createValidParams($userType));
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_test_invalid_user_type($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $usernameKey = $model->getfieldName('username');
        $username = factory($userModel)->create()->{$usernameKey};

        /**
         * Test if user_type is not valid
         */
        $field = 'user_type';

        $validErrorResponse = [
            "The user type field is required.",
            "The selected user type is invalid."
        ];
        /**
         * Test if user_type is not null
         */
        $this->assertions($username, $userType, $validErrorResponse, $field);

        /**
         * Test if user_type does exist in the list of valid values
         */
        $this->assertions($username, $userType, $validErrorResponse, $field, 0);


    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_test_invalid_nickname($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $usernameKey = $model->getfieldName('username');
        $username = factory($userModel)->create()->{$usernameKey};

        $field = 'nickname';

        $validErrorResponse = [
            "The nickname may not be greater than 50 characters.",
            "The nickname field is invalid."
        ];

        /**
         * Test if nickname is valid format
         */
        $this->assertions($username, $userType, $validErrorResponse, $field, 'nickname%');

        /**
         * Test if nickname does not exceed maximum character length
         */
        $this->assertions($username, $userType, $validErrorResponse, $field, str_random(51));
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_test_invalid_status($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $usernameKey = $model->getfieldName('username');
        $username = factory($userModel)->create()->{$usernameKey};

        $field = 'status';

        $validErrorResponse = [
            "The selected status is invalid."
        ];

        /**
         * Test if status does exist in the list of valid values
         */
        $this->assertions($username, $userType, $validErrorResponse, $field, 'test');
    }

    public function createValidParams($userType)
    {
        return [
            'nickname' => generate_random_string(8),
            'status' => 'ENABLED',
            'user_type' => $userType
        ];
    }

    public function createInvalidParam($userType, $testField, $value = null)
    {
        $return = [
            'nickname' => generate_random_string(),
            'status' => 'ENABLED',
            'user_type' => $userType
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }

    private function getUser()
    {
        $user = factory(User::class)->create();

        $user = User::where('username', $user->username)->first();

        return $user;
    }

    public function assertions($username, $userType, array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->put('/v1/user/' . $username, $this->createInvalidParam($userType, $field));
        } else {
            $response = $this->put('/v1/user/' . $username, $this->createInvalidParam($userType, $field, $param));
        }

        $this->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors"
        ]);

        $content = json_decode($response->response->getContent());

        $this->assertSame('Validation error!', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
