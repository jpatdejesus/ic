<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class GetUserInfoFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * Function test for valid get user info.
     *
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_valid_get_user_info_response($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->get('v1/user/' . $user->getField('username') . '?user_type=' . $userType);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data",
        ]);
    }

    /**
     * Function test for user not found get user info.
     *
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_user_not_found_get_user_info_response($userModel, $userType)
    {
        $response = $this->get('v1/user/not_found_user?user_type=' . $userType);
        $response->assertResponseStatus(404);
        $response->seeJsonStructure([
            "error",
        ]);
    }
}
