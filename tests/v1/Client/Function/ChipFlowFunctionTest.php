<?php

use App\Models\v1\User;
use App\Models\v1\UserPHP;
use Laravel\Lumen\Testing\WithoutMiddleware;

class ChipFlowFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @group ignore
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_get_all_chip_flow_data($userModel, $userType)
    {
        $response = $this->callChipFlowApi('', $userType, 1, 25);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data',
            'count_records',
            'count_results',
        ]);
    }

    /**
     * @group ignore
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_get_chip_flow_data($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callChipFlowApi($user->getField('username'), $userType, 1, 25);

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data',
            'count_records',
            'count_results',
        ]);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_not_accept_invalid_length_parameter($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callChipFlowApi($user->getField('username'), $userType, 1, 255);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'length',
            ],
        ]);
    }

    /**
     * @param $username
     * @param $start
     * @param $length
     * @return mixed
     */
    private function callChipFlowApi($username, $userType, $start, $length)
    {
        $url = $this->getUrl($userType, $start, $length, $username);
        $response = $this->get($url);
        return $response;
    }

    public function getUrl($userType, $start, $length, $username = '')
    {
        if (!$username) {
            return 'v1/history/chipflow' . '?user_type=' . $userType . '&start=' . $start . '&length=' . $length;
        }

        return 'v1/history/chipflow/' . $username
        . '?user_type=' . $userType . '&start=' . $start . '&length=' . $length;
    }
}
