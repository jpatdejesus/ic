<?php

class AuthFunctionTest extends TestCase
{
    use \App\Traits\AuthToken;
    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * It should generate token.
     * auth [POST]
     * @test
     * @return void
     */
    public function it_should_generate_token()
    {
        $user = factory(\App\Models\v1\RoleCredential::class)->create();
        $response = $this->put('v1/auth/token', [
            'username' => $user->username,
            'password' => 'password123!',
        ]);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should validate token.
     * auth [POST]
     * @test
     * @return void
     */
    public function it_should_validate_token()
    {
        $response = $this->get('v1/auth/token', $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    /**
     * It should refresh token.
     * auth [POST]
     * @test
     * @return void
     *
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function it_should_refresh_token()
    {
        $user = factory(\App\Models\v1\RoleCredential::class)->create();
        $user->password = 'password123!';
        $this->token = [
            'token' => $this->loginAs($user)->token,
        ];
        $response = $this->post('v1/auth/token', $this->token, $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "data"
        ]);
    }

    /**
     * It should destroy token.
     * auth [POST]
     * @test
     * @return void
     */
    public function it_should_destroy_token()
    {
        $response = $this->delete('v1/auth/token', [], $this->token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }
}
