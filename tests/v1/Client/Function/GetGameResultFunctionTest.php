<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 3/29/19
 * Time: 11:21 AM
 */

use App\Models\v1\GameResult;
use Laravel\Lumen\Testing\WithoutMiddleware;

class GetGameResultFunctionTest extends TestCase
{
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Function test for getting valid game result via game codes.
     * @group ignore
     * @test
     */
    public function it_should_have_valid_game_result_response()
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);
        foreach ($gameCodes as $gameCode) {
            $response = $this->get('/v1/history/gameresult?game_code='.$gameCode.'&start=1&length=25');
            $response->seeJsonStructure([
                "message",
                "count_records",
                "count_results",
                "data"
            ]);
            $response->assertResponseOk();
        }
    }

    /**
     * Function test for getting invalid game result via game codes.
     *
     * @test
     */
    public function it_should_have_invalid_game_result_response()
    {
        $response = $this->get('/v1/history/gameresult?game_code=invalid-game-result&start=1&length=25');
        $response->assertResponseStatus(500);
    }
}