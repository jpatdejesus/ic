<?php

use Laravel\Lumen\Testing\WithoutMiddleware;

class InsertGameResultFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @var \Faker\Generator
     */
    public $faker;

    public function setup()
    {
        parent::setUp();

        $this->faker = Faker\Factory::create();
    }

    /**
     * @test
     */
    public function it_should_successfully_create_game_result()
    {
        $gameCode = $this->getGameCode();

        $data = $this->makeGameResult()->toArray();

        $url = $this->getUrl($gameCode);
        $response = $this->post($url, $data);

        $response->assertResponseOk();
        $response->seeJsonStructure([
            'message',
        ]);

        $data = collect($data)->except(['created_at'])->toArray();

        $response->seeInDatabase("{$gameCode}_game_results", $data, 'datarepo');
    }

    /**
     * @test
     */
    public function it_should_validate_result_parameter()
    {
        $data = ['result' => 1];
        $this->executeAssertErrorCreateGameResult($data);
        $data = ['result' => true];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /**
     * @test
     */
    public function it_should_validate_shoehandnumber_parameter()
    {
        $data = ['shoehandnumber' => $this->faker->words(50, true)];
        $this->executeAssertErrorCreateGameResult($data);
        $data = ['shoehandnumber' => true];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /**
     * @test
     */
    public function it_should_validate_shoe_date_parameter()
    {
        $data = ['shoe_date' => 'INVALID_FORMAT'];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /**
     * @test
     */
    public function it_should_validate_table_no_parameter()
    {
        $data = ['table_no' => $this->faker->words(50, true)];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /**
     * @test
     */
    public function it_should_validate_values_parameter()
    {
        $data = ['values' => true];
        $this->executeAssertErrorCreateGameResult($data);

        $data = ['values' => $this->faker->words(11000, true)];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /**
     * @test
     */
    public function it_should_validate_empty_result_id_parameter()
    {
        $data = ['result_id' => ''];
        $this->executeAssertErrorCreateGameResult($data);
    }

    /** ============================================================================================== */
    /**
     * Code below are just helper functions
     * Code below are just helper functions
     * Code below are just helper functions
     */
    /** ============================================================================================== */
    public function makeGameResult($data = [])
    {
        return factory(App\Models\v1\GameResult::class)->make($data);
    }

    public function getUrl($gameCode)
    {
        return '/v1/history/gameresult?game_code=' . $gameCode;
    }

    public function getGameCode()
    {
        if (!$game = App\Models\v1\Game::first()) {
            throw \Exception("Game is empty!");
        }
        return $game->game_code;
    }

    public function assertErrorCreateGameResult($seeErrorFields = [], $data = [], $gameCode = '')
    {
        $url = $this->getUrl($gameCode);
        $response = $this->post($url, $data);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => $seeErrorFields,
        ]);
        return $response;
    }

    public function executeAssertErrorCreateGameResult($data, $gameCode = null)
    {
        if (!$gameCode) {
            $gameCode = $this->getGameCode();
        }
        $this->assertErrorCreateGameResult(array_keys($data), $data, $gameCode);
        $this->assertErrorCreateGameResult(array_keys($data), [], $gameCode);
    }
}
