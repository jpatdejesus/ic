<?php

use App\Models\v1\User;
use Illuminate\Support\Str;
use App\Repositories\v1\CashFlowRepository;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Http\v1\Client\Controllers\BetBulkBalanceController;

class BetBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_bet_amount($userModel, $userType)
    {
        $this->createMockCashflowPHPTableIfNotExist();
        $this->createMockUsersPHPTableIfNotExist();

        $user = factory($userModel)->create();

        $user->setField('balance', 100); //set desired balance
        $user->save();

        $bets = [
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
        ];

        $amount = 0;

        foreach ($bets as $key => $bet) {
            $amount += $bet['amount'];
        }

        $balance = number_format(($user->getField('balance') - $amount), 5, '.', '');

        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $cashflow = get_user_type_model($userType, 'cashflow');

        $response->assertResponseStatus(200);
        $response->seeJsonContains([
            'message' => BetBulkBalanceController::MESSAGE_UPDATE_SUCCESS,
            'data' => [
                'current_balance' => $balance,
            ],
        ]);

        $response->seeInDatabase($user->getTable(), [
            $user->getKeyName() => $user->getField('id'),
            $user->getFieldName('balance') => $balance,
        ], 'ichips');

        foreach ($bets as $key => $bet) {
            $response->seeInDatabase($cashflow->getTable(), [
                'username' => $user->getField('username'),
                'trans_amount' => $bet['amount'],
                'balance' => $balance,
                'game_code' => $bet['game_code'],
                'betting_code' => $bet['betting_code'],
                'trans_type' => CashFlowRepository::BET_TRANS_TYPE,
            ], 'datarepo');
        }
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_bet_max_data($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $user->setField('balance', 100); //set desired balance
        $user->save();
        $bet = 51;
        for ($i = 0; $i < $bet; $i++) {
            $bets[] =
                [
                'amount' => 1,
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ];
        }
        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_non_integer_balance_parameter($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $bets = [
            [
                'amount' => 'test',
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
        ];

        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_amount_higher_than_balance($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $user->setField('balance', 100); //set desired balance
        $user->save();

        $bets = [
            [
                'amount' => 1000,
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => Str::random(32),
            ],
        ];

        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_game_code($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $bets = [
            [
                'amount' => rand(5, 20),
                'game_code' => 'baccarats',
                'betting_code' => Str::random(32),
            ],
        ];

        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'game_code',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_betting_code_in_array_list($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $bettingCodes = [];
        $bets = [
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => 1,
            ],
            [
                'amount' => rand(5, 20),
                'game_code' => config('ichips.default_games.0.game_code'),
                'betting_code' => 1,
            ],
        ];

        $response = $this->callBetApi($user->getField('username'), $userType, $bets);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'betting_code',
            ],
        ]);
    }

    /**
     * Call Bet API
     * @param $username
     * @param $amount
     * @param $betting_code
     * @param $game_code
     *
     * @return mixed
     */

    private function callBetApi($username, $userType = 'MARS', $data)
    {
        return $this->post(url('/v1/balance/bet/' . $username), ['data' => $data, 'user_type' => $userType]);
    }
}
