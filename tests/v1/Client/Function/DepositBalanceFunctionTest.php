<?php

use App\Models\v1\Transfer;
use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Repositories\v1\CashFlowRepository;
use App\Repositories\v1\TransferRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Laravel\Lumen\Testing\WithoutMiddleware;

class DepositBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_deposit_balance($userModel, $userType)
    {
        $this->createMockUsersPHPTableIfNotExist();
        $this->createMockTransferPHPTableIfNotExist();
        $this->createMockCashflowPHPTableIfNotExist();

        $user = factory($userModel)->create();

        $user->setField('balance', 100);
        $user->save();

        $amount = 1;
        $balance = ($user->getField('balance') + $amount);
        $action = 'DEPOSIT';

        $response = $this->callDepositApi($user->getField('username'), $this->dummyData($amount, Str::random(32), $userType));
        $transfer = get_user_type_model($userType, 'transfer')->whereUsername($user->getField('username'))->latest()->first();

        $transferTable = get_user_type_model($userType, 'transfer');
        $cashflowTable = get_user_type_model($userType, 'cashflow');

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data' => [
                'transfer_id',
                'current_balance',
            ],
        ]);

        $response->seeInDatabase($transferTable->getTable(), [
            'username' => $user->getField('username'),
            'balance' => $balance,
            'actions' => TransferRepository::DEPOSIT_ACTION,
        ], 'datarepo');

        $response->seeInDatabase($user->getTable(), [
            $user->getKeyName() => $user->getField('id'),
            $user->getFieldName('balance') => $balance,
        ], 'ichips');
        
        $response->seeInDatabase($cashflowTable->getTable(), [
            'username' => $user->getField('username'),
            'trans_type' => CashFlowRepository::DEPOSIT_TRANS_TYPE,
            'betting_code' => $transfer->getField('transfer_id'),
        ], 'datarepo');
    }

    /**
     * @test
     */
    public function it_should_validate_non_integer_balance_parameter()
    {
        $user = factory(User::class)->create();

        $response = $this->callDepositApi($user->username, $this->dummyData('test'));

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_all_required_fields()
    {
        $user = factory(User::class)->create();
        $response = $this->callDepositApi($user->username, $this->dummyData('', '', 'MARS'));
        
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
                'op_transfer_id',
            ],
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_unique_op_transfer_id_param()
    {
        $amount = 1;
        $user = factory(User::class)->create();
        $op_transfer_id = time();
        $op_transfer = factory(Transfer::class)->create([
            'op_transfer_id' => $op_transfer_id,
        ]);

        $response = $this->callDepositApi(
            $user->username,
            $this->dummyData($amount, $op_transfer->op_transfer_id)
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'op_transfer_id',
            ],
        ]);
    }

    private function callDepositApi($username, $data)
    {
        $response = $this->post(url('v1/balance/deposit/' . $username), $data);
        return $response;
    }

    private function dummyData($amount, $opTransferId = '00000', $userType = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => $opTransferId,
            'user_type' => $userType,
        ];

        return $data;
    }
}
