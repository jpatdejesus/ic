<?php

use App\Http\v1\Client\Controllers\PayoutBalanceController;
use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Repositories\v1\CashFlowRepository;
use Laravel\Lumen\Testing\WithoutMiddleware;

class PayoutBalanceFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_payout_the_user($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');

        $this->createMockCashflowPHPTableIfNotExist();
        $this->createMockUsersPHPTableIfNotExist();

        $user = factory($userModel)->create([$model->getFieldName('balance') => 100,]);
        $balance = rand(0, 100);

        $user->setField('balance', $balance);
        $user->save();

        $newBalance = (float)$user->balance + (float)($balance);
        $betting_code = CashFlowRepository::PAYOUT_TRANS_TYPE . "_". $userType . "_". strtoupper(substr(md5(time()), 0, 5));
        $response = $this->call_payout_api($user->getField('username'), $balance, $betting_code, $userType);
        $cashflow = get_user_type_model($userType, 'cashflow');

        $response->assertResponseOk();
        $response->seeJsonStructure([
            'message',
            'data' => [
                'current_balance'
            ],
        ]);
        /*$response->seeInDatabase($cashflow->getTable(), [
            'username' => $user->getField('username'),
            'betting_code' => $betting_code,
            'trans_amount' => (float)$balance,
            'balance' => $newBalance,
            'trans_type' => CashFlowRepository::PAYOUT_TRANS_TYPE,
            'game_code' => 'baccarat'
        ], 'datarepo');*/
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_null_values_request($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $user = factory($userModel)->create([
            $model->getFieldName('balance') => 100,
        ]);
        $balance = null;
        $betting_code = CashFlowRepository::PAYOUT_TRANS_TYPE . "_". strtoupper(substr(md5(time()), 0, 5));
        $response = $this->call_payout_api($user->getField('username'), $balance, $betting_code);
        $this->seeStatusCode(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_non_integer_balance_request($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $user = factory($userModel)->create([
            $model->getFieldName('balance') => 100,
        ]);
        $balance = "1234abc";
        $betting_code = CashFlowRepository::PAYOUT_TRANS_TYPE . "_". strtoupper(substr(md5(time()), 0, 5));
        $response = $this->call_payout_api($user->getField('username'), $balance, $betting_code);
        $this->seeStatusCode(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [],
        ]);
    }

    /**
     * @param $username
     * @param $payout_balance
     * @param $code
     * @return mixed
     */
    private function call_payout_api($username, $amount, $code, $userType = 'MARS')
    {
        $action = 'PAYOUT';

        $data = [
            'amount' => $amount,
            'game_code' => "baccarat",
            'betting_code' => $code,
            'trans_type' => $action,
            'user_type' => $userType
        ];

        return $this->post(url('v1/balance/payout/' . $username), $data);
    }

}
