<?php

use Laravel\Lumen\Testing\WithoutMiddleware;
use App\Models\v1\User;
use App\Models\v1\UserPHP;

class UpdateUserPointOrExpTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_add_point($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdatePointUrl($user->getField('username'), $userType);
        $data = [
            'user_points_method' => '+',
            'user_points_value' => 100.00,
        ];
        $seeInDatabaseData = [
            $user->getFieldName('username') => $user->getField('username'),
            $user->getFieldName('point') => $user->getField('point') + $data['user_points_value']
        ];
        $responseData = [ 'point' => "100.00000" ];
        $this->assertSuccessAddOrDeductUpdate($user, $url, $data, $seeInDatabaseData, $responseData);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_deduct_point($userClass, $userType)
    {
        $user = factory($userClass)->make();
        $user = $this->getUser($userClass, [$user->getFieldName('point') => 100.00]);
        $url = $this->getUpdatePointUrl($user->getField('username'), $userType);
        $data = [
            'user_points_method' => '-',
            'user_points_value' => 50.00,
        ];
        $seeInDatabaseData = [
            $user->getFieldName('username') => $user->getField('username'),
            $user->getFieldName('point') => $user->getField('point') - $data['user_points_value']
        ];
        $responseData = [ 'point' => "50.00000" ];
        $this->assertSuccessAddOrDeductUpdate($user, $url, $data, $seeInDatabaseData, $responseData);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_max_user_points_value_to_deduct($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdatePointUrl($user->getField('username'), $userType);
        $data = [
            'user_points_method' => '-',
            'user_points_value' => 50.00,
        ];
        $errorFieldList = [
            'user_points_value' => ['The user points value to be deducted is invalid.']
        ];
        $this->assertErrorDeductUpdate($url, $data, $errorFieldList);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_empty_and_incorrect_point_params($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdatePointUrl($user->getField('username'), $userType);
        $data = [
            'user_points_method' => 'INCORRECT_PARAMS',
            'user_points_value' => 'INCORRECT_PARAMS',
        ];
        //with empty data params
        $this->assertErrorAddOrDeductUpdate($url, [], array_keys($data));
        // with incorrect data params
        $this->assertErrorAddOrDeductUpdate($url, $data, array_keys($data));
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_negative_user_points_value_params($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdatePointUrl($user->getField('username'), $userType);
        $data = [
            'user_points_method' => '+',
            'user_points_value' => '-1',
        ];
        $this->assertErrorAddOrDeductUpdate($url, $data, ['user_points_value']);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_add_exp($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdateExpUrl($user->getField('username'), $userType);
        $data = [
            'user_exp_method' => '+',
            'user_exp_value' => 100.00,
        ];
        $seeInDatabaseData = [
            $user->getFieldName('username') => $user->getField('username'),
            $user->getFieldName('exp') => $user->getField('exp') + $data['user_exp_value']
        ];
        $responseData = [ 'exp' => "100.00000" ];
        $this->assertSuccessAddOrDeductUpdate($user, $url, $data, $seeInDatabaseData, $responseData);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_successfully_deduct_exp($userClass, $userType)
    {
        $user = factory($userClass)->make();
        $user = $this->getUser($userClass, [$user->getFieldName('exp') => 100.00]);
        $url = $this->getUpdateExpUrl($user->getField('username'), $userType);
        $data = [
            'user_exp_method' => '-',
            'user_exp_value' => 50.00,
        ];
        $seeInDatabaseData = [
            $user->getFieldName('username') => $user->getField('username'),
            $user->getFieldName('exp') => $user->getField('exp') - $data['user_exp_value']
        ];
        $responseData = [ 'exp' => "50.00000" ];
        $this->assertSuccessAddOrDeductUpdate($user, $url, $data, $seeInDatabaseData, $responseData);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_max_user_exp_value_to_deduct($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdateExpUrl($user->getField('username'), $userType);
        $data = [
            'user_exp_method' => '-',
            'user_exp_value' => 50.00,
        ];
        $errorFieldList = [
            'user_exp_value' => ['The user exp value to be deducted is invalid.']
        ];
        $this->assertErrorDeductUpdate($url, $data, $errorFieldList);
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_empty_and_incorrect_exp_params($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdateExpUrl($user->getField('username'), $userType);
        $data = [
            'user_exp_method' => 'INCORRECT_PARAMS',
            'user_exp_value' => 'INCORRECT_PARAMS',
        ];
        //with empty data params
        $this->assertErrorAddOrDeductUpdate($url, [], array_keys($data));
        // with incorrect data params
        $this->assertErrorAddOrDeductUpdate($url, $data, array_keys($data));
    }

    /**
     * @test
     * @dataProvider userTypes
     */
    public function it_should_validate_negative_exp_value_params($userClass, $userType)
    {
        $user = $this->getUser($userClass);
        $url = $this->getUpdateExpUrl($user->getField('username'), $userType);
        $data = [
            'user_exp_method' => '+',
            'user_exp_value' => '-1',
        ];
        $this->assertErrorAddOrDeductUpdate($url, $data, ['user_exp_value']);
    }

    public function assertSuccessAddOrDeductUpdate($user, $url, $data, $seeInDatabaseData, $responseData)
    {
        $response = $this->put($url, $data);
        $response->assertResponseOk();
        $response->seeJsonEquals([
            'message' => 'Success',
            'data' => $responseData
        ]);
        $response->seeInDatabase($user->getTable(), $seeInDatabaseData);
    }

    public function assertErrorAddOrDeductUpdate($url, $data, $errorFieldList)
    {
        $response = $this->put($url, $data);
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => $errorFieldList,
        ]);
    }

    public function assertErrorDeductUpdate($url, $data, $errorFieldList)
    {
        $response = $this->put($url, $data);
        $response->assertResponseStatus(422);
        $response->seeJsonEquals([
            'message' => 'Validation error!',
            'errors' => $errorFieldList,
        ]);
    }

    public function getTableName($userClass)
    {
        return (new $userClass())->getTableName();
    }

    public function getUser($userClass, $data = [])
    {
        return factory($userClass)->create($data);
    }

    public function getUpdatePointUrl($username, $userType)
    {
        return '/v1/user/point/' . $username . '?user_type=' . $userType;
    }

    public function getUpdateExpUrl($username, $userType)
    {
        return '/v1/user/exp/' . $username . '?user_type=' . $userType;

    }

}
