<?php

use Laravel\Lumen\Testing\WithoutMiddleware;
use Carbon\Carbon;
use App\Models\v1\User;
use App\Models\v1\UserPHP;

class GetTransactionFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @group ignore
     * @test
     */
    public function it_should_get_all_transactions_data($userModel, $userType)
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);
        $gameCode = $gameCodes[rand(0, sizeof($gameCodes) - 1)];
        $start_date = Carbon::now()->format('Y-m-d h:i:s');
        $end_date = Carbon::now()->format('Y-m-d h:i:s');

        $response = $this->get('/v1/history/transaction?user_type='.$userType.'&game_code=' . $gameCode .
            '&start=1&length=25&start_date=' . $start_date . '&end_date=' . $end_date .
            '&bet_code=test&tablenumber=1&shoehandnumber=1');

        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();
    }

    /**
     * @dataProvider userTypes
     * @group ignore
     * @test
     */
    public function it_should_have_valid_response_data($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $usernameKey = $model->getfieldName('username');
        $username = $model->where($usernameKey, 'test_account_' . strtolower($userType))->first()->{$usernameKey};
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);
        $gameCode = $gameCodes[rand(0, sizeof($gameCodes) - 1)];
        $start_date = Carbon::now()->format('Y-m-d h:i:s');
        $end_date = Carbon::now()->format('Y-m-d h:i:s');

        $response = $this->get('/v1/history/transaction/' . $username . '?user_type='.$userType.'&game_code=' .
            $gameCode . '&start=1&length=25&start_date=' . $start_date . '&end_date=' . $end_date .
            '&tablenumber=1&shoehandnumber=1');

        $response->seeJsonStructure([
            "count_records",
            "count_results",
            "data"
        ]);
        $response->assertResponseOk();
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_invalid_response_data($userModel, $userType)
    {
        $model = get_user_type_model($userType, 'users');
        $usernameKey = $model->getfieldName('username');
        $username = $model->where($usernameKey, 'test_account_' . strtolower($userType))->first()->{$usernameKey};
        $start_date = Carbon::now()->format('Y-m-d h:i:s');
        $end_date = Carbon::now()->format('Y-m-d h:i:s');

        $response = $this->get('/v1/history/transaction/' . $username . '?user_type=' . $userType .
            '&game_code=test&start=1&length=25&start_date=' . $start_date . '&end_date=' . $end_date .
            '&tablenumber=1&shoehandnumber=1');

        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
        $response->assertResponseStatus(422);

        $content = json_decode($response->response->getContent());

        $this->assertSame('Get transaction history fail.', $content->message);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_invalid_date_format_response($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->get('/v1/history/transaction/' . $user->getField('username') . '?user_type=' .
            $userType . '&game_code=test&start=1&length=25&start_date=test&end_date=test&tablenumber=1&shoehandnumber=1');

        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
        $response->assertResponseStatus(422);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_have_invalid_date_format_all_transaction_response($userModel, $userType)
    {
        $response = $this->get('/v1/history/transaction?user_type=' . $userType .
            '&game_code=test&start=1&length=25&start_date=test&end_date=test&tablenumber=1&shoehandnumber=1');

        $response->seeJsonStructure([
            "message",
            "errors"
        ]);
        $response->assertResponseStatus(422);
    }
}
