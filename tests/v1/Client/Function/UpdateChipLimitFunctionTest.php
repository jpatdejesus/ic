<?php

use App\Models\v1\User;
use App\Models\v1\UserPHP;
use App\Models\v1\Transfer;
use Laravel\Lumen\Testing\WithoutMiddleware;

class UpdateChipLimitFunctionTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_successfully_update_chip_limit($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            $userType,
            1,
            1000
        );

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message'
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_non_existing_game_code($userModel, $userType)
    {
        $user = factory($userModel)->create();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            'NON_EXISTING_GAME_CODE',
            $userType,
            1,
            1000
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'game_code',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_non_existing_user_type($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            'NON_EXISTING_USER_TYPE',
            1,
            1000
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'user_type',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_missing_min_value($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            $userType,
            null,
            1000
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'min',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_missing_max_value($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            $userType,
            1,
            null
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'max',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_error_min_max_value($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            $userType,
            0,
            999999999
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'min',
                'max',
            ],
        ]);
    }

    /**
     * @dataProvider userTypes
     * @test
     */
    public function it_should_validate_min_max_less_and_greater_than_value($userModel, $userType)
    {
        $user = factory($userModel)->create();
        $games = \App\Models\v1\Game::first();

        $response = $this->callUpdateChipLimitApi(
            $user->getField('username'),
            $games->game_code,
            $userType,
            99999999,
            1
        );

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'min',
                'max',
            ],
        ]);
    }

    /**
     * Call Update Chip Limit API
     *
     * @param $username
     * @param $game_code
     * @param $user_type
     * @param $min
     * @param $max
     *
     * @return mixed
     */
    private function callUpdateChipLimitApi(
        $username,
        $game_code = null,
        $user_type = null,
        $min = null,
        $max = null
    )
    {
        $response = $this->put(url('v1/chiplimit/' . $username . '?game_code=' . $game_code . '&user_type=' . $user_type . '&min=' . $min . '&max=' . $max));

        return $response;
    }
}
