<?php

use App\Traits\AuthToken;
use App\Models\v1\UserPHP;
use App\Repositories\v1\CashFlowRepository;
use App\Repositories\v1\TransferRepository;

class Case09Test extends TestCase
{
    use AuthToken;

    /**
     * @var mixed
     */
    protected $player;

    /**
     * Function test for Case Nine
     *
     * @dataProvider userTypes
     * @test
     */
    public function case_nine_test($userModel, $userType)
    {
        $createPlayerParameter = [
            'currency' => 'CNY',
            'username' => 'user_' . time(),
            'nickname' => generate_random_string(8),
            'status' => 'ENABLED',
            'user_type' => $userType,
        ];

        $response = $this->registerPlayer($userModel, $userType, $createPlayerParameter);

        echo 'It should register player...' . PHP_EOL;
        echo 'Response -> ' . print_r($response, true) . PHP_EOL;
        echo 'Success' . PHP_EOL;

        $responseMessage = $this->depositAmount($userModel, $userType, $this->player);

        echo 'It should add a 1000 amount in his account...' . PHP_EOL;
        echo 'Response -> ' . print_r($responseMessage, true) . PHP_EOL;
        echo 'Success' . PHP_EOL;

        $responseMessage = $this->withdrawAmount($userType, $this->player);
        $balance = get_user_type_model($userType, 'users')->whereUsername($this->player->getField('username'))->first()->getField('balance');

        echo 'It should not withdraw a 2000 amount in his account...' . PHP_EOL;
        echo 'Response -> ' . print_r($responseMessage, true) . PHP_EOL;
        echo 'User -> ' . $this->player->username . PHP_EOL;
        echo 'Balance -> ' . $balance . PHP_EOL;
        echo 'Success' . PHP_EOL;
    }

    /**
     * @param $userModel
     * @param $userType
     * @param $createPlayerParameter
     * @return string
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function registerPlayer($userModel, $userType, $createPlayerParameter)
    {
        if ($userModel === UserPHP::class) {
            $user = factory($userModel)->create();
            $this->player = get_user_type_model($userType, 'users')->whereUsername($user->getField('username'))->first();

            return $user->getField('username');
        }

        $response = $this->post('/v1/user', $createPlayerParameter, $this->login());
        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message",
            "data" => ['user_id'],
        ]);

        $this->player = get_user_type_model($userType, 'users')->whereUsername($createPlayerParameter['username'])->first();

        return $response->response->getContent();
    }

    /**
     * @param $userModel
     * @param $userType
     * @param $player
     * @return string
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function depositAmount($userModel, $userType, $player)
    {
        $amount = 1000;
        $balance = ($player->getField('balance') + $amount);

        $params = [
            'amount' => $amount,
            'op_transfer_id' => rand(11111, 99999) . time(),
            'user_type' => $userType,
        ];

        $response = $this->post(url('v1/balance/deposit/' . $player->getField('username')), $params, $this->login());
        $transfer = get_user_type_model($userType, 'transfer')->whereUsername($player->getField('username'))->latest()->first();

        $transferTable = get_user_type_model($userType, 'transfer');
        $cashflowTable = get_user_type_model($userType, 'cashflow');

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            'message',
            'data' => [
                'transfer_id',
                'current_balance',
            ],
        ]);

        $response->seeInDatabase($transferTable->getTable(), [
            'username' => $player->getField('username'),
            'balance' => $balance,
            'actions' => TransferRepository::DEPOSIT_ACTION,
        ], 'datarepo');

        $response->seeInDatabase($player->getTable(), [
            $player->getKeyName() => $player->getField('id'),
            $player->getFieldName('balance') => $balance,
        ], 'ichips');

        $response->seeInDatabase($cashflowTable->getTable(), [
            'username' => $player->getField('username'),
            'trans_type' => CashFlowRepository::DEPOSIT_TRANS_TYPE,
            'betting_code' => $transfer->getField('transfer_id'),
        ], 'datarepo');

        return $response->response->getContent();
    }

    /**
     * @param $userType
     * @param $player
     * @return mixed
     */
    public function withdrawAmount($userType, $player)
    {
        $params = [
            'amount' => 2000,
            'op_transfer_id' => rand(11111, 99999) . time(),
            'user_type' => $userType,
        ];

        $response = $this->post(url('v1/balance/withdraw/' . $player->getField('username')), $params, $this->login());
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'message',
            'errors' => [
                'amount',
            ],
        ]);

        return $response->response->getContent();
    }
}
