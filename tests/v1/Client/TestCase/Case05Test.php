<?php

use App\Traits\AuthToken;

class Case05Test extends TestCase
{
    use AuthToken;

    /**
     * Function test for Case Five.
     *
     * @dataProvider userTypes
     * @test
     */
    public function case_five_should_be_successful($userModel, $userType)
    {
        echo "Player access the system." . PHP_EOL;

        $token = $this->login();
        $userData = factory($userModel)->create();
        echo "Operator disabled a player..." . PHP_EOL;

        $this->disableUser($userData, $userType, $token);

        echo "System updated the player’s status." . PHP_EOL;
        echo "Player cannot play a game." . PHP_EOL;
        echo "Player request to enable account." . PHP_EOL;
        echo "Operator enables the player’s account..." . PHP_EOL;

        $this->enableUser($userData, $userType, $token);

        echo "System updated the player’s status." . PHP_EOL;
        echo "Case five ran successfully!" . PHP_EOL;
    }

    private function disableUser($userData, $userType, $token) {
        $response = $this->delete('v1/user/' . $userData->getField('username') . "?user_type=" . $userType, [], $token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }

    private function enableUser($userData, $userType, $token) {
        $response = $this->put('/v1/user/' . $userData->getField('username') . "?user_type=" . $userType, ['nickname' => 'test_nickname', 'status' => 'ENABLED'], $token);
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message"
        ]);
    }
}
