<?php

use Illuminate\Support\Str;
use App\Models\v1\RoleCredential;

class Case03Test extends TestCase
{
    use \App\Traits\AuthToken;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * Case Three Test
     *
     * @test
     * @dataProvider userTypes
     */
    public function it_should_have_case_three_successful($userClass, $userType)
    {
        // 1. Player registers to the system
        $user = factory($userClass)->make();
        $user = factory($userClass)->create([
            $user->getFieldName('balance') => 0,
        ]);

        $userRoleCredential = factory(RoleCredential::class)->create();
        echo PHP_EOL."Player registers to the system".PHP_EOL;

        // 2. Player access the system
        $playerRegisterAndAccessResponse = $this->put('v1/auth/token', [
            'username' => $userRoleCredential->username,
            'password' => 'password123!',
        ]);
        $playerRegisterAndAccessResponse->assertResponseStatus(200);
        echo "Player access the system".PHP_EOL;

        // 3. Player and system deposits a total amount of 2000 to account
        $depositAmount = 2000;
        $action = 'DEPOSIT';
        $depositResponse = $this->callDepositApi($user->getField('username'), $depositAmount, $userType);
        $depositResponse->assertResponseStatus(200);
        echo "Player deposits 1000 to his account".PHP_EOL;
        echo "System adds 1000 to player's account".PHP_EOL;

        //4. Player bets a total amount of 1000 to Banker and Player
        $betAmount = 1000;
        $action = 'BET';
        $betResponse = $this->callBetApi($user->getField('username'), $betAmount, $userType);
        $betResponse->assertResponseStatus(200);
        echo "Player bets an amount of 500 on Banker".PHP_EOL;
        echo "Player bets an amount of 500 on Player".PHP_EOL;

        // 5. Player's account earns EXP
        $expResponse = $this->callUpdateExpApi($user->getField('username'), $userType);
        $expResponse->assertResponseStatus(200);
        echo "Player's account earns EXP".PHP_EOL;

        // 6. Player's current balance is deducted by 1000
        $balanceDeductResponse =  $this->callGetBalanceApi($user->getField('username'), $userType);
        $balanceDeductResponse->assertResponseStatus(200);
        $balanceDeductResponseContent = json_decode($balanceDeductResponse->response->getContent());
        $balanceDeductResponse->assertEquals($balanceDeductResponseContent->data->{$user->getFieldName('balance')}, 1000);
        echo "Player's current balance is deducted by 1000".PHP_EOL;

        // 7. Player wins on Banker and getting an amount of 1000 added to his current balance
        $winAmount = 1000;
        $action = 'DEPOSIT';
        $winResponse = $this->callDepositApi($user->getField('username'), $winAmount, $userType);
        // $winResponse->assertResponseStatus(200);
        // $winResponseContent = json_decode($winResponse->response->getContent());
        // $winResponse->assertEquals($winResponseContent->data->balance, 2000);
        echo "Player's wins on Banker and getting an amount of 1000 added to his current balance".PHP_EOL;

        // 8. Player's account earns Points
        $pointResponse = $this->callUpdatePointApi($user->getField('username'), $userType);
        $pointResponse->assertResponseStatus(200);
        echo "Player's account earns Points".PHP_EOL;
    }

    private function callDepositApi($username, $amount, $userType = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => rand(11111,99999) . time(),
            'user_type' => $userType,
        ];

        $response = $this->post(url('v1/balance/deposit/' . $username), $data, $this->token);

        return $response;
    }

    private function callBetApi($username, $amount, $userType = 'MARS')
    {   
        $data = [
            'amount' => $amount,
            'game_code' => config('ichips.default_games.0.game_code'),
            'betting_code' => Str::random(32),
            'user_type' => $userType,
        ];

        $response = $this->post(url('/v1/balance/bet/' . $username), $data, $this->token);

        return $response;
    }

    private function callUpdateExpApi($username, $userType = 'MARS')
    {
        $data = [
            'user_exp_method' => '+',
            'user_exp_value' => 100.00,
        ];

        $response = $this->put(url('/v1/user/exp/' . $username . '?user_type=' . $userType), $data, $this->token);
        
        return $response;
    }

    private function callUpdatePointApi($username, $userType = 'MARS')
    {
        $data = [
            'user_points_method' => '+',
            'user_points_value' => 100.00,
        ];

        $response = $this->put(url('/v1/user/point/' . $username . '?user_type=' . $userType), $data, $this->token);

        return $response;
    }

    private function callGetBalanceApi($username, $userType = 'MARS')
    {
        $response = $this->get(url('/v1/balance/' . $username . '?user_type=' . $userType), $this->token);

        return $response;
    }
}
