<?php

use App\Models\v1\User;
use App\Traits\AuthToken;

class Case07Test extends TestCase
{
    use AuthToken;

    const SEARCH_BY_ID = 1;
    const SEARCH_BY_NAME = 2;

    /**
     * @test
     * @dataProvider userTypes
     */
    public function case_seven_should_have_valid_get_specific_user($userModel, $userType)
    {
        $userData = factory($userModel)->create();
        echo 'Scenario 1: Search User by Username' . PHP_EOL;
        $this->searchUserByUsername($userData, $userType);
        echo 'Scenario 2: Search User by User ID' . PHP_EOL;
        $this->searchUserByID($userData, $userType);
        echo 'Scenario 3: Get Game Results' . PHP_EOL;
        $this->getGameResults($userType);
    }

    // Search User by Username
    public function searchUserByUsername($username, $userType)
    {
        $response = $this->get('v1/user/search/' . $username->getField('username') . '?search=' . self::SEARCH_BY_NAME . '&user_type=' . $userType, $this->login());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    // Search User by User ID
    public function searchUserByID($username, $userType)
    {
        $response = $this->get('v1/user/search/' . $username->getField('id') . '?search=' . self::SEARCH_BY_ID . '&user_type=' . $userType , $this->login());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    // Get Game Results
    public function getGameResults()
    {
        $response = $this->get('v1/history/gameresult?game_code=' . array_rand($this->gameTypes()) . '&start=1&length=25', $this->login());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }


    /** ============================================================================================== */
    /**
     * Codes below are just helper functions
     */
    /** ============================================================================================== */

    public function gameTypes()
    {
        $data = [
            'baccarat' => 'baccarat',
            'dragontiger' => 'dragontiger',
            'fantan' => 'fantan',
            'moneywheel' => 'moneywheel',
            'roulette' => 'roulette',
            'sicbo' => 'sicbo',
        ];

        return $data;
    }
}
