<?php
use App\Models\v1\Transfer;
use Illuminate\Support\Str;
use App\Models\v1\RoleCredential;

class Case02Test extends TestCase
{
    use \App\Traits\AuthToken;

    /**
     * Determines if the test will create new user or will use an existing one
     * If false, then $userId should have a value
     *
     * @var boolean
     */
    public $createNewUser = true;

    /**
     * @var mixed
     */
    public $token;

    public function setUp()
    {
        parent::setUp();

        $this->token = $this->login();
    }

    /**
     * Function test for Case One.
     *
     * @test
     * @dataProvider userTypes
     */
    public function it_should_have_case_two_successful($userClass, $userType)
    {
        //Player registers to the system
        $user = factory($userClass)->make();
        $user = factory($userClass)->create([
            $user->getFieldName('balance') => 0,
        ]);

        $userRoleCredential = factory(RoleCredential::class)->create();
        echo PHP_EOL . "Player registers to the system" . PHP_EOL;

        //Player access the system
        $playerRegisterAndAccessResponse = $this->put('v1/auth/token', [
            'username' => $userRoleCredential->username,
            'password' => 'password123!',
        ]);
        $playerRegisterAndAccessResponse->assertResponseStatus(200);
        echo "Player access the system" . PHP_EOL;

        // Deposit Test

        $amount = 1000;
        $response = $this->callDepositApi($user->getField('username'), $amount, $userType);
        
        $response->assertResponseStatus(200);
        echo "Response -> Player deposit an amount of 1000 on player account" . PHP_EOL;
        echo "Response -> System add an amount of 1000 on player account" . PHP_EOL;
        echo "Response -> " . print_r($response->response->content(), true) . PHP_EOL;

        //Bet Test
        $amount = 500;
        $betResponse = $this->callBetApi($user->getField('username'), $amount, $userType);
        $betResponse->assertResponseStatus(200);
        echo "Response -> Player bets an amount of 500 on Banker" . PHP_EOL;

        //Player earn exp
        $expResponse = $this->callUpdateExpApi($user->getField('username'), $userType);
        $expResponse->assertResponseStatus(200);
        echo "Response ->Player's account earns EXP" . print_r($response->response->content(), true) . PHP_EOL;

        //Deduct Player
        $balanceDeductResponse = $this->callGetBalanceApi($user->getField('username'), $userType);
        $balanceDeductResponse->assertResponseStatus(200);
        $balanceDeductResponseContent = json_decode($balanceDeductResponse->response->getContent());
        $balanceDeductResponse->assertEquals($balanceDeductResponseContent->data->{$user->getFieldName('balance')}, 500);
        echo "Response -> Player's current balance is deducted by 500" . PHP_EOL;

        //Player win
        $winAmount = 1000;
        $winResponse = $this->callDepositApi($user->getField('username'), $amount, $userType);
        $transfer = Transfer::whereUsername($user->getField('username'))->latest()->first();
        echo "Response -> Player's wins and getting an amount 1000" . PHP_EOL;

        // Playerearnpoints
        $pointResponse = $this->callUpdatePointApi($user->getField('username'), $userType);
        $pointResponse->assertResponseStatus(200);
        echo "Response ->Player's account earns Points" . PHP_EOL;

        //Player Withdraw
        $amount = 500;
        $response = $this->callWithdrawApi($user->getField('username'), $amount, $userType);
        $response->assertResponseStatus(200);
        echo "Response -> Player withdraw an amount 500" . PHP_EOL;

        //Deduct Player
        $balanceDeductResponse = $this->callGetBalanceApi($user->getField('username'), $userType);
        $balanceDeductResponse->assertResponseStatus(200);
        $balanceDeductResponseContent = json_decode($balanceDeductResponse->response->getContent());
        $balanceDeductResponse->assertEquals($balanceDeductResponseContent->data->{$user->getFieldName('balance')}, 500);
        echo "Response -> Player's current balance is deducted by 500" . PHP_EOL;

    }

    /**
     * @param $usernameclear
     * @param $data
     * @return mixed
     */
    private function callDepositApi($username, $amount, $userType = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => rand(11111, 99999) . time(),
            'user_type' => $userType,
        ];

        $response = $this->post(url('v1/balance/deposit/' . $username), $data, $this->token);

        return $response;
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callUpdatePointApi($username, $userType)
    {
        $data = [
            'user_points_method' => '+',
            'user_points_value' => 100.00,
        ];

        $response = $this->put(url('/v1/user/point/' . $username . '?user_type=' . $userType), $data, $this->token);

        return $response;
    }

    /**
     * @param $username
     * @param $amount
     * @return mixed
     */

    private function callBetApi($username, $amount, $userType = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'game_code' => config('ichips.default_games.0.game_code'),
            'betting_code' => Str::random(32),
            'user_type' => $userType,
        ];

        $response = $this->post(url('/v1/balance/bet/' . $username), $data, $this->token);
        return $response;
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callGetBalanceApi($username, $userType = 'MARS')
    {
        $response = $this->get(url('/v1/balance/' . $username . '?user_type=' . $userType), $this->token);

        return $response;
    }

    /**
     * @param $username
     * @return mixed
     */
    private function callUpdateExpApi($username, $userType)
    {
        $data = [
            'user_exp_method' => '+',
            'user_exp_value' => 100.00,
        ];

        $response = $this->put(url('/v1/user/exp/' . $username . '?user_type=' . $userType), $data, $this->token);

        return $response;
    }

    /**
     * @param $username
     * @param $amount
     * @param $op_transfer_id
     * @return mixed
     */
    private function callWithdrawApi($username, $amount, $userType = 'MARS')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => rand(11111, 99999) . time(),
            'user_type' => $userType,
        ];

        $response = $this->post(url('v1/balance/withdraw/' . $username), $data, $this->token);

        return $response;
    }
}
