<?php

use App\Models\v1\UserPHP;
use App\Traits\AuthToken;

class Case08Test extends TestCase
{
    use AuthToken;

    protected $userInfo;

    protected $player;

    /**
     * @dataProvider userTypes
     * @test
     */
    public function case_eight_should_return_valid_response($userModel, $userType)
    {
        $userData = $this->userInfo($userType);

        echo 'Scenario 1: Player register to the system' . PHP_EOL;
        $this->playerRegister($userModel, $userType, $userData);
        echo 'Scenario 2: Player withdraw from his account' . PHP_EOL;
        $this->playerWithdraw($userType);
        echo 'Scenario 3: Check player\'s Balance' . PHP_EOL;
        $this->checkPlayerBalance($userType);
    }

    // Scenario 1: Player register to the system
    public function playerRegister($userModel, $userType, $data)
    {
        if ($userModel === UserPHP::class) {
            $user = factory($userModel)->create();

            $this->player = get_user_type_model($userType, 'users')->whereUsername($user->getField('username'))->first();

            return [
                'username' => $user->getField('username'),
            ];
        }

        $response = $this->post('/v1/user', $data, $this->login());
        $response->assertResponseStatus(201);
        $response->seeJsonStructure([
            "message",
        ]);

        $this->player = get_user_type_model($userType, 'users')->whereUsername($data['username'])->first();

        return $response->response->getContent();
    }

    // Scenario 2: Player withdraw from his account
    public function playerWithdraw($userType)
    {
        $validParams = [
            'op_transfer_id' => str_random(8),
            'amount' => 1000,
        ];

        $response = $this->post('v1/balance/withdraw/' . $this->player->getField('username') . '?user_type=' . $userType, $validParams, $this->login());
        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            "message",
            "errors" => [
                "amount"
            ],
        ]);
    }

    // Scenario 3: Check player's Balance
    public function checkPlayerBalance($userType)
    {
        $response = $this->get('v1/balance/' . $this->player->getField('username') . '?user_type=' . $userType, $this->login());
        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
        ]);
    }

    /** ============================================================================================== */
    /**
     * Code below is a just helper function
     */
    /** ============================================================================================== */

    public function userInfo($userType)
    {
        $data = [
            'currency' => factory(\App\Models\v1\Currency::class)->create()->currency_code,
            'username' => strtolower(str_random(8)),
            'nickname' => generate_random_string(8),
            'status' => 'ENABLED',
            'user_type' => $userType
        ];

        return $data;
    }
}
