<?php

use App\Traits\AuthToken;

class Case06Test extends TestCase
{
    use AuthToken;

    /**
     * Function test for Case Six.
     *
     * @dataProvider userTypes
     * @test
     */
    public function case_six_should_be_successful($userModel, $userType)
    {
        $this->createMockUsersPHPTableIfNotExist();
        $this->createMockTransferPHPTableIfNotExist();
        $this->createMockCashflowPHPTableIfNotExist();

        echo "Player registers to the system." . PHP_EOL;

        $userData = factory($userModel)->create();

        echo "Player access the system." . PHP_EOL;

        $token = $this->login();

        echo "Player deposit “1000” to his account." . PHP_EOL;

        $this->depositBalance($userData, 1000, $userType, $token);

        echo "System adds an amount of “1000” to the player’s account." . PHP_EOL;
        sleep(1);
        echo "Player deposit “1000” to his account." . PHP_EOL;

        $this->depositBalance($userData, 1000, $userType, $token);

        echo "System adds an amount of “1000” to the player’s account." . PHP_EOL;
        sleep(1);
        echo "Player withdraw “500” from his account." . PHP_EOL;

        $this->withdrawBalance($userData, 500, $userType, $token);

        echo "System deducts an amount of “500” to the player’s account." . PHP_EOL;

        echo "Player query his transfer records." . PHP_EOL;

        $this->getTransferHistory($userData, $userType, $token);

        echo "System displays the transfer records of player." . PHP_EOL;
        echo "Case six ran successfully!" . PHP_EOL;
    }

    public function depositBalance($userData, $amount, $userType, $token)
    {
        $userData = get_user_type_model($userType, 'users')->whereUsername($userData->getField('username'))->firstOrFail();
        $balance = ($userData->getField('balance') + $amount);
        $response = $this->callDepositApi($userData->getField('username'), $this->dummyData($balance, \Carbon\Carbon::now()), $userType, $token);

        $transfer = get_user_type_model($userType, 'transfer')->whereUsername($userData->getField('username'))->latest()->first();
        $cashflow = get_user_type_model($userType, 'cashflow');

        $response->seeInDatabase($transfer->getTable(), [
            'username' => $userData->getField('username'),
            'balance' => ($userData->getField('balance') + $balance),
            'actions' => \App\Repositories\v1\TransferRepository::DEPOSIT_ACTION,
        ], 'datarepo');

        $response->seeInDatabase($userData->getTable(), [
            $userData->getKeyName() => $userData->getField('id'),
        ], 'ichips');

        $response->seeInDatabase($cashflow->getTable(), [
            'username' => $userData->getField('username'),
            'trans_type' => \App\Repositories\v1\CashFlowRepository::DEPOSIT_TRANS_TYPE,
            'betting_code' => $transfer->getField('transfer_id'),
        ], 'datarepo');
    }

    public function getTransferHistory($userData, $userType, $token)
    {
        $response = $this->get(url('v1/history/transfer/' . $userData->getField('username') . '?user_type=' . $userType . '&start=1&length=50'), $token);

        $response->assertResponseStatus(200);
        $response->seeJsonStructure([
            "message",
            "count_records",
            "count_results",
            "data"
        ]);
    }

    public function withdrawBalance($userData, $amount, $userType, $token)
    {
        $userData = get_user_type_model($userType, 'users')->whereUsername($userData->getField('username'))->firstOrFail();
        $formattedBalance = number_format(($userData->getField('balance') - $amount), 5, '.', '');

        $balance = ($userData->getField('balance') - $amount);
        $transferAction = \App\Repositories\v1\TransferRepository::WITHDRAW_ACTION;
        $cashFlowAction = \App\Repositories\v1\CashFlowRepository::WITHDRAW_TRANS_TYPE;

        $response = $this->callWithdrawApi($userData->getField('username'), $amount, time(), $userType, $token);
        $transfer = get_user_type_model($userType, 'transfer')->whereUsername($userData->getField('username'))->latest()->first();
        $cashflow = get_user_type_model($userType, 'cashflow');

        $response->assertResponseStatus(200);

        $response->seeJsonContains([
            'message' => \App\Http\v1\Client\Controllers\WithdrawBalanceController::MESSAGE_UPDATE_SUCCESS,
            'data' => [
                'transfer_id' => $transfer->transfer_id,
                'current_balance' => $formattedBalance,
            ],
        ]);
        $response->seeInDatabase($transfer->getTable(), [
            'username' => $userData->getField('username'),
            'amount' => $amount,
            'balance' => $balance,
            'actions' => $transferAction,
        ], 'datarepo');

        $response->seeInDatabase($userData->getTable(), [
            $userData->getKeyName() => $userData->getField('id'),
        ], 'ichips');

        $response->seeInDatabase($cashflow->getTable(), [
            'username' => $userData->getField('username'),
            'trans_amount' => $amount,
            'balance' => $balance,
            'trans_type' => $cashFlowAction,
            'betting_code' => $transfer->getField('transfer_id'),
            'game_code' => null,
        ], 'datarepo');
    }

    private function callDepositApi($username, $data, $userType, $token)
    {
        $response = $this->post(url('v1/balance/deposit/' . $username . '?user_type=' . $userType), $data, $token);
        return $response;
    }

    private function dummyData($amount, $op_transfer_id = '00000')
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => $op_transfer_id,
        ];

        return $data;
    }

    private function callWithdrawApi($username, $amount, $op_transfer_id = '0000', $userType, $token)
    {
        $data = [
            'amount' => $amount,
            'op_transfer_id' => $op_transfer_id,
        ];

        $response = $this->post(url('v1/balance/withdraw/' . $username . '?user_type=' . $userType), $data, $token);

        return $response;
    }
}
