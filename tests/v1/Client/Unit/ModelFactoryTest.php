<?php

use Illuminate\Support\Facades\Config;
use App\Services\UserType\UserTypeConfigException;

/**
 *
 */
class ModelFactoryTest extends TestCase
{

    /**
     * @test
     */
    public function it_should_generate_correct_model_on_helper()
    {
        $userType = 'MARS';
        $entity = 'users';
        $model = get_user_type_model($userType, $entity);

        return $this->assertTrue(isset($model));
    }
    /**
     * @test
     * @expectedException App\Services\UserType\ConfigException
     * 
     */
    public function it_should_expect_exception_on_invalid_config_user_type()
    {
        $userType = 'USER_TYPE_NOT_FOUND';
        $entity = 'users';

        \App\Services\UserType\ModelFactory::make($userType, $entity);
    }
}
