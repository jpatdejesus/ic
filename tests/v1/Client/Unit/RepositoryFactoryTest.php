<?php

use App\Services\UserType\RepositoryFactory;
use App\Repositories\Repository;

/**
 *
 */
class RepositoryFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_generate_correct_repository_on_helper()
    {
        $userType = 'MARS';
        $entity = 'users';
        $repository = get_user_type_repository($userType, $entity);

        return $this->assertTrue($repository instanceof Repository);
    }

    /**
     * @test
     */
    public function it_should_expect_exception_on_invalid_config_user_type()
    {
        $userType = 'USER_TYPE_NOT_FOUND';
        $entity = 'users';

        $repository = RepositoryFactory::make($userType, $entity);

        $this->assertFalse($repository);
    }
}
