<?php

use App\Traits\AuthToken;

class ValidateUserIdParamMiddlewareTest extends TestCase
{
    use AuthToken;

    public function setUp()
    {
        parent::setUp();

        $router = $this->app->router;

        $this->app->router->group(['middleware' => 'validate_user_id_param.v1'], function () use ($router) {
            $router->get('/_test/{user_id}', function () {
                return response('OK');
            });
        });

    }

    /**
     * @test
     */
    public function it_should_validate_non_existing_user()
    {
        $userId = 'non_existing_user';
        $response = $this->get('/_test/' . $userId, [], $this->login());

        $response->assertResponseStatus(404);
        $response->seeJsonEquals([
            'message' => 'User not found.',
        ]);
    }

    /**
     * @test
     */
    public function it_should_successfully_call_api()
    {

        $user = factory(\App\Models\v1\User::class)->create();
        $response = $this->get('/_test/' . $user->id);

        $response->assertResponseStatus(200);
    }
}
