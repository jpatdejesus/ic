<?php

use App\Models\v1\User;
use App\Models\v1\UserType;

class ValidateCorrectUsernameParamMiddlewareTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $router = $this->app->router;

        $this->app->router->group(['middleware' => 'validate_username_param.v1'], function () use ($router) {
            $router->get('/_test/{username}', function () {
                return response('OK');
            });
        });
    }

    /**
     * @test
     */
    public function it_should_successfully_call_api_for_mars_usertype()
    {
        $userType = 'MARS';
        $model = get_user_type_model($userType, 'users');
        $user = factory(get_class($model))->create();
        $response = $this->get('/_test/' . $user->username . '?user_type=' . $userType);

        $response->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_should_successfully_call_api_for_og_cash_usertype()
    {
        $userType = 'OG_CASH';
        $model = get_user_type_model($userType, 'users');
        $this->createMockUsersPHPTableIfNotExist();
        $user = factory(get_class($model))->create();
        $response = $this->get('/_test/' . $user->UserName . '?user_type=' . $userType);

        $response->assertResponseStatus(200);
    }

    /**
     * @test
     */
    public function it_should_validate_non_existing_user()
    {
        $username = 'non_existing_user';
        $userType = UserType::first();
        $response = $this->get('/_test/' . $username . '?user_type=' . $userType->user_type_name);

        $response->assertResponseStatus(404);
        $response->seeJsonStructure([
            'error',
        ]);
    }

    /**
     * @test
     */
    public function it_should_validate_non_existing_user_type()
    {
        $userType = 'non_existing_user_type';
        $user = factory(\App\Models\v1\User::class)->create();
        $response = $this->get('/_test/' . $user->username . '?user_type=' . $userType);

        $response->assertResponseStatus(422);
        $response->seeJsonStructure([
            'errors' => [
                'user_type',
            ],
        ]);
    }
}
