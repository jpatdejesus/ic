<?php

use App\Traits\AuthToken;

class ValidatePermissionMiddlewareTest extends TestCase
{
    use AuthToken;


    public function setUp()
    {
        parent::setUp();

        $router = $this->app->router;
        $this->app->router->group(['middleware' => 'permission.v1', 'as' => 'Testing Module'], function () use ($router) {
            $router->get(
                '/testing_permission',
                [
                    'name' => 'Testing Name',
                    'description' => 'Testing Description',
                    'default' => ['iChips', 'Panda', 'Games']
                ]
            );
        });
    }

    /**
     * @test
     */
    public function it_should_not_access_the_routes()
    {
        $user = (new \App\Models\v1\RoleCredential)->whereUsername('niuniu')->firstOrFail();
        $user->password = "password123!";
        $token = $this->loginAs($user)->token;
        $response = $this->post('/v1/user', [], ['token' => $token]);
        $response->assertResponseStatus(403);
    }

    /**
     * @test
     */
    public function it_should_access_the_routes()
    {
        $token = $this->login();

        $response = $this->get('/v1/user/search/username?user_type=MARS&search=1', ['token' => $token]);

        $response->assertResponseStatus(404);
    }
}
