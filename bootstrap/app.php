<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(dirname(__DIR__)))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

$app->withEloquent();

$app->configure('app');

$app->configure('jwt');

$app->configure('ichips');

$app->configure('filesystems');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Filesystem\Factory::class,
    function ($app) {
        return new Illuminate\Filesystem\FilesystemManager($app);
    }
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    App\Http\v1\Client\Middleware\HttpLoggerMiddleware::class,
    App\Http\v1\Middleware\AppTypeMiddleware::class,
    App\Http\v1\Middleware\CorsMiddleware::class,
]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/
$app->register(App\Providers\AppServiceProvider::class);

$app->register(App\Providers\EntityServiceProvider::class);

$app->register(\Yajra\DataTables\DataTablesServiceProvider::class);

$app->register(Appzcoder\LumenRoutesList\RoutesCommandServiceProvider::class);

$app->register(App\Providers\FormRequestServiceProvider::class);

$app->register(App\Providers\ValidatorServiceProvider::class);

$app->register(\Almazik\LaravelUploader\FileUploaderServiceProvider::class);

// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

if (!class_exists('DataTables')) {
    class_alias(\Yajra\DataTables\Facades\DataTables::class, 'DataTables');
}

foreach (explode(',', config('app.api_version')) as $key) {
    /*
    |--------------------------------------------------------------------------
    | Register Middleware
    |--------------------------------------------------------------------------
    |
    | Next, we will register the middleware with the application. These can
    | be global middleware that run before and after each request into a
    | route or middleware that'll be assigned to some specific routes.
    |
    */
    $admin = config('ichips.app_types.admin');
    $client = config('ichips.app_types.client');

    $app->routeMiddleware([
        'jwt.auth.' . $key => 'App\Http\\' . $key . '\Middleware\JwtMiddleware::class',
        'validate_nullable_username_param.' . $key => 'App\Http\\' . $key . '\\' . $client . '\Middleware\ValidateNullableUsernameParamMiddleware::class',
        'validate_username_param.' . $key => 'App\Http\\' . $key . '\\' . $client . '\Middleware\ValidateCorrectUsernameParamMiddleware::class',
        'validate_user_id_param.' . $key => 'App\Http\\' . $key . '\\' . $client . '\Middleware\ValidateUserIdParamMiddleware::class',
        'validate_game_code_param.' . $key => 'App\Http\\' . $key . '\\' . $client . '\Middleware\ValidateGameCodeParamMiddleware::class',
        'permission.' . $key => 'App\Http\\' . $key . '\Middleware\PermissionMiddleware::class',
        'throttle.' . $key => 'App\Http\\' . $key . '\Middleware\RateLimits',
        'ip_whitelist.' . $key => 'App\Http\\' . $key . '\Middleware\IpWhitelistMiddleware::class',
    ]);
    /*
    |--------------------------------------------------------------------------
    | Load The Application Routes
    |--------------------------------------------------------------------------
    |
    | Next we will include the routes file so that they can all be added to
    | the application. This will provide all of the URLs the application
    | can respond to, as well as the controllers that may handle them.
    |
    */

    foreach (config('ichips.app_types') as $item => $value) {
        $app->router->group([
            'namespace' => 'App\Http\\' . $key . '\\' . $value . '\Controllers',
            'middleware' => ['throttle.' . $key . ':'. config('ichips.max_throttle_per_minute') .',1'],
            'prefix' => $key . (($value == config('ichips.app_types.admin')) ? '/admin' : null),
            'as' => (($value == config('ichips.app_types.admin')) ? 'ADMIN' : (($value === config('ichips.app_types.client')) ? 'CLIENT' : 'OTHERS')),
        ], function ($router) use ($key, $value) {
            foreach (glob(__DIR__ . '/../routes/' . $key . '/' . $value . '/*.php') as $route_file) {
                require $route_file;
            }
        });
    }
}

return $app;
