<?php

require_once __DIR__ . '/app.php';

$app->boot();

use Illuminate\Support\Facades\Artisan;

function makeSqliteDependencies()
{
    $stubsPath = database_path('/stubs/');
    $testingPath = database_path('/testing/');

    if (!file_exists($stubsPath)) {
        mkdir($stubsPath);
    }

    $connections = config('database.connections');
    foreach ($connections as $key => $value) {
        $file = fopen($stubsPath . $key . '.sqlite', 'w');
        fclose($file);
    }

    if (!file_exists($testingPath)) {
        mkdir($testingPath);
    }

    Artisan::call('migrate:refresh');
    Artisan::call('db:seed');

   setSqliteEnv();
}

function setSqliteEnv()
{
    putenv('DB_DATABASE_ICHIPS_ADMIN_LITE=database/testing/ichips-admin.sqlite');
    putenv('DB_DATABASE_ICHIPS_LITE=database/testing/ichips.sqlite');
    putenv('DB_DATABASE_DATAREPO_LITE=database/testing/datarepo.sqlite');
    putenv('DB_DATABASE_LOGS_LITE=database/testing/logs.sqlite');
}

makeSqliteDependencies();
