<?php

return [

    'version' => 'v1.0.18',

    'max_throttle_per_minute' => env('MAX_THROTTLE_PER_MINUTE', 200),

    /**
     * iChips App Types
     */
    'app_types' => [
        'admin' => 'Admin',
        'client' => 'Client',
    ],

    /**
     * iChips Defaults
     */
    'user_statuses' => [
        'enabled' => 'ENABLED',
        'disabled' => 'DISABLED',
        'suspended' => 'SUSPENDED',
    ],

    'default_roles' => [
        'SUPER_ADMIN' => ['ichips'],
        'ADMIN' => ['panda'],
        'GENERAL' => [
            'Baccarat',
            'Dragontiger',
            'Fantan',
            'Moneywheel',
            'Roulette',
            'Sicbo',
        ],
        'GET' => [
            'ceres',
        ],
        'OTHERS' => [
            'Niuniu',
            '3cards',
        ],
        'GMT' => [
            'gmtadmin'
        ]
    ],

    'admin_default_roles_and_users' => [
        'SUPER_ADMIN' => [
            'super_admin' => [
                [
                    'email' => 'superadmin@nomail.com',
                    'first_name' => 'Super',
                    'last_name' => 'Admin',
                    'nickname' => 'SuperAdmin',
                    'country' => 'Afghanistan',
                ],
                [
                    'email' => 'superadmin2@nomail.com',
                    'first_name' => 'Super',
                    'last_name' => 'Admin2',
                    'nickname' => 'SuperAdmin2',
                    'country' => 'Bangladesh',
                ],
            ],
        ],
        'ADMIN' => [
            'admin' => [
                [
                    'email' => 'admin@nomail.com',
                    'first_name' => 'Admin',
                    'last_name' => 'User',
                    'nickname' => 'AdminUser',
                    'country' => 'Bouvet Island',
                ],
            ],
        ],
        'AUDIT' => [
            'cs' => [
                [
                    'email' => 'cs@nomail.com',
                    'first_name' => 'CS',
                    'last_name' => 'User',
                    'nickname' => 'CsUser',
                    'country' => 'Ecuador',
                ],
            ],
        ],
    ],

    'app_type_entities' => [
        'ADMIN' => [
            'user_credentials' => [
                'model' => \App\Models\v1\AdminUser::class,
                'repository' => \App\Repositories\v1\AdminUserRepository::class,
            ],
            'access_tokens' => [
                'model' => \App\Models\v1\AdminAccessToken::class,
                'repository' => \App\Repositories\v1\AccessTokenRepository::class,
            ],
            'roles' => [
                'model' => \App\Models\v1\AdminRole::class,
                'repository' => \App\Repositories\v1\RoleManagementRepository::class,
            ],
            'user_roles' => [
                'model' => \App\Models\v1\AdminUserRole::class,
                'repository' => \App\Repositories\v1\UserRoleRepository::class,
            ],
            'permissions' => [
                'model' => \App\Models\v1\AdminPermission::class,
                'repository' => \App\Repositories\v1\PermissionRepository::class,
            ],
            'role_has_permissions' => [
                'model' => \App\Models\v1\AdminRoleHasPermission::class,
                'repository' => \App\Repositories\v1\RoleHasPermissionRepository::class,
            ],
        ],
        'CLIENT' => [
            'user_credentials' => [
                'model' => \App\Models\v1\RoleCredential::class,
                'repository' => \App\Repositories\v1\RoleCredentialRepository::class,
            ],
            'access_tokens' => [
                'model' => \App\Models\v1\AccessToken::class,
                'repository' => \App\Repositories\v1\AccessTokenRepository::class,
            ],
            'roles' => [
                'model' => \App\Models\v1\Role::class,
                'repository' => \App\Repositories\v1\RoleManagementRepository::class,
            ],
            'user_roles' => [
                'model' => \App\Models\v1\UserRole::class,
                'repository' => \App\Repositories\v1\UserRoleRepository::class,
            ],
            'role_has_permissions' => [
                'model' => \App\Models\v1\RoleHasPermission::class,
                'repository' => \App\Repositories\v1\RoleHasPermissionRepository::class,
            ],
            'permissions' => [
                'model' => \App\Models\v1\Permission::class,
                'repository' => \App\Repositories\v1\PermissionRepository::class,
            ],
        ],
    ],

    'user_types' => [
        'MARS' => [
            'users' => [
                'model' => App\Models\v1\User::class,
                'repository' => \App\Repositories\v1\UserRepository::class,
            ],
            'cashflow' => [
                'model' => App\Models\v1\Cashflow::class,
                'repository' => App\Repositories\v1\CashFlowRepository::class,
            ],
            'transfer' => [
                'model' => App\Models\v1\Transfer::class,
                'repository' => App\Repositories\v1\TransferRepository::class,
            ],
        ],
        'OG_PLUS' => [
            'users' => [
                'model' => App\Models\v1\User::class,
                'repository' => \App\Repositories\v1\UserRepository::class,
            ],
            'cashflow' => [
                'model' => App\Models\v1\Cashflow::class,
                'repository' => App\Repositories\v1\CashFlowRepository::class,
            ],
            'transfer' => [
                'model' => App\Models\v1\Transfer::class,
                'repository' => App\Repositories\v1\TransferRepository::class,
            ],
        ],
        'OG_CASH' => [
            'users' => [
                'model' => App\Models\v1\UserPHP::class,
                'repository' => \App\Repositories\v1\UserPHPRepository::class,
            ],
            'cashflow' => [
                'model' => App\Models\v1\CashflowPHP::class,
                'repository' => App\Repositories\v1\CashFlowPHPRepository::class,
            ],
            'transfer' => [
                'model' => App\Models\v1\TransferPHP::class,
                'repository' => App\Repositories\v1\TransferPHPRepository::class,
            ],
        ],
        'OG_CASHAPI' => [
            'users' => [
                'model' => App\Models\v1\UserPHP::class,
                'repository' => \App\Repositories\v1\UserPHPRepository::class,
            ],
            'cashflow' => [
                'model' => App\Models\v1\CashflowPHP::class,
                'repository' => App\Repositories\v1\CashFlowPHPRepository::class,
            ],
            'transfer' => [
                'model' => App\Models\v1\TransferPHP::class,
                'repository' => App\Repositories\v1\TransferPHPRepository::class,
            ],
        ],
        'OG_AGENT' => [
            'users' => [
                'model' => App\Models\v1\UserPHP::class,
                'repository' => \App\Repositories\v1\UserPHPRepository::class,
            ],
            'cashflow' => [
                'model' => App\Models\v1\CashflowPHP::class,
                'repository' => App\Repositories\v1\CashFlowPHPRepository::class,
            ],
            'transfer' => [
                'model' => App\Models\v1\TransferPHP::class,
                'repository' => App\Repositories\v1\TransferPHPRepository::class,
            ],
        ],
    ],

    'currencies' => [
        'USD' => [
            'conversion_by_rmb' => '0.144648'
        ],
        'CNY' => [
            'conversion_by_rmb' => '1.00000'
        ],
        'KRW' => [
            'conversion_by_rmb' => '172.825'
        ],
        'THB' => [
            'conversion_by_rmb' => '4.60699'
        ],
        'VND' => [
            'conversion_by_rmb' => '3382.23'
        ],
        'MYR' => [
            'conversion_by_rmb' => '0.604683'
        ],
        'IDR' => [
            'conversion_by_rmb' => '2092.53'
        ],
        'EUR' => [
            'conversion_by_rmb' => '0.129644'
        ],
        'JPY' => [
            'conversion_by_rmb' => '15.9139'
        ],
    ],

    'game_types' => [
        'GAME_TYPE_1',
        'GAME_TYPE_2',
        'GAME_TYPE_3',
        'GAME_TYPE_4',
        'GAME_TYPE_5',
        'GAME_TYPE_6',
        'GAME_TYPE_7',
        'GAME_TYPE_8',
    ],

    'default_games' => [
        [
            'game_type_name' => 'GAME_TYPE_1',
            'game_code' => 'baccarat',
            'game_name' => 'Baccarat',
        ], [
            'game_type_name' => 'GAME_TYPE_2',
            'game_code' => 'dragontiger',
            'game_name' => 'Dragontiger',
        ], [
            'game_type_name' => 'GAME_TYPE_3',
            'game_code' => 'moneywheel',
            'game_name' => 'Moneywheel',
        ], [
            'game_type_name' => 'GAME_TYPE_4',
            'game_code' => 'roulette',
            'game_name' => 'Roulette',
        ], [
            'game_type_name' => 'GAME_TYPE_5',
            'game_code' => 'sicbo',
            'game_name' => 'sicbo',
        ], [
            'game_type_name' => 'GAME_TYPE_6',
            'game_code' => 'fantan',
            'game_name' => 'Fantan',
        ],[
            'game_type_name' => 'GAME_TYPE_7',
            'game_code' => 'niuniu',
            'game_name' => 'NiuNiu',
        ], [
            'game_type_name' => 'GAME_TYPE_8',
            'game_code' => '3cards',
            'game_name' => '3Cards',
        ],
    ],

    'default_game_code' => [
        'baccarat',
        'dragontiger',
        'moneywheel',
        'roulette',
        'sicbo',
        'fantan',
        'niuniu',
        '3cards',
    ], 

    'default_currency_min_max' => [
        [
            'currency_code' => 'USD',
            'min' => 1,
            'max' => 300,
        ],
        [
            'currency_code' => 'CNY',
            'min' => 1,
            'max' => 2000,
        ],
        [
            'currency_code' => 'KRW',
            'min' => 170,
            'max' => 338000,
        ],
        [
            'currency_code' => 'THB',
            'min' => 52,
            'max' => 9500,
        ],
        [
            'currency_code' => 'VND',
            'min' => 3500,
            'max' => 7000000,
        ],
        [
            'currency_code' => 'MYR',
            'min' => 1,
            'max' => 1200,
        ],
        [
            'currency_code' => 'IDR',
            'min' => 2200,
            'max' => 4245000,
        ],
        [
            'currency_code' => 'EUR',
            'min' => 1,
            'max' => 270,
        ],
        [
            'currency_code' => 'JPY',
            'min' => 20,
            'max' => 3320,
        ],
        [
            'currency_code' => null,
            'min' => 1,
            'max' => 2000,
        ],
    ],

    'ip_whitelist' => format_config_ip_whitelists(env('IP_WHITELISTS')),

    'update_balance_buffer_hours' => env('UPDATE_BALANCE_BUFFER_HOURS', 80),

    'update_balance_max_rmb_conversion' => env('UPDATE_BALANCE_MAX_RMB_CONVERSION', 100),

    'max_bet_data' => env('MAX_BET_DATA', 50),
    'max_payout_data' => env('MAX_PAYOUT_DATA', 50),
    /**
    *|--------------------------------------------------------------------------
    *| iChips Admin Tool DEFAULT PROFILE PICTURE
    *|--------------------------------------------------------------------------
    *|
    *| Default image for iChips Admin Tool profile picture
    *|
    */
    'default_pp' => env('DEFAULT_PROFILE_PICTURE', 'default_pp.png'),
    'game_name_aliases' => [
        'dragontiger' => ['NEW DT', 'CLASSIC DT'],
        'moneywheel' => ['DRAGON'],
        '3cards' => ['THREE CARDS', 'WIN3CARD'],
    ],
    'request_max_tries' => 5,
    'server_timezone' => env('SERVER_TIMEZONE', 'UTC'),
];
