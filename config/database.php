<?php

$db_config = [
    'default' => 'ichips',
    'migrations' => 'migrations',
    'connections' => [
        'ichips-admin' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'port' => env('DB_PORT', 3306),
            'database' => (env('DB_DRIVER') === 'sqlite' ? env('DB_DATABASE_ICHIPS_ADMIN_LITE') : env('DB_DATABASE_ICHIPS_ADMIN')),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'ichips' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'port' => env('DB_PORT', 3306),
            'database' => (env('DB_DRIVER') === 'sqlite' ? env('DB_DATABASE_ICHIPS_LITE') : env('DB_DATABASE_ICHIPS')),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'datarepo' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'port' => env('DB_PORT', 3306),
            'database' => (env('DB_DRIVER') === 'sqlite' ? env('DB_DATABASE_DATAREPO_LITE') : env('DB_DATABASE_DATAREPO')),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'logs' => [
            'driver' => env('DB_DRIVER', 'mysql'),
            'port' => env('DB_PORT', 3306),
            'database' => (env('DB_DRIVER') === 'sqlite' ? env('DB_DATABASE_LOGS_LITE') : env('DB_DATABASE_LOGS')),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ],
];

//db read write connection
$db_rw_connection = [
    'write' => [
        'host' => env('DB_HOST_MASTER'),
        'username' => env('DB_USERNAME_MASTER'),
        'password' => env('DB_PASSWORD_MASTER'),
    ],
    'read' => [
        'host' => env('DB_HOST_SLAVE'),
        'username' => env('DB_USERNAME_SLAVE'),
        'password' => env('DB_PASSWORD_SLAVE'),
    ],
];

//append read write connection if driver is mysql.
//fix for pipeline sqlite db connection
if (env('DB_DRIVER') === 'mysql') {
    foreach ($db_config['connections'] as $key => $value) {
        $db_config['connections'][$key] = array_merge($db_rw_connection, $value);
    }
}

return $db_config;
