
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200109_30_ICHIPS_TAIWAN_GAME_RESULTS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game results table, make result_id and bet_code as unique, add column created_at';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/* tw_3cards_game_results and tw_3cards_transactions TABLE */
ALTER TABLE `tw_3cards_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_3cards_transactions` DROP INDEX `bet_code`;

/* tw_banker_niuniu_game_results and tw_banker_niuniu_transactions TABLE */
ALTER TABLE `tw_banker_niuniu_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_banker_niuniu_transactions` DROP INDEX `bet_code`;

/* tw_dragontiger_game_results and tw_dragontiger_transactions TABLE */
ALTER TABLE `tw_dragontiger_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_dragontiger_transactions` DROP INDEX `bet_code`;

/* tw_hundred_3cards_game_results and tw_hundred_3cards_transactions TABLE */
ALTER TABLE `tw_hundred_3cards_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_hundred_3cards_transactions` DROP INDEX `bet_code`;

/* tw_hundred_niuniu_game_results and tw_hundred_niuniu_transactions TABLE */
ALTER TABLE `tw_hundred_niuniu_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_hundred_niuniu_transactions` DROP INDEX `bet_code`;

/* tw_hundred_texas_holdm_game_results and tw_hundred_texas_holdm_transactions TABLE */
ALTER TABLE `tw_hundred_texas_holdm_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_hundred_texas_holdm_transactions` DROP INDEX `bet_code`;

/* tw_landlord_game_results and tw_landlord_transactions TABLE */
ALTER TABLE `tw_landlord_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_landlord_transactions` DROP INDEX `bet_code`;

/* tw_niuniu_game_results and tw_niuniu_transactions TABLE */
ALTER TABLE `tw_niuniu_game_results` DROP INDEX `result_id`, DROP COLUMN `created_at`;
ALTER TABLE `tw_niuniu_transactions` DROP INDEX `bet_code`;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/