
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200107_20_ICHIPS_TAIWAN_GAME_TRANSACTIONS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game transactions table';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/* tw_3cards_transactions TABLE */
ALTER TABLE `tw_3cards_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_banker_niuniu_transactions TABLE */
ALTER TABLE `tw_banker_niuniu_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_dragontiger_transactions TABLE */
ALTER TABLE `tw_dragontiger_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_hundred_3cards_transactions TABLE */
ALTER TABLE `tw_hundred_3cards_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_hundred_niuniu_transactions TABLE */
ALTER TABLE `tw_hundred_niuniu_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_hundred_texas_holdm_transactions TABLE */
ALTER TABLE `tw_hundred_texas_holdm_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_landlord_transactions TABLE */
ALTER TABLE `tw_landlord_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/* tw_niuniu_transactions TABLE */
ALTER TABLE `tw_niuniu_transactions` 
	DROP COLUMN `platform_code`,
	DROP COLUMN `error_status`,
	DROP COLUMN `withhold`,
	DROP COLUMN `bet_device`,
	DROP COLUMN `bet_browser`,
	MODIFY `bet_amount` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'valid bet', 
	MODIFY `win_loss` DECIMAL(50,5) NULL DEFAULT '0.00000' COMMENT 'win lose of this bet ',
	MODIFY `balance_after_bet` DECIMAL(50,5) NOT NULL DEFAULT '0.00000' COMMENT 'balance afters bet';

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/