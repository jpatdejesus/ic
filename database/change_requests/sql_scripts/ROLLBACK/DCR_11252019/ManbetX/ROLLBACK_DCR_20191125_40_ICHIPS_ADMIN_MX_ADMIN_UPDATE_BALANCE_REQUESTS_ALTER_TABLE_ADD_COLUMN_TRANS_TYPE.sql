
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20191125_40_ICHIPS_ADMIN_MX_ADMIN_UPDATE_BALANCE_REQUESTS_ALTER_TABLE_ADD_COLUMN_TRANS_TYPE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert initial data for balance adjustment types';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

ALTER TABLE `admin_update_balance_requests` DROP COLUMN `trans_type`;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/