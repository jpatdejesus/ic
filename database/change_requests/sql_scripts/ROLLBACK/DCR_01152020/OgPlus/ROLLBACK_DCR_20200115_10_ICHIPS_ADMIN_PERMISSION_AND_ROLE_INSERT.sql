
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200115_10_ICHIPS_ADMIN_PERMISSION_AND_ROLE_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Rollback script for created admin permission and role for batch update';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

DELETE FROM `admin_role_has_permissions` WHERE `permission_id` = (SELECT id FROM `admin_permissions` WHERE `code` = 'POST::Batch_Update_Balance');
DELETE FROM `admin_permissions` WHERE `code` = 'POST::Batch_Update_Balance';
/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/