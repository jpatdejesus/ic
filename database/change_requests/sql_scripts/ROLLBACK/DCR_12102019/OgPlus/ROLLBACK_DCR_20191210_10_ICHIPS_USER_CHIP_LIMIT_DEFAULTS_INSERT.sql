
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20191210_10_ICHIPS_USER_CHIP_LIMIT_DEFAULTS_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert user chip limit default for taiwan games';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_9');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_10');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_11');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_12');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_13');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_14');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_15');
DELETE FROM `user_chip_limit_defaults` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_16');

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/