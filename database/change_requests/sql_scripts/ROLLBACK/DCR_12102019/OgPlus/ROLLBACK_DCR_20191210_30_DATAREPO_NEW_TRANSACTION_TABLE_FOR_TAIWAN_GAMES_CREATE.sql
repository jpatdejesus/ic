
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20191210_30_DATAREPO_NEW_TRANSACTION_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Rollback script for created transactions table for taiwan games';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/** Landlord **/
DROP TABLE `tw_landlord_transactions`;

/** Hundred Niuniu **/
DROP TABLE `tw_hundred_niuniu_transactions`;

/** Banker Niuniu **/
DROP TABLE `tw_banker_niuniu_transactions`;

/** Hundred 3Cards **/
DROP TABLE `tw_hundred_3cards_transactions`;

/** Hundred Texas HoldM **/
DROP TABLE `tw_hundred_texas_holdm_transactions`;

/** Niuniu **/
DROP TABLE `tw_niuniu_transactions`;

/** 3Cards **/
DROP TABLE `tw_3cards_transactions`;

/** Dragontiger **/
DROP TABLE `tw_dragontiger_transactions`;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/