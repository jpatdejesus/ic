
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20191210_40_DATAREPO_NEW_GAME_RESULT_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Rollback script for created game results table for taiwan games';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/** Landlord **/
DROP TABLE `tw_landlord_game_results`;

/** Hundred Niuniu **/
DROP TABLE `tw_hundred_niuniu_game_results`;

/** Banker Niuniu **/
DROP TABLE `tw_banker_niuniu_game_results`;

/** Hundred 3Cards **/
DROP TABLE `tw_hundred_3cards_game_results`;

/** Hundred Texas HoldM **/
DROP TABLE `tw_hundred_texas_holdm_game_results`;

/** Niuniu **/
DROP TABLE `tw_niuniu_game_results`;

/** 3Cards **/
DROP TABLE `tw_3cards_game_results`;

/** Dragontiger **/
DROP TABLE `tw_dragontiger_game_results`;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/