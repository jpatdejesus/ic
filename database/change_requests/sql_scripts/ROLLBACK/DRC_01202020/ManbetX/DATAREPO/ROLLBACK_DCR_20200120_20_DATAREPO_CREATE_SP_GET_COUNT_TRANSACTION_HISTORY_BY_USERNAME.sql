
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200120_20_DATAREPO_CREATE_SP_GET_COUNT_TRANSACTION_HISTORY_BY_USERNAME.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Create Stored procedure for data table quries';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

DROP PROCEDURE IF EXISTS sp_get_count_all_transaction_history_by_username;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/