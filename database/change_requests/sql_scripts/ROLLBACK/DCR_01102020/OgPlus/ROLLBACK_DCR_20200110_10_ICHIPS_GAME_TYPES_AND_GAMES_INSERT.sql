
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200110_10_ICHIPS_GAME_TYPES_AND_GAMES_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert additional 5 game types and games for taiwan';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

DELETE FROM `games` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_17');
DELETE FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_17';

DELETE FROM `games` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_18');
DELETE FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_18';

DELETE FROM `games` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_19');
DELETE FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_19';

DELETE FROM `games` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_20');
DELETE FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_20';

DELETE FROM `games` WHERE `game_type_id` = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_21');
DELETE FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_21';

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/