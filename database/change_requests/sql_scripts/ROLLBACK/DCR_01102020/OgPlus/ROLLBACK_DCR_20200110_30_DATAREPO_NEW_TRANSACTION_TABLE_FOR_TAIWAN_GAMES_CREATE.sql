
/**************************** Script Header ******************************/
SET @File_script_name := 'ROLLBACK_DCR_20200110_30_DATAREPO_NEW_TRANSACTION_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Rollback script for created transactions table for taiwan games';
SET @Rollback_Script_name := '';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/** Quick Three Cards **/
DROP TABLE `tw_quick_3cards_transactions`;

/** Double LANDLORD **/
DROP TABLE `tw_double_landlord_transactions`;

/** Gem Wheel **/
DROP TABLE `tw_gem_wheel_transactions`;

/** Three-Face Card **/
DROP TABLE `tw_3face_card_transactions`;

/** Two-Eight Bar **/
DROP TABLE `tw_2eight_bar_transactions`;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/