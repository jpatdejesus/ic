
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_<CR_NUMBER>_<SCHEMA>_<TABLE_NAME>_<ACTION>.sql';
SET @Author_Name := 'Author_Name';
SET @Database_Version := '1.0';
SET @Change_request_cd := '<CR Code>';
SET @Script_Description :='Description of the change';
SET @Rollback_Script_name := 'DCR_<CR_NUMBER>_<SCHEMA>_<TABLE_NAME>_<ACTION>.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/




























/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/