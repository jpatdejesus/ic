
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200204_10_ICHIPS_MX_UPDATE_CHIP_LIMIT_DEFAULTS_TABLE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Update Chip limit default values for CNY currency';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200204_10_ICHIPS_MX_UPDATE_CHIP_LIMIT_DEFAULTS_TABLE.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

UPDATE `user_chip_limit_defaults` SET `min` = 10.00000, `max` = 20000.00 WHERE `currency_id` IN (SELECT `id` FROM `currencies` WHERE `currency_code` = 'CNY');

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/