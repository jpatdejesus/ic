
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200115_10_ICHIPS_ADMIN_PERMISSION_AND_ROLE_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert admin permission and role for batch update';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200115_10_ICHIPS_ADMIN_PERMISSION_AND_ROLE_INSERT.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

INSERT INTO `admin_permissions` (`name`, `code`, `description`, `parent_id`, `created_at`, `updated_at`) VALUES ('Batch Update Balance', 'POST::Batch_Update_Balance', 'Batch update balance', (SELECT ap.id FROM `admin_permissions` ap WHERE ap.code = 'module_update_balance' LIMIT 1), NOW(), NOW());
INSERT INTO `admin_role_has_permissions` (`permission_id`, `role_id`) VALUES (LAST_INSERT_ID(), (SELECT ar.id FROM `admin_roles` ar WHERE ar.role_group = 'SUPER_ADMIN'));

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/