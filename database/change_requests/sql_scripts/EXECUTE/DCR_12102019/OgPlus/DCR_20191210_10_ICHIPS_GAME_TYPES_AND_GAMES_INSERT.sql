
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20191210_10_ICHIPS_GAME_TYPES_AND_GAMES_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert game types and games for taiwan';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20191210_20_ICHIPS_GAME_TYPES_AND_GAMES_INSERT.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_9');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_niuniu', 'TwNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_10');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_3cards', 'Tw3Cards', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_11');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_landlord', 'Landlord', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_12');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_niuniu', 'HundredNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_13');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_banker_niuniu', 'BankerNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_14');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_3cards', 'Hundred3Cards', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_15');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_texas_holdm', 'HundredTexasHoldm', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_16');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_dragontiger', 'TwDragontiger', LAST_INSERT_ID());

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/