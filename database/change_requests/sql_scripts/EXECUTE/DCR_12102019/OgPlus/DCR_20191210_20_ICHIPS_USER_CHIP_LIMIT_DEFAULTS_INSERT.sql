
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20191210_20_ICHIPS_USER_CHIP_LIMIT_DEFAULTS_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert user chip limit default for taiwan games';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20191210_10_ICHIPS_USER_CHIP_LIMIT_DEFAULTS_INSERT.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

SET @Min_Null = 1.00000;
SET @Max_Null = 2000.00000;

SET @Min_1 = 1.00000;
SET @Max_1 = 300.00000;

SET @Min_2 = 1.00000;
SET @Max_2 = 2000.00000;

SET @Min_3 = 170.00000;
SET @Max_3 = 338000.00000;

SET @Min_4 = 52.00000;
SET @Max_4 = 9500.00000;

SET @Min_5 = 3500.00000;
SET @Max_5 = 7000000.00000;

SET @Min_6 = 1.00000;
SET @Max_6 = 1200.00000;

SET @Min_7 = 2200.00000;
SET @Max_7 = 4245000.00000;

SET @Min_8 = 1.00000;
SET @Max_8 = 270.00000;

SET @Min_9 = 20.00000;
SET @Max_9 = 3320.00000;

/******************************** GAME_TYPE_9 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_9');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_10 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_10');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_11 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_11');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_12 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_12');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_13 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_13');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_14 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_14');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_15 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_15');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);


/******************************** GAME_TYPE_16 ********************************/
SET @GameTypeId = (SELECT id FROM `game_types` WHERE `game_type_name` = 'GAME_TYPE_16');

INSERT INTO `user_chip_limit_defaults` (`game_type_id`, `currency_id`, `min`, `max`) 
VALUES 
	(@GameTypeId, NULL, @Min_Null, @Max_Null),
	(@GameTypeId, 1, @Min_1, @Max_1),
	(@GameTypeId, 2, @Min_2, @Max_2),
	(@GameTypeId, 3, @Min_3, @Max_3),
	(@GameTypeId, 4, @Min_4, @Max_4),
	(@GameTypeId, 5, @Min_5, @Max_5),
	(@GameTypeId, 6, @Min_6, @Max_6),
	(@GameTypeId, 7, @Min_7, @Max_7),
	(@GameTypeId, 8, @Min_8, @Max_8),
	(@GameTypeId, 9, @Min_9, @Max_9);

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/