
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200110_30_DATAREPO_NEW_TRANSACTION_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Transaction tables for taiwan games';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200110_30_DATAREPO_NEW_TRANSACTION_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/** Quick Three Cards **/
CREATE TABLE `tw_quick_3cards_transactions` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.',
	`bet_code` VARCHAR(50) NOT NULL COMMENT 'betting code[game round id]+[serial number]',
	`betting_code_bet` CHAR(32) NOT NULL COMMENT 'betting code-bet',
	`betting_code_payout` CHAR(32) NULL DEFAULT NULL COMMENT 'betting code-payout',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'game id',
	`gamename` VARCHAR(50) NULL DEFAULT NULL COMMENT 'game name',
	`room_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'rooom id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'user id',
	`user_type` VARCHAR(255) NOT NULL COMMENT 'user type',
	`username` CHAR(40) NOT NULL DEFAULT '0' COMMENT 'user name',
	`result` VARCHAR(256) NULL DEFAULT NULL COMMENT 'games of bet type',
	`error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure',
	`withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets',
	`bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	`effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet', 
	`win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet ',
	`balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'game statuss : 1.unfinish ,2.finish , 3. fal , 4. returnq bys dealer , 5.Reissue',
	`bet_date` DATETIME NULL DEFAULT NULL COMMENT 'bet  time',
	`bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device',
	`bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser',
	`second_order_time` DATETIME NULL DEFAULT NULL COMMENT 'time of repair this  bet record',
	`copy_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`remark` TEXT NULL COMMENT 'remark',
	`created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
	`updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'update time',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** Double LANDLORD **/
CREATE TABLE `tw_double_landlord_transactions` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.',
	`bet_code` VARCHAR(50) NOT NULL COMMENT 'betting code[game round id]+[serial number]',
	`betting_code_bet` CHAR(32) NOT NULL COMMENT 'betting code-bet',
	`betting_code_payout` CHAR(32) NULL DEFAULT NULL COMMENT 'betting code-payout',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'game id',
	`gamename` VARCHAR(50) NULL DEFAULT NULL COMMENT 'game name',
	`room_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'rooom id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'user id',
	`user_type` VARCHAR(255) NOT NULL COMMENT 'user type',
	`username` CHAR(40) NOT NULL DEFAULT '0' COMMENT 'user name',
	`result` VARCHAR(256) NULL DEFAULT NULL COMMENT 'games of bet type',
	`error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure',
	`withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets',
	`bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	`effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet', 
	`win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet ',
	`balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'game statuss : 1.unfinish ,2.finish , 3. fal , 4. returnq bys dealer , 5.Reissue',
	`bet_date` DATETIME NULL DEFAULT NULL COMMENT 'bet  time',
	`bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device',
	`bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser',
	`second_order_time` DATETIME NULL DEFAULT NULL COMMENT 'time of repair this  bet record',
	`copy_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`remark` TEXT NULL COMMENT 'remark',
	`created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
	`updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'update time',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** Gem Wheel **/
CREATE TABLE `tw_gem_wheel_transactions` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.',
	`bet_code` VARCHAR(50) NOT NULL COMMENT 'betting code[game round id]+[serial number]',
	`betting_code_bet` CHAR(32) NOT NULL COMMENT 'betting code-bet',
	`betting_code_payout` CHAR(32) NULL DEFAULT NULL COMMENT 'betting code-payout',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'game id',
	`gamename` VARCHAR(50) NULL DEFAULT NULL COMMENT 'game name',
	`room_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'rooom id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'user id',
	`user_type` VARCHAR(255) NOT NULL COMMENT 'user type',
	`username` CHAR(40) NOT NULL DEFAULT '0' COMMENT 'user name',
	`result` VARCHAR(256) NULL DEFAULT NULL COMMENT 'games of bet type',
	`error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure',
	`withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets',
	`bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	`effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet', 
	`win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet ',
	`balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'game statuss : 1.unfinish ,2.finish , 3. fal , 4. returnq bys dealer , 5.Reissue',
	`bet_date` DATETIME NULL DEFAULT NULL COMMENT 'bet  time',
	`bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device',
	`bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser',
	`second_order_time` DATETIME NULL DEFAULT NULL COMMENT 'time of repair this  bet record',
	`copy_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`remark` TEXT NULL COMMENT 'remark',
	`created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
	`updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'update time',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** Three-Face Card **/
CREATE TABLE `tw_3face_card_transactions` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.',
	`bet_code` VARCHAR(50) NOT NULL COMMENT 'betting code[game round id]+[serial number]',
	`betting_code_bet` CHAR(32) NOT NULL COMMENT 'betting code-bet',
	`betting_code_payout` CHAR(32) NULL DEFAULT NULL COMMENT 'betting code-payout',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'game id',
	`gamename` VARCHAR(50) NULL DEFAULT NULL COMMENT 'game name',
	`room_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'rooom id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'user id',
	`user_type` VARCHAR(255) NOT NULL COMMENT 'user type',
	`username` CHAR(40) NOT NULL DEFAULT '0' COMMENT 'user name',
	`result` VARCHAR(256) NULL DEFAULT NULL COMMENT 'games of bet type',
	`error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure',
	`withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets',
	`bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	`effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet', 
	`win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet ',
	`balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'game statuss : 1.unfinish ,2.finish , 3. fal , 4. returnq bys dealer , 5.Reissue',
	`bet_date` DATETIME NULL DEFAULT NULL COMMENT 'bet  time',
	`bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device',
	`bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser',
	`second_order_time` DATETIME NULL DEFAULT NULL COMMENT 'time of repair this  bet record',
	`copy_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`remark` TEXT NULL COMMENT 'remark',
	`created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
	`updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'update time',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** Two-Eight Bar **/
CREATE TABLE `tw_2eight_bar_transactions` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.',
	`bet_code` VARCHAR(50) NOT NULL COMMENT 'betting code[game round id]+[serial number]',
	`betting_code_bet` CHAR(32) NOT NULL COMMENT 'betting code-bet',
	`betting_code_payout` CHAR(32) NULL DEFAULT NULL COMMENT 'betting code-payout',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'game id',
	`gamename` VARCHAR(50) NULL DEFAULT NULL COMMENT 'game name',
	`room_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'rooom id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT 'user id',
	`user_type` VARCHAR(255) NOT NULL COMMENT 'user type',
	`username` CHAR(40) NOT NULL DEFAULT '0' COMMENT 'user name',
	`result` VARCHAR(256) NULL DEFAULT NULL COMMENT 'games of bet type',
	`error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure',
	`withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets',
	`bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	`effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet', 
	`win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet ',
	`balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'game statuss : 1.unfinish ,2.finish , 3. fal , 4. returnq bys dealer , 5.Reissue',
	`bet_date` DATETIME NULL DEFAULT NULL COMMENT 'bet  time',
	`bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device',
	`bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser',
	`second_order_time` DATETIME NULL DEFAULT NULL COMMENT 'time of repair this  bet record',
	`copy_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`remark` TEXT NULL COMMENT 'remark',
	`created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
	`updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'update time',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/