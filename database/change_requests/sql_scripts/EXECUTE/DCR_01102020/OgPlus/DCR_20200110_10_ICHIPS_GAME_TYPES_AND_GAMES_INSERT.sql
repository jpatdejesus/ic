
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200110_10_ICHIPS_GAME_TYPES_AND_GAMES_INSERT.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert additional 5 game types and games for taiwan';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200110_10_ICHIPS_GAME_TYPES_AND_GAMES_INSERT.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_17');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_quick_3cards', 'Quick Three Cards', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_18');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_double_landlord', 'Double LANDLORD', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_19');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_gem_wheel', 'Gem Wheel', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_20');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_3face_card', 'Three-Face Card', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_21');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_2eight_bar', 'Two-Eight Bar', LAST_INSERT_ID());

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/