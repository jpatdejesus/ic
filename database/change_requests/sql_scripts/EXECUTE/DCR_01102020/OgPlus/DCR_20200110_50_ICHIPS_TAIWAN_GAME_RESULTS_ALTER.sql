
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200110_50_ICHIPS_TAIWAN_GAME_RESULTS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game results table, make result_id and bet_code as unique, add column created_at';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200110_50_ICHIPS_TAIWAN_GAME_RESULTS_ALTER.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/* tw_quick_3cards_game_results and tw_quick_3cards_transactions TABLE */
ALTER TABLE `tw_quick_3cards_game_results` ADD UNIQUE (result_id), ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time';
ALTER TABLE `tw_quick_3cards_transactions` ADD UNIQUE (bet_code);

/* tw_double_landlord_game_results and tw_double_landlord_transactions TABLE */
ALTER TABLE `tw_double_landlord_game_results` ADD UNIQUE (result_id), ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time';
ALTER TABLE `tw_double_landlord_transactions` ADD UNIQUE (bet_code);

/* tw_gem_wheel_game_results and tw_gem_wheel_transactions TABLE */
ALTER TABLE `tw_gem_wheel_game_results` ADD UNIQUE (result_id), ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time';
ALTER TABLE `tw_gem_wheel_transactions` ADD UNIQUE (bet_code);

/* tw_3face_card_game_results and tw_3face_card_transactions TABLE */
ALTER TABLE `tw_3face_card_game_results` ADD UNIQUE (result_id), ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time';
ALTER TABLE `tw_3face_card_transactions` ADD UNIQUE (bet_code);

/* tw_2eight_bar_game_results and tw_2eight_bar_transactions TABLE */
ALTER TABLE `tw_2eight_bar_game_results` ADD UNIQUE (result_id), ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time';
ALTER TABLE `tw_2eight_bar_transactions` ADD UNIQUE (bet_code);

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/