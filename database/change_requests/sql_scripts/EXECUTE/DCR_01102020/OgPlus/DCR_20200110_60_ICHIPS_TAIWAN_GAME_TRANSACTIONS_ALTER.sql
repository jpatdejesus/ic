
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200110_60_ICHIPS_TAIWAN_GAME_TRANSACTIONS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game transactions, set result as not null default none';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200110_60_ICHIPS_TAIWAN_GAME_TRANSACTIONS_ALTER.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

ALTER TABLE `tw_landlord_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_hundred_niuniu_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_banker_niuniu_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_hundred_3cards_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_hundred_texas_holdm_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_niuniu_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_3cards_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_dragontiger_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_quick_3cards_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_double_landlord_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_gem_wheel_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_3face_card_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';
ALTER TABLE `tw_2eight_bar_transactions` MODIFY result varchar(256) NOT NULL COMMENT 'games of bet type';

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/