
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200110_40_DATAREPO_NEW_GAME_RESULT_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Game Result tables for taiwan games';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200110_40_DATAREPO_NEW_GAME_RESULT_TABLE_FOR_TAIWAN_GAMES_CREATE.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/** tw_quick_3cards **/
CREATE TABLE `tw_quick_3cards_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** tw_double_landlord **/
CREATE TABLE `tw_double_landlord_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** tw_gem_wheel **/
CREATE TABLE `tw_gem_wheel_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** tw_3face_card **/
CREATE TABLE `tw_3face_card_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/** tw_2eight_bar **/
CREATE TABLE `tw_2eight_bar_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/