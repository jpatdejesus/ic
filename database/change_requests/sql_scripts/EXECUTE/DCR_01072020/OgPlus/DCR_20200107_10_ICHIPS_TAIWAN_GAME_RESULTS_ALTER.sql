
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200107_10_ICHIPS_TAIWAN_GAME_RESULTS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game results table';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200107_10_ICHIPS_TAIWAN_GAME_RESULTS_ALTER.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/* tw_3cards_game_results TABLE */
ALTER TABLE `tw_3cards_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_banker_niuniu_game_results TABLE */
ALTER TABLE `tw_banker_niuniu_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_dragontiger_game_results TABLE */
ALTER TABLE `tw_dragontiger_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_hundred_3cards_game_results TABLE */
ALTER TABLE `tw_hundred_3cards_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_hundred_niuniu_game_results TABLE */
ALTER TABLE `tw_hundred_niuniu_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_hundred_texas_holdm_game_results TABLE */
ALTER TABLE `tw_hundred_texas_holdm_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_landlord_game_results TABLE */
ALTER TABLE `tw_landlord_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/* tw_niuniu_game_results TABLE */
ALTER TABLE `tw_niuniu_game_results` MODIFY `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.';

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/