
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20200107_20_ICHIPS_TAIWAN_GAME_TRANSACTIONS_ALTER.sql';
SET @Author_Name := 'Mel Reyes';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Alter all taiwan game transactions table';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20200107_20_ICHIPS_TAIWAN_GAME_TRANSACTIONS_ALTER.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

/* tw_3cards_transactions TABLE */
ALTER TABLE `tw_3cards_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_banker_niuniu_transactions TABLE */
ALTER TABLE `tw_banker_niuniu_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_dragontiger_transactions TABLE */
ALTER TABLE `tw_dragontiger_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_hundred_3cards_transactions TABLE */
ALTER TABLE `tw_hundred_3cards_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_hundred_niuniu_transactions TABLE */
ALTER TABLE `tw_hundred_niuniu_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_hundred_texas_holdm_transactions TABLE */
ALTER TABLE `tw_hundred_texas_holdm_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';
	
/* tw_landlord_transactions TABLE */
ALTER TABLE `tw_landlord_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/* tw_niuniu_transactions TABLE */
ALTER TABLE `tw_niuniu_transactions` 
	ADD COLUMN `platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'It’s code for platform, in order to distinguish between different sources. Default is Mars.' AFTER `id`,
	ADD COLUMN `error_status` TINYINT(1) NULL DEFAULT NULL COMMENT '0:unbalanced; 1:balanced; 2:balanced failure; 3:refund failure' AFTER `result`,
	ADD COLUMN `withhold` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'In the opening game, in order to prevent players from making insufficient deductions due to bets' AFTER `error_status`,
	ADD COLUMN `bet_device` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet device' AFTER `bet_date`,
	ADD COLUMN `bet_browser` VARCHAR(8) NOT NULL DEFAULT '0' COMMENT 'bet browser' AFTER `bet_device`,
	DROP COLUMN `transaction_status`,
	DROP COLUMN `transaction_reason`,
	MODIFY `bet_amount` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'bet amount',
	MODIFY `effective_bet_amount` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'valid bet',
	MODIFY `win_loss` DECIMAL(50,6) NULL DEFAULT '0.000000' COMMENT 'win lose of this bet',
	MODIFY `balance_after_bet` DECIMAL(50,6) NOT NULL DEFAULT '0.000000' COMMENT 'balance afters bet';

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/