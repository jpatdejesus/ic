DELIMITER $$
CREATE PROCEDURE `sp_get_count_admin_get_blacklists_datatable`(
    IN `username` VARCHAR(50),
    IN `remark` VARCHAR(50),
    IN `date_start` VARCHAR(50),
    IN `date_end` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
    
SET @v_main_query := CONCAT('
    SELECT COUNT(*) as row_count
    FROM `ichips_staging`.`withdraw_black_lists` AS wbl
    WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `wbl`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `username` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `wbl`.`username` LIKE ", CONCAT("'%", `username`, "%'"));
END IF;

IF `remark` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `wbl`.`remark` LIKE ", CONCAT("'%", `remark`, "%'"));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
