DELIMITER $$
CREATE PROCEDURE `sp_paginate_admin_operator_inquiry_datatable`(
    IN `query_type` VARCHAR(10),
    IN `param` VARCHAR(200),
    IN `start` INT,
    IN `length` INT,
    IN `order_by` VARCHAR(50),
    IN `order_sort` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Operator Inquiry paginate datatable endpoint in IChips Admin Tool'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT
        `UserId`,
        `LoginName`,
        `RealName`,
        `StateId`,
        `LastLogin`,
        `LastModify`,
        `LastModifiedBy`,
        `Path`,
        `Layer`,
        `VideoBetLimitIDs`,
        `RouletteBetLimitIDs`,
        `limits`,
        `Prefix`,
        `platformbetcontrol`
    FROM `ichips_staging`.`operators`
    WHERE 1
');

IF `query_type` = 'ID' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `UserId` = ", CONCAT("'", `param`, "'"));
ELSEIF `query_type` = 'NAME' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `LoginName` LIKE ", CONCAT("'%", `param`, "%'"));
    SET @v_main_query := CONCAT(@v_main_query, " OR `RealName` LIKE ", CONCAT("'%", `param`, "%'"));
END IF;

SET @v_main_query := CONCAT(@v_main_query, " ORDER BY ", `order_by`, " ", `order_sort`);
SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
