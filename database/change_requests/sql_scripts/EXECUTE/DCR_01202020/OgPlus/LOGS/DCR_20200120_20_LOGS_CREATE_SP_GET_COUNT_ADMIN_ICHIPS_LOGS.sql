DELIMITER $$
CREATE PROCEDURE `sp_get_count_admin_ichips_logs_datatable`(
    IN `user_type` VARCHAR(50),
    IN `table_name` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;

SET @v_main_query := CONCAT('SELECT COUNT(*) as row_count FROM ');
SET @v_main_query := CONCAT(@v_main_query, `table_name`, ' WHERE 1');

IF `user_type` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `user_type` = ", CONCAT("'", `user_type`, "'"));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
