DELIMITER $$
CREATE PROCEDURE `sp_get_count_all_transfers_history`(
    IN `transfer_code` VARCHAR(199),
    IN `date_start` VARCHAR(20),
    IN `date_end` VARCHAR(20),
    IN `table_alias` VARCHAR(12)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Get count all transfers history for IChips API'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;

    IF `table_alias` = 'transfers' THEN
        SET @v_main_query := CONCAT('
            SELECT COUNT(*) AS row_count
            FROM `datarepo_staging`.`transfers` as t
            WHERE 1
        ');

        IF `transfer_code` <> '' THEN
            SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`transfer_id` = ", CONCAT("'", `transfer_code`, "'"));
        END IF;

        IF `date_start` <> '' AND `date_end` <> '' THEN
            SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
            SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
        END IF;

    ELSE
        SET @v_main_query := CONCAT('
            SELECT COUNT(*) AS row_count
            FROM `datarepo_staging`.`transfers_php` as t
            WHERE 1
        ');

        IF `transfer_code` <> '' THEN
            SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`transfer_id` = ", CONCAT("'", `transfer_code`, "'"));
        END IF;

        IF `date_start` <> '' AND `date_end` <> '' THEN
            SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
            SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
        END IF;
    END IF;

    SET @qfinal := @v_main_query;
    PREPARE stmt FROM @qfinal;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
