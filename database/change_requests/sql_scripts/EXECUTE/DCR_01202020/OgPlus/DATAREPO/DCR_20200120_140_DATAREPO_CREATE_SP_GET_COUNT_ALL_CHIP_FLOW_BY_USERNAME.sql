DELIMITER $$
CREATE PROCEDURE `sp_get_count_chip_flow_by_username_history`(
    IN `username` VARCHAR(200),
    IN `table_alias` VARCHAR(12),
    IN `date_start` VARCHAR(20),
    IN `date_end` VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Get count chip_flow by username history for IChips API'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) AS row_count
    FROM `datarepo_staging`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `table_alias`, "`"), " AS cf");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");
SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`username` = ", CONCAT("'", `username`, "'"));

IF `date_start` <> '' AND `date_end` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
    SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
