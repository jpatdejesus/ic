DELIMITER $$
CREATE PROCEDURE `sp_get_count_admin_whitelist_requests_datatable`(
    IN `status` VARCHAR(50),
    IN `date_start` VARCHAR(50),
    IN `date_end` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Withdrawal Verification Datatable Count SP'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
    
SET @v_main_query := CONCAT('
    SELECT COUNT(*) as row_count
    FROM `ichips_mx_staging`.`withdraw_white_lists` AS wwl
    WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `status` <> 'ALL' AND `status` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`status` = ", CONCAT("'", `status`, "'"));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
