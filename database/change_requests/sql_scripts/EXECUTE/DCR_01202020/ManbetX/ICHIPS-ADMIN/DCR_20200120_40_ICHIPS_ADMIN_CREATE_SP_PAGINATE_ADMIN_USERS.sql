DELIMITER $$
CREATE PROCEDURE `sp_paginate_admin_users_datatable`(
    IN `email` VARCHAR(199),
    IN `user_status` VARCHAR(20),
    IN `admin_role_id` INT,
    IN `user_id` INT,
    IN `start` INT,
    IN `length` INT,
    IN `order_by` VARCHAR(50),
    IN `order_sort` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;
    
SET @v_main_query := CONCAT('
    SELECT
        `au`.`id`,
        `au`.`first_name`,
        `au`.`last_name`,
        `au`.`nickname`,
        `au`.`email`,
        `au`.`status`,
        `au`.`created_at`,
        `au`.`updated_at`,
        `au`.`deleted_at`,
        `ar`.`name` AS role
    FROM `ichips_admin_mx_staging`.`admin_users` AS au 
    JOIN `ichips_admin_mx_staging`.`admin_user_roles` AS aur ON aur.user_id = au.id
    JOIN `ichips_admin_mx_staging`.`admin_roles` AS ar ON ar.id = aur.role_id
    WHERE 1
');


IF `email` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `au`.`email` LIKE ", CONCAT("'%", `email`, "%'"));
END IF;

IF `user_status` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `au`.`status` = ", CONCAT("'", `user_status`, "'"));
END IF;

IF `admin_role_id` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `ar`.`id` = ", `admin_role_id`);
END IF;

IF `user_id` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `au`.`id` <> ", `user_id`);
END IF;


SET @v_main_query := CONCAT(@v_main_query, " AND `au`.`deleted_at` IS NULL");
SET @v_main_query := CONCAT(@v_main_query, " ORDER BY ", `order_by`, " ", `order_sort`);
SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
