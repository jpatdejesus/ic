DELIMITER $$
CREATE PROCEDURE `sp_get_count_all_chip_flow_history`(
    IN `table_alias` VARCHAR(255)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Get count all chip_flow history for IChips API'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) AS row_count
    FROM `datarepo_mx_staging`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `table_alias`, "`"), " AS t ");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
