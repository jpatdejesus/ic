DELIMITER $$
CREATE PROCEDURE `sp_get_count_game_result_history`(
    IN `game_name` VARCHAR(255),
    IN `result_id` VARCHAR(255),
    IN `date_start` VARCHAR(20),
    IN `date_end` VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Get count game results history for IChips API'
BEGIN

/**************************** Procedure Header ****************************
Version         Author              Date            Description
1.0         <Jayson Mag-isa>     <01-21-2020>          Initial Creation


**************************** Main Script ******************************/

DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) AS row_count
    FROM `datarepo_mx_staging`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `game_name`, "_game_results`"), " AS t ");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");

IF `result_id` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `result_id` = ", CONCAT("'", `result_id`, "'" ));
END IF;

IF `date_start` <> '' AND `date_end` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
    SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/**************************** End of script ******************************/
END $$
DELIMITER ;
