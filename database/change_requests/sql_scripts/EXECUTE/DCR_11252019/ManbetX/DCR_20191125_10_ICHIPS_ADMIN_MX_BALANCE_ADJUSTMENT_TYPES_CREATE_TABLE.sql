
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20191125_10_ICHIPS_ADMIN_MX_BALANCE_ADJUSTMENT_TYPES_CREATE_TABLE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Create table balance adjustment types';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20191125_10_ICHIPS_ADMIN_MX_BALANCE_ADJUSTMENT_TYPES_CREATE_TABLE.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

CREATE TABLE IF NOT EXISTS `balance_adjustment_types` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `type_name` VARCHAR(50) NOT NULL,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/