
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20191125_30_DATAREPO_MX_CASHFLOW_UPDATE_NULL_GAME_CODE_FOR_ADMIN_TOOL_UPDATE_BALANCE.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert initial data for balance adjustment types';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20191125_30_DATAREPO_MX_CASHFLOW_UPDATE_NULL_GAME_CODE_FOR_ADMIN_TOOL_UPDATE_BALANCE.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

UPDATE `cashflow` SET `game_code`='balance_adjustment' WHERE `game_code` IS NULL AND (`trans_type` = 'MODIFY_ADD' OR `trans_type` = 'MODIFY_DEDUCT');

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/