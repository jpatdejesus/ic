
/**************************** Script Header ******************************/
SET @File_script_name := 'DCR_20191125_20_ICHIPS_MX_ADMIN_BALANCE_ADJUSTMENT_TYPES_INSERT_TYPES.sql';
SET @Author_Name := 'Jayson Mag-isa';
SET @Database_Version := '1.0';
SET @Change_request_cd := '';
SET @Script_Description :='Insert initial data for balance adjustment types';
SET @Rollback_Script_name := 'ROLLBACK_DCR_20191125_20_ICHIPS_MX_ADMIN_BALANCE_ADJUSTMENT_TYPES_INSERT_TYPES.sql';

/********************** Constants No change here **************************/

SELECT DATABASE() INTO @database_name;

CALL `utility`.`sp_script_log_insert`(
	@File_script_name,
	@Author_Name,
	@Database_Version,
	@database_name,
	@Change_request_cd,
	@Script_Description,
	@Rollback_Script_name,
	@script_log_id
);

/**************************** Main Script Here ******************************/

INSERT INTO `balance_adjustment_types` (`type_name`) VALUES ('balance_adjustment');
INSERT INTO `balance_adjustment_types` (`type_name`) VALUES ('promo');

/**************************** Logging Script ******************************/
CALL `utility`.`sp_script_log_update`(@script_log_id);


/**************************** End of Script ******************************/