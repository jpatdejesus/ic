<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPhpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_php', function (Blueprint $table) {
            $table->increments('userid');
            $table->string('UserName', 30);
            $table->string('TrueName', 20);
            $table->string('userpass', 50)->nullable();
            $table->string('pre_idTrueName', 50)->nullable();
            $table->string('pre_idName', 50)->nullable();
            $table->integer('pre_id')->nullable();
            $table->string('telephone', 20)->nullable();
            $table->string('QQ', 15)->default('1');
            $table->string('Email', 50)->nullable();
            $table->string('WithdrawalPassword', 50)->nullable();
            $table->integer('isuseable')->nullable();
            $table->integer('classid')->nullable();
            $table->integer('IsLogin')->nullable();
            $table->string('IsOnline', 3)->nullable();
            $table->char('abc', 1)->nullable();
            $table->char('pltype', 8)->nullable();
            $table->char('islock', 1)->nullable();
            $table->string('remark', 3)->default('0');
            $table->string('sessionid', 50)->nullable();
            $table->string('pre_sequence', 60)->nullable();
            $table->dateTime('updatetime')->nullable();
            $table->string('moneysort', 3)->nullable();
            $table->decimal('curMoney1', 18, 2)->nullable();
            $table->string('VideoBetLimitIDs',100)->nullable();
            $table->string('RouletteBetLimitIDs',100)->nullable();
            $table->string('SportsBetLimitIDs',25)->nullable();
            $table->string('ElectronicGameBetLimitIDs',25)->nullable();
            $table->string('LotteryGameBetLimitIDs',25)->nullable();
            $table->string('ChipVideo',50)->nullable();
            $table->string('ChipRoulette',50)->nullable();
            $table->string('limits',200)->nullable();
            $table->string('Operator',50)->nullable();
            $table->dateTime('AddTime')->nullable();
            $table->string('AddressIP',50)->nullable();
            $table->tinyInteger('CredentialsType')->nullable();
            $table->string('CredentialsNumber', 60)->nullable();
            $table->text('Memo')->default('');
            $table->decimal('CenterWallet', 18 , 2)->nullable();
            $table->tinyInteger('UserType')->default(1);
            $table->string('PlatformBetControl', 10)->default('1,1,1');
            $table->string('RegisterIP', 18)->nullable();
            $table->string('SourceInfo', 255)->nullable();
            $table->decimal('winPoint', 18, 2)->nullable();
            $table->string('Question', 100)->nullable();
            $table->string('Answer', 100)->nullable();
            $table->decimal('Point', 50, 5)->nullable();
            $table->decimal('Exp', 50, 5)->nullable();
            $table->dateTime('last_login')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_php');
    }
}
