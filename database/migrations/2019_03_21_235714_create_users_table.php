<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('currency_id');
            $table->string('username');
            $table->string('nickname');
            $table->decimal('balance', 50, 5)->default(0.00000);
            $table->decimal('point', 50, 5)->default(0.00000);
            $table->decimal('exp', 50, 5)->default(0.00000);
            $table->enum('status', array_values(config('ichips.user_statuses')));
            $table->timestamp('last_login_time')->nullable();
            $table->timestamp('last_action_time')->nullable();
            $table->foreign('currency_id', 'FK_user_currency_id')->references('id')->on('currencies')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('users');
    }
}
