<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('game_code', 32)->unique();
            $table->string('game_name', 255);
            $table->integer('game_type_id');
            $table->foreign('game_type_id', 'FK_games_game_type_id')->references('id')->on('game_types')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('games');
    }
}
