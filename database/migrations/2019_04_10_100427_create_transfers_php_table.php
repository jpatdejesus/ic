<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersPHPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('datarepo')->create('transfers_php', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transfer_id', 50)->nullable();
            $table->string('op_transfer_id', 100)->unique();
            $table->string('username', 50);
            $table->char('currency_code', 3);
            $table->string('actions', 10);
            $table->decimal('amount', 50, 5);
            $table->decimal('balance', 50, 5);
            $table->timestamp('created_at')->useCurrent();
            $table->string('path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('datarepo')->dropIfExists('transfers_php');
    }
}
