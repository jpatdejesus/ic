<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);

        foreach ($gameCodes as $gameCode) {
            Schema::connection('datarepo')->create($gameCode . '_transactions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('bet_code', 255)->unique();
                $table->decimal('bet_amount', 50, 5);
                $table->string('username', 255);
                $table->string('user_type', 255);
                $table->string('bet_place', 255);
                $table->decimal('effective_bet_amount', 50, 5);
                $table->string('shoehandnumber', 255);
                $table->integer('gameset_id');
                $table->string('gamename', 255);
                $table->string('tablenumber', 255);
                $table->decimal('balance', 50, 5);
                $table->decimal('win_loss', 50, 5);
                $table->integer('result_id');
                $table->string('result');
                $table->dateTime('bet_date', 0);
                $table->boolean('super_six')->default(0);
                $table->boolean('is_sidebet')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);

        foreach ($gameCodes as $gameCode) {
            Schema::connection('datarepo')->dropIfExists($gameCode . '_transactions');
        }
    }
}
