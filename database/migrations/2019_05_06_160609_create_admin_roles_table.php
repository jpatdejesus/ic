<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips-admin')->create('admin_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('is_default', [1,0]);
            $table->enum('role_group', ['SUPER_ADMIN', 'ADMIN', 'AUDIT']);
            $table->integer('master_role_id')->nullable();
            $table->unique(['name']);
            $table->foreign('master_role_id', 'FK_admin_roles_master_role_id')->references('id')->on('admin_roles')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
        }

        Schema::connection('ichips-admin')->dropIfExists('admin_roles');

        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}
