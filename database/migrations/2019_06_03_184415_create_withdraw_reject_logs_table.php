<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawRejectLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('withdraw_reject_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('user_type', 50);
            $table->enum('action', ['DUPLICATE','REJECTED']);
            $table->string('remark', 2000);
            $table->string('transfer_id');
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('withdraw_reject_logs');
    }
}
