<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Collection;
use App\Services\Log\TableNameBuilder;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $routeNameList = $this->getTableNameList();

        $routeNameList->each(function ($item, $key) {
            $tableName = TableNameBuilder::build($item);
            Schema::connection('logs')->create($tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('api_consumer_username', 255);
                $table->string('username', 255);
                $table->string('user_type', 255);
                $table->longText('request_content');
                $table->longText('response_content');
                $table->string('remark', 255);
                $table->timestamp('created_at')->useCurrent();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $routeNameList = $this->getTableNameList();

        $routeNameList->each(function ($item, $key) {
            $tableName = TableNameBuilder::build($item);
            var_dump($tableName);
            Schema::connection('logs')->dropIfExists($tableName);
        });
    }

    /**
     * Get all registered route name
     *
     * @return Collection
     */
    private function getTableNameList()
    {
        $routeNameList = collect(Route::getRoutes())->pluck('action.name')->unique();
        $routeNameList->push('Testing Name');
        $filteredRouteNameList = $this->removeNull($routeNameList);
        return $filteredRouteNameList;
    }

    /**
     * Remove null from route name list
     *
     * @param Collection $collection
     * @return void
     */
    private function removeNull(Collection $collection)
    {
        return $collection->filter(function ($item) {
            return $item !== null;
        });
    }
}
