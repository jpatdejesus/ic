<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminBalanceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips-admin')->create('admin_update_balance_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('user_type');
            $table->decimal('amount', 50, 5)->default(0.00000);
            $table->string('reason')->nullable();
            $table->string('trans_type', 50)->default('balance_adjustment')->nullable();
            $table->enum('type', ['ADD', 'DEDUCT']);
            $table->enum('status', ['PENDING', 'APPROVED', 'REJECTED'])->default('PENDING');
            $table->integer('created_by');
            $table->foreign('created_by')->references('id')->on('admin_users');
            $table->integer('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('admin_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips-admin')->dropIfExists('admin_update_balance_requests');
    }
}
