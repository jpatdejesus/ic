<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawWhiteListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('withdraw_white_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('user_type', 50);
            $table->timestamp('expire_time')->default('0000-00-00 00:00:00');
            $table->string('remark', 2000);
            $table->string('whitelisted_by');
            $table->string('updated_by')->nullable();
            $table->enum('status', ['PENDING','APPROVED','REJECTED'])->default('PENDING');
            $table->timestamp('date_approved')->default('0000-00-00 00:00:00');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('withdraw_white_lists');
    }
}
