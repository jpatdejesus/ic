<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpidGameidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('opid_gameid', function (Blueprint $table) {
            $table->bigInteger('operator_id');
            $table->bigInteger('game_id');
            $table->primary(['operator_id', 'game_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('opid_gameid');
    }
}
