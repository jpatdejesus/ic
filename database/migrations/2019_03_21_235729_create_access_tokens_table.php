<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('access_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('role_credential_id');
            $table->string('token');
            $table->timestamp('expires_at')->nullable()->default(null);
            $table->foreign('role_credential_id', 'FK_access_tokens_role_credential_id')->references('id')->on('role_credentials')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips')->dropIfExists('access_tokens');
    }
}
