<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('datarepo')->create('transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transfer_id', 50)->nullable();
            $table->string('op_transfer_id', 100)->unique();
            $table->string('username', 255);
            $table->char('currency_code', 3);
            $table->string('actions', 10);
            $table->decimal('amount', 50, 5);
            $table->decimal('balance', 50, 5);
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('datarepo')->dropIfExists('transfers');
    }
}
