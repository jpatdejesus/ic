<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips')->create('operators', function (Blueprint $table) {
            $table->increments('UserId');
            $table->string('LoginName');
            $table->string('RealName');
            $table->string('Pwd');
            $table->string('StateId');
            $table->string('Remark')->nullable();
            $table->string('LastLogin')->nullable();
            $table->string('LastModify')->nullable();
            $table->string('LastModifiedBy')->nullable();
            $table->string('CheckSumUser')->nullable();
            $table->string('ExpireTime')->nullable();
            $table->string('ParentID')->nullable();
            $table->string('Path')->nullable();
            $table->string('Layer')->nullable();
            $table->string('OccupyRate')->nullable();
            $table->string('UserType')->nullable();
            $table->string('VideoBetLimitIDs')->nullable();
            $table->string('RouletteBetLimitIDs')->nullable();
            $table->string('SportsBetLimitIDs')->nullable();
            $table->string('ElectronicGameBetLimitIDs')->nullable();
            $table->string('LotteryGameBetLimitIDs')->nullable();
            $table->string('limits')->nullable();
            $table->string('SessionID')->nullable();
            $table->string('telephone')->nullable();
            $table->string('QQ')->nullable();
            $table->string('Email');
            $table->string('WithdrawalPassword')->nullable();
            $table->string('CredentialsType')->nullable();
            $table->string('CredentialsNumber')->nullable();
            $table->string('SubParentID')->nullable();
            $table->string('BankID')->nullable();
            $table->string('BankName')->nullable();
            $table->string('BankNumber')->nullable();
            $table->string('BankProvince')->nullable();
            $table->string('BankCity')->nullable();
            $table->string('PromotionURL')->nullable();
            $table->string('PromotionOtherURL')->nullable();
            $table->string('Prefix')->nullable();
            $table->string('SiteNumber')->nullable();
            $table->string('userkey')->nullable();
            $table->string('commendCode')->nullable();
            $table->string('domain')->nullable();
            $table->string('curMoney1')->nullable();
            $table->string('isPay')->nullable();
            $table->string('whkind')->nullable();
            $table->string('xfkind')->nullable();
            $table->string('Regdate')->nullable();
            $table->string('platformbetcontrol')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
        }

        Schema::connection('ichips')->dropIfExists('operators');

        if (DB::connection()->getDriverName() === 'mysql') {
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}
