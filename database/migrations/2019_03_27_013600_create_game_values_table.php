<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('datarepo')->create('game_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("values");
            $table->integer("game_type");
            $table->integer("result_id");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('datarepo')->dropIfExists('game_values');
    }
}
