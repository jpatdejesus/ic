<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashflowPHPTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('datarepo')->create('cashflow_php', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("username", 32);
            $table->string("game_code", 32)->nullable();
            $table->string("trans_type", 45);
            $table->string("betting_code", 100)->nullable()->comment('Handles betting_code if BET/PAYOUT or transfer_id if WITHDRAW/DEPOSIT or NULL');
            $table->decimal("trans_amount", 50, 5);
            $table->decimal("balance", 50, 5);
            $table->timestamps();
            $table->softDeletes();
            $table->string('path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('datarepo')->dropIfExists('cashflow_php');
    }
}
