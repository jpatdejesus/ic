<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);

        foreach ($gameCodes as $gameCode) {
            Schema::connection('datarepo')->create($gameCode . '_game_results', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('result', 255);
                $table->string("shoehandnumber");
                $table->dateTime("shoe_date");
                $table->string("table_no");
                $table->string("result_id", 50);
                $table->text("values");
                $table->timestamp('created_at')->useCurrent();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);

        foreach ($gameCodes as $gameCode) {
            Schema::connection('datarepo')->dropIfExists($gameCode . '_game_results');
        }
    }
}
