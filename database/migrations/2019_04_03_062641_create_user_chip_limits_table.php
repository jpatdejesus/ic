<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChipLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_chip_limits', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->integer('user_type_id');
            $table->integer('game_type_id');
            $table->decimal('min', 50, 5);
            $table->decimal('max', 50, 5);
            $table->foreign('user_type_id', 'FK_user_chip_limits_game_type_id')->references('id')->on('user_types')->onDelete('no action')->onUpdate('no action');
            $table->foreign('game_type_id', 'FK_user_chip_limits_game_type_id')->references('id')->on('game_types')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_chip_limits');
    }
}
