<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('ichips-admin')->create('admin_audit_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email');
            $table->string('role')->nullable();
            $table->string('event')->nullable();
            $table->string('old_values')->nullable();
            $table->string('new_values')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('platform')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('ichips-admin')->dropIfExists('admin_audit_logs');
    }
}
