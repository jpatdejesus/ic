<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChipLimitDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_chip_limit_defaults', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_type_id');
            $table->integer('currency_id')->nullable();
            $table->decimal('min', 50, 5);
            $table->decimal('max', 50, 5);
            $table->foreign('game_type_id', 'FK_user_chip_limit_defaults_game_type_id')->references('id')->on('game_types')->onDelete('no action')->onUpdate('no action');
            $table->foreign('currency_id', 'FK_user_chip_limit_defaults_currency_id')->references('id')->on('currencies')->onDelete('no action')->onUpdate('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_chip_limit_defaults');
    }
}
