<?php

use Illuminate\Database\Seeder;
use App\Models\v1\ApiConfiguration;

class ApiConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiConfiguration::create([
            'feature_name' => 'withdraw_verification',
        ]);
    }
}
