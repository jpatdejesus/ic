<?php

use Illuminate\Database\Seeder;

class AuditTrailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\v1\AdminAuditLogs::create([
            'email' => 'test_account@gmail.com',
            'role'=> null,
            'event'=> 'login',
            'old_values'=> null,
            'new_values'=> null,
            'ip_address'=> null,
            'platform'=> null,
            'remarks'=> 'login successfully!',
        ]);
    }
}
