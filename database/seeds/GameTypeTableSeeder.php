<?php

use Illuminate\Database\Seeder;

class GameTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gameTypes = collect(config('ichips.game_types'));

        foreach ($gameTypes as $gameType){
            \App\Models\v1\GameType::insert(['game_type_name' => $gameType]);
        }
    }
}
