<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = collect(config('ichips.user_types'));

        foreach ($userTypes as $keyUserType => $userType){
            \App\Models\v1\UserType::insert(['user_type_name' => $keyUserType]);
        }
    }
}
