<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = collect(config('ichips.currencies'));

        foreach ($currencies as $currency => $currency_data){
            \App\Models\v1\Currency::insert(['currency_code' => $currency, 'conversion_by_rmb' => $currency_data['conversion_by_rmb']]);
        }
    }
}
