<?php

use Carbon\Carbon;
use App\Models\v1\AdminRole;
use Illuminate\Database\Seeder;

class AdminRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = Carbon::now();
        foreach (config('ichips.admin_default_roles_and_users') as $roleGroup => $roles) {
            foreach ($roles as $role => $defaultAccounts) {
                $newData = AdminRole::where(
                    [
                        'name' => $role,
                        'role_group' => $roleGroup,
                    ]
                )->first();
                if ($newData === null) {
                    (new AdminRole)->create([
                        'name' => $role,
                        'role_group' => $roleGroup,
                        'is_default' => 1,
                        'created_at' => $dateNow,
                        'updated_at' => $dateNow
                    ]);
                }
            }
        }
    }
}
