<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameResultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gameCodes = (new \App\Repositories\v1\GameRepository())->getGameCodes();

        foreach ($gameCodes as $gameCode) {
            for ($i = 0; $i < 10; $i++) {
                $data = factory(\App\Models\v1\GameResult::class)->make()->toArray();
                DB::connection('datarepo')
                    ->table("{$gameCode}_game_results")
                    ->insert($data);
            }
        }
    }
}
