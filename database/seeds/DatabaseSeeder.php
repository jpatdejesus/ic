<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CurrencyTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleCredentialTableSeeder::class);
        $this->call(GameTypeTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(ChipLimitDefaultsTableSeeder::class);
        $this->call(GameTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleHasPermissionTableSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(AdminUserTableSeeder::class);
        $this->call(AdminRoleTableSeeder::class);
        $this->call(AdminUserRoleTableSeeder::class);
        $this->call(AdminPermissionTableSeeder::class);
        $this->call(AdminRoleHasPermissionTableSeeder::class);
        $this->call(ApiConfigurationTableSeeder::class);
        $this->call(BalanceAdjustmentTypesSeeder::class);
        
        if (DB::connection()->getDriverName() !== 'sqlite') {
            //Uncomment this for testing purposes only
            /*$this->call(TransactionTableSeeder::class);
            $this->call(TransferTableSeeder::class);
            $this->call(GameResultTableSeeder::class);
            $this->call(AuditTrailsSeeder::class);*/
        }
    }
}
