<?php

use App\Models\v1\Role;
use App\Models\v1\RoleCredential;
use App\Models\v1\UserRole;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('ichips.default_roles') as $key => $item) {
            foreach ($item as $realItem) {
                $roleCredID = RoleCredential::select('id')->where('username', $realItem)->first();
                $roleID = Role::select('id')->where('name', $realItem)->first();
                if ($roleCredID !== null && $roleID !== null) {
                    $userRole = UserRole::where([
                        'role_credential_id' => $roleCredID->id,
                        'role_id' => $roleID->id,
                    ])->first();
                    if ($userRole === null) {
                        (new UserRole)->create([
                            'role_credential_id' => $roleCredID->id,
                            'role_id' => $roleID->id,
                        ]);
                    }
                }
            }
        }
    }
}
