<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = config('ichips.user_types');

        foreach ($userTypes as $key => $value) {
            $model = get_user_type_model($key, 'users');
            $params = [
                $model->getFieldName('username') => 'test_account_' . strtolower($key),
                $model->getFieldName('nickname') => 'test_account_' . strtolower($key),
            ];

            if ($value['users']['model'] === \App\Models\v1\UserPHP::class) {
                $params['moneysort'] = factory(\App\Models\v1\Currency::class)->create(['currency_code' => 'CNY'])->currency_code;
                $params['isuseable'] = 1;
            } else {
                $params['currency_id'] = factory(\App\Models\v1\Currency::class)->create(['currency_code' => 'CNY'])->id;
                $params['status'] = 'ENABLED';
            }

            if (($value['users']['model'] === \App\Models\v1\UserPHP::class &&
                    !$model->where($model->getFieldName('username'), 'test_account_' . strtolower($key))->first()) ||
                $value['users']['model'] === \App\Models\v1\User::class) {
                $model->create($params);
            }

        }
    }
}
