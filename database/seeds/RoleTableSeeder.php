<?php

use Carbon\Carbon;
use App\Models\v1\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_now = Carbon::now();
        foreach (config('ichips.default_roles') as $key => $value) {
            foreach ($value as $role) {
                $newData = Role::where(
                    [
                        'name' => $role,
                        'role_group' => $key,
                    ]
                )->first();
                if ($newData === null) {
                    (new Role)->create([
                        'name' => $role,
                        'role_group' => $key,
                        'is_default' => 1,
                        'created_at' => $date_now,
                        'updated_at' => $date_now
                    ]);
                }
            }
        }
    }
}
