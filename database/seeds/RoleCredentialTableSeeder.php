<?php

use Illuminate\Database\Seeder;

class RoleCredentialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleCredentials = [
            'ichips',
            'panda',
            'ceres',
            'baccarat',
            'dragontiger',
            'fantan',
            'moneywheel',
            'roulette',
            'sicbo',
            'niuniu',
            '3cards',
            'gmtadmin',
        ];


        foreach ($roleCredentials as $roleCredential) {
            \App\Models\v1\RoleCredential::create([
                'username' => $roleCredential,
                'password' => \Illuminate\Support\Facades\Hash::make('password123!')
            ]);
        }
    }
}
