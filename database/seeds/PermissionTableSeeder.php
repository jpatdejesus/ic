<?php

use Carbon\Carbon;
use App\Models\v1\Permission;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Route;

class PermissionTableSeeder extends Seeder
{

    public $routesList = [];
    public $permissionList = [];

    public function __construct()
    {
        $this->routesList = Route::getRoutes();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->routesList as $routes) {
            $routeType = explode('.', $routes['action']['as'])[0];
            $asLength = count(explode('.', $routes['action']['as']));

            if ($asLength <= 1) {
                continue;
            }
            if ($routeType !== "CLIENT") {
                continue;
            }
            $this->permissionList[$routes['action']['as']][] = [
                'name' => $routes['action']['name'],
                'code' => $routes['method'] . '::' . str_replace(' ', '_', $routes['action']['name']),
                'description' => $routes['action']['description'],
            ];
        }

        foreach ($this->permissionList as $key => $value) {
            $parentCode = explode('.', $key)[1];
            $parentName = ucwords(str_replace('_', ' ', $parentCode));
            $parent = $this->startSeed($parentName, 'Group of function handled ' . $parentName, $parentCode);
            foreach ($value as $item) {
                $this->startSeed($item['name'], $item['description'], $item['code'], $parent->id);
            }
        }
    }

    public function startSeed($permissionName, $permissionDescription, $code, $parentID = null)
    {
        $newData = Permission::where('code', $code)->first();
        if ($newData === null) {
            $date_now = Carbon::now();
            $newData = (new Permission)->create([
                'name' => $permissionName,
                'code' => $code,
                'parent_id' => $parentID,
                'description' => $permissionDescription,
                'created_at' => $date_now,
                'updated_at' => $date_now
            ]);
        }
        return $newData;
    }
}
