<?php

use App\Models\v1\Role;
use App\Models\v1\AdminUser;
use App\Models\v1\AdminRole;
use App\Models\v1\AdminUserRole;
use Illuminate\Database\Seeder;

class AdminUserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('ichips.admin_default_roles_and_users') as $roleGroup => $roles) {
            foreach ($roles as $role => $defaultAccounts) {
                foreach ($defaultAccounts as $key => $value) {
                    $adminUserID = AdminUser::select('id')->where('email', $value['email'])->first();
                    $adminRoleID = AdminRole::select('id')->where('name', $role)->first();
                    if ($adminUserID !== null && $adminRoleID !== null) {
                        $adminUserRole = AdminUserRole::where([
                            'user_id' => $adminUserID->id,
                            'role_id' => $adminUserID->id,
                        ])->first();
                        if ($adminUserRole === null) {
                            (new AdminUserRole)->create([
                                'user_id' => $adminUserID->id,
                                'role_id' => $adminRoleID->id,
                            ]);
                        }
                    }
                }
            }
        }
    }
}
