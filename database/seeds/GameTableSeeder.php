<?php

use Illuminate\Database\Seeder;
use App\Models\v1\Game;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = collect(config('ichips.default_games'));

        foreach ($games as $game){
            $gameType = \App\Models\v1\GameType::whereGameTypeName($game['game_type_name'])->firstOrFail();
            unset($game['game_type_name']);
            $game['game_type_id'] = $gameType->id;
            Game::insert($game);
        }
    }
}
