<?php

use Illuminate\Database\Seeder;

class ChipLimitDefaultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default_currency_min_max = collect(config('ichips.default_currency_min_max'));
        $game_types = \App\Models\v1\GameType::all()->toArray();

        foreach ($default_currency_min_max as $d_c_min_max) {
            foreach ($game_types as $game_type) {
                $new_d_c_min_max = $d_c_min_max;
                $new_d_c_min_max['game_type_id'] = $game_type['id'];
                $new_d_c_min_max['currency_id'] = null;

                unset($new_d_c_min_max['currency_code']);

                if ($d_c_min_max['currency_code']) {
                    $currency = \App\Models\v1\Currency::whereCurrencyCode($d_c_min_max['currency_code'])->firstOrFail();

                    if ($currency) {
                        $new_d_c_min_max['currency_id'] = $currency->id;
                    }
                }

                \App\Models\v1\UserChipLimitDefaults::insert($new_d_c_min_max);
            }
        }
    }
}
