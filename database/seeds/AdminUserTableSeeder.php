<?php

use Illuminate\Database\Seeder;

class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('ichips.admin_default_roles_and_users') as $roleGroup => $roles){
            foreach ($roles as $role => $defaultAccounts){
                if (count($defaultAccounts)) {
                    foreach ($defaultAccounts as $key => $value){
                        \App\Models\v1\AdminUser::create([
                            'email' => $value['email'],
                            'first_name' => $value['first_name'],
                            'last_name' => $value['last_name'],
                            'nickname' => $value['nickname'],
                            'password' => \Illuminate\Support\Facades\Hash::make('password123!')
                        ]);
                    }
                }
            }
        }
    }
}
