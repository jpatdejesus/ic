<?php

use Carbon\Carbon;
use App\Models\v1\AdminPermission;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Route;

class AdminPermissionTableSeeder extends Seeder
{

    public $routesList = [];
    public $permissionList = [];

    public function __construct()
    {
        $this->routesList = Route::getRoutes();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->routesList as $routes) {
            $routeType = explode('.', $routes['action']['as'])[0];
            $asLength = count(explode('.', $routes['action']['as']));

            if ($asLength <= 1) {
                continue;
            }
            if ($routeType !== "ADMIN") {
                continue;
            }
            $this->permissionList[$routes['action']['as']][] = [
                'name' => $routes['action']['name'],
                'code' => $routes['method'] . '::' . str_replace(' ', '_', $routes['action']['name']),
                'description' => $routes['action']['description'],
                'linked_route_code' => isset($routes['action']['linked_route']) ? $routes['action']['linked_route'] : null,
            ];
        }

        foreach ($this->permissionList as $key => $value) {
            $parentCode = explode('.', $key)[1];
            $parentName = ucwords(str_replace('_', ' ', $parentCode));
            $parent = $this->startSeed($parentName, 'Group of function handled ' . $parentName, $parentCode);
            foreach ($value as $item) {
                $this->startSeed($item['name'], $item['description'], $item['code'], $parent->id, $item['linked_route_code']);
            }
        }
    }

    public function startSeed($permissionName, $permissionDescription, $code, $parentID = null, $linkedRouteCode = null)
    {
        $linkedRouteId = null;
        $newData = AdminPermission::where('code', $code)->first();

        if ($linkedRouteCode) {
            $linkedRoute = AdminPermission::where('name', $linkedRouteCode)->first();
            if ($linkedRoute) {
                $linkedRouteId = $linkedRoute->id;
            }
        }
        if ($newData === null) {
            $dateNow = Carbon::now();
            $newData = (new AdminPermission)->create([
                'name' => $permissionName,
                'code' => $code,
                'parent_id' => $parentID,
                'linked_route_id' => $linkedRouteId,
                'description' => $permissionDescription,
                'created_at' => $dateNow,
                'updated_at' => $dateNow
            ]);
        }

        return $newData;
    }
}
