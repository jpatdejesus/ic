<?php

use Carbon\Carbon;
use App\Models\v1\Role;
use App\Models\v1\Permission;
use Illuminate\Database\Seeder;
use App\Models\v1\RoleHasPermission;
use \Illuminate\Support\Facades\Route;

class RoleHasPermissionTableSeeder extends Seeder
{
    public $routesList = [];
    public $permissionList = [];
    public $roleList = [];

    public function __construct()
    {
        $this->routesList = Route::getRoutes();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->routesList as $routes) {
            $defaultRole = [];
            if (isset($routes['action']['as'])) {
                if (explode('.', $routes['action']['as'])[0] == strtoupper(config('ichips.app_types.client'))) {
                    if (array_key_exists('default', $routes['action'])){
                        foreach ($routes['action']['default'] as $defaultRoles) {
                            $defaultRole = array_merge($defaultRole, $defaultRoles);
                        }
                        foreach ($defaultRole as $key => $value) {
                            $this->insert(explode('.', $routes['action']['as'])[1], $value);
                        }
                        foreach ($defaultRole as $key => $value) {
                            $this->insert($routes['method'] . '::' . str_replace(' ', '_', $routes['action']['name']), $value);
                        }
                    }
                }
            }
        }
    }

    public function insert($code, $role)
    {
        if ($this->check($code, $role) === null) {
            RoleHasPermission::create(['permission_id' => $this->permissionList[$code], 'role_id' => $this->roleList[$role]]);
        }
    }

    public function check($code, $role)
    {

        if (!isset($this->roleList[$code])) {
            $this->roleList[$role] = Role::select('id')
                ->where([
                    'name' => $role,
                    'is_default' => 1,
                ])
                ->first()
                ->id;
        }

        if (!isset($this->permissionList[$role])) {
            $this->permissionList[$code] = Permission::select('id')
                ->where('code', $code)
                ->first()
                ->id;
        }

        return RoleHasPermission::where([
                'permission_id' => $this->permissionList[$code],
                'role_id' => $this->roleList[$role],
            ])
            ->first();
    }
}
