<?php

use Illuminate\Database\Seeder;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function run()
    {
        $transaction = new \App\Models\v1\Transaction();

        $gameCodes = (new \App\Repositories\v1\GameRepository())->getGameCodes();

        $records = 10;

        $userTypes = config('ichips.user_types');

        foreach ($userTypes as $key => $value) {
            $model = get_user_type_model($key, 'users');

            $usernameKey = $model->getfieldName('username');

            $username = $model->where($usernameKey, 'test_account_' . strtolower($key))->first()->{$usernameKey};

            foreach ($gameCodes as $gameCode) {
                $transaction->setTable($gameCode . '_transactions');

                for ($i = 0; $i < $records; $i++) {
                    $transaction->create([
                        "bet_code" => $username . $i,
                        "bet_amount" => $i,
                        "username" => $username,
                        "user_type" => $key,
                        "bet_place" => 1,
                        "effective_bet_amount" => $i,
                        "shoehandnumber" => 1,
                        "gameset_id" => 1,
                        "gamename" => 1,
                        "tablenumber" => 1,
                        "shoehandnumber" => 1,
                        "balance" => 1,
                        "win_loss" => 1,
                        "result_id" => 1,
                        "result" => 'player',
                        "bet_date" => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                        "super_six" => 0,
                        "is_sidebet" => 0,
                        "created_at" => \Carbon\Carbon::now(),
                    ]);
                }
            }
        }
    }
}
