<?php

use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\Transfer::class, function (Faker\Generator $faker) {
    return [
        'transfer_id' => $faker->md5,
        'op_transfer_id' => $faker->md5,
        'username' => $faker->userName,
        'currency_code' => $faker->currencyCode,
        'actions' => $faker->randomElement($array = array ('WITHDRAW', 'DEPOSIT')),
        'amount' => $faker->randomDigitNotNull,
        'balance' => 100,
        'created_at' => Carbon::now()
    ];
});
