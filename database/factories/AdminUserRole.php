<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\AdminUserRole::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(\App\Models\v1\AdminUser::class)->create()->id,
        'role_id' => factory(\App\Models\v1\AdminRole::class)->create()->id
    ];
});
