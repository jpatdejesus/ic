<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\Operator::class, function (Faker\Generator $faker) {
    return [
        'LoginName' => $faker->unique()->name,
        'RealName' => $faker->unique()->name,
        'Pwd' => str_random(8),
        'StateId' => rand(0, 2),
        'Email' => $faker->unique()->email
    ];
});
