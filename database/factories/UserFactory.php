<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\User::class, function (Faker\Generator $faker) {
    return [
        'currency_id' => factory(\App\Models\v1\Currency::class)->create()->id,
        'username' => strtolower($faker->userName),
        'nickname' => generate_random_string(8),
        'balance' => 0.00000,
        'point' => 0.00000,
        'exp' => 0.00000,
        'status' => 'ENABLED'
    ];
});
