<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 3/29/19
 * Time: 12:38 PM
 */

/* @var $factory Closure */

use App\Models\v1\GameResult;

$factory->define(GameResult::class, function (Faker\Generator $faker) {
    return [
        'result' => $faker->word . rand(),
        'shoehandnumber' => $faker->word . $faker->numberBetween(1, 100),
        'shoe_date' => $faker->date('Y-m-d H:i:s', 'now'),
        'table_no' => $faker->word . $faker->numberBetween(1, 100),
        'result_id' => $faker->word . $faker->numberBetween(1, 100),
        'values' => $faker->paragraph(),
        'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
    ];
});
