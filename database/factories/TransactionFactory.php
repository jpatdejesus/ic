<?php

use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\Transaction::class, function (Faker\Generator $faker) {
    $userTypes = config('ichips.user_types');
    $userTypesKey = array_keys(config('ichips.user_types'));
    $userType = $userTypesKey[rand(0, sizeof($userTypesKey) - 1)];

    $username = factory($userTypes[$userType]['users']['model'])->create()->username;

    return [
        "bet_code" => substr(\Illuminate\Support\Facades\Hash::make('BET_CODE'), 0, 50),
        "bet_amount" => rand(0, 100000),
        "username" => $username,
        "user_type" => $userType,
        "bet_place" => $faker->randomElement(['tie', 'player', 'banker']),
        "effective_bet_amount" => $faker->randomFloat(2, 0, 1000000),
        "shoehandnumber" => $faker->word,
        "gameset_id" => 1,
        "gamename" => $faker->word,
        "tablenumber" => $faker->word,
        "balance" => $faker->randomFloat(2, 0, 1000000),
        "win_loss" => $faker->randomFloat(2, -10000, 1000000),
        "result_id" => 1,
        "result" => $faker->randomElement(['player', 'banker']),
        "bet_date" => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        "super_six" => $faker->randomElement([0,1]),
        "is_sidebet" => $faker->randomElement([0,1]),
        "created_at" => \Carbon\Carbon::now(),
        "updated_at" => \Carbon\Carbon::now(),
    ];
});
