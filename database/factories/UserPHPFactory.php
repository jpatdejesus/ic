<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

$factory->define(\App\Models\v1\UserPHP::class, function (Faker\Generator $faker) {
    return [
        'UserName' => strtolower($faker->userName),
        'TrueName' => generate_random_string(8),
        'userpass' => \Illuminate\Support\Facades\Hash::make('password123'),
        'pre_sequence' => $faker->userName,
        'winPoint' => 0,
        'curMoney1' => 0.00000,
        'moneysort' => 'CNY',
    ];
});
