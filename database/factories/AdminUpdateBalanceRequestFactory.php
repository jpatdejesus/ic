<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

USE App\Models\v1\AdminUpdateBalanceRequest;

$factory->define(\App\Models\v1\AdminUpdateBalanceRequest::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName(),
        'user_type' => 'MARS',
        'amount' => "100",
        'status' => AdminUpdateBalanceRequest::STATUS_PENDING,
        'reason' => $faker->text(255),
        'type' => $faker->randomElement([AdminUpdateBalanceRequest::TYPE_ADD, AdminUpdateBalanceRequest::TYPE_DEDUCT]),
        'created_by' => factory(\App\Models\v1\AdminUser::class)->create()->id,
        'updated_by' => null,
    ];
});
