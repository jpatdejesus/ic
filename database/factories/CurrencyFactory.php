<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\Currency::class, function (Faker\Generator $faker) {
    return [
        'currency_code' => $faker->currencyCode,
        'conversion_by_rmb' => 1.00000
    ];
});
