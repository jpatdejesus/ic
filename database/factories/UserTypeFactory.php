<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\App\Models\v1\UserType::class, function (Faker\Generator $faker) {
    return [
        'user_type_name' => strtoupper(str_random(6))
    ];
});
