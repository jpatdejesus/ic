CREATE TABLE IF NOT EXISTS `{TABLE_NAME}` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `api_consumer_username` VARCHAR(50) NOT NULL,
    `username` VARCHAR(50) NULL DEFAULT NULL,
    `user_type` VARCHAR(255) NULL DEFAULT NULL,
    `request_content` LONGTEXT NULL DEFAULT NULL,
    `response_content` LONGTEXT NULL DEFAULT NULL,
    `remark` VARCHAR(4000) NULL DEFAULT NULL,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;