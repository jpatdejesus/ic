DROP PROCEDURE IF EXISTS drop_column_if_exists;

CREATE PROCEDURE drop_column_if_exists(
  tname VARCHAR(64),
  cname VARCHAR(64)
)
  BEGIN
    IF column_exists(tname, cname)
    THEN
      SET @drop_column_if_exists = CONCAT('ALTER TABLE `', tname, '` DROP COLUMN `', cname, '`');
      PREPARE drop_query FROM @drop_column_if_exists;
      EXECUTE drop_query;
    END IF;
  END;

DROP PROCEDURE IF EXISTS drop_foreign_if_exists;

CREATE PROCEDURE drop_foreign_if_exists(
  tname VARCHAR(64),
  fname VARCHAR(64)
)
  BEGIN
    IF foreign_exists(tname, fname)
    THEN
      SET @drop_foreign_if_exists = CONCAT('ALTER TABLE `', tname, '` DROP FOREIGN KEY `', fname, '`');
      PREPARE drop_query FROM @drop_foreign_if_exists;
      EXECUTE drop_query;
    END IF;
  END;
