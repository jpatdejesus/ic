CREATE TABLE IF NOT EXISTS `users` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `currency_id` INT(11) NOT NULL,
    `username` VARCHAR(255) COLLATE 'utf8_unicode_ci',
    `nickname` VARCHAR(255) COLLATE 'utf8_unicode_ci',
    `prefix` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
    `balance` DECIMAL(50,5) NULL DEFAULT '0.00000',
    `point` DECIMAL(50,5) NULL DEFAULT '0.00000',
    `exp` DECIMAL(50,5) NULL DEFAULT '0.00000',
    `status` enum('ENABLED','DISABLED','SUSPENDED') COLLATE utf8mb4_unicode_ci NOT NULL,
    `last_login_time` TIMESTAMP NULL DEFAULT NULL,
    `last_action_time` TIMESTAMP NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_users_currency_id` (`currency_id`),
    CONSTRAINT `FK_users_currency_id` FOREIGN KEY (`currency_id`)
        REFERENCES `ichips`.`currencies` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
