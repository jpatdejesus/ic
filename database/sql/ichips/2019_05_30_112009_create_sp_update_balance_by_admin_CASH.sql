#sp_update_balance_CASH
DROP PROCEDURE IF EXISTS `sp_update_balance_by_admin_CASH`;
CREATE PROCEDURE `sp_update_balance_by_admin_CASH`(IN `operator` varchar(3),IN `amount` decimal(50, 5),IN `userid2` bigint)
BEGIN
  SET @q1 = CONCAT("update users_php set curMoney1 = @balance := CAST(curMoney1 as decimal(50, 5)) ", operator, " CAST('", amount, "' as decimal(50,5)) where userid = '", userid2, "'");
  
  SET @beforeBalance = (select curMoney1 as beforebalance from users_php where userid = userid2); 
  PREPARE stmt from @q1;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
  SET @afterBalance = (select curMoney1 as beforebalance from users_php where userid = userid2); 
  
  select @beforeBalance as beforeBalance, @afterBalance as afterBalance;
END