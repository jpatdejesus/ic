#sp_update_balance_CASH
DROP PROCEDURE IF EXISTS `sp_update_balance_CASH`;
CREATE PROCEDURE `sp_update_balance_CASH`(IN `operator` varchar(3),IN `amount` decimal(50, 5),IN `userid2` bigint)
BEGIN
  SET @q1 = CONCAT("update users_php set curMoney1 = @balance := CAST(curMoney1 as decimal(50, 5)) ", operator, " CAST('", amount, "' as decimal(50,5)) where userid = '", userid2, "'");
  
  SET @beforeBalance = (select curMoney1 as beforebalance from users_php where userid = userid2); 
  SET @willUpdate = TRUE;
  IF operator = '-' AND amount > @beforeBalance THEN
  	SET @willUpdate = FALSE;
  END IF;

  IF @willUpdate = TRUE THEN
	  PREPARE stmt from @q1;
	  EXECUTE stmt;
	  DEALLOCATE PREPARE stmt;
	  SET @afterBalance = (select curMoney1 as beforebalance from users_php where userid = userid2); 

	  SELECT @beforeBalance as beforeBalance, @afterBalance as afterBalance, 'Success' as status;
   ELSE
  	  SELECT 'Error' as status, CONCAT('Amount (', amount, ') is greater than user balance ', @beforeBalance) as error_message;
   END IF;
END