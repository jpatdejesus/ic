CREATE TABLE IF NOT EXISTS `user_roles` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `role_credential_id` BIGINT(20) NOT NULL,
    `role_id` INT(10) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `user_roles_role_credential_id_unique` (`role_credential_id`),
    INDEX `FK_user_roles_role_id` (`role_id`),
    CONSTRAINT `FK_user_roles_role_credential_id` FOREIGN KEY (`role_credential_id`) REFERENCES `role_credentials` (`id`),
    CONSTRAINT `FK_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
