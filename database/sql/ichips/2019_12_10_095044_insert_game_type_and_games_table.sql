INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_9');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_niuniu', 'TwNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_10');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_3cards', 'Tw3Cards', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_11');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_landlord', 'Landlord', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_12');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_niuniu', 'HundredNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_13');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_banker_niuniu', 'BankerNiuNiu', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_14');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_3cards', 'Hundred3Cards', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_15');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_hundred_texas_holdm', 'HundredTexasHoldm', LAST_INSERT_ID());

INSERT INTO `game_types` (`game_type_name`) VALUES ('GAME_TYPE_16');
INSERT INTO `games` (`game_code`, `game_name`, `game_type_id`) VALUES ('tw_dragontiger', 'TwDragontiger', LAST_INSERT_ID());
