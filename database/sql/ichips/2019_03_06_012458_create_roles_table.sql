CREATE TABLE IF NOT EXISTS `roles` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `is_default` ENUM('1','0') NOT NULL,
    `role_group` ENUM('SUPER_ADMIN', 'ADMIN', 'GENERAL') NOT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE(`name`, `role_group`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
