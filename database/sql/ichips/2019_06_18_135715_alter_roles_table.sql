ALTER TABLE `roles`
	ALTER `role_group` DROP DEFAULT;
ALTER TABLE `roles`
	CHANGE COLUMN `role_group` `role_group` ENUM('SUPER_ADMIN','ADMIN','GENERAL','GET','OTHERS','GMT') NOT NULL COLLATE 'utf8_unicode_ci' AFTER `is_default`;
