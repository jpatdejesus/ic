
#sp_update_balance_MARS
DROP PROCEDURE IF EXISTS `sp_update_balance_MARS`;
CREATE PROCEDURE `sp_update_balance_MARS`(IN `operator` varchar(3),IN `amount` decimal(50, 5),IN `userid` bigint)
BEGIN
  SET @q1 = CONCAT("update users set balance = @balance := CAST(balance as decimal(50, 5)) ", operator, " CAST('", amount, "' as decimal(50,5)) where id = '", userid, "'");
  
  SET @beforeBalance = (select balance as beforebalance from users where id = userid); 
  SET @willUpdate = TRUE;
  IF operator = '-' AND amount > @beforeBalance THEN
  	SET @willUpdate = FALSE;
  END IF;

  IF @willUpdate = TRUE THEN
	  PREPARE stmt from @q1;
	  EXECUTE stmt;
	  DEALLOCATE PREPARE stmt;
	  SET @afterBalance = (select balance as beforebalance from users where id = userid); 
  
	  SELECT @beforeBalance as beforeBalance, @afterBalance as afterBalance, 'Success' as status;
  ELSE
  	  SELECT 'Error' as status, CONCAT('Amount (', amount, ') is greater than user balance ', @beforeBalance) as error_message;
  END IF;
END