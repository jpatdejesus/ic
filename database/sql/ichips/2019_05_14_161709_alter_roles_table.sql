ALTER TABLE `roles`
	ADD COLUMN `master_role_id` INT(11) NULL DEFAULT NULL AFTER `role_group`,
	ADD CONSTRAINT `FK_roles_master_role_id` FOREIGN KEY (`master_role_id`) REFERENCES `roles` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `roles`
	DROP INDEX `name`,
	ADD UNIQUE INDEX `name` (`name`);
