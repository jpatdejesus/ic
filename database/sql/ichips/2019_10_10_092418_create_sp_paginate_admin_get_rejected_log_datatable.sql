DROP PROCEDURE IF EXISTS sp_paginate_admin_get_rejected_log_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_paginate_admin_get_rejected_log_datatable`(
	IN `username` VARCHAR(50),
	IN `transfer_id` VARCHAR(50),
	IN `remark` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50),
	IN `start` INT,
	IN `length` INT,
	IN `order_by` VARCHAR(50),
	IN `order_sort` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;
	
SET @v_main_query := CONCAT('
	SELECT `wrl`.*
	FROM `ichips`.`withdraw_reject_logs` AS wrl
	WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wrl`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `username` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wrl`.`username` LIKE ", CONCAT("'%", `username`, "%'"));
END IF;

IF `transfer_id` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wrl`.`transfer_id` LIKE ", CONCAT("'%", `transfer_id`, "%'"));
END IF;

IF `remark` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wrl`.`remark` LIKE ", CONCAT("'%", `remark`, "%'"));
END IF;

SET @v_main_query := CONCAT(@v_main_query, " ORDER BY `wrl`.", `order_by`, " ", `order_sort`);
SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;