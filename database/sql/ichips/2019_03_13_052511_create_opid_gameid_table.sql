CREATE TABLE IF NOT EXISTS `opid_gameid` (
    `operator_id` BIGINT(20) NOT NULL,
    `game_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`operator_id`, `game_id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
