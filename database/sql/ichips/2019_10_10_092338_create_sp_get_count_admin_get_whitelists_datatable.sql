DROP PROCEDURE IF EXISTS sp_get_count_admin_get_whitelists_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_admin_get_whitelists_datatable`(
	IN `username` VARCHAR(50),
	IN `expire_time` VARCHAR(50),
	IN `remark` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;
	
SET @v_main_query := CONCAT('
	SELECT COUNT(*) as row_count
	FROM `ichips`.`withdraw_white_lists` AS wwl
	WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `username` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`username` LIKE ", CONCAT("'%", `username`, "%'"));
END IF;

IF `remark` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`remark` LIKE ", CONCAT("'%", `remark`, "%'"));
END IF;

SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`expire_time` >= ", CONCAT("'", `expire_time`, "'"));
SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`status` = 'APPROVED'");
SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`deleted_at` IS NULL");

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;