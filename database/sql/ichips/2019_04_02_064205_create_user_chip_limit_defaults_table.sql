CREATE TABLE IF NOT EXISTS `user_chip_limit_defaults` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `game_type_id` INT(11) NOT NULL,
    `currency_id` INT(11) NULL DEFAULT NULL,
    `min` DECIMAL(50,5) NOT NULL,
    `max` DECIMAL(50,5) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `UNIQUE` (`game_type_id`, `currency_id`),
    INDEX `FK_user_chip_limit_defaults_currency_id` (`currency_id`),
    CONSTRAINT `FK_user_chip_limit_defaults_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_user_chip_limit_defaults_game_type_id` FOREIGN KEY (`game_type_id`) REFERENCES `game_types` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
