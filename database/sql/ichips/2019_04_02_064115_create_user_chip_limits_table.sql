CREATE TABLE IF NOT EXISTS `user_chip_limits` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(20) NOT NULL,
    `user_type_id` INT(11) NOT NULL,
    `game_type_id` INT(11) NOT NULL,
    `min` DECIMAL(50,5) NOT NULL,
    `max` DECIMAL(50,5) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `UNIQUE` (`user_id`, `user_type_id`, `game_type_id`),
    INDEX `FK_user_chip_limits_user_type_id` (`user_type_id`),
    INDEX `FK_user_chip_limits_game_type_id` (`game_type_id`),
    CONSTRAINT `FK_user_chip_limits_game_type_id` FOREIGN KEY (`game_type_id`) REFERENCES `game_types` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_user_chip_limits_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT `FK_user_chip_limits_user_type_id` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
