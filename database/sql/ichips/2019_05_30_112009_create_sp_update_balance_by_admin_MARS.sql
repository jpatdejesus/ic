#sp_update_balance_CASH
DROP PROCEDURE IF EXISTS `sp_update_balance_by_admin_MARS`;
CREATE PROCEDURE `sp_update_balance_by_admin_MARS`(IN `operator` varchar(3),IN `amount` decimal(50, 5),IN `userid` bigint)
BEGIN
  SET @q1 = CONCAT("update users set balance = @balance := CAST(balance as decimal(50, 5)) ", operator, " CAST('", amount, "' as decimal(50,5)) where id = '", userid, "'");
  
  SET @beforeBalance = (select balance as beforebalance from users where id = userid); 
  PREPARE stmt from @q1;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
  SET @afterBalance = (select balance as beforebalance from users where id = userid); 
  
  select @beforeBalance as beforeBalance, @afterBalance as afterBalance;
END