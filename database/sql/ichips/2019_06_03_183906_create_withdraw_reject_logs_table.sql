CREATE TABLE IF NOT EXISTS `withdraw_reject_logs` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `action` ENUM('DUPLICATE','REJECTED') NOT NULL,
  `user_type` VARCHAR(50) NOT NULL,
  `transfer_id` VARCHAR(255) NOT NULL,
  `remark` VARCHAR(2000) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;