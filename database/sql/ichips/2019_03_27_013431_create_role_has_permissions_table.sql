CREATE TABLE IF NOT EXISTS `role_has_permissions` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `permission_id` INT(11) NOT NULL,
    `role_id` INT(11) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `UNQ_permission_and_role_id` UNIQUE(`permission_id`, `role_id`),
    KEY `FK_role_has_permissions_role_id` (`role_id`),
    KEY `FK_role_has_permissions_permission_id` (`permission_id`),
    CONSTRAINT `FK_role_has_permissions_role_id` FOREIGN KEY (`role_id`)
        REFERENCES `ichips`.`roles` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_role_has_permissions_permission_id` FOREIGN KEY (`permission_id`)
        REFERENCES `ichips`.`permissions` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
