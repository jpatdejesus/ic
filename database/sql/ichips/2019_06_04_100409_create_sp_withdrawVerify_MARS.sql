DROP PROCEDURE IF EXISTS `sp_withdrawVerify_MARS`;

CREATE PROCEDURE `sp_withdrawVerify_MARS`(IN `inputUserName` varchar(50), IN `inputUserType` varchar(50))
BEGIN
  	set @sDate1 = DATE_SUB( NOW(),INTERVAL 168 HOUR);
  	set @sDate2 = STR_TO_DATE('2019-06-05 00:00:00','%Y-%m-%d %H:%i:%s');
  	set @rtnValue = '';
 	set @inBlocklist = (select count(username) from withdraw_black_lists where BINARY username = BINARY inputUserName AND BINARY user_type = BINARY inputUserType) ;

  	if @inBlocklist > 0 then 
		set @rtnValue = 'fail'; 
		select @rtnValue as status, 'isBlacklisted' as isBlacklisted;
  	else 
  		#For overriding if we encounter a big problem on the specific day
  		if(@sDate1 > @sDate2) then
			set @startDate = @sDate1 ;
		else  
			set @startDate = @sDate2 ;
		end if ;

		set @countofRecord = (select count(0) from datarepo.cashflow where BINARY username = BINARY inputUserName and  created_at >= @startDate ) ;
		if @countofRecord < 2 then 
			set @rtnValue = 'pass';
			select @rtnValue as status;
		else 
			set @initBalance = (
					SELECT CASE
						    WHEN trans_type = 'MODIFY_ADD' OR trans_type = 'DEPOSIT' OR trans_type = 'PAYOUT' THEN balance-trans_amount
						    WHEN trans_type = 'MODIFY_DEDUCT' OR trans_type = 'WITHDRAW' OR trans_type = 'BET' THEN balance+trans_amount
						END from datarepo.cashflow
						where BINARY username = BINARY inputUserName  and  created_at >= @startDate  order by id  limit 1
				);
			set @SumOfCashFlowAdd = (select COALESCE(sum(trans_amount), 0) from datarepo.cashflow where BINARY username = BINARY inputUserName  and  created_at >= @startDate AND trans_type IN ('MODIFY_ADD', 'DEPOSIT', 'PAYOUT'));
			set @SumOfCashFlowDeduct = (select COALESCE(sum(trans_amount), 0) from datarepo.cashflow where BINARY username = BINARY inputUserName  and  created_at >= @startDate AND trans_type IN ('MODIFY_DEDUCT', 'WITHDRAW', 'BET'));
			set @SumOfCashFlow = @SumOfCashFlowAdd - @SumOfCashFlowDeduct;
			set @EndingBalance = (select balance from datarepo.cashflow where BINARY username = BINARY inputUserName  and  created_at >= @startDate  order by id desc limit 1);

			if @SumOfCashFlow is NULL then 
				set @SumOfCashFlow = 0 ;
			end if;

			set @rtnValue = if( ((@initBalance + @SumOfCashFlow - @EndingBalance) = 0), 'pass', 'fail' ); 

			select @rtnValue as status, 
				@initBalance as initBalance, 
				@SumOfCashFlow as SumOfCashFlow, 
				@EndingBalance as EndingBalance;
		end if;
  end if;
END