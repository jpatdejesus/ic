DROP PROCEDURE IF EXISTS sp_paginate_admin_whitelist_requests_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_paginate_admin_whitelist_requests_datatable`(
	IN `status` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50),
	IN `start` INT,
	IN `length` INT,
	IN `order_by` VARCHAR(50),
	IN `order_sort` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Withdrawal Verification Datatable SP'
BEGIN
DECLARE v_main_query TEXT;
	
SET @v_main_query := CONCAT('
	SELECT `wwl`.*
	FROM `ichips`.`withdraw_white_lists` AS wwl
	WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `status` <> 'ALL' AND `status` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `wwl`.`status` = ", CONCAT("'", `status`, "'"));
END IF;

SET @v_main_query := CONCAT(@v_main_query, " ORDER BY ", `order_by`, " ", `order_sort`);
SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;