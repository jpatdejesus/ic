CREATE TABLE IF NOT EXISTS `games` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `game_code` VARCHAR(32) NOT NULL COLLATE 'utf8_unicode_ci',
    `game_name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
    `game_type_id` INT(11) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `game_code` (`game_code`),
    INDEX `FK_games_game_type_id` (`game_type_id`),
    CONSTRAINT `FK_games_game_type_id` FOREIGN KEY (`game_type_id`) REFERENCES `game_types` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
