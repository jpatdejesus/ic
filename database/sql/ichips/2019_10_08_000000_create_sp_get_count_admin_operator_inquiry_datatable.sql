DROP PROCEDURE IF EXISTS sp_get_count_admin_operator_inquiry_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_admin_operator_inquiry_datatable`(
	IN `query_type` VARCHAR(10),
	IN `param` VARCHAR(200)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Operator Inquiry paginate datatable endpoint in IChips Admin Tool'
BEGIN
DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) AS row_count
    FROM `ichips`.`operators`
    WHERE 1
');

IF `query_type` = 'ID' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `UserId` = ", CONCAT("'", `param`, "'"));
ELSEIF `query_type` = 'NAME' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `LoginName` LIKE ", CONCAT("'%", `param`, "%'"));
    SET @v_main_query := CONCAT(@v_main_query, " OR `RealName` LIKE ", CONCAT("'%", `param`, "%'"));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;