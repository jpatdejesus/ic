CREATE TABLE IF NOT EXISTS `game_values` (
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `values` text COLLATE utf8_unicode_ci NOT NULL,
    `game_type` int(11) NOT NULL,
    `result_id` int(11) NOT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,
    `deleted_at` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;