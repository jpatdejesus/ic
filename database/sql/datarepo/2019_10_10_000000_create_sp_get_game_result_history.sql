DROP PROCEDURE IF EXISTS sp_get_game_result_history;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_game_result_history`(
	IN `game_name` VARCHAR(255),
	IN `result_id` VARCHAR(255),
	IN `start` INT,
	IN `length` INT,
	IN `date_start` VARCHAR(20),
	IN `date_end` VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Get game results history for IChips API'
BEGIN
DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
    SELECT *
    FROM `datarepo`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `game_name`, "_game_results`"));
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");

IF `result_id` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `result_id` = ", CONCAT("'", `result_id`, "'" ));
END IF;

IF `date_start` <> '' AND `date_end` <> '' THEN
    SET @v_main_query := CONCAT(@v_main_query, " AND `created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
    SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
END IF;

SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;