CREATE TABLE IF NOT EXISTS `transfers` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `transfer_id` VARCHAR(50) DEFAULT NULL,
    `op_transfer_id` VARCHAR(100) UNIQUE NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `currency_code` CHAR(3) NOT NULL,
    `actions` VARCHAR(10) DEFAULT 'IN',
    `amount` DECIMAL(50, 5) DEFAULT '0.00000',
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `transfers_op_transfer_id` (`op_transfer_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
