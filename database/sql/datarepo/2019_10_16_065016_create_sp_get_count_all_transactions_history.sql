DROP PROCEDURE IF EXISTS sp_get_count_all_transactions_history;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_all_transactions_history`(
	IN `user_type` VARCHAR(50),
	IN `game_code` VARCHAR(50),
	IN `bet_code` VARCHAR(50),
	IN `shoe_hand_number` VARCHAR(50),
	IN `table_number` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) as row_count
    FROM `datarepo`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `game_code`, "_transactions`"), " AS at");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");
SET @v_main_query := CONCAT(@v_main_query, " AND `at`.`user_type` = ", CONCAT("'", `user_type`, "'"));

IF `bet_code` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `at`.`bet_code` = ", CONCAT("'", `bet_code`, "'"));
END IF;

IF `shoe_hand_number` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `at`.`shoehandnumber` = ", CONCAT("'", `shoe_hand_number`, "'"));
END IF;

IF `table_number` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `at`.`tablenumber` = ", CONCAT("'", `table_number`, "'"));
END IF;

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `at`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
	SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;