CREATE TABLE `tw_landlord_game_results` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'serial number',
	`game_type` INT(4) NOT NULL DEFAULT '0' COMMENT 'game type',
	`game_id` INT(4) NOT NULL DEFAULT '0' COMMENT 'game id',
	`room_id` INT(8) NOT NULL DEFAULT '0' COMMENT 'room id',
	`room_number` VARCHAR(4) NOT NULL DEFAULT '0' COMMENT 'room number',
	`result_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT 'game round id [date6]+[roomid6]+[roomnumber4]+[round number4]',
	`ante` INT(8) NOT NULL DEFAULT '0' COMMENT 'bet limit(min) of this room',
	`balance_limit` INT(8) NOT NULL DEFAULT '0' COMMENT 'min balance for enter this room',
	`game_result` TEXT NULL COMMENT 'game result',
	`status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'status : 0-pending(unfinish) 1-finish , 2-fail , 3-cancel ',
	`start_date` DATETIME NOT NULL COMMENT 'start time of this game round',
	`settle_date` DATETIME NULL DEFAULT NULL COMMENT 'settle time',
	`close_date` DATETIME NULL DEFAULT NULL COMMENT 'time of stop bet',
	`copy_order_time` DATETIME NULL DEFAULT NULL COMMENT 'copy time',
	`platform_code` VARCHAR(20) NOT NULL DEFAULT 'MARS' COMMENT 'operator id',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
