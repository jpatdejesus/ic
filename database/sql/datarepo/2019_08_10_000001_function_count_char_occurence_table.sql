DROP FUNCTION IF EXISTS sum_of_list;

DELIMITER $$
CREATE FUNCTION count_char_occurence(c varchar(191), s TEXT) RETURNS int(11)
    NO SQL
    DETERMINISTIC
BEGIN
    DECLARE res INT DEFAULT 0;
        SELECT LENGTH(s) - LENGTH(REPLACE( s, c, '')) into res;
        RETURN res;
END $$
DELIMITER ;


