ALTER TABLE `{GAME_CODE}_transactions`
DROP COLUMN `user_id`,
DROP COLUMN `shoehand_id`,
DROP COLUMN `game_result`,
DROP COLUMN `table_id`,
DROP COLUMN `bet_place_id`,
ADD COLUMN `username` VARCHAR(255) NOT NULL AFTER `bet_amount`,
ADD COLUMN `bet_place` VARCHAR(255) NOT NULL AFTER `username`,
ADD COLUMN `shoehandnumber` VARCHAR(255) NOT NULL AFTER `effective_bet_amount`,
ADD COLUMN `gameset_id` INT(11) NOT NULL DEFAULT 0 AFTER `shoehandnumber`,
ADD COLUMN `gamename` VARCHAR(255) NOT NULL AFTER `gameset_id`,
ADD COLUMN `tablenumber` VARCHAR(255) NOT NULL AFTER `gamename`,
ADD COLUMN `result` VARCHAR(255) NOT NULL AFTER `result_id`,
ADD COLUMN `is_sidebet` TINYINT(2) NOT NULL DEFAULT 0 AFTER `super_six`,
ADD COLUMN `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP AFTER `created_at`
;