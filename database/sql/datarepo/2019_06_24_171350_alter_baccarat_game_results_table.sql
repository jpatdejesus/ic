ALTER TABLE `baccarat_game_results`
	ADD COLUMN `result_id` VARCHAR(50) NOT NULL AFTER `table_no`,
	ADD UNIQUE INDEX `result_id` (`result_id`);