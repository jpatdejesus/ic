ALTER TABLE `transfers`
ADD `balance` DECIMAL(50 , 5 ) NOT NULL
AFTER `amount`,
ENGINE=InnoDB
;