DROP PROCEDURE IF EXISTS sp_paginate_admin_player_inquiry_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_paginate_admin_player_inquiry_datatable`(
	IN `username` VARCHAR(199),
	IN `user_id` BIGINT,
	IN `date_start` VARCHAR(20),
	IN `date_end` VARCHAR(20),
	IN `table_alias` VARCHAR(12),
	IN `start` INT,
	IN `length` INT,
	IN `order_by` VARCHAR(50),
	IN `order_sort` VARCHAR(50)
)

LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Player Inquiry paginate datatable endpoint in IChips Admin Tool'
BEGIN
DECLARE v_main_query TEXT;
DECLARE v_count INT;

	IF `table_alias` = 'cashflow' THEN
		SET @v_main_query := CONCAT('
			SELECT
				`cf`.`id`,
				`cf`.`game_code`,
				`cf`.`trans_type`,
				`cf`.`betting_code`,
				`cf`.`trans_amount`,
				`cf`.`balance`,
				`cf`.`created_at`,
				`cf`.`updated_at`
			FROM `datarepo`.`cashflow` AS cf
			JOIN `ichips`.`users` AS u ON u.username = cf.username
			WHERE 1
		');

		IF `username` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`username` = ", CONCAT("'", `username`, "'"));
		END IF;

		IF `user_id` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`id` = ", CONCAT("'", `user_id`, "'"));
		END IF;

		IF `date_start` <> '' AND `date_end` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
			SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
		END IF;

	ELSEIF `table_alias` = 'cashflow_php' THEN
		SET @v_main_query := CONCAT('
			SELECT
				`cf`.`id`,
				`cf`.`game_code`,
				`cf`.`trans_type`,
				`cf`.`betting_code`,
				`cf`.`trans_amount`,
				`cf`.`balance`,
				`cf`.`created_at`,
				`cf`.`updated_at`,
			FROM `datarepo`.`cashflow_php` AS cf
			JOIN `ichips`.`users_php` AS u ON u.UserName = cf.username
			WHERE 1
		');

		IF `username` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`username` = ", CONCAT("'", `username`, "'"));
		END IF;

		IF `user_id` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`userid` = ", CONCAT("'", `user_id`, "'"));
		END IF;

		IF `date_start` <> '' AND `date_end` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
			SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
		END IF;
	END IF;

	SET @v_main_query := CONCAT(@v_main_query, " ORDER BY ", `order_by`, " ", `order_sort`);
	SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

	SET @qfinal := @v_main_query;
	PREPARE stmt FROM @qfinal;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;