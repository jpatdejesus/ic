DROP PROCEDURE IF EXISTS sp_get_count_tw_admin_player_transaction_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_tw_admin_player_transaction_datatable`(
	IN `username` VARCHAR(255),
	IN `user_id` BIGINT,
	IN `game_name` VARCHAR(255),
    IN `game_name_alias` VARCHAR(255),
	IN `is_in_array` TINYINT(1),
	IN `bet_code` VARCHAR(50),
	IN `user_type` VARCHAR(255),
	IN `date_start` VARCHAR(20),
	IN `date_end` VARCHAR(20)
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Player Transaction get count datatable endpoint in IChips Admin Tool'
BEGIN
DECLARE v_main_query TEXT;
DECLARE v_count INT;

SET @v_main_query := CONCAT('
	SELECT COUNT(*) AS row_count
	FROM `datarepo`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `game_name`, "_transactions`"), " AS t ");
SET @v_main_query := CONCAT(@v_main_query, " JOIN `ichips`.`users` AS u ON u.username = t.username ");
SET @v_main_query := CONCAT(@v_main_query, " JOIN `ichips`.`currencies` AS c ON c.id = u.currency_id ");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");
SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`user_type` = ", CONCAT("'", `user_type`, "'"));

IF `username` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`username` = ", CONCAT("'", `username`, "'"));
END IF;

IF `user_id` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`id` = ", CONCAT("'", `user_id`, "'"));
END IF;

IF `bet_code` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`bet_code` = ", CONCAT("'", `bet_code`, "'"));
END IF;

IF `is_in_array` = 1 AND `game_name_alias` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`gamename` REGEXP ", CONCAT("'", `game_name_alias`, "'"));
ELSE
	SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`gamename` LIKE ", CONCAT("'%", `game_name_alias`, "%'"));
END IF;

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
	SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;