CREATE TABLE IF NOT EXISTS `moneywheel_game_results` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `result` double NOT NULL UNIQUE,
  `shoehandnumber` VARCHAR(50) NOT NULL,
  `shoe_date` datetime NOT NULL,
  `table_no` VARCHAR(50) NOT NULL,
  `values` TEXT NULL DEFAULT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;