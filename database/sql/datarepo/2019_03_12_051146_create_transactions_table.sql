CREATE TABLE IF NOT EXISTS `{GAME_CODE}_transactions` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `bet_code` VARCHAR(50) NOT NULL UNIQUE,
    `bet_amount` DECIMAL(50, 5) NOT NULL,
    `user_id` BIGINT(20) NOT NULL,
    `bet_place_id` INT(11) NOT NULL,
    `effective_bet_amount` DECIMAL(50, 5) NOT NULL,
    `shoehand_id` INT(11) NOT NULL,
    `table_id` INT(11) NOT NULL,
    `balance` DECIMAL(50, 5) NOT NULL,
    `win_loss` DECIMAL(50, 5) NOT NULL,
    `result_id` INT(11) NOT NULL,
    `game_result` LONGTEXT NULL DEFAULT NULL,
    `bet_date` DATE NOT NULL,
    `super_six` TINYINT(2) NOT NULL DEFAULT 0,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;