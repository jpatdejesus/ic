CREATE TABLE IF NOT EXISTS `game_result` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `result` double NOT NULL,
  `shoehandnumber` double NOT NULL,
  `shoe_date` datetime NOT NULL,
  `table_no` int(11) NOT NULL,
  `game_code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `values` TEXT NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;