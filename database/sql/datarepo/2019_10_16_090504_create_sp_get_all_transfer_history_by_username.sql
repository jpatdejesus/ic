DROP PROCEDURE IF EXISTS sp_get_all_transfer_history_by_username;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_all_transfer_history_by_username`(
	IN `username` VARCHAR(50),
	IN `table_name` VARCHAR(50),
	IN `start` INT,
	IN `length` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;

SET @v_main_query := CONCAT('
    SELECT 
	 	id,
		transfer_id,
		op_transfer_id,
		username,
		currency_code as currency,
		actions,
		amount,
		balance,
		created_at
	 FROM `datarepo`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `table_name`, "`"), " AS t");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");
SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`username` = ", CONCAT("'", `username`, "'"));
SET @v_main_query := CONCAT(@v_main_query, " LIMIT ", `length`, " OFFSET ", `start`);

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;