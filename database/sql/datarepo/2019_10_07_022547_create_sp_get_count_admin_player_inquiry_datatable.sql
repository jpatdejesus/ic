DROP PROCEDURE IF EXISTS sp_get_count_admin_player_inquiry_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_admin_player_inquiry_datatable`(
	IN `username` VARCHAR(199),
	IN `user_id` BIGINT,
	IN `date_start` VARCHAR(20),
	IN `date_end` VARCHAR(20),
	IN `table_alias` VARCHAR(12)
)

LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT 'Player Inquiry count datatable endpoint in IChips Admin Tool'
BEGIN
DECLARE v_main_query TEXT;
DECLARE v_count INT;

	IF `table_alias` = 'cashflow' THEN
		SET @v_main_query := CONCAT('
			SELECT COUNT(*) AS row_count
			FROM `datarepo`.`cashflow` AS cf
			JOIN `ichips`.`users` AS u ON u.username = cf.username
			WHERE 1
		');

		IF `username` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`username` = ", CONCAT("'", `username`, "'"));
		END IF;

		IF `user_id` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`id` = ", CONCAT("'", `user_id`, "'"));
		END IF;

		IF `date_start` <> '' AND `date_end` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
			SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
		END IF;

	ELSEIF `table_alias` = 'cashflow_php' THEN
		SET @v_main_query := CONCAT('
			SELECT COUNT(*) AS row_count
			FROM `datarepo`.`cashflow_php` AS cf
			JOIN `ichips`.`users_php` AS u ON u.UserName = cf.username
			WHERE 1
		');

		IF `username` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`username` = ", CONCAT("'", `username`, "'"));
		END IF;

		IF `user_id` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `u`.`userid` = ", CONCAT("'", `user_id`, "'"));
		END IF;

		IF `date_start` <> '' AND `date_end` <> '' THEN
			SET @v_main_query := CONCAT(@v_main_query, " AND `cf`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "'" ));
			SET @v_main_query := CONCAT(@v_main_query, " AND ", CONCAT("'", `date_end`, "'" ));
		END IF;
	END IF;

	SET @qfinal := @v_main_query;
	PREPARE stmt FROM @qfinal;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;