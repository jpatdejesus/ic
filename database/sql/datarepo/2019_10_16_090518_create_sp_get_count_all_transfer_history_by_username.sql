DROP PROCEDURE IF EXISTS sp_get_count_all_transfer_history_by_username;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_all_transfer_history_by_username`(
	IN `username` VARCHAR(50),
	IN `table_name` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;

SET @v_main_query := CONCAT('
    SELECT COUNT(*) as row_count
	 FROM `datarepo`.
');

SET @v_main_query := CONCAT(@v_main_query, CONCAT("`", `table_name`, "`"), " AS t");
SET @v_main_query := CONCAT(@v_main_query, " WHERE 1 ");
SET @v_main_query := CONCAT(@v_main_query, " AND `t`.`username` = ", CONCAT("'", `username`, "'"));

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;