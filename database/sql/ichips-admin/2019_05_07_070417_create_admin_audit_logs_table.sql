CREATE TABLE IF NOT EXISTS `admin_audit_logs` (
	`id` BIGINT(20) AUTO_INCREMENT,
	`email` VARCHAR(191) NOT NULL,
	`role` INT(10) NULL,
	`event` VARCHAR(50) NULL,
	`old_values` VARCHAR(50) NULL,
	`new_values` VARCHAR(50) NULL,
	`ip_address` VARCHAR(50) NULL,
	`platform` VARCHAR(255) NULL,
	`remarks` VARCHAR(50) NULL,
	`updated_at` TIMESTAMP NULL,
	`created_at` TIMESTAMP NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
