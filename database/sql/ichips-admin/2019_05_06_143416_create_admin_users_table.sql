CREATE TABLE IF NOT EXISTS `admin_users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`last_name` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`email` VARCHAR(191) NOT NULL COLLATE 'utf8_unicode_ci',
	`password` VARCHAR(191) NOT NULL COLLATE 'utf8_unicode_ci',
	`nickname` VARCHAR(50) NOT NULL COLLATE 'utf8_unicode_ci',
	`profile_picture` VARCHAR(191) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`status` ENUM('ENABLED','DISABLED','SUSPENDED') NOT NULL DEFAULT 'ENABLED' COLLATE 'utf8_unicode_ci',
	`login_attempts` TINYINT(3) NOT NULL DEFAULT '0',
	`last_login_ip` VARCHAR(15) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`last_login_time` TIMESTAMP NULL DEFAULT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`deleted_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `EMAIL` (`email`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
