ALTER TABLE `admin_update_balance_requests` DROP INDEX `approved_by`, ADD INDEX `updated_by` (`updated_by`) USING BTREE;
