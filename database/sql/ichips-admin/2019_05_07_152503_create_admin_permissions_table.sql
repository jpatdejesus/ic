CREATE TABLE IF NOT EXISTS `admin_permissions` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR (255) UNIQUE NOT NULL,
    `code` VARCHAR (255) UNIQUE NOT NULL,
    `description` VARCHAR (255) NOT NULL,
    `parent_id` INT(11) NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_admin_permissions_parent_id` (`parent_id`),
    CONSTRAINT `FK_admin_permissions_parent_id` FOREIGN KEY (`parent_id`)
        REFERENCES `ichips-admin`.`admin_permissions` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
