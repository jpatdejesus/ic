DROP PROCEDURE IF EXISTS sp_get_count_admin_audit_trails_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_admin_audit_trails_datatable`(
	IN `email` VARCHAR(50),
	IN `role_group` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;
	
SET @v_main_query := CONCAT('
	SELECT COUNT(*) AS row_count
	FROM `ichips-admin`.`admin_audit_logs` AS aal
	LEFT JOIN `ichips-admin`.`admin_roles` AS ar ON aal.role = ar.id
	WHERE 1
');

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aal`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `email` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aal`.`email` = ", CONCAT("'", `email`, "'"));
END IF;

IF `role_group` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `ar`.`role_group` = ", CONCAT("'", `role_group`, "'"));
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;