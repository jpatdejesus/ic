CREATE TABLE IF NOT EXISTS `balance_adjustment_types` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `type_name` VARCHAR(50) NOT NULL,
    `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;


INSERT INTO `balance_adjustment_types` (`type_name`) VALUES ('balance_adjustment');
INSERT INTO `balance_adjustment_types` (`type_name`) VALUES ('promo');