CREATE TABLE IF NOT EXISTS `admin_role_has_permissions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`permission_id` INT(11) NOT NULL,
	`role_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `UNIQUE` (`permission_id`, `role_id`),
	INDEX `FK_admin_role_has_permission_role_id` (`role_id`),
	INDEX `FK_admin_role_has_permission_permission_id` (`permission_id`),
	CONSTRAINT `FK_admin_role_has_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `admin_permissions` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK_admin_role_has_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
