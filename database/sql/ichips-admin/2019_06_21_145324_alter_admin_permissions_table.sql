ALTER TABLE `admin_permissions`
	ADD COLUMN `linked_route_id` INT(11) NULL DEFAULT NULL AFTER `parent_id`,
	ADD CONSTRAINT `FK_admin_permissions_linked_route_id` FOREIGN KEY (`linked_route_id`) REFERENCES `admin_permissions` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
