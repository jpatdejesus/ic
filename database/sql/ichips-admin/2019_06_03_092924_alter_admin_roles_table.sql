ALTER TABLE admin_roles
	CHANGE COLUMN is_default is_default TINYINT(1) NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci' AFTER name;
ALTER TABLE admin_roles
	DROP INDEX `name`,
	ADD UNIQUE INDEX `name` (`name`);
