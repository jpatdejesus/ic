CREATE TABLE IF NOT EXISTS `admin_roles` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`is_default` ENUM('1','0') NOT NULL COLLATE 'utf8_unicode_ci',
	`role_group` ENUM('SUPER_ADMIN','ADMIN','AUDIT') NOT NULL COLLATE 'utf8_unicode_ci',
	`master_role_id` INT(11) NULL DEFAULT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`, `role_group`),
	INDEX `FK_roles_master_role_id` (`master_role_id`),
	CONSTRAINT `FK_roles_master_role_id` FOREIGN KEY (`master_role_id`) REFERENCES `admin_roles` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
