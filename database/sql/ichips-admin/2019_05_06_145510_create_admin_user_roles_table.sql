CREATE TABLE IF NOT EXISTS `admin_user_roles` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`role_id` INT(10) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `admin_user_roles_user_id_unique` (`user_id`),
	INDEX `FK_user_roles_role_id` (`role_id`),
	CONSTRAINT `FK_admin_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `admin_users` (`id`),
	CONSTRAINT `FK_admin_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `admin_roles` (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
