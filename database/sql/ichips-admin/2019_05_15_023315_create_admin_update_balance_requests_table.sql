CREATE TABLE IF NOT EXISTS `admin_update_balance_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `amount` decimal(50,5) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `type` enum('ADD','DEDUCT') NOT NULL,
  `status` enum('PENDING','APPROVED','REJECTED') NOT NULL DEFAULT 'PENDING',
  `created_by` int(11) NOT NULL,
  `approved_by` int(11) NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `approved_by` (`approved_by`),
  CONSTRAINT `admin_update_balance_requests_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `admin_users` (`id`),
  CONSTRAINT `admin_update_balance_requests_ibfk_2` FOREIGN KEY (`approved_by`) REFERENCES `admin_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;