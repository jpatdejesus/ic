CREATE TABLE IF NOT EXISTS `admin_access_tokens` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `token` VARCHAR(191) NOT NULL COLLATE 'utf8_unicode_ci',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expires_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_admin_access_tokens_user_id` (`user_id`),
  CONSTRAINT `FK_admin_access_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `admin_users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;
