DROP PROCEDURE IF EXISTS sp_get_count_admin_balance_requests_datatable;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_get_count_admin_balance_requests_datatable`(
	IN `user_type` VARCHAR(50),
	IN `status` VARCHAR(50),
	IN `date_start` VARCHAR(50),
	IN `date_end` VARCHAR(50),
	IN `role_name` VARCHAR(50),
	IN `created_by` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
NO SQL
SQL SECURITY DEFINER
COMMENT ''
BEGIN
DECLARE v_main_query TEXT;
	
SET @v_main_query := CONCAT('
	SELECT COUNT(*) AS row_count
	FROM `ichips-admin`.`admin_update_balance_requests` AS aubr
	LEFT JOIN `ichips-admin`.`admin_users` AS au ON aubr.updated_by = au.id
	LEFT JOIN `ichips-admin`.`admin_users` AS creator ON aubr.created_by = creator.id
	WHERE 1
');

IF `user_type` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aubr`.`user_type` = ", CONCAT("'", `user_type`, "'"));
END IF;

IF `status` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aubr`.`status` = ", CONCAT("'", `status`, "'"));
END IF;

IF `date_start` <> '' AND `date_end` <> '' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aubr`.`created_at` BETWEEN ", CONCAT("'", `date_start`, "' AND "), CONCAT("'", `date_end`, "'"));
END IF;

IF `role_name` <> 'super_admin' THEN
	SET @v_main_query := CONCAT(@v_main_query, " AND `aubr`.`created_by` = ", `created_by`);
END IF;

SET @qfinal := @v_main_query;
PREPARE stmt FROM @qfinal; 
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

END $$
DELIMITER ;