<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\IChipsLogsRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class AdminIChipsLogsDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        return (new IChipsLogsRepository())->getIChipsLogs($this->getDataTableParams());
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'id',
            'api_consumer_username',
            'username',
            'request_content',
            'response_content',
            'remark',
            'created_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'username' => 'username',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id'=>'id',
            'api_consumer_username'=>'api_consumer_username',
            'username'=>'username',
            'request_content'=>'request_content',
            'response_content'=>'response_content',
            'remark'=>'remark',
            'created_at' => 'created_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'id',
        ];
    }
}
