<?php

namespace App\DataTables\v1\Admin;

use App\Services\Datatable\DataTableService as BaseDataTable;
use App\Repositories\v1\AdminUserRepository;

class AdminUserDataTable extends BaseDataTable
{
    /**
     * @return mixed
     */
    public function query()
    {
        // return (new AdminUserRepository())->getAllUsers($this->tableColumns());
        return (new AdminUserRepository())->getAllUsers($this->getDataTableParams());
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'admin_users.id',
            'admin_users.first_name',
            'admin_users.last_name',
            'admin_users.nickname',
            'admin_users.email',
            'admin_users.status',
            'admin_users.created_at',
            'admin_users.updated_at',
            'admin_roles.name AS role ',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'email' => 'admin_users.email',
            'status' => 'admin_users.status',
            'role_id' => 'admin_roles.id',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'admin_users.id',
            'first_name' => 'admin_users.first_name',
            'last_name' => 'admin_users.last_name',
            'nickname' => 'admin_users.nickname',
            'email' => 'admin_users.email',
            'status' => 'admin_users.status',
            'created_at' => 'admin_users.created_at',
            'updated_at' => 'admin_users.updated_at',
            'role' => 'admin_roles.name',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'admin_users.id',
            'admin_users.first_name',
            'admin_users.last_name',
            'admin_users.nickname',
            'admin_users.email',
            'admin_users.status',
            'admin_roles.id',
            'admin_roles.name',
        ];
    }
}
