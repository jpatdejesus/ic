<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;
use App\Services\RoleManagement\RoleManagementService;

class WithdrawWhiteListRequestsDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        $params = $this->getDataTableParams();
        $params->status = request()->status;
        $params->date_from = request()->date_from;
        $params->date_to = request()->date_to;

        return (new WithdrawBlackAndWhiteListRepository())->getWhiteListRequests($params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        $return = [];
        $roleGroup = (new RoleManagementService())->getRoleGroups(true);
        $userData = request()->user_data;

        switch ($userData->role_group) {
            case $roleGroup['super_admin']:
                $return = [
                    'withdraw_white_lists.id',
                    'withdraw_white_lists.created_at',
                    'withdraw_white_lists.whitelisted_by',
                    'withdraw_white_lists.status',
                    'withdraw_white_lists.updated_by',
                    'withdraw_white_lists.date_approved',
                    'withdraw_white_lists.username',
                ];
                break;
            case $roleGroup['admin']:
                $return = [
                    'withdraw_white_lists.id',
                    'withdraw_white_lists.created_at',
                    'withdraw_white_lists.whitelisted_by',
                    'withdraw_white_lists.status',
                    'withdraw_white_lists.date_approved',
                    'withdraw_white_lists.username',
                ];
                break;
        }

        return $return;
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'status' => 'withdraw_white_lists.status',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        $return = [];
        $roleGroup = (new RoleManagementService())->getRoleGroups(true);
        $userData = request()->user_data;

        switch ($userData->role_group) {
            case $roleGroup['super_admin']:
                $return = [
                    'id' => 'withdraw_white_lists.id',
                    'created_at' => 'withdraw_white_lists.created_at',
                    'whitelisted_by' => 'withdraw_white_lists.whitelisted_by',
                    'status' => 'withdraw_white_lists.status',
                    'updated_by' => 'withdraw_white_lists.updated_by',
                    'date_approved' => 'withdraw_white_lists.date_approved',
                    'username' => 'withdraw_white_lists.username',
                ];
                break;
            case $roleGroup['admin']:
                $return = [
                    'id' => 'withdraw_white_lists.id',
                    'created_at' => 'withdraw_white_lists.created_at',
                    'whitelisted_by' => 'withdraw_white_lists.whitelisted_by',
                    'status' => 'withdraw_white_lists.status',
                    'date_approved' => 'withdraw_white_lists.date_approved',
                    'username' => 'withdraw_white_lists.username',
                ];
                break;
        }

        return $return;
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'withdraw_white_lists.id',
        ];
    }
}
