<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\AuditTrailsRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class AuditTrailsDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        // return (new AuditTrailsRepository())->getAuditTrails($this->tableColumns());
        return (new AuditTrailsRepository())->getAuditTrails($this->getDataTableParams());
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'admin_audit_logs.id',
            'admin_audit_logs.email',
            'admin_roles.name',
            'admin_audit_logs.event',
            'admin_audit_logs.old_values',
            'admin_audit_logs.new_values',
            'admin_audit_logs.ip_address',
            'admin_audit_logs.platform',
            'admin_audit_logs.remarks',
            'admin_audit_logs.created_at',
            'admin_audit_logs.updated_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'email' => 'admin_users.email',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'admin_audit_logs.id',
            'email' => 'admin_audit_logs.email',
            'name' => 'admin_roles.name',
            'event' => 'admin_audit_logs.event',
            'old_values' => 'admin_audit_logs.old_values',
            'new_values' => 'admin_audit_logs.new_values',
            'ip_address' => 'admin_audit_logs.ip_address',
            'platform' => 'admin_audit_logs.platform',
            'remarks' => 'admin_audit_logs.remarks',
            'created_at' => 'admin_audit_logs.created_at',
            'updated_at' => 'admin_audit_logs.updated_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'admin_audit_logs.id',
        ];
    }
}
