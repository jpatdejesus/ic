<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class WithdrawWhiteListDataTable extends BaseDataTable
{
    /**
     * @return mixed|void
     */
    public function query()
    {
        $params = $this->getDataTableParams();
        $params->remark = request()->remark;
        $params->username = request()->username;
        $params->date_from = request()->date_from;
        $params->date_to = request()->date_to;

        return (new WithdrawBlackAndWhiteListRepository())->getWhiteList($params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'withdraw_white_lists.id',
            'withdraw_white_lists.username',
            'withdraw_white_lists.expire_time',
            'withdraw_white_lists.whitelisted_by',
            'withdraw_white_lists.remark',
            'withdraw_white_lists.created_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'username' => 'withdraw_white_lists.username',
            'remark' => 'withdraw_white_lists.remark',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'withdraw_white_lists.id',
            'username' => 'withdraw_white_lists.username',
            'expire_time' => 'withdraw_white_lists.expire_time',
            'whitelisted_by' => 'withdraw_white_lists.whitelisted_by',
            'remark' => 'withdraw_white_lists.remark',
            'created_at' => 'withdraw_white_lists.created_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'withdraw_white_lists.id',
        ];
    }
}
