<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class WithdrawBlackListDataTable extends BaseDataTable
{
    /**
     * @return mixed|void
     */
    public function query()
    {
        $params = $this->getDataTableParams();
        $params->username = request()->username;
        $params->remark = request()->remark;
        $params->date_from = request()->date_from;
        $params->date_to = request()->date_to;

        return (new WithdrawBlackAndWhiteListRepository())->getBlackList($params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'withdraw_black_lists.id',
            'withdraw_black_lists.username',
            'withdraw_black_lists.remark',
            'withdraw_black_lists.created_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'username' => 'withdraw_black_lists.username',
            'remark' => 'withdraw_black_lists.remark',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'withdraw_black_lists.id',
            'username' => 'withdraw_black_lists.username',
            'remark' => 'withdraw_black_lists.remark',
            'created_at' => 'withdraw_black_lists.created_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'withdraw_black_lists.id',
        ];
    }
}
