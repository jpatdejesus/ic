<?php

namespace App\DataTables\v1\Admin;

use App\Services\UserType\EntityService;
use App\Services\Datatable\DataTableService as BaseDataTable;
use App\Repositories\v1\PlayerTransactionRepository;

class PlayerTransactionDataTable extends BaseDataTable
{
    /**
     * @var mixed
     */
    protected $userFilter;

    /**
     * @var mixed
     */
    protected $request;

    /**
     * @return array|mixed
     * @throws \App\Exceptions\RepositoryInternalException
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function query()
    {
        $params = $this->getDataTableParams();
        $params->userFilter = $this->getUserFilter();
        $params->date_start = $this->getRequest()->date_start;
        $params->date_end = $this->getRequest()->date_end;
        $params->user_type = $this->getRequest()->user_type;
        $params->bet_code = $this->getRequest()->bet_code;
        $params->game_name = $this->getRequest()->game_name;

        return (new PlayerTransactionRepository())->getAllAdminTransaction($params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'id',
            'bet_code',
            'bet_amount',
            'username',
            'bet_place',
            'effective_bet_amount',
            'shoehandnumber',
            'gamename',
            'tablenumber',
            'balance',
            'win_loss',
            'result',
            'bet_date',
            'currency_code',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'id',
            'username'=> 'username',
            'gamename' => 'gamename',
            'bet_code' => 'bet_code',
            'user_type' => 'user_type',
            'created_at' => 'created_at',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'id',
            'bet_date' => 'bet_date',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'id'
        ];
    }

    /**
     * @return mixed
     */
    public function getUserFilter()
    {
        return $this->userFilter;
    }

    /**
     * @param mixed $username
     *
     * @return self
     */
    public function setUserFilter($username)
    {
        $this->userFilter = $username;

        return $this;
    }
}
