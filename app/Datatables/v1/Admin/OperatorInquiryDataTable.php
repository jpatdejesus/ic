<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\OperatorRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class OperatorInquiryDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        $this->setQueryCondition(false);
        $this->setDefaultSortValue('LastLogin');

        return (new OperatorRepository())->getOperator($this->getDataTableParams());
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'UserId',
            'LoginName',
            'RealName',
            'StateId',
            'LastLogin',
            'LastModify',
            'LastModifiedBy',
            'Path',
            'Layer',
            'VideoBetLimitIDs',
            'RouletteBetLimitIDs',
            'limits',
            'Prefix',
            'platformbetcontrol'
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            ['operators.LoginName', parent::CONDITION_LIKE],
            ['operators.RealName', parent::CONDITION_LIKE],
            ['operators.UserId', parent::CONDITION_EQUAL]
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'LastLogin' =>'operators.LastLogin',
            'LastModify' =>'operators.LastModify'
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'operators.UserId',
            'operators.LoginName',
            'operators.RealName',
        ];
    }
}
