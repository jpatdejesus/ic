<?php

namespace App\DataTables\v1\Admin;

use App\Services\UserType\EntityService;
use App\Services\Datatable\DataTableService as BaseDataTable;

class PlayerCashflowDataTable extends BaseDataTable
{
    /**
     * @var mixed
     */
    protected $username;

    /**
     * @var mixed
     */
    protected $request;

    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryNotFoundException
     * @throws \App\Exceptions\ValidationResponseException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function query()
    {
        $userEntityService = new EntityService('users');
        $userRepository = $userEntityService->getRepository();

        $cashflowEntityService = new EntityService('cashflow');
        $cashFlowRepository = $cashflowEntityService->getRepository();

        $params = $this->getDataTableParams();
        $params->table_alias = $cashFlowRepository->getTable();

        return $cashFlowRepository->getPlayerCashflowByAdminInquiry($this->getUsername(), $this->getRequest(), $params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'id',
            'game_code',
            'trans_type',
            'betting_code',
            'trans_amount',
            'balance',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [

        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'id',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'id'
        ];
    }
}
