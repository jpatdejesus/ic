<?php

namespace App\DataTables\v1\Admin;

use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;
use App\Services\Datatable\DataTableService as BaseDataTable;

class WithdrawRejectLogsDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        $params = $this->getDataTableParams();
        $params->username = request()->username;
        $params->transfer_id = request()->transfer_id;
        $params->remark = request()->remark;
        $params->date_from = request()->date_from;
        $params->date_to = request()->date_to;

        return (new WithdrawBlackAndWhiteListRepository())->getRejectedLogs($params);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'withdraw_reject_logs.id',
            'withdraw_reject_logs.username',
            'withdraw_reject_logs.user_type',
            'withdraw_reject_logs.action',
            'withdraw_reject_logs.remark',
            'withdraw_reject_logs.transfer_id',
            'withdraw_reject_logs.created_at',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            'username' => 'withdraw_reject_logs.username',
            'transfer_id' => 'withdraw_reject_logs.transfer_id',
            'remarks' => 'withdraw_reject_logs.remarks',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'withdraw_reject_logs.id',
            'username' => 'withdraw_reject_logs.username',
            'user_type' => 'withdraw_reject_logs.user_type',
            'action' => 'withdraw_reject_logs.action',
            'remark' => 'withdraw_reject_logs.remark',
            'transfer_id' => 'withdraw_reject_logs.transfer_id',
            'created_at' => 'withdraw_reject_logs.created_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'withdraw_reject_logs.id',
        ];
    }
}
