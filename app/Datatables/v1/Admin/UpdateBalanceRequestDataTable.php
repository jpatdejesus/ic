<?php

namespace App\DataTables\v1\Admin;

use DB;
use App\Models\v1\AdminUpdateBalanceRequest;
use App\Services\Datatable\DataTableService as BaseDataTable;
use App\Repositories\v1\AdminUpdateBalanceRequestRepository;

class UpdateBalanceRequestDataTable extends BaseDataTable
{
    /**
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function query()
    {
        return (new AdminUpdateBalanceRequestRepository())->getAllUpdateBalanceRequests($this->getDataTableParams());

        // $query = AdminUpdateBalanceRequest::with('adminUser')
        //     ->select(
        //         'admin_update_balance_requests.*',
        //         DB::raw("CONCAT(admin_users.first_name, ' ',admin_users.last_name) as updated_by"),
        //         DB::raw("CONCAT(creator.first_name, ' ',creator.last_name) as created_by")
        //     )
        //     ->join(
        //         DB::raw(
        //             "(" . $this->getRawUserTables() . ") users
        //             ON
        //                 users.username = admin_update_balance_requests.username
        //                 AND
        //                 users.flag = admin_update_balance_requests.user_type"
        //         ),
        //         function ($query) {
        //         }
        //     )->leftJoin('admin_users', 'admin_update_balance_requests.updated_by', '=', 'admin_users.id')
        //     ->leftJoin('admin_users as creator', 'admin_update_balance_requests.created_by', '=', 'creator.id');

        // if ($this->getRequest()->date_start && $this->getRequest()->date_end) {
        //     $query->whereBetween('admin_update_balance_requests.created_at', [$this->getRequest()->date_start, $this->getRequest()->date_end]);
        // }

        // if ($this->getRequest()->userData()->role_name === 'super_admin') {
        //     // @todo to change to constant!
        //     //if super_admin
        //     return $query;
        // }

        // //if admin/cs
        // return $query->whereCreatedBy($this->getRequest()->userData()->id);
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            'admin_update_balance_requests.id',
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            // 'username' => 'admin_update_balance_requests.username',
            'status' => 'admin_update_balance_requests.status',
            'user_type' => 'admin_update_balance_requests.user_type',
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            'id' => 'admin_update_balance_requests.id',
            'created_at' => 'admin_update_balance_requests.created_at',
            'updated_at' => 'admin_update_balance_requests.updated_at',
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            'admin_update_balance_requests.id',
        ];
    }

    /**
     * Generate raw users table for union all query
     */
    private function getRawUserTables()
    {
        $ichipsDatabase = config('database.connections.ichips.database');
        $queryArray = [];

        foreach (config('ichips.user_types') as $user_type => $user_type_entity_handler) {
            $model = app($user_type_entity_handler['users']['model']);

            array_push($queryArray, "SELECT ('" . $user_type . "' COLLATE utf8_unicode_ci) as flag,
                    " . $model->getTable() . "." . $model->getFieldName('balance') . " as balance,
                    " . $model->getTable() . "." . $model->getFieldName('username') . " as username
                    FROM " . $ichipsDatabase . "." . $model->getTable() . " AS " . $model->getTable());
        }

        return implode(' UNION ALL ', $queryArray);
    }
}
