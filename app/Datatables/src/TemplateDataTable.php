<?php

namespace App\DataTables;

use App\Services\Datatable\DataTableService as BaseDataTable;

class TemplateDataTable extends BaseDataTable
{
    /**
     * @return mixed|void
     */
    public function query()
    {
        /**
         * Return should be an instance of:
         *
         * Illuminate\Database\Query\Builder
         * Illuminate\Database\Schema\Builder
         * Illuminate\Database\Eloquent\Builder
         */
    }

    /**
     * @return array
     */
    public function tableColumns(): array
    {
        return [
            //'admin_users.first_name',
            //'roles.role_id'
        ];
    }

    /**
     * @return array
     */
    public function searchColumns(): array
    {
        return [
            //'first_name' => 'admin_users.first_name',
            //'role_id' => 'roles.role_id'
        ];
    }

    /**
     * @return array
     */
    public function sortColumns(): array
    {
        return [
            //'first_name' => 'admin_users.first_name',
            //'role_id' => 'roles.role_id'
        ];
    }

    /**
     * @return array
     */
    public function groupByColumns(): array
    {
        return [
            //'first_name' => 'admin_users.first_name',
            //'role_id' => 'roles.role_id'
        ];
    }
}
