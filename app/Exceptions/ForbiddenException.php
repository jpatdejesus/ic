<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ForbiddenException extends BaseException
{
    public function report()
    {
        $this->setMessage(self::ERROR_MESSAGE);
        $message = $this->getMessage();

        Log::error("[PERMISSION_REQUEST_EXCEPTION] - {$message}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_FORBIDDEN;
    }

    /**
     * @return array
     */
    public function setParams()
    {
        return [
            'message' => $this->getMessage(),
        ];
    }
}
