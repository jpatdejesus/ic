<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class RepositoryBadRequestException extends BaseException
{
    public function report()
    {
        $message = $this->getMessage();

        $this->setMessage($this->getMessage());

        Log::error("[REPOSITORY_REQUEST_EXCEPTION] - {$message}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }

    /**
     * @return array
     * @throws BaseException
     */
    public function setParams()
    {
        return [
            'error' => $this->getMessage()
        ];
    }
}
