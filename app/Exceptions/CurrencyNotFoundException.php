<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class CurrencyNotFoundException extends BaseException
{
    public function report()
    {
        $message = $this->getMessage();

        $this->setMessage($this->getMessage());

        Log::error("[REPOSITORY_NOT_FOUND_EXCEPTION] - {$message}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_NOT_FOUND;
    }

    /**
     * @return array
     * @throws BaseException
     */
    public function setParams()
    {
        return [
            'error' => $this->getMessage()
        ];
    }
}
