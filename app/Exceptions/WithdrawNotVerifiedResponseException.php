<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WithdrawNotVerifiedResponseException extends BaseException
{
    const MESSAGE = 'Withdraw Verification Error';

    public function report()
    {
        Log::error("[WITHDRAW_NOT_VERIFIED_RESPONSE_EXCEPTION] - {". self::MESSAGE ."}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }

    /**
     * @return array
     * @throws BaseException
     */
    public function setParams()
    {
        return [
            'message' => self::MESSAGE,
        ];
    }
}
