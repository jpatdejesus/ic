<?php

namespace App\Http\v1\Admin\Requests;

use App\Http\v1\Requests\FormRequest;

class AdminGetRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_group' => 'required|role_group'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'role_group' => "The selected :attribute is invalid.",
        ];
    }
}
