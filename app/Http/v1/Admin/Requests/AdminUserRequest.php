<?php

namespace App\Http\v1\Admin\Requests;

use App\Exceptions\ValidationResponseException;
use App\Http\v1\Requests\FormRequest;

class AdminUserRequest extends FormRequest
{
    const ROUTE_UPDATE_PROFILE = 'Update User Profile';
    const ROUTE_UPDATE_USER = 'Update User Details';

    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        $return = [];
        switch ($this->method()) {
            case parent::POST_METHOD:
                $return = [
                    'first_name' => 'required|admin_name_regex|min:1|max:50|regex:/^\S+(?: \S+)*$/i',
                    'last_name' => 'required|admin_name_regex|min:1|max:50|regex:/^\S+(?: \S+)*$/i',
                    'nickname' => 'required|nickname_regex|min:1|max:50|regex:/^\S+(?: \S+)*$/i',
                    'email' => 'required|email|max:50|unique:ichips-admin.admin_users,email',
                    'password' => 'required|min:8|max:50|password_regex',
                    'status' => "required|user_status",
                    'role' => "required|exists:ichips-admin.admin_roles,id"
                ];
                break;
            case parent::PATCH_METHOD:
                break;
            case parent::GET_METHOD:
                break;
            case parent::PUT_METHOD:
                $route = request()->route();
                $routeName = $route[1]['name'];
                $return = [];
                $params = $this->all();

                if ($routeName === self::ROUTE_UPDATE_PROFILE) {
                    $return = [
                        'nickname' => 'required|nickname_regex|min:1|max:50',
                        'current_password' => 'current_password|required_with:password',
                    ];

                    if (isset($params['current_password']) && $params['current_password'] !== '' && $params['current_password'] !== null) {
                        $return['password'] = 'required|min:8|max:50|password_regex|confirmed|new_password';
                        $return['password_confirmation'] = 'required|required_with:password|same:password';
                    } else {
                        if (isset($params['password'])) {
                            $return['password'] = 'min:8|max:50|password_regex|confirmed|new_password|required_with:password';
                        }
                        if (isset($params['password_confirmation'])) {
                            $return['password_confirmation'] = 'required_with:password|same:password';
                        }
                    }
                } elseif ($routeName === self::ROUTE_UPDATE_USER) {
                    $return = [
                        'first_name' => 'required|admin_name_regex|min:1|max:50',
                        'last_name' => 'required|admin_name_regex|min:1|max:50',
                        'nickname' => 'required|nickname_regex|min:1|max:50',
                        'password' => 'min:8|max:50|password_regex',
                        'status' => "required|user_status",
                        'role' => "required|exists:ichips-admin.admin_roles,id"
                    ];
                }

                break;
        }
        return $return;
    }

    protected function messages()
    {
        return [
            '*.current_password' => 'The :attribute field is invalid.',
            'password.new_password' => 'The :attribute field cannot be the same with your current password.',
            '*.*_regex' => 'The :attribute field is invalid.',
            '*.user_*' => 'The selected :attribute is invalid.',
        ];
    }
}
