<?php

namespace App\Http\v1\Admin\Requests;

use App\Http\v1\Requests\FormRequest;

class AdminAddRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if (request()->request_app_type === 'ADMIN') {
            $rules = [
                'role_name' => 'required|min:1|max:50|role_name_regex|unique:ichips-admin.admin_roles,name|regex:/^\S+(?: \S+)*$/i',
                'role_group' => 'required|role_group',
            ];
        } elseif (request()->request_app_type === 'CLIENT') {
            $rules = [
                'role_name' => 'required|min:1|max:50|role_name_regex|unique:ichips.roles,name',
                'role_group' => 'required|role_group',
            ];
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'role_group' => "The selected :attribute is invalid.",
            'role_name.role_name_regex' => "The :attribute is invalid.",
        ];
    }
}
