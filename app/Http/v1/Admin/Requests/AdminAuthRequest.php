<?php

namespace App\Http\v1\Admin\Requests;

use App\Http\v1\Requests\FormRequest;

class AdminAuthRequest extends FormRequest
{
    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation Rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ":attribute is required"
        ];
    }
}
