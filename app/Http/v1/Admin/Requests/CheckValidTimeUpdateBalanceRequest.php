<?php

namespace App\Http\v1\Admin\Requests;

use App\Http\v1\Requests\FormRequest;
use App\Models\v1\AdminUpdateBalanceRequest;
use Illuminate\Validation\Rule;

class CheckValidTimeUpdateBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type' => 'required|exists:user_types,user_type_name',
            'type' => 'required|' . Rule::in([AdminUpdateBalanceRequest::TYPE_ADD, AdminUpdateBalanceRequest::TYPE_DEDUCT]),
        ];
    }
}
