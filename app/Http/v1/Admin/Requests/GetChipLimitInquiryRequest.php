<?php

namespace App\Http\v1\Admin\Requests;

use App\Models\v1\User;
use App\Http\v1\Requests\FormRequest;

class GetChipLimitInquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'param' => 'required',
            'user_type' => 'required|exists:user_types,user_type_name',
        ];
    }

    protected function messages()
    {
        return [
            'param.required' => 'The player id/username field is required.',
        ];
    }
}
