<?php

namespace App\Http\v1\Admin\Requests;

use App\Models\v1\User;
use App\Http\v1\Requests\FormRequest;
use App\Services\UserType\EntityService;
use App\Repositories\v1\CurrencyRepository;
use App\Models\v1\AdminUpdateBalanceRequest;

class UpdateBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     * @throws \App\Exceptions\RepositoryNotFoundException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function rules()
    {
        $return = [
            'reason' => 'required|max:255',
            'type' => 'required|in:' . AdminUpdateBalanceRequest::TYPE_ADD . ',' . AdminUpdateBalanceRequest::TYPE_DEDUCT,
            'user_type' => 'required|exists:user_types,user_type_name',
            'category' => 'required',
        ];

        switch ($this->type) {
            case AdminUpdateBalanceRequest::TYPE_ADD:
                $userEntityService = new EntityService('users');
                $userRepository = $userEntityService->getRepository();
                $user = $userRepository->getUserInformation(request()->route('username'))->first();
                $currencyRepository = app(CurrencyRepository::class);

                $max_amount = $currencyRepository->getRMBConversion($user->currency, config('ichips.update_balance_max_rmb_conversion'));

                $return['amount'] = 'required|money|numeric|min:' . AdminUpdateBalanceRequest::MIN_ADD_AMOUNT . '|max:' . $max_amount;

                break;
            case AdminUpdateBalanceRequest::TYPE_DEDUCT:
                $return['amount'] = 'required|money|numeric|min:' . AdminUpdateBalanceRequest::MIN_DEDUCT_AMOUNT .'|max:' . AdminUpdateBalanceRequest::MAX_DEDUCT_AMOUNT;
                //|not_in:0
                break;
        }

        return $return;
    }

    public function messages()
    {
        return [
            'amount.money' => 'The :attribute should have :decimal decimal/s at the most.',
            // 'amount.not_in' => 'The :attribute should not be 0.',
        ];
    }
}
