<?php

namespace App\Http\v1\Admin\Requests;

use App\Http\v1\Requests\FormRequest;

class GetBalanceUpdateRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_start' => 'date',
            'date_end' => 'required_with:date_start|date|after_or_equal:date_start',
        ];
    }
}
