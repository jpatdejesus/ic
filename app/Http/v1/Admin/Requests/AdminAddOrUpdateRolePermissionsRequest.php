<?php

namespace App\Http\v1\Admin\Requests;

use Illuminate\Validation\Rule;
use App\Http\v1\Requests\FormRequest;
use App\Services\RoleManagement\RoleManagementService;

class AdminAddOrUpdateRolePermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required|integer',
            'permissions' => 'required|array'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'permissions.required' => 'Requires at least one permission.',
        ];
    }
}
