<?php

namespace App\Http\v1\Admin\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\DataTables\v1\Admin\WithdrawBlackListDataTable;
use App\Http\v1\Admin\Requests\AddUserToWhitelistRequest;
use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;

class WithdrawalBlackListController extends BaseController
{

    private $withdrawBlackAndWhiteList;

    /**
     * UserController constructor.
     *
     * @param WithdrawBlackAndWhiteListRepository $withdrawBlackAndWhiteListRepository
     */
    public function __construct(
        WithdrawBlackAndWhiteListRepository $withdrawBlackAndWhiteListRepository
    ) {
        $this->withdrawBlackAndWhiteList = $withdrawBlackAndWhiteListRepository;
    }

    /**
     * @param $withdrawBlackListDataTable
     * @return mixed
     * @throws ValidationResponseException
     */
    public function getBlackList(WithdrawBlackListDataTable $withdrawBlackListDataTable)
    {
        // return $this->withdrawBlackAndWhiteList->getBlackList();
        return $withdrawBlackListDataTable->build();
    }

    /**
     * @param $addUserToWhitelist
     * @return mixed
     * @throws ValidationResponseException
     */
    public function addUserToWhitelist(AddUserToWhitelistRequest $addUserToWhitelist)
    {
        $response = $this->withdrawBlackAndWhiteList->addUserToWhitelist($addUserToWhitelist->all());

        if ($response) {
            return $this->sendResponseOk($response, $this->withdrawBlackAndWhiteList::ADD_TO_WHITELIST_SUCCESS);
        }

        return $this->sendUnauthorized([], $this->withdrawBlackAndWhiteList::ADD_TO_WHITELIST_FAILED);
    }
}
