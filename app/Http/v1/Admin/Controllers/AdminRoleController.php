<?php

namespace App\Http\v1\Admin\Controllers;

use Laravel\Lumen\Http\Request;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\Http\v1\Admin\Requests\AdminAddRoleRequest;
use App\Http\v1\Admin\Requests\AdminGetRoleRequest;
use App\Repositories\v1\RoleAndPermissionRepository;
use App\Services\RoleManagement\RoleManagementService;
use App\Http\v1\Admin\Requests\AdminUpdateRoleRequest;
use App\Http\v1\Admin\Requests\AdminDeleteRoleRequest;
use App\Http\v1\Admin\Requests\AdminDuplicateRoleAndPermissionsRequest;

class AdminRoleController extends BaseController
{
    private $roleAndPermissionRepository;
    private $roleManagementService;

    /**
     * UserController constructor.
     *
     * @param RoleAndPermissionRepository $roleAndPermissionRepository
     * @param RoleManagementService $roleManagementService
     */
    public function __construct(
        RoleAndPermissionRepository $roleAndPermissionRepository,
        RoleManagementService $roleManagementService
    ) {
        $this->roleAndPermissionRepository = $roleAndPermissionRepository;
        $this->roleManagementService = $roleManagementService;
    }

    /**
     * Get Roles
     *
     * @param AdminGetRoleRequest $getRoleRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getRoles(AdminGetRoleRequest $getRoleRequest)
    {
        $params = $getRoleRequest->only(['role_group']);
        $response = $this->roleAndPermissionRepository->getRoles($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * Get Role Groups
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getRoleGroups(Request $request)
    {
        $response = $this->roleAndPermissionRepository->getRoleGroups();

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * add Roles
     *
     * @param AdminAddRoleRequest $addRoleRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function addRole(AdminAddRoleRequest $addRoleRequest)
    {
        $params = $addRoleRequest->only(['role_name', 'role_group']);
        $response = $this->roleAndPermissionRepository->addRole($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['ADD_NEW_ROLE_ERROR']);
    }

    /**
     * Delete Role
     *
     * @param AdminDeleteRoleRequest $addRoleRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function deleteRole(AdminDeleteRoleRequest $addRoleRequest)
    {
        $params = $addRoleRequest->only(['role_id', 'role_group']);
        $response = $this->roleAndPermissionRepository->deleteRole($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['ADD_NEW_ROLE_ERROR']);
    }

    /**
     * Update Role
     *
     * @param AdminUpdateRoleRequest $updateRoleRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function updateRole(AdminUpdateRoleRequest $updateRoleRequest)
    {
        $params = $updateRoleRequest->only(['role_id', 'role_name', 'role_group']);
        $response = $this->roleAndPermissionRepository->updateRole($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['ADD_NEW_ROLE_ERROR']);
    }

    /**
     * Duplicate Role and Permissions
     *
     * @param AdminDuplicateRoleAndPermissionsRequest $duplicateRoleAndPermissionsRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function duplicateRoleAndPermissions(AdminDuplicateRoleAndPermissionsRequest $duplicateRoleAndPermissionsRequest)
    {
        $params = $duplicateRoleAndPermissionsRequest->only(['role_id', 'role_name', 'role_group']);
        $response = $this->roleAndPermissionRepository->duplicateRoleAndPermissions($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['DUPLICATE_ROLE_ERROR']);
    }
}
