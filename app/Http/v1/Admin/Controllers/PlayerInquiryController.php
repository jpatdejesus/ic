<?php

namespace App\Http\v1\Admin\Controllers;

use App\DataTables\v1\Admin\PlayerCashflowDataTable;
use App\Services\UserType\EntityService;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\RepositoryNotFoundException;
use App\Http\v1\Admin\Requests\AdminPlayerInquiryRequest;
use App\Http\v1\Client\Requests\UserTypeMiddlewareRequest;

class PlayerInquiryController extends BaseController
{
    const MESSAGE_GET_SUCCESS = 'Success!';

    const MESSAGE_PLAYER_NOT_FOUND = 'Player not found.';

    /**
     * @var mixed
     */
    protected $userRepository;

    /**
     * @var mixed
     */
    protected $cashFlowRepository;

    /**
     * PlayerInquiryController constructor.
     *
     * @param UserTypeMiddlewareRequest $userTypeMiddlewareRequest
     * @throws RepositoryNotFoundException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function __construct(UserTypeMiddlewareRequest $userTypeMiddlewareRequest)
    {
        $userEntityService = new EntityService('users');
        $this->userRepository = $userEntityService->getRepository();

        $cashflowEntityService = new EntityService('cashflow');
        $this->cashFlowRepository = $cashflowEntityService->getRepository();
    }

    /**
     * @param PlayerCashflowDataTable $playerCashflowDataTable
     * @param AdminPlayerInquiryRequest $playerInquiryRequest
     * @param $userFilter
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryNotFoundException
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function __invoke(PlayerCashflowDataTable $playerCashflowDataTable, AdminPlayerInquiryRequest $playerInquiryRequest, $userFilter)
    {
        $user = $this->userRepository->getPlayerInfoByAdminInquiry($userFilter)->first();
        if ($user) {
            $playerCashflowDataTable->setUsername($user->username);
            $playerCashflowDataTable->setRequest($playerInquiryRequest);

            $user->cashflow = $playerCashflowDataTable->build();
            $user->id = (int) $user->id;
            $user->balance = number_format($user->balance, 5, '.', '');

            return $this->sendResponseOk($user->toArray(), self::MESSAGE_GET_SUCCESS);
        }
        throw new RepositoryNotFoundException(self::MESSAGE_PLAYER_NOT_FOUND);
    }
}
