<?php

namespace App\Http\v1\Admin\Controllers;

use App\DataTables\v1\Admin\WithdrawRejectLogsDataTable;
use Illuminate\Http\Request;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\DataTables\v1\Admin\WithdrawWhiteListDataTable;
use App\Repositories\v1\WithdrawBlackAndWhiteListRepository;
use App\DataTables\v1\Admin\WithdrawWhiteListRequestsDataTable;

class WithdrawalWhiteListController extends BaseController
{

    private $withdrawBlackAndWhiteList;

    /**
     * UserController constructor.
     *
     * @param WithdrawBlackAndWhiteListRepository $withdrawBlackAndWhiteListRepository
     */
    public function __construct(
        WithdrawBlackAndWhiteListRepository $withdrawBlackAndWhiteListRepository
    ) {
        $this->withdrawBlackAndWhiteList = $withdrawBlackAndWhiteListRepository;
    }

    /**
     * @param $withdrawWhiteListDataTable
     * @return mixed
     * @throws ValidationResponseException
     */
    public function getWhiteList(WithdrawWhiteListDataTable $withdrawWhiteListDataTable)
    {
        // return $this->withdrawBlackAndWhiteList->getWhiteList();
        return $withdrawWhiteListDataTable->build();
    }

    /**
     * @param $withdrawWhiteListRequestsDataTable
     * @return mixed
     * @throws ValidationResponseException
     */
    public function getWhiteListRequests(WithdrawWhiteListRequestsDataTable $withdrawWhiteListRequestsDataTable)
    {
        // return $this->withdrawBlackAndWhiteList->getWhiteListRequest();
        return $withdrawWhiteListRequestsDataTable->build();
    }

    /**
     * @param $withdrawRejectLogsDataTable
     * @return mixed
     * @throws ValidationResponseException
     */
    public function getRejectedLogs(WithdrawRejectLogsDataTable $withdrawRejectLogsDataTable)
    {
        // return $this->withdrawBlackAndWhiteList->getRejectedLogs();
        return $withdrawRejectLogsDataTable->build();
    }

    /**
     * @param $request
     * @param $request_id
     * @param $status
     * @return mixed
     */
    public function approveOrRejectWhiteListRequest(Request $request, $request_id, $status)
    {
        $response = $this->withdrawBlackAndWhiteList->approveOrRejectWhiteListRequest($request_id, $status);

        if ($response) {
            return $this->sendResponseOk($response, $this->withdrawBlackAndWhiteList::UPDATE_WHITELIST_REQUEST_SUCCESS);
        }

        return $this->sendUnauthorized([], $this->withdrawBlackAndWhiteList::UPDATE_WHITELIST_REQUEST_FAILED);
    }
}
