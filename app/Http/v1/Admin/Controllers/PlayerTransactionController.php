<?php

namespace App\Http\v1\Admin\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\DataTables\v1\Admin\PlayerTransactionDataTable;
use App\Http\v1\Admin\Requests\AdminPlayerTransactionRequest;
use App\Repositories\v1\PlayerTransactionRepository;

class PlayerTransactionController extends BaseController
{
     /**
     * @var mixed
     */
    protected $playerTransactionRepository;

    public function __construct(PlayerTransactionRepository $playerTransactionRepository)
    {
        $this->playerTransactionRepository = $playerTransactionRepository;
    }

    /**
     * @param PlayerTransactionDataTable $playerTransactionDatatable
     * @param AdminPlayerTransactionRequest $adminPLayerTransactionRequest
     * @param null $userFilter
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function __invoke(PlayerTransactionDataTable $playerTransactionDatatable, AdminPLayerTransactionRequest $adminPLayerTransactionRequest, $userFilter = null)
    {
        $playerTransactionDatatable->setUserFilter($userFilter);

        return $playerTransactionDatatable->build();

        //return $this->playerTransactionRepository->getAllAdminTransaction($userFilter);
    }
}
