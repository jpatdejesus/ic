<?php

namespace App\Http\v1\Admin\Controllers;

use App\Models\v1\UserType;
use App\Http\v1\Controllers\BaseController;

class UserTypeController extends BaseController
{
    /**
     * @return mixed
     */
    public function __invoke()
    {
        return UserType::select('user_type_name')->get();
    }
}
