<?php

namespace App\Http\v1\Admin\Controllers;

use App\DataTables\v1\Admin\OperatorInquiryDataTable;
use App\Http\v1\Controllers\BaseController;
use App\Repositories\v1\OperatorRepository;

class OperatorInquiryController extends BaseController
{
    /**
     * @param OperatorInquiryDataTable $operatorInquiryDataTable
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function __invoke(OperatorInquiryDataTable $operatorInquiryDataTable)
    {
        return $operatorInquiryDataTable->build();
    }
}
