<?php

namespace App\Http\v1\Admin\Controllers;

use App\Http\v1\Controllers\BaseController;

class IPWhitelistController extends BaseController
{
    /**
     * @return int
     */
    public function __invoke()
    {
        if (is_ip_whitelisted()) {
            return response('Your IP is whitelisted!');
        }

        return response()->json(
            [
                'error' => 'IP not Whitelisted',
            ],
            403
        );
    }
}
