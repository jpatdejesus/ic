<?php

namespace App\Http\v1\Admin\Controllers;

use DB;
use App\Models\v1\Log;
use Laravel\Lumen\Http\Request;
use App\Http\v1\Controllers\BaseController;
// use App\DataTables\v1\Admin\AuditTrailsDataTable;
use App\Repositories\v1\AuditTrailsRepository;
use App\Http\v1\Client\Requests\IChipsLogRequest;
use App\Http\v1\Admin\Requests\AuditTrailsRequest;
use App\DataTables\v1\Admin\AdminIChipsLogsDataTable;
use App\Repositories\v1\IChipsLogsRepository;
use App\DataTables\v1\Admin\AuditTrailsDataTable;

class AuditTrailsController extends BaseController
{
    /**
     * Audit Trails
     *
     * @param AuditTrailsDataTable $auditTrailsDatatable
     * @param AuditTrailsRequest $auditTrailsRequest
     * @return mixed
     */
    public function index(AuditTrailsDataTable $auditTrailsDataTable)
    {
        return $auditTrailsDataTable->build();
        // return (new AuditTrailsRepository())->getAuditTrails();
    }

    /**
     * @param Request $request
     */
    public function getAllRoutes(Request $request)
    {
        $routeList = [];
        $logTableName = DB::connection(Log::LOGS_TABLE)
            ->table('information_schema.tables')
            ->select(['table_name'])
            ->where('table_schema', config('database.connections.logs.database'))
            ->get()
            ->pluck('table_name');

        foreach ($logTableName as $key => $value) {
            array_push($routeList, substr($logTableName[$key], 0, strpos($logTableName[$key], '_log')));
        }

        return array_unique($routeList);
    }

    /**
     * @param IChipsLogRequest $ichipsRequest
     * @param AdminIChipsLogsDataTable $adminIChipsLogsDataTable
     * @return mixed
     */
    public function iChipsApiLogs(IChipsLogRequest $ichipsRequest, AdminIChipsLogsDataTable $adminIChipsLogsDataTable)
    {
        return $adminIChipsLogsDataTable->build();
        // return (new IChipsLogsRepository())->getIChipsLogs();
    }
}
