<?php

namespace App\Http\v1\Admin\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\UserType\EntityService;
use App\Http\v1\Controllers\BaseController;
use App\Models\v1\AdminUpdateBalanceRequest;
use App\Exceptions\RepositoryNotFoundException;
use App\Exceptions\ValidationResponseException;
use App\Http\v1\Admin\Requests\RejectBalanceRequest;
use App\Http\v1\Admin\Requests\UpdateBalanceRequest;
use App\Repositories\v1\BalanceAdjustmentRepository;
use App\Repositories\v1\CashFlowRepositoryInterface;
use App\Http\v1\Admin\Requests\ApproveBalanceRequest;
use App\Http\v1\Admin\Requests\GetBalanceUpdateRequests;
use App\DataTables\v1\Admin\UpdateBalanceRequestDataTable;
use App\Http\v1\Client\Requests\UserTypeMiddlewareRequest;
use App\Repositories\v1\AdminUpdateBalanceRequestRepository;
use App\Http\v1\Admin\Requests\CheckValidTimeUpdateBalanceRequest;
use Illuminate\Support\Facades\Log;
use App\Exceptions\RepositoryBadRequestException;
use App\Services\CsvService;

class BalanceController extends BaseController
{
    const MESSAGE_GET_SUCCESS = 'Success!';

    const MESSAGE_APPROVED = 'Request has been approved!';

    const MESSAGE_REJECTED = 'Request has been rejected!';
 
    const MESSAGE_PLAYER_NOT_FOUND = 'Player not found.';

    /**
     * @var mixed
     */
    protected $userRepository;

    /**
     * @var mixed
     */
    protected $cashFlowRepository;

    /**
     * @var mixed
     */
    protected $adminUpdateBalanceRequestRepository;

    /**
     * @param AdminUpdateBalanceRequestRepository $adminUpdateBalanceRequestRepository
     */
    public function __construct(AdminUpdateBalanceRequestRepository $adminUpdateBalanceRequestRepository)
    {
        $this->adminUpdateBalanceRequestRepository = $adminUpdateBalanceRequestRepository;
    }

    /**
     * @param $userFilter
     */
    public function getPlayer(UserTypeMiddlewareRequest $userTypeMiddlewareRequest, $userFilter)
    {
        $this->setEntityServices();

        if ($user = $this->userRepository->getPlayerForUpdateBalance($userFilter)->first()) {
            $user->id = (int) $user->id;
            $user->balance = number_format($user->balance, 5, '.', '');

            return $this->sendResponseOk($user->toArray(), self::MESSAGE_GET_SUCCESS);
        }

        throw new RepositoryNotFoundException(self::MESSAGE_PLAYER_NOT_FOUND);
    }

    /**
     * @param UpdateBalanceRequest $updateBalanceRequest
     * @param $username
     * @return mixed
     */
    public function updateBalanceRequest(CashFlowRepositoryInterface $cashflowRepository, UpdateBalanceRequest $updateBalanceRequest, $username)
    {
        $this->setEntityServices();
        if ($this->userRepository->isUserExist($username)) {
            $user = $this->userRepository->getUserByUsername($username);
            $data = [
                'username' => $username,
                'user_type' => $updateBalanceRequest->user_type,
                'amount' => $updateBalanceRequest->amount,
                'reason' => $updateBalanceRequest->reason,
                'trans_type' => $updateBalanceRequest->category,
                'type' => $updateBalanceRequest->type,
                'created_by' => $updateBalanceRequest->userData()->id,
            ];

            if ($updateBalanceRequest->userData()->role_name == 'super_admin') {
                //for auto approved
                $data['status'] = AdminUpdateBalanceRequest::STATUS_APPROVED;
                $data['updated_by'] = $updateBalanceRequest->userData()->id;
            }

            DB::transaction(function () use ($cashflowRepository, $updateBalanceRequest, $data, $user) {
                $balanceRequestModel = AdminUpdateBalanceRequest::create($data);

                if ($updateBalanceRequest->userData()->role_name == 'super_admin') {
                    //for auto approved
                    $userBalances = $this->userRepository->updatePlayerBalanceByAdmin($balanceRequestModel->username, $balanceRequestModel->type, $balanceRequestModel->amount);

                    $cashflowRepository->modifyByAdmin($balanceRequestModel, $user, $userBalances[0]->afterBalance);
                }
            });

            return $this->sendResponseOk([], self::MESSAGE_GET_SUCCESS);
        }

        throw new RepositoryNotFoundException(self::MESSAGE_PLAYER_NOT_FOUND);
    }

    /**
     * @param GetBalanceUpdateRequests $request
     * @param UpdateBalanceRequestDataTable $updateBalanceRequestDataTable
     * @return mixed
     */
    public function getBalanceUpdateRequests(GetBalanceUpdateRequests $request, UpdateBalanceRequestDataTable $updateBalanceRequestDataTable)
    {
        // return $this->adminUpdateBalanceRequestRepository->getAllUpdateBalanceRequests();
        return $updateBalanceRequestDataTable->build();
    }

    /**
     * @param ApproveBalanceRequest $request
     * @param $id
     * @return mixed
     */
    public function approve(ApproveBalanceRequest $request, $id)
    {
        $balanceRequestModel = AdminUpdateBalanceRequest::where('status', AdminUpdateBalanceRequest::STATUS_PENDING)->findOrFail($id);

        $balanceRequestModel = DB::transaction(function () use ($request, $balanceRequestModel) {
            $userRepository = get_user_type_repository($balanceRequestModel->user_type, 'users');
            $cashFlowRepository = get_user_type_repository($balanceRequestModel->user_type, 'cashflow');
            $user = $userRepository->getUserByUsername($balanceRequestModel->username);
            $userBalances = $userRepository->updatePlayerBalanceByAdmin($balanceRequestModel->username, $balanceRequestModel->type, $balanceRequestModel->amount);

            $cashFlowRepository->modifyByAdmin($balanceRequestModel, $user, $userBalances[0]->afterBalance);

            $balanceRequestModel->update([
                'status' => AdminUpdateBalanceRequest::STATUS_APPROVED,
                'updated_by' => $request->userData()->id,
            ]);

            return $balanceRequestModel;
        });

        return $this->sendResponseOk($balanceRequestModel->toArray(), self::MESSAGE_APPROVED);
    }

    /**
     * @param RejectBalanceRequest $request
     * @param $id
     * @return mixed
     */
    public function reject(RejectBalanceRequest $request, $id)
    {
        $balanceRequestModel = AdminUpdateBalanceRequest::where('status', AdminUpdateBalanceRequest::STATUS_PENDING)->findOrFail($id);

        $balanceRequestModel->update([
            'status' => AdminUpdateBalanceRequest::STATUS_REJECTED,
            'updated_by' => $request->userData()->id,
        ]);

        return $this->sendResponseOk($balanceRequestModel->toArray(), self::MESSAGE_REJECTED);
    }

    /**
     * Set entityt services needed
     *
     */
    private function setEntityServices()
    {
        $userEntityService = new EntityService('users');

        $this->userRepository = $userEntityService->getRepository();
    }

    /**
     * @param $updateBalanceRequest
     * @param $username
     */
    public function checkHoursIfValidToRequest(CheckValidTimeUpdateBalanceRequest $updateBalanceRequest, $username)
    {
        if ($updateBalanceRequest->type == AdminUpdateBalanceRequest::TYPE_DEDUCT) {
            return true;
        }

        $adminUpdateBalanceRequest = AdminUpdateBalanceRequest::where('user_type', $updateBalanceRequest->user_type)
            ->where('username', $username)
            ->where('type', AdminUpdateBalanceRequest::TYPE_ADD)
            ->where('created_by', $updateBalanceRequest->userData()->id)
            ->whereIn('status', [AdminUpdateBalanceRequest::STATUS_PENDING, AdminUpdateBalanceRequest::STATUS_APPROVED])
            ->latest()
            ->first();

        if ($adminUpdateBalanceRequest) {
            $createdAt = to_utc_tz($adminUpdateBalanceRequest->created_at)->format('Y-m-d H:i:s');

            $date = Carbon::parse($createdAt)->addHours(config('ichips.update_balance_buffer_hours'));
            $now = Carbon::now();
            $now = to_utc_tz($now);

            $timeDiff = $now->diffAsCarbonInterval($date);
            $formattedDate = $date->format('Y-m-d H:i:s');
            $diff = $now->diffInHours($date);

            if ($diff <= (int)config('ichips.update_balance_buffer_hours') && $updateBalanceRequest->userData()->role_name != 'super_admin') {
                throw new ValidationResponseException(
                    'Wait until ' . $formattedDate . ' before you can request again for this player, that is ' . $timeDiff . ' from now.',
                    []
                );
            }
        }

        return $this->sendResponseOk([], self::MESSAGE_GET_SUCCESS);
    }

    /**
     * @param BalanceAdjustmentRepository $balanceAdjustmentRepository
     * @return mixed
     */
    public function getAllCategory()
    {
        $data = (new BalanceAdjustmentRepository())->getBalanceCategory();
        return $this->sendResponseOk($data, self::MESSAGE_GET_SUCCESS);
    }

    /**
     * @param CashFlowRepositoryInterface $cashflowRepository
     * @param Request $request
     * @return mixed
     */
    public function batchUpdateBalance(CashFlowRepositoryInterface $cashflowRepository, Request $request)
    {
        $this->setEntityServices();
        $csvData = (new CsvService())->parseCsv($request);

        DB::beginTransaction();
        try {
            $this->adminUpdateBalanceRequestRepository->batchAdminUpdateBalanceRequest($csvData);
            $balances = $this->userRepository->batchUpdatePlayerBalanceByAdmin($csvData);
            $cashflowRepository->batchModifyByAdmin($balances);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            throw new RepositoryBadRequestException('Bad Request Found.');
        }
        DB::commit();

        return $this->sendResponseOk([], self::MESSAGE_GET_SUCCESS);
    }
}
