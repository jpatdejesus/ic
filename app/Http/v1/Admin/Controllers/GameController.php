<?php

namespace App\Http\v1\Admin\Controllers;

use App\Models\v1\Game;
use Laravel\Lumen\Http\Request;
use App\Http\v1\Controllers\BaseController;

class GameController extends BaseController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke()
    {
        return Game::select(['game_code', 'game_name'])->get();
    }
}
