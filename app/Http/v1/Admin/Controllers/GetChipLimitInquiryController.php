<?php

namespace App\Http\v1\Admin\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Http\v1\Admin\Requests\GetChipLimitInquiryRequest;
use App\Repositories\v1\UserChipLimitInquiryRepository;

class GetChipLimitInquiryController extends BaseController
{
    const MESSAGE_SUCCESS = 'Success!';

    /**
     * Get chip limit of user
     *
     * @param GetChipLimitInquiryRequest $getChipLimitInquiryRequest
     * @param UserChipLimitInquiryRepository $userChipLimitInquiryRepository
     * @return mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function __invoke(GetChipLimitInquiryRequest $getChipLimitInquiryRequest, UserChipLimitInquiryRepository $userChipLimitInquiryRepository)
    {
        return $userChipLimitInquiryRepository->getChipLimit($getChipLimitInquiryRequest);
    }
}
