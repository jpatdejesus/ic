<?php

namespace App\Http\v1\Admin\Controllers;

use App\Http\v1\Admin\Requests\AdminUserRequest;
use App\Http\v1\Admin\Requests\UploadProfilePictureRequest;
use App\Http\v1\Controllers\BaseController;
use App\Repositories\v1\AdminUserRepository;
use Illuminate\Support\Facades\DB;
use App\DataTables\v1\Admin\AdminUserDataTable;

class AdminUserController extends BaseController
{
    const MESSAGE_SUCCESS_UPLOAD = 'Profile picture successfully uploaded!';
    const MESSAGE_FAILED_UPLOAD = 'Failed to upload profile picture';

    protected $adminUserRepository;

    public function __construct(AdminUserRepository $adminUserRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
    }

    /**
     * @param AdminUserDataTable $adminUserDataTable
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function index(AdminUserDataTable $adminUserDataTable)
    {
        // return $this->adminUserRepository->getAllUsers();
        return $adminUserDataTable->build();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminUserRequest $adminUserRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function create(AdminUserRequest $adminUserRequest)
    {
        DB::beginTransaction();

        if ($response = $this->adminUserRepository->createAdminUser($adminUserRequest)) {
            DB::commit();

            $this->flagAction = true;

            return $this->sendResponseOk([], $this->adminUserRepository::CREATE_SUCCESS);
        }
        DB::rollBack();

        return $this->sendBadRequest([], $this->adminUserRepository::CREATE_FAILED);
    }

    /**
     * Show the specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        if ($response = $this->adminUserRepository->getUser($id)) {
            return $this->sendResponseOk($response->toArray(), 'Edit admin user.');
        }

        return $this->sendBadRequest([], 'User not Found!');
    }

    /**
     * Show the specified resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showProfile()
    {
        if ($response = $this->adminUserRepository->getUser()) {
            return $this->sendResponseOk($response->toArray(), 'Edit profile.');
        }

        return $this->sendBadRequest([], 'User not Found!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminUserRequest $adminUserRequest
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function update(AdminUserRequest $adminUserRequest, int $id)
    {
        DB::beginTransaction();

        if ($this->adminUserRepository->updateAdminUser($adminUserRequest, $id)) {
            DB::commit();

            return $this->sendResponseOk([], $this->adminUserRepository::UPDATE_SUCCESS);
        }
        DB::rollBack();

        return $this->sendBadRequest([], $this->adminUserRepository::UPDATE_FAILED);
    }

    /**
     * Update profile of the logged in user
     *
     * @param AdminUserRequest $adminUserRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function updateProfile(AdminUserRequest $adminUserRequest)
    {
        DB::beginTransaction();

        $response = $this->adminUserRepository->updateProfile($adminUserRequest);

        if ($response) {
            DB::commit();

            return $this->sendResponseOk([], $this->adminUserRepository::UPDATE_SUCCESS);
        }
        DB::rollBack();

        return $this->sendBadRequest([], $this->adminUserRepository::UPDATE_FAILED);
    }

    /**
     * Disable the admin user account
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RepositoryBadRequestException
     */
    public function disable(int $id)
    {
        DB::beginTransaction();

        if ($this->adminUserRepository->isDisabled($id)) {
            DB::commit();

            return $this->sendResponseOk([], $this->adminUserRepository::UPDATE_SUCCESS);
        }
        DB::rollBack();

        return $this->sendBadRequest([], $this->adminUserRepository::UPDATE_FAILED);
    }

    /**
     * Upload profile picture image
     *
     * @param UploadProfilePictureRequest $request
     * @return bool
     * @throws \App\Exceptions\RepositoryBadRequestException
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function uploadProfilePicture(UploadProfilePictureRequest $request)
    {
        if ($this->adminUserRepository->uploadAdminUserProfilePicture($request)) {
            return $this->sendResponseOk([], self::MESSAGE_SUCCESS_UPLOAD);
        }
        return $this->sendBadRequest([], self::MESSAGE_FAILED_UPLOAD);
    }

    /**
     * Get profile picture image url
     *
     * @return mixed
     */
    public function getProfilePictureUrl()
    {
        return $this->adminUserRepository->getProfilePictureUrl();
    }
}
