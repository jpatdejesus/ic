<?php

namespace App\Http\v1\Admin\Controllers;

use Laravel\Lumen\Http\Request;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\Repositories\v1\RoleAndPermissionRepository;
use App\Services\RoleManagement\RoleManagementService;
use App\Http\v1\Admin\Requests\AdminGetRoleAndPermissionsRequest;
use App\Http\v1\Admin\Requests\AdminAddOrUpdateRolePermissionsRequest;

class AdminPermissionController extends BaseController
{
    private $roleAndPermissionRepository;
    private $roleManagementService;

    /**
     * UserController constructor.
     *
     * @param RoleAndPermissionRepository $roleAndPermissionRepository
     * @param RoleManagementService $roleManagementService
     */
    public function __construct(
        RoleAndPermissionRepository $roleAndPermissionRepository,
        RoleManagementService $roleManagementService
    ) {
        $this->roleAndPermissionRepository = $roleAndPermissionRepository;
        $this->roleManagementService = $roleManagementService;
    }

    /**
     * Get Role Permissions
     *
     * @param AdminGetRoleAndPermissionsRequest $adminGetRoleAndPermissionsRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getRolePermissions(AdminGetRoleAndPermissionsRequest $adminGetRoleAndPermissionsRequest)
    {
        $params = $adminGetRoleAndPermissionsRequest->only(['role_group']);
        $response = $this->roleAndPermissionRepository->getRoleAndPermissions($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * Add or Update Role and Permissions
     *
     * @param AdminAddOrUpdateRolePermissionsRequest $adminAddOrUpdateRolePermissionsRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function addOrUpdateRolePermissions(AdminAddOrUpdateRolePermissionsRequest $adminAddOrUpdateRolePermissionsRequest)
    {
        $params = $adminAddOrUpdateRolePermissionsRequest->only(['role_id', 'permissions']);
        $response = $this->roleAndPermissionRepository->addOrUpdateRolePermissions($params);

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * Get User Role and Permissions
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getUserRoleAndPermissions(Request $request)
    {
        $response = $this->roleAndPermissionRepository->getUserRoleAndPermissions();

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * Get Roles by Access Level
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getRolesByAccessLevel(Request $request)
    {
        $response = $this->roleAndPermissionRepository->getRolesByAccessLevel();

        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }

    /**
     * Get Roles by Access Level
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getModulesAndPermissions(Request $request)
    {
        $response = $this->roleAndPermissionRepository->getGroupedPermissions();

        if (count($response)) {
            return $this->sendResponseOk($response, 'List of Modules and Permissions.');
        }

        throw new ValidationResponseException($this->roleAndPermissionRepository::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
    }
}
