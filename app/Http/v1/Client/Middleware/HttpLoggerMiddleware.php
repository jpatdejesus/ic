<?php

namespace App\Http\v1\Client\Middleware;

use Closure;
use App\Models\v1\Log;
use Illuminate\Http\Request;
use App\Services\Log\LogService;
use App\Services\Log\DataBuilder;
use Illuminate\Support\Facades\DB;
use App\Services\Log\TableNameBuilder;
use App\Services\Log\TableQueryBuilder;
use App\Services\SqlRunner\Runner as SqlRunner;

class HttpLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate(Request $request, $response)
    {
        if (!$routeName = route_info('name')) {
            return;
        }

        if ($request->app_type != strtoupper(config('ichips.app_types.client'))) {
            return;
        }

        $logTableName = TableNameBuilder::build($routeName);
        $dbConnection = DB::connection(LogService::DATABASE_CONNECTION);
        
        $createTableQuery = (new TableQueryBuilder($logTableName))->build();
        (new SqlRunner($dbConnection))->run($createTableQuery);

        $logModel = new Log();
        $logData = (new DataBuilder($request, $response))->build();
        (new LogService($logModel, $logTableName))->log($logData);
    }
}
