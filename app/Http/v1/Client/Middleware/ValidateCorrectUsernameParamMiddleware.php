<?php

namespace App\Http\v1\Client\Middleware;

use Closure;
use App\Models\v1\User;
use App\Http\v1\Client\Requests\UserTypeMiddlewareRequest;

class ValidateCorrectUsernameParamMiddleware
{
    const USER_NOT_FOUND_MESSAGE = 'User not found.';

    /**
     * @param UserTypeMiddlewareRequest $usernameMiddlewareRequest
     */
    public function __construct(UserTypeMiddlewareRequest $usernameMiddlewareRequest)
    {
        $this->usernameMiddlewareRequest = $usernameMiddlewareRequest;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $username = $request->route('username');
        $userType = $request->user_type;
        $userModel = get_user_type_model($userType, 'users');
        
        $user = $userModel
            ->where($userModel->getFieldName('username'), $username)
            ->first();

        if (!$user) {
            return response([
                'error' => self::USER_NOT_FOUND_MESSAGE,
            ], 404);
        }

        return $next($request);
    }
}
