<?php

namespace App\Http\v1\Client\Middleware;

use Closure;
use App\Models\v1\User;

class ValidateUserIdParamMiddleware
{
    const USER_NOT_FOUND_MESSAGE = 'User not found.';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->route('user_id');
        $user = User::where('id', $userId)
            ->first();

        if (!$user) {
            return response([
                'message' => self::USER_NOT_FOUND_MESSAGE
            ], 404);
        }

        return $next($request);
    }
}
