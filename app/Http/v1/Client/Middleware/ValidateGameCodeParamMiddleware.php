<?php

namespace App\Http\v1\Client\Middleware;

use App\Repositories\v1\GameRepository;
use App\Repositories\v1\GameResultRepository;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class ValidateGameCodeParamMiddleware
{
    const GAME_CODE_NOT_FOUND = 'Game code not found.';

    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory|mixed
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function handle($request, Closure $next)
    {
        $gameCode = $request->get('game_code');

        if (!$gameCode) {
            return $this->returnResponse();
        }

        if (!((new GameRepository())->findBy('game_code', $gameCode))) {
            return $this->returnResponse();
        }

        if (!((new GameResultRepository())->isTableExist($gameCode))) {
            return $this->returnResponse();
        }

        return $next($request);
    }

    private function returnResponse()
    {
        return response([
            'error' => self::GAME_CODE_NOT_FOUND,
        ], Response::HTTP_NOT_FOUND);
    }
}
