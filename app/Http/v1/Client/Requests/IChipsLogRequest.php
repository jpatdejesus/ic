<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class IChipsLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'nullable',
            'date_from' => 'date|required',
            'date_to' => 'date|required',
            'end_point' => 'required',
        ];
    }
}
