<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use App\Repositories\v1\UpdateUserPointOrExpRepository;
use App\Repositories\v1\UserRepositoryInterface;

class UpdateUserExpRequest extends FormRequest
{
    public $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $updateUserRepo = new UpdateUserPointOrExpRepository();
        $userExpRules = 'required|numeric|min:0';

        if (request('user_exp_method') === UpdateUserPointOrExpRepository::METHOD_SUBTRACT) {
            if ($user = $this->userRepository->getUserByUsername(request('username'))) {
                $userExpRules .= '|max:' . number_format($user->getField('exp'), 2, '.', '');
            }
        }

        return [
            'user_exp_method' => 'required|in:+,-',
            'user_exp_value' =>  $userExpRules
        ];
    }

    /**
     * Get the error messages for the defined validation Rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.max' => "The :attribute to be deducted is invalid."
        ];
    }
}
