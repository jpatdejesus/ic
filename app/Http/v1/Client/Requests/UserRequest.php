<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use App\Models\v1\User;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        $return = [];
        switch ($this->method()) {
            case parent::POST_METHOD: //userCreate
                $return = [
                    'currency' => 'required|exists:ichips.currencies,currency_code',
                    'username' => 'required|username_regex|unique:users,username|min:6|max:50',
                    'nickname' => "required|nickname_regex|min:1|max:50",
                    'status' => "required|user_status",
                    'user_type' => "required|exists:ichips.user_types,user_type_name",
                    'chip_limits' => "json",
                ];
                break;
            case parent::PATCH_METHOD:
                break;
            case parent::GET_METHOD:
                $return = [
                    'search' => 'required|integer|between:1,2',
                    'user_type' => "required|exists:ichips.user_types,user_type_name",
                ];
                break;
            case parent::PUT_METHOD:
                $return = [
                    'nickname' => 'required|nickname_regex|min:1|max:50',
                    'status' => [
                        'sometimes',
                        'required',
                        Rule::in(User::STATUS),
                    ],
                    'user_type' => 'required|exists:ichips.user_types,user_type_name',
                ];
                break;
        }
        return $return;
    }

    protected function messages()
    {
        return [
            '*.*_regex' => 'The :attribute field is invalid.',
            '*.user_*' => 'The selected :attribute is invalid.',
        ];
    }
}
