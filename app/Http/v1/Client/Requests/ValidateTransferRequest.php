<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class ValidateTransferRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'transfer_id' => 'required|regex:/^[a-zA-Z0-9._-]*$/',
            'transfer_type' => 'required|in:' . implode(',', ['DEPOSIT', 'WITHDRAW', 'BET', 'PAYOUT']),
            'transfer_amount' => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation Rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
