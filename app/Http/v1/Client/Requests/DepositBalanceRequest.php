<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class DepositBalanceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $transfers = get_user_type_model($this->user_type, 'transfer');

        return [
            'op_transfer_id' => 'required|unique:datarepo.' . $transfers->getTable(),
            'amount' => 'required|string|min:1|numeric',
            'user_type' => 'required',
        ];
    }
}
