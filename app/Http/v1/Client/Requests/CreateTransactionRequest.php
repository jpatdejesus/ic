<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use Illuminate\Support\Str;

class CreateTransactionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (Str::startsWith($this->game_code, 'tw_')) {
            $return = [
                "betting_code_bet" => 'required|string|max:32',
                "betting_code_payout" => 'string|max:32',
                "game_type" => 'required|numeric',
                "game_id" => 'required|numeric',
                "room_id" => 'required|numeric',
                "room_number" => 'required|string|max:4',
                "user_id" => 'required|numeric',
                "balance_after_bet" => 'required|numeric',
                "status" => 'required|numeric',
                "error_status" => 'numeric|max:10',
                "withhold" => 'required|numeric|min:0|max:999999999999999.99',
                "bet_device" => 'required|string|max:8',
                "bet_browser" => 'required|string|max:8',
                "platform_code" => 'string|max:20',
            ];
        } else {
            $return = [
                "game_code" => 'required|string|game_code', //|game_code
                "bet_place" => 'required|string|max:255',
                "shoehandnumber" => 'required|string|max:50',
                "gameset_id" => 'required|integer|max:99999999999',
                "tablenumber" => 'required|string|max:50',
                "balance" => 'required|numeric|min:0|max:999999999999999.99',
                "super_six" => 'required|boolean',
                "is_sidebet" => 'required|boolean',
            ];
        }

        $same_return = [
            "bet_amount" => 'required|numeric|min:0|max:999999999999999.99',
            "bet_code" => 'required|string|max:50|bet_code',
            "bet_date" => 'required|date',
            "effective_bet_amount" => 'required|numeric|min:0|max:999999999999999.99',
            "gamename" => 'required|string|max:255',
            "result_id" => 'required|integer|max:99999999999',
            "result" => 'required|string|max:255',
            "win_loss" => 'required|numeric|min:-999999999999999.99|max:999999999999999.99',
        ];

        $return = array_merge($same_return, $return);

        return $return;
    }

    public function messages()
    {
        return [
            'bet_code'=> "The :attribute has already been taken.",
            'game_code'=> "The selected :attribute is invalid."
        ];
    }
}
