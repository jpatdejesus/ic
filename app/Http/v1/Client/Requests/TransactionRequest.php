<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class TransactionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $return = [
            'game_code' => 'required',
            'start_date' => 'required|date_format:Y-m-d H:i:s',
            'end_date' => 'required|date_format:Y-m-d H:i:s',
        ];

        if (preg_match("~\btransactions\b~", $this->path())) {
            unset($return['game_code']);
            $return['games'] = 'required|array';
            $return['order_by'] = 'required|in:' . implode(',', ['created_at', 'gamename', 'bet_date']);
            $return['order_sort'] = 'required|in:' . implode(',', ['ASC', 'DESC']);
        }

        return $return;
    }
}
