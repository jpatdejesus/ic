<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use Illuminate\Validation\Rule;

class FilterPromotionRequest extends FormRequest
{
    const GREATER_THAN_OR_EQUAL_BET_AMOUNT = '>=';
    const LESS_THAN_OR_EQUAL_BET_AMOUNT = '<=';
    const EQUAL_BET_AMOUNT = '=';

    const DRAW_TYPE_COMBINATION_3 = 'COMBINATION_THREE_NUMBERS';
    const DRAW_TYPE_COMBINATION_2 = 'COMBINATION_TWO_NUMBERS';
    const DRAW_TYPE_GAME_RESULT_PAIR = 'PAIR';
    const DRAW_TYPE_FIX_NUMBERS = 'FIXED';
    const DRAW_TYPE_GAME_RESULT = 'GAME_RESULT';

    const DRAW_TYPE_VALID_COMBINATIONS = [
        self::DRAW_TYPE_COMBINATION_3,
        self::DRAW_TYPE_COMBINATION_2,
        self::DRAW_TYPE_GAME_RESULT_PAIR,
        self::DRAW_TYPE_FIX_NUMBERS,
        self::DRAW_TYPE_GAME_RESULT
    ];

    const BET_AMOUNT_CONDITIONS = [
        self::GREATER_THAN_OR_EQUAL_BET_AMOUNT,
        self::LESS_THAN_OR_EQUAL_BET_AMOUNT,
        self::EQUAL_BET_AMOUNT,
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'id' => 'required|integer',
            'promotion_name' => 'sometimes|string',
            'from' => 'required|before:to',
            'to' => 'required|after:from',
            'bet_amount_condition' => 'required|'. Rule::in(self::BET_AMOUNT_CONDITIONS),
            'bet_amount' => 'required|numeric',
            'draw_type' => 'required|'. Rule::in(self::DRAW_TYPE_VALID_COMBINATIONS),
            'tables' => 'required|json',
        ];

        $request = $this->request->all();
        if (isset($request['draw_type']) && $request['draw_type'] !== '') {
            $drawType = $request['draw_type'];
            switch ($drawType) {
                case self::DRAW_TYPE_COMBINATION_3:
                    $return['draw_type_value'] = 'required|array|min:3|max:3';
                    break;
                case self::DRAW_TYPE_COMBINATION_2:
                    $return['draw_type_value'] = 'required|numeric';
                    break;
                case self::DRAW_TYPE_GAME_RESULT_PAIR:
                    $return['draw_type_value'] = 'required|array';
                    break;
                case self::DRAW_TYPE_FIX_NUMBERS:
                    $return['draw_type_value'] = 'required|numeric';
                    break;
                case self::DRAW_TYPE_GAME_RESULT:
                    $return['draw_type_value'] = 'required|array';
                    break;
            }
        }

        return $return;
    }

    /**
     * Get the error messages for the defined validation Rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
