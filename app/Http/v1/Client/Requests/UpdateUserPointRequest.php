<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use App\Repositories\v1\UpdateUserPointOrExpRepository;
use App\Repositories\v1\UserRepositoryInterface;

class UpdateUserPointRequest extends FormRequest
{
    public $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $updateUserRepo = new UpdateUserPointOrExpRepository();
        $userPointsRules = 'required|numeric|min:0';

        if (request('user_points_method') === UpdateUserPointOrExpRepository::METHOD_SUBTRACT) {
            if ($user = $this->userRepository->getUserByUsername(request('username'))) {
                $userPointsRules .= '|max:' . number_format($user->getField('point'), 2, '.', '');
            }
        }
        
        return [
            'user_points_method' => 'required|in:+,-',
            'user_points_value' =>  $userPointsRules
        ];
    }

     /**
     * Get the error messages for the defined validation Rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.max' => "The :attribute to be deducted is invalid."
        ];
    }
}
