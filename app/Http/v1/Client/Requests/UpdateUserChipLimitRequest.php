<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class UpdateUserChipLimitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'game_code' => 'required|exists:ichips.games,game_code',
            'user_type' => 'required|exists:ichips.user_types,user_type_name',
            'min' => 'required|numeric|min:1|lt:max',
            'max' => 'required|numeric|max:99999999|gt:min',
        ];
    }

    protected function messages()
    {
        return [
            'min.lt' => 'The :attribute field should be less than the max field.',
            'max.gt' => 'The :attribute field should be greater than the min field.',
        ];
    }
}
