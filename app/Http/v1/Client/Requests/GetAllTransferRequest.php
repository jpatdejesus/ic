<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class GetAllTransferRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'start_date' => 'date_format:Y-m-d H:i:s',
            'end_date' => 'date_format:Y-m-d H:i:s',
        ];
    }
}
