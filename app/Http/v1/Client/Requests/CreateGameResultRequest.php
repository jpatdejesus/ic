<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;
use Illuminate\Support\Str;

class CreateGameResultRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if (Str::startsWith($this->game_code, 'tw_')) {
            $return = [
                "game_type" => 'required|numeric',
                "game_id" => 'required|numeric',
                "room_id" => 'required|numeric',
                "room_number" => 'required|string|max:4',
                "ante" => 'required|numeric',
                "balance_limit" => 'required|numeric',
                "game_result" => 'required',
                "status" => 'required|numeric',
                "start_date" => 'required|date',
                "settle_date" => 'date',
                "close_date" => 'date',
                "copy_order_time" => 'date',
                "platform_code" => 'string|max:20',
            ];
        } else {
            $return = [
                "result" => 'required|string|max:255',
                "shoehandnumber" => 'required|string|max:50',
                "shoe_date" => 'required|date',
                "table_no" => 'required|string|max:50',
                "values" => 'required|string|max:65535',
            ];
        }

        $same_return = [
            "result_id" => 'required|max:50|string|result_id',
        ];

        $return = array_merge($same_return, $return);

        return $return;
    }

    public function messages()
    {
        return [
            'result.result' => 'The :attribute field already exist.',
            'result_id.result_id' => 'The :attribute already exist.'
        ];
    }
}
