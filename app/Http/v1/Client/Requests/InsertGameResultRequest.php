<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class InsertGameResultRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "result" => 'required|numeric',
            "shoehandnumber" => 'required|string',
            "shoe_date" => 'required|date',
            "table_no" => 'required|string',
            "values" => 'required|max:1000',
        ];
    }
}
