<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class PayoutBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'amount' => 'required|numeric|min:1',
            'amount' => 'required|numeric|string', // removed min as per JR. 30-05-2019
            'game_code' => 'required|exists:games,game_code',
            'betting_code' => "required|unique:datarepo.cashflow",
            'user_type' => "required|exists:ichips.user_types,user_type_name",
        ];
    }
}
