<?php

namespace App\Http\v1\Client\Requests;

use App\Http\v1\Requests\FormRequest;

class WithdrawBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation Rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|string|numeric|min:1|max_balance',
            'op_transfer_id' => 'required',
        ];
    }

    protected function messages()
    {
        return [
            'amount.max_balance' => 'The deduct balance may not be greater than :balance.',
        ];
    }
}
