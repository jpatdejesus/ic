<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Exceptions\RepositoryInternalException;
use App\Exceptions\ValidationResponseException;
use App\Http\v1\Client\Requests\UpdateUserPointRequest;
use App\Repositories\v1\UserRepositoryInterface;

class UpdateUserPointController extends BaseController
{
    protected $updateUserPointOrExpRepository;

    const MESSAGE_UPDATE_SUCCESS = 'Success';
    const MESSAGE_UPDATE_ERROR = 'Update user point fail';

    /**
     * Update the specified resource in storage.
     * @param  UpdateUserPointRequest $request
     * @return mixed
     * @throws ValidationResponseException
     * @throws RepositoryInternalException
     */
    public function __invoke(UserRepositoryInterface $userRepository, UpdateUserPointRequest $request, $username)
    {
        if ($response = $userRepository->updatePoint($request, $username)) {
            return $this->sendResponseOk($response, self::MESSAGE_UPDATE_SUCCESS);
        }
        return $this->sendBadRequest([], self::MESSAGE_UPDATE_ERROR);
    }
}
