<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Services\UserType\EntityService;
use App\Services\HistoryService;
use App\Http\v1\Client\Requests\GetAllTransferRequest;
use App\Http\v1\Client\Requests\UserTypeMiddlewareRequest;
use App\Http\v1\Client\Requests\ValidateTransferRequest;

class TransferController extends BaseController
{
    /**
     * @var mixed
     */
    protected $transferRepository;

    /**
     * TransferController constructor.
     */
    public function __construct(UserTypeMiddlewareRequest $UserTypeMiddlewareRequest)
    {
        $entityService = new EntityService('transfer');

        $this->transferRepository = $entityService->getRepository();
    }

    /**
     * Get transfer history of user by username.
     *
     * @param Request $request
     * @param HistoryService $historyService
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getTransfer(Request $request, HistoryService $historyService, $username)
    {
        $validParams = [
            'username' => $username,
            'user_type' => $request->user_type,
        ];

        return $historyService->build($this->transferRepository->getTransferHistoryByUsername($validParams, $historyService->buildParams()));

        // return $this->transferRepository->getTransferHistoryByUsername($request->username, $historyService->buildParams());
    }

    /**
     * Get all transfer history.
     *
     * @param GetAllTransferRequest $request
     * @param HistoryService $historyService
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getAllTransfer(GetAllTransferRequest $request, HistoryService $historyService)
    {
        $validParams = [
            'transfer_code' => $request->transfer_code,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ];

        return $historyService->build($this->transferRepository->getAllTransferHistory($validParams, $historyService->buildParams()));

        //return $this->transferRepository->getAllTransferHistory($validParams, $historyService->buildParams());
    }

    /**
     * Validate transfer history.
     *
     * @return mixed
     */
    public function validateTransfer(ValidateTransferRequest $validateTransferRequest, $username)
    {
        $transfer = $this->transferRepository->validateTransferHistory($validateTransferRequest)->toArray();

        if ($transfer) {
            return $this->sendResponseOk([], 'Success.');
        }

        return response()->json([
            'error' => 'No transfer record found for the past 24 hours.'
        ], 404, ['Content-Type' => 'application/json']);
    }
}
