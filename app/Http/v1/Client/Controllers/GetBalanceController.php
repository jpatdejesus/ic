<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Repositories\v1\UserRepositoryInterface;
use Illuminate\Http\Request;

class GetBalanceController extends BaseController
{
    /**
     * Get balance by user id.
     *
     * @param UserRepositoryInterface $userRepo
     * @param Request $request
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UserRepositoryInterface $userRepo, Request $request, $username)
    {
        return $this->sendResponseOk($userRepo->getUserBalance($username), 'Success');
        // $userBalance = $userRepo->getUserBalance($username);
        // dd($userBalance->getField('balance'));
        // $userBalance = $userRepo->getUserBalance($username)->first()->toArray();
        // $userBalance = [
        //     'balance' => get_truncated_val($userBalance[$userBalance->getField('balance')]),
        // ];
        // dd($userBalance);

        // return $this->sendResponseOk($userBalance, 'Success');
    }
}
