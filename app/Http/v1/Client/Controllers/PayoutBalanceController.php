<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Http\v1\Client\Requests\PayoutBalanceRequest;
use App\Repositories\v1\UserRepositoryInterface;
use App\Repositories\v1\CashFlowRepositoryInterface;

class PayoutBalanceController extends BaseController
{

    const MESSAGE_UPDATE_SUCCESS = 'Payout Success!';

    /**
     * @var mixed
     */
    protected $cashFlowRepo;

    /**
     * Balance Controller.
     *
     * @param  $request
     * @return mixed
     */
    public function __invoke(
        UserRepositoryInterface $userRepo,
        CashFlowRepositoryInterface $cashFlowRepo,
        PayoutBalanceRequest $request,
        $username
    ) {
        $user = $userRepo->getUserByUsername($username);
        $response = $cashFlowRepo->payout($request, $user);
        
        if ($response['code'] == 0) {
            $current_balance = array(
                'current_balance'=> get_truncated_val($response['current_balance'])
            );
            return $this->sendResponseOk($current_balance, self::MESSAGE_UPDATE_SUCCESS);
        }
    }
}
