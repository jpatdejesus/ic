<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use App\Http\v1\Client\Requests\DepositBalanceRequest;
use App\Repositories\v1\UserRepositoryInterface;
use App\Repositories\v1\CashFlowRepositoryInterface;
use App\Repositories\v1\TransferRepositoryInterface;

class DepositBalanceController extends BaseController
{
    const MESSAGE_UPDATE_SUCCESS = 'Deposit Success!';

    /**
     * Deposit transaction of the given user.
     *
     * @param UserRepositoryInterface $userRepo
     * @param TransferRepositoryInterface $transferRepo
     * @param CashFlowRepositoryInterface $cashFlowRepo
     * @param DepositBalanceRequest $depositBalanceRequest
     * @param $userName
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(
        UserRepositoryInterface $userRepo,
        TransferRepositoryInterface $transferRepo,
        CashFlowRepositoryInterface $cashFlowRepo,
        DepositBalanceRequest $depositBalanceRequest,
        $userName
    ) {
        $user = $userRepo->getUserByUsername($userName);

        DB::beginTransaction();

        $response = $this->saveTransfer(
            $depositBalanceRequest,
            $user,
            $transferRepo,
            $cashFlowRepo
        );

        if ($response) {
            DB::commit();
            return $this->sendResponseOk([
                'transfer_id' => $response['transfer']->transfer_id,
                'current_balance' => $response['updatedBalance'],
            ], self::MESSAGE_UPDATE_SUCCESS);
        }

        DB::rollBack();
        $this->message = 'User Deposit Error.';
        return $this->sendJson([], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $request
     * @param $user
     * @param $transferRepo
     * @param $cashFlowRepo
     * @return array|bool
     */
    private function saveTransfer(
        $request,
        $user,
        $transferRepo,
        $cashFlowRepo
    ) {
        //user
        $userBalances = $user->spUpdateBalance('+', get_truncated_val($request->amount), $user->getField('id'));
        $updatedBalance = get_truncated_val($userBalances[0]->afterBalance);

        //transfer
        $transfer = $transferRepo->deposit($request, $user, $updatedBalance);

        //cash flow
        $isSavedToCashFlow = $cashFlowRepo->isDeposit($request, $user, $updatedBalance, $transfer);

        if ($transfer && $isSavedToCashFlow) {
            return [
                'transfer' => $transfer,
                'updatedBalance' => $updatedBalance
            ];
        }

        return false;
    }
}
