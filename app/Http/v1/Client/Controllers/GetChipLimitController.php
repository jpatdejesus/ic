<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Http\v1\Client\Requests\GetChipLimitRequest;
use App\Repositories\v1\UserChipLimitRepository;

class GetChipLimitController extends BaseController
{
    const MESSAGE_SUCCESS = 'Success!';

    /**
     * Get chip limit of user
     *
     * @param $getChipLimitRequest
     * @return mixed
     *
     */
    public function __invoke(GetChipLimitRequest $getChipLimitRequest, UserChipLimitRepository $chipLimitRepository, $username)
    {
        return $this->sendResponseOk(
            $chipLimitRepository->getChipLimit(
                $getChipLimitRequest->game_code,
                $getChipLimitRequest->user_type,
                $username
            ),
            self::MESSAGE_SUCCESS
        );
    }
}
