<?php

namespace App\Http\v1\Client\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\v1\ApiConfiguration;
use App\Models\v1\WithdrawRejectLog;
use App\Models\v1\WithdrawWhiteList;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\Repositories\v1\UserRepositoryInterface;
use App\Repositories\v1\CashFlowRepositoryInterface;
use App\Repositories\v1\TransferRepositoryInterface;
use App\Http\v1\Client\Requests\WithdrawBalanceRequest;
use App\Exceptions\WithdrawNotVerifiedResponseException;

class WithdrawBalanceController extends BaseController
{
    const MESSAGE_UPDATE_SUCCESS = 'Withdraw Success!';

    const WITHDRAW_VERIFY_STATUS_PASS = 'pass';

    const WITHDRAW_VERIFY_STATUS_FAIL = 'fail';

    /**
     * @var mixed
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $userRepo;
    /**
     * @var mixed
     */
    protected $transferRepo;
    /**
     * @var mixed
     */
    protected $cashFlowRepo;
    /**
     * @var mixed
     */
    protected $withdrawBalanceRequest;

    /**
     * @var mixed
     */
    protected $username;

    /**
     * @var mixed
     */
    protected $rejectResponseData;

    /**
     * @param $userId
     * @return mixed
     */
    public function __invoke(
        UserRepositoryInterface $userRepo,
        TransferRepositoryInterface $transferRepo,
        CashFlowRepositoryInterface $cashFlowRepo,
        WithdrawBalanceRequest $withdrawBalanceRequest,
        $username
    ) {
        $this->userRepo = $userRepo;
        $this->transferRepo = $transferRepo;
        $this->cashFlowRepo = $cashFlowRepo;
        $this->withdrawBalanceRequest = $withdrawBalanceRequest;
        $this->username = $username;
        $this->user = $this->userRepo->getUserByUsername($this->username);

        //check duplicate
        if ($this->transferRepo->isDuplicate($this->withdrawBalanceRequest->op_transfer_id)) {
            $this->saveDuplicateLogAndThrowResponse(); //save reject log and throw here
        }

        //check verify module is false or user is in white list. Allow user to withdraw
        if (!$this->isWithdrawVerificationActive() || $this->isInWhiteList()) {
            return $this->withdraw();
        }

        //check if withdraw verified
        if (!$this->isWithdrawVerified()) {
            $this->saveRejectLogAndThrowResponse();
        }

        //withdraw
        return $this->withdraw();
    }

    /**
     * @param UserRepositoryInterface $userRepo
     * @param TransferRepositoryInterface $transferRepo
     * @param CashFlowRepositoryInterface $cashFlowRepo
     * @param WithdrawBalanceRequest $withdrawBalanceRequest
     * @param $username
     * @return mixed
     */
    private function withdraw()
    {
        //handle db transaction
        $data = DB::transaction(function () {
            $user = $this->user;
            $userBalances = $user->spUpdateBalance('-', get_truncated_val($this->withdrawBalanceRequest->amount), $user->getField('id'));

            $balance = get_truncated_val($userBalances[0]->afterBalance);

            //transfers table
            $transfer = $this->transferRepo->withdraw($this->withdrawBalanceRequest, $user, $balance);

            //cash flow
            $cashflow = $this->cashFlowRepo->withdraw($this->withdrawBalanceRequest, $user, $balance, $transfer);

            //redis (paused)

            return [
                'transfer' => $transfer,
                'cashflow' => $cashflow,
                'user' => $user,
                'balance' => $balance,
            ];
        });

        return $this->sendResponseOk([
            'transfer_id' => $data['transfer']->transfer_id,
            'current_balance' => $data['balance'],
        ], self::MESSAGE_UPDATE_SUCCESS);
    }

    private function saveDuplicateLogAndThrowResponse()
    {
        $remark = 'Transfer ID already exist: ' . $this->withdrawBalanceRequest->op_transfer_id;
        $this->saveRejectLog(WithdrawRejectLog::ACTION_DUPLICATE, $remark);

        throw new ValidationResponseException(
            'Validation error!',
            ['op_transfer_id' => ['The op transfer id has already been taken.']]
        );
    }

    private function saveRejectLogAndThrowResponse()
    {
        $rejectResponseData = $this->getRejectResponseData();

        if (isset($rejectResponseData->isBlacklisted)) {
            $remark = 'User is Black listed!';
        } else {
            $remark = 'Incorrect Cashflow.' . PHP_EOL .
            'Initial Balance: ' . $rejectResponseData->initBalance . '.' . PHP_EOL .
            'Total Cashflow transaction amount: ' . $rejectResponseData->SumOfCashFlow . '.' . PHP_EOL .
            'Current balance: ' . $rejectResponseData->EndingBalance;
        }

        $this->saveRejectLog(WithdrawRejectLog::ACTION_REJECT, $remark);

        throw new WithdrawNotVerifiedResponseException();
    }

    /**
     * @param $type
     */
    private function saveRejectLog($type, $remark)
    {
        WithdrawRejectLog::insert([
            'username' => $this->username,
            'user_type' => $this->withdrawBalanceRequest->user_type,
            'action' => $type,
            'remark' => $remark,
            'transfer_id' => $this->withdrawBalanceRequest->op_transfer_id,
        ]);
    }

    private function isWithdrawVerificationActive(): bool
    {
        return ApiConfiguration::where('feature_name', ApiConfiguration::FEATURE_WITHDRAW_VERIFICATION)
            ->where('status', ApiConfiguration::STATUS_ACTIVE)
            ->exists();
    }

    private function isInWhiteList(): bool
    {
        return WithdrawWhiteList::where('username', $this->username)
            ->where('user_type', $this->withdrawBalanceRequest->user_type)
            ->where('status', '=', WithdrawWhiteList::STATUS['approved'])
            ->where('expire_time', '>=', Carbon::now())
            ->exists();
    }

    /**
     * @return mixed
     */
    private function isWithdrawVerified(): bool
    {
        $verifyWithdraw = $this->user->spVerifyWithdrawal($this->user->username, $this->withdrawBalanceRequest->user_type);

        $this->setRejectResponseData($verifyWithdraw[0]);

        return $verifyWithdraw[0]->status == self::WITHDRAW_VERIFY_STATUS_PASS ? true : false;
    }

    /**
     * @return mixed
     */
    private function getRejectResponseData()
    {
        return $this->rejectResponseData;
    }

    /**
     * @param mixed $rejectResponseData
     *
     * @return self
     */
    private function setRejectResponseData($rejectResponseData)
    {
        $this->rejectResponseData = $rejectResponseData;

        return $this;
    }
}
