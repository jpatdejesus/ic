<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Http\v1\Client\Requests\CreateGameResultRequest;
use App\Repositories\v1\GameResultRepository;
use App\Services\HistoryService;

class GameResultController extends BaseController
{
    protected $gameResultRepository;
    protected $userRepository;

    public function __construct(GameResultRepository $gameResultRepository)
    {
        $this->gameResultRepository = $gameResultRepository;
    }

    /**
     * Create new game result
     *
     * @param CreateGameResultRequest $request
     * @param string $gameCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateGameResultRequest $request)
    {
        $failMessage = 'Insert game result history fail.';
        $successMessage = 'Success';

        if ($this->gameResultRepository->createGameResult($request->game_code, $request->all())) {
            return $this->sendResponseOk([], $successMessage);
        }

        return $this->sendBadRequest([], $failMessage);
    }

    /**
     * Get game result game code.
     *
     * @param GameResultRepository $gameResultRepository
     * @param HistoryService $historyService
     * @return array
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getGameResult(GameResultRepository $gameResultRepository, HistoryService $historyService)
    {
        return $historyService->build($gameResultRepository->getGameResult(request()->query('game_code'), $historyService->buildParams()));

        //return $gameResultRepository->getGameResult(request()->query('game_code'), $historyService->buildParams());
    }
}
