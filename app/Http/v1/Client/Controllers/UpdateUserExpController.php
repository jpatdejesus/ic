<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Exceptions\RepositoryInternalException;
use App\Exceptions\ValidationResponseException;
use App\Http\v1\Client\Requests\UpdateUserExpRequest;
use App\Repositories\v1\UserRepositoryInterface;

class UpdateUserExpController extends BaseController
{
    protected $updateUserPointOrExpRepository;

    const MESSAGE_UPDATE_SUCCESS = 'Success';
    const MESSAGE_UPDATE_ERROR = 'Update user exp fail';


    /**
     * Update the specified resource in storage.
     * @param  UpdateUserExpRequest $request
     * @return mixed
     * @throws ValidationResponseException
     * @throws RepositoryInternalException
     */
    public function __invoke(UserRepositoryInterface $userRepository, UpdateUserExpRequest $request, $username)
    {
        if ($response = $userRepository->updateExp($request, $username)) {
            return $this->sendResponseOk($response, self::MESSAGE_UPDATE_SUCCESS);
        }
        
        return $this->sendBadRequest([], self::MESSAGE_UPDATE_ERROR);
    }
}
