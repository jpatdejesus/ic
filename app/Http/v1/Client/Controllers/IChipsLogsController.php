<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\DataTables\v1\Admin\AdminIChipsLogsDataTable;
use App\Http\v1\Client\Requests\IChipsLogRequest;

class IChipsLogsController extends BaseController
{
    /**
     * IChips Logs
     *
     * @param IChipsLogsRepository $auditRepo
     * @param IChipsLogRequest $ichipsRequest
     * @return mixed
     */
    public function __invoke(IChipsLogRequest $ichipsRequest, AdminIChipsLogsDataTable $adminIChipsLogsDataTable)
    {
        return $adminIChipsLogsDataTable->build();
    }
}
