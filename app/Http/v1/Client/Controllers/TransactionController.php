<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\Repositories\v1\GameRepository;
use App\Repositories\v1\Transaction\TransactionRepository;
use App\Services\HistoryService;
use App\Repositories\v1\UserRepository;
use App\Http\v1\Client\Requests\CreateTransactionRequest;
use App\Http\v1\Client\Requests\TransactionRequest;
use App\Http\v1\Client\Requests\GetAllTransactionRequest;

class TransactionController extends BaseController
{
    protected $transactionRepository;
    protected $userRepository;
    protected $gameRepository;
    protected $historyService;

    public function __construct(TransactionRepository $transactionRepository, UserRepository $userRepository, GameRepository $gameRepository, HistoryService $historyService)
    {
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
        $this->gameRepository = $gameRepository;
        $this->historyService = $historyService;
    }

    /**
     * Get transaction history by user id and game code.
     *
     * @param TransactionRequest $request
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getTransaction(
        TransactionRequest $request,
        $username
    ) {
        $validParams = [
            'username' => $username,
            'user_type' => $request->user_type,
            'game_code' => $request->game_code,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'shoe_hand_number' => $request->shoe_hand_number,
            'table_number' => $request->table_number
        ];

        if (!$request->has('game_code')) {
            $this->historyService->errors['game_code'][] = $this->historyService::PARAMETER_REQUIRED_ERROR;
        }

        if (!$this->gameRepository->validateGameCode($request->input('game_code'))) {
            $this->historyService->errors['game_code'][] = $this->historyService::PARAMETER_INVALID_ERROR;
        }

        // return $this->historyService->formatResponse($this->transactionRepository->getAllTransactionsByUsernameAndGameCode($validParams));
        // return $this->transactionRepository->getAllTransactionsByUsernameAndGameCode($validParams, $this->historyService->buildParams());
        return $this->historyService->build($this->transactionRepository->getAllTransactionsByUsernameAndGameCode($validParams, $this->historyService->buildParams()));
    }

    /**
     * Get transaction history.
     *
     * @param GetAllTransactionRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getAllTransaction(GetAllTransactionRequest $request)
    {
        $validParams = [
            'game_code' => $request->game_code,
            'user_type' => $request->user_type,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'bet_code' => $request->bet_code,
            'shoe_hand_number' => $request->shoe_hand_number,
            'table_number' => $request->table_number
        ];

        if (!$request->has('game_code')) {
            $this->historyService->errors['game_code'][] = $this->historyService::PARAMETER_REQUIRED_ERROR;
        }

        if (!$this->gameRepository->validateGameCode($request->input('game_code'))) {
            $this->historyService->errors['game_code'][] = $this->historyService::PARAMETER_INVALID_ERROR;
        }

        // return $this->transactionRepository->getAllTransactions($validParams, $this->historyService->buildParams());
        return $this->historyService->build($this->transactionRepository->getAllTransactions($validParams, $this->historyService->buildParams()));
    }

    /**
     * Create new transaction
     *
     * @param CreateTransactionRequest $request
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function create(CreateTransactionRequest $request, $username)
    {
        $failMessage = 'Insert transaction history fail.';
        $successMessage = 'Success';
        
        $additionalData['username'] = $username;
        $transactionData = array_merge($request->all(), $additionalData);

        if ($this->transactionRepository->createTransaction($request->game_code, $transactionData)) {
            return $this->sendResponseOk([], $successMessage);
        }
        
        return $this->sendBadRequest([], $failMessage);
    }

    /**
     * Get transaction history by user id and game code.
     *
     * @param TransactionRequest $request
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function getAllTableTransactions(
        TransactionRequest $request,
        $username
    ) {
        $validParams = [
            'username' => $username,
            'user_type' => $request->user_type,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'shoe_hand_number' => $request->shoe_hand_number,
            'table_number' => $request->table_number,
            'games' => $request->games,
            'order_by' => $request->order_by,
            'order_sort' => $request->order_sort,
        ];

        return $this->historyService->build($this->transactionRepository->getAllTableTransactionsByUsername($validParams, $this->historyService->buildParams()));
    }
}
