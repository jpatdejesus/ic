<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Services\UserType\EntityService;
use App\Services\HistoryService;

class ChipFlowController extends BaseController
{
    /**
     * @var mixed
     */
    protected $userRepo;

    /**
     * @var mixed
     */
    protected $cashFlowRepo;

    /**
     * [__construct ]
     */
    public function __construct()
    {
        $usersEntityService = new EntityService('users');
        $this->userRepo = $usersEntityService->getRepository();

        $cashflowEntityService = new EntityService('cashflow');
        $this->cashFlowRepo = $cashflowEntityService->getRepository();
    }

    /**
     * Get Chip Flow by username
     *
     * @param HistoryService $historyService
     * @param $username
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getChipFlow(
        HistoryService $historyService,
        $username
    ) {
        $user = $this->userRepo->getUserByUsername($username);

        return $historyService->build($this->cashFlowRepo->getChipFlow($user, $historyService->buildParams()));

        //return $this->cashFlowRepo->getChipFlow($user, $historyService->buildParams());
    }

    /**
     * Get all Chip Flow
     *
     * @param HistoryService $historyService
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getAllChipFlow(HistoryService $historyService)
    {
        return $historyService->build($this->cashFlowRepo->getAllChipFlow($historyService->buildParams()));

        //return $this->cashFlowRepo->getAllChipFlow($historyService->buildParams());
    }
}
