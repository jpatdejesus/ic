<?php

namespace App\Http\v1\Client\Controllers;

use App\Models\v1\Game;
use Illuminate\Support\Facades\DB;
use App\Http\v1\Controllers\BaseController;
use App\Exceptions\ValidationResponseException;
use App\Repositories\v1\UserRepositoryInterface;
use App\Repositories\v1\CashFlowRepositoryInterface;
use App\Http\v1\Client\Requests\BetBulkBalanceRequest;

class BetBulkBalanceController extends BaseController
{
    const MESSAGE_BET_MAX_DATA = 'The bet should not be greater than 50';
    const MESSAGE_UPDATE_SUCCESS = 'Bet Success!';
    const MESSAGE_REQUIRED_FIELDS = 'Please check required fields. Required array fields amount, game_code, and betting_code';
    const MESSAGE_BETTING_CODE_UNIQUE = 'Betting code field must be unique';
    const MESSAGE_AMOUNT_STRING = 'Amount field must be string.';
    const MESSAGE_AMOUNT_NUMERIC = 'Amount field must be a valid numeric string.';
    const MESSAGE_BET_AMOUNT = 'Bet amount is greater than user balance.';
    const MESSAGE_GAME_CODE = 'Game code is invalid';

    /**
     * @var mixed
     */
    protected $cashFlowRepo;

    /**
     * @param $userId
     * @return mixed
     */
    public function __invoke(
        UserRepositoryInterface $userRepo,
        BetBulkBalanceRequest $betBulkBalanceRequest,
        CashFlowRepositoryInterface $cashFlowRepo,
        $username
    ) {
        $this->setCashFlowRepo($cashFlowRepo);

        //handle db transaction
        $data = DB::transaction(function () use ($betBulkBalanceRequest, $cashFlowRepo, $userRepo, $username) {
            $user = $userRepo->getUserByUsername($username);

            //validate field and sum amount

            $betData = $betBulkBalanceRequest->data;

            if (count($betBulkBalanceRequest->data) > config('ichips.max_bet_data')) {
                throw new ValidationResponseException(
                    self::MESSAGE_BET_MAX_DATA,
                    []
                );
            }

            $totalAmount = $this->validateArrayFieldAndGetTotalAmount($betData);
            $this->validateUserBalance($totalAmount, $user);

            //update balance
            $userBalances = $user->spUpdateBalance('-', $totalAmount, $user->getField('id'));

            \Log::info('user previous balance: ' . $userBalances[0]->beforeBalance);
            \Log::info('bet amount: ' . $totalAmount);
            \Log::info('user balance: ' . $userBalances[0]->afterBalance);

            //cash flow
            $cashflow = $cashFlowRepo->betBulk($betData, $username, $userBalances[0]->afterBalance);

            //redis

            return [
                'userBalances' => $userBalances,
            ];
        }, config('ichips.request_max_tries'));

        return $this->sendResponseOk([
            'current_balance' => get_truncated_val($data['userBalances'][0]->afterBalance),
            'username' => $username
        ], self::MESSAGE_UPDATE_SUCCESS);
    }

    /**
     * @param $betData
     * @return string
     * @throws ValidationResponseException
     */
    private function validateArrayFieldAndGetTotalAmount($betData)
    {
        $totalAmount = '0';
        $bettingCodes = [];
        $gameCodes = [];

        foreach ($betData as $key => $value) {
            if (!isset($value['amount']) || !isset($value['game_code']) || !isset($value['betting_code'])) {
                throw new ValidationResponseException(
                    self::MESSAGE_REQUIRED_FIELDS,
                    []
                );
            }

            if (!is_string($value['amount'])) {
                throw new ValidationResponseException(
                    'Validation error!',
                    ['amount' => self::MESSAGE_AMOUNT_STRING]
                );
            }

            if (!is_numeric($value['amount'])) {
                throw new ValidationResponseException(
                    'Validation error!',
                    ['amount' => self::MESSAGE_AMOUNT_NUMERIC]
                );
            }

            //check unique betting code in array passed from client
            if (in_array($value['betting_code'], $bettingCodes)) {
                throw new ValidationResponseException(
                    'Validation error!',
                    ['betting_code' => self::MESSAGE_BETTING_CODE_UNIQUE]
                );
            }

            $totalAmount += get_truncated_val($value['amount']);
            $bettingCodes[] = $value['betting_code'];
            $gameCodes[$value['game_code']] = $value['game_code'];
        }

        $this->validateUniqueBettingCode($bettingCodes);
        $this->validateIfExistGameCode($gameCodes);

        return $totalAmount;
    }

    /**
     * @param $bettingCodes
     */
    private function validateUniqueBettingCode($bettingCodes)
    {
        //check unique betting code in array from database
        $cashFlowByBettingCode = $this->getCashFlowRepo()->getCashFlowByBulkBettingCode($bettingCodes)->count();

        if ($cashFlowByBettingCode) {
            throw new ValidationResponseException(
                'Validation error!',
                ['betting_code' => self::MESSAGE_BETTING_CODE_UNIQUE]
            );
        }
    }

    /**
     * @param $gameCodes
     * @throws ValidationResponseException
     */
    private function validateIfExistGameCode($gameCodes)
    {
        foreach ($gameCodes as $key => $gameCode) {
            # code...
            //check unique game code from database
            $game = Game::where('game_code', $gameCode)->count();

            if (!$game) {
                throw new ValidationResponseException(
                    'Validation error!',
                    ['game_code' => self::MESSAGE_GAME_CODE]
                );
            }
        }
    }

    /**
     * @param $totalAmount
     * @param $user
     * @throws ValidationResponseException
     */
    private function validateUserBalance($totalAmount, $user)
    {
        //validate user amount vs bet
        if ($totalAmount > $user->getField('balance')) {
            throw new ValidationResponseException(
                'Validation error!',
                ['amount' => self::MESSAGE_BET_AMOUNT]
            );
        }
    }

    /**
     * @return mixed
     */
    public function getCashFlowRepo()
    {
        return $this->cashFlowRepo;
    }

    /**
     * @param mixed $cashFlowRepo
     *
     * @return self
     */
    public function setCashFlowRepo($cashFlowRepo)
    {
        $this->cashFlowRepo = $cashFlowRepo;

        return $this;
    }
}
