<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Repositories\v1\UserChipLimitRepository;
use App\Http\v1\Client\Requests\UpdateUserChipLimitRequest;

class UpdateUserChipLimitController extends BaseController
{
    const MESSAGE_SUCCESS = 'Success!';

    /**
     * Get chip limit of user
     *
     * @param $username
     * @param $updateUserChipLimitRequest
     * @param $updateUserChipLimitRepository
     * @return mixed
     *
     */
    public function __invoke(
        $username,
        UpdateUserChipLimitRequest $updateUserChipLimitRequest,
        UserChipLimitRepository $updateUserChipLimitRepository
    ) {
        $response = $updateUserChipLimitRepository->updateOrCreateChipLimit($updateUserChipLimitRequest, $username);

        if ($response) {
            return $this->sendResponseOk([], self::MESSAGE_SUCCESS);
        }
    }
}
