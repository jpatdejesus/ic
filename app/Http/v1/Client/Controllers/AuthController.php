<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Models\v1\User;
use Illuminate\Http\Request;
use App\Http\v1\Client\Requests\AuthRequest;
use App\Repositories\v1\AuthRepository;
use App\Exceptions\RepositoryBadRequestException;
use App\Exceptions\ValidationResponseException;

class AuthController extends BaseController
{
    private $user;
    private $authRepository;

    const MESSAGE_LOGIN_SUCCESS = 'Token generated!';
    const MESSAGE_LOGIN_ERROR = 'These credentials do not match our records.';
    const MESSAGE_REFRESH_TOKEN_SUCCESS = 'Token refreshed!';
    const MESSAGE_REFRESH_TOKEN_ERROR = 'Error refreshing token.';
    const MESSAGE_DESTROY_TOKEN_SUCCESS = 'Token destroyed!';
    const MESSAGE_DESTROY_TOKEN_ERROR = 'Error destroying token.';
    const MESSAGE_VALIDATE_TOKEN_SUCCESS = 'Token is valid!';

    /**
     * UserController constructor.
     *
     * @param User $user
     * @param AuthRepository $authRepository
     */
    public function __construct(
        User $user,
        AuthRepository $authRepository
    ) {
        $this->user = $user;
        $this->authRepository = $authRepository;
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param $authRequest
     * @return mixed
     */
    public function token(AuthRequest $authRequest)
    {
        $response = $this->authRepository->token($authRequest->all());

        if ($response) {
            return $this->sendResponseOk($response, self::MESSAGE_LOGIN_SUCCESS);
        }

        return $this->sendUnauthorized([], self::MESSAGE_LOGIN_ERROR);
    }

    /**
     * Refresh Token.
     *
     * @param $request
     * @return mixed
     */
    public function refreshToken(Request $request)
    {
        $response = $this->authRepository->refreshToken($request);

        if ($response) {
            return $this->sendResponseOk($response, self::MESSAGE_REFRESH_TOKEN_SUCCESS);
        }

        return $this->sendBadRequest([], self::MESSAGE_REFRESH_TOKEN_ERROR);
    }

    /**
     * Destroy Token.
     *
     * @param $request
     * @return mixed
     */
    public function destroyToken(Request $request)
    {
        $response = $this->authRepository->destroyToken($request);

        if ($response) {
            return $this->sendResponseOk([], self::MESSAGE_DESTROY_TOKEN_SUCCESS);
        }

        return $this->sendBadRequest([], self::MESSAGE_DESTROY_TOKEN_ERROR);
    }

    /**
     * Validate token.
     *
     * @param $request
     * @return mixed
     */
    public function validateToken(Request $request)
    {
        return $this->sendResponseOk([], self::MESSAGE_VALIDATE_TOKEN_SUCCESS);
    }
}
