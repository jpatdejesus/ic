<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Services\UserType\EntityService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\v1\Client\Requests\UserRequest;
use App\Http\v1\Client\Requests\GetUserInfoRequest;
use App\Repositories\v1\UserChipLimitRepository;
use App\Exceptions\RepositoryNotFoundException;
use App\Exceptions\ValidationResponseException;
use App\Http\v1\Client\Requests\UserTypeMiddlewareRequest;

class UserController extends BaseController
{
    protected $user;
    protected $userRepository;

    const MESSAGE_UPDATE_SUCCESS = 'User update successed';
    const MESSAGE_UPDATE_ERROR = 'User update failed';
    const PARAMETER_TYPE_ERROR = 'Value cannot be all numeric.';

    /**
     * UserController constructor.
     *
     * @param UserTypeMiddlewareRequest $UserTypeMiddlewareRequest
     * @throws RepositoryNotFoundException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function __construct(UserTypeMiddlewareRequest $UserTypeMiddlewareRequest)
    {
        $entityService = new EntityService('users');

        $this->user = $entityService->getModel();
        $this->userRepository = $entityService->getRepository();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $userRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(UserRequest $userRequest)
    {
        DB::beginTransaction();

        if ($response = $this->userRepository->createUser($userRequest)) {
            DB::commit();

            $this->flagAction = true; // to return http_code 201 instead of 200

            return $this->sendResponseOk($response, $this->userRepository::CREATE_SUCCESS);
        }

        DB::rollBack();

        return $this->sendBadRequest([], $this->userRepository::CREATE_FAILED);
    }

    /**
     * Show the specified resource.
     *
     * @param GetUserInfoRequest $getUserInfoRequest
     * @param UserChipLimitRepository $chipLimitRepository
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryNotFoundException
     * @throws ValidationResponseException
     */
    public function show(GetUserInfoRequest $getUserInfoRequest, UserChipLimitRepository $chipLimitRepository, $username)
    {
        if (is_numeric($username)) {
            throw new ValidationResponseException(
                'Get user fail.',
                ['username' => self::PARAMETER_TYPE_ERROR]
            );
        }

        if ($this->userRepository->isUserExist($username)) {
            if (!isset($getUserInfoRequest->game_code)) {
                $user = $this->userRepository->getUserInformation($username)->first();
    
                return response()->json(
                    [
                        'message' => "Success!",
                        'data' => [
                            'user' => $user
                        ],
                    ]
                );
            } else {
                $user = $this->userRepository->getUserInformation($username)->first();
    
                $chip_limit = $chipLimitRepository->getChipLimit(
                    $getUserInfoRequest->game_code,
                    $getUserInfoRequest->user_type,
                    $username
                );
    
                return response()->json(
                    [
                        'message' => "Success!",
                        'data' => [
                            'user' => $user,
                            'chip_limit' => $chip_limit
                        ],
                    ]
                );
            }
        } else {
            throw new RepositoryNotFoundException("User not found.");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserRequest $userRequest
     * @return mixed
     */
    public function update(UserRequest $userRequest, $username)
    {
        DB::beginTransaction();

        $response = $this->userRepository->updateUser($userRequest, $username);
        if ($response) {
            DB::commit();
            return $this->sendResponseOk([], $this->userRepository::UPDATE_SUCCESS);
        }
        DB::rollBack();
        return $this->sendBadRequest([], $this->userRepository::UPDATE_FAILED);
    }

    /**
     * Search for record with filter
     *
     * @param UserRequest $request
     * @param $query
     * @return \Illuminate\Http\JsonResponse
     * @throws RepositoryNotFoundException
     */
    public function search(UserRequest $request, $query)
    {
        //userDatatable
        $response = $this->userRepository->search($request->all(), $query);

        if (!empty($response)) {
            return $this->sendResponseOk($response, "Success!");
        }
        throw new RepositoryNotFoundException('User not found.');
    }

    /**
     * User disabled status
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function disable(Request $request, $username)
    {
        DB::beginTransaction();

        if ($this->userRepository->isDisabled($username)) {
            DB::commit();

            return $this->sendResponseOk([], $this->userRepository::UPDATE_SUCCESS);
        }

        DB::rollBack();

        return $this->sendBadRequest([], $this->userRepository::UPDATE_FAILED);
    }
}
