<?php

namespace App\Http\v1\Client\Controllers;

use App\Http\v1\Controllers\BaseController;
use App\Repositories\v1\PromotionRepository;
use App\Http\v1\Client\Requests\FilterPromotionRequest;

class PromotionController extends BaseController
{
    /**
     * Filter Promotion
     *
     * @param FilterPromotionRequest $filterPromotionRequest
     * @param PromotionRepository $promotionRepository
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function filterExport(FilterPromotionRequest $filterPromotionRequest, PromotionRepository $promotionRepository)
    {
        $promotionRepository->setFilterPromotionRequest($filterPromotionRequest);

        $responseData = $promotionRepository->filterGameTransactions();

        return $responseData;
    }
}
