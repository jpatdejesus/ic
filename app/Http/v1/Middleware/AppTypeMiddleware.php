<?php

namespace App\Http\v1\Middleware;

use Closure;
use App\Http\v1\Requests\FormRequest;
use Illuminate\Http\Request;

class AppTypeMiddleware
{

    public function handle(Request $request, Closure $next, $guard = null)
    {
        $appTypeClient = strtoupper(config('ichips.app_types.client'));
        $appTypeAdmin = strtoupper(config('ichips.app_types.admin'));

        $appType = $appTypeClient;
        $arrUri = explode('/', $request->getRequestUri());

        if (count($arrUri) >= 3) {
            if (strtoupper($arrUri[2]) === $appTypeAdmin) {
                $appType = $appTypeAdmin;
            }

            FormRequest::$APP_TYPE = $appType;
            $request->app_type = $appType;
        }

        return $next($request);
    }
}
