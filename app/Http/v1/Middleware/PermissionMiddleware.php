<?php

namespace App\Http\v1\Middleware;

use Closure;
use App\Exceptions\ForbiddenException;
use App\Repositories\v1\JwtRepository;
use App\Models\v1\RoleHasPermission;
use App\Models\v1\AdminRoleHasPermission;

class PermissionMiddleware
{

    private $jwtRepository;
    const ERROR_MESSAGE = 'Permission Denied';

    /**
     * UserController constructor.
     *
     * @param $jwtRepository
     */
    public function __construct(JwtRepository $jwtRepository)
    {
        $this->jwtRepository = $jwtRepository;
    }

    /**
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws ForbiddenException
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');

        $info = $this->jwtRepository->validateToken($token);

        $route = $request->route();

        if (request()->app_type == strtoupper(config('ichips.app_types.admin'))) {
            $permission = AdminRoleHasPermission::select('admin_permissions.code', 'admin_user_roles.user_id')
                ->join(
                    'admin_user_roles',
                    'admin_role_has_permissions.role_id',
                    '=',
                    'admin_user_roles.role_id'
                )
                ->join(
                    'admin_permissions',
                    'admin_role_has_permissions.permission_id',
                    '=',
                    'admin_permissions.id'
                )
                ->where([
                    'admin_permissions.code' => $request->method() . '::' . str_replace(' ', '_', $route[1]['name']),
                    'admin_user_roles.user_id' => $info->id
                ])
                ->first();
        } else {
            $permission = RoleHasPermission::select('permissions.code', 'user_roles.role_credential_id')
                ->join(
                    'user_roles',
                    'role_has_permissions.role_id',
                    '=',
                    'user_roles.role_id'
                )
                ->join(
                    'permissions',
                    'role_has_permissions.permission_id',
                    '=',
                    'permissions.id'
                )
                ->where([
                    'permissions.code' => $request->method() . '::' . str_replace(' ', '_', $route[1]['name']),
                    'user_roles.role_credential_id' => $info->id
                ])
                ->first();
        }
        if ($permission === null) {
            throw new ForbiddenException('Permission Denied!');
        }

        return $next($request);
    }
}
