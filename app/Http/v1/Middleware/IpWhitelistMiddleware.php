<?php

namespace App\Http\v1\Middleware;

use Closure;

class IpWhitelistMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param $guard
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //check if ip is whitelisted here
        
        if (is_ip_whitelisted()) {
            return $next($request);
        }

        return response()->json(
            [
                'error' => 'IP '. get_client_ip_address() .' not Whitelisted',
            ],
            403
        );
    }
}
