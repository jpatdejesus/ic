<?php

namespace App\Http\v1\Middleware;

use Illuminate\Routing\Middleware\ThrottleRequests;

class RateLimits extends ThrottleRequests
{
    /**
     * @param $request
     * @return mixed
     */
    protected function resolveRequestSignature($request)
    {
        return sha1(implode('|', [
            $request->method(),
            $request->root(),
            $request->path(),
            $request->ip(),
            $request->query('access_token'),
        ]));

        // return $request->fingerprint();
    }

    /**
     * Resolve the number of attempts if the user is authenticated or not.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int|string  $maxAttempts
     * @return int
     */
    protected function resolveMaxAttempts($request, $maxAttempts)
    {
        return (int) $maxAttempts;
    }
}
