<?php

namespace App\Http\v1\Middleware;

use Closure;
use App\Http\v1\Requests\FormRequest;
use App\Repositories\v1\JwtRepository;

class JwtMiddleware
{
    private $jwtRepository;

    /**
     * UserController constructor.
     *
     * @param $jwtRepository
     */
    public function __construct(JwtRepository $jwtRepository)
    {
        $this->jwtRepository = $jwtRepository;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');

        if ($token) {
            $response = $this->jwtRepository->validateToken($token);

            if ($response) {
                FormRequest::$USER_DATA = $response;
                $request->user_data = $response;
                return $next($request);
            }

            return response()->json(
                [
                    'error' => 'Invalid token.'
                ],
                401
            );
        }

        // Unauthorized response if token not there
        return response()->json(
            [
                'error' => 'Token not provided.'
            ],
            400
        );
    }
}
