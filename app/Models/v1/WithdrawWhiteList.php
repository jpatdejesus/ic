<?php

namespace App\Models\v1;

use DB;
use Carbon\Carbon;
use App\BaseModels\v1\IChips;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithdrawWhiteList extends IChips
{
    use SoftDeletes;

    const STATUS = [
        'pending' => 'PENDING',
        'approved' => 'APPROVED',
        'rejected' => 'REJECTED',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'user_type',
        'expire_time',
        'whitelisted_by',
        'updated_by',
        'date_approved',
        'remark', // Computer IP
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeGetWhiteLists($query, $params)
    {
        $resultData = DB::connection('ichips')->select("call sp_paginate_admin_get_whitelists_datatable
        (
            :username,
            :expire_time,
            :remark,
            :date_start,
            :date_end,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'username' => isset($params->username) && $params->username !== '' ? $params->username : '',
            'expire_time' => Carbon::now()->toDateTimeString(),
            'remark' => isset($params->remark) && $params->remark !== '' ? $params->remark : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips')->select("call sp_get_count_admin_get_whitelists_datatable
        (
            :username,
            :expire_time,
            :remark,
            :date_start,
            :date_end
        )", [
            'username' => isset($params->username) && $params->username !== '' ? $params->username : '',
            'expire_time' => Carbon::now()->toDateTimeString(),
            'remark' => isset($params->remark) && $params->remark !== '' ? $params->remark : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];
        // return $query->select($params['table_columns'])
        //     ->where('expire_time', '>=', Carbon::now()->toDateTimeString())
        //     ->whereStatus(WithdrawWhiteList::STATUS['approved'])
        //     ->when(request()->date_from && request()->date_to, function ($query) {
        //         $query->whereBetween('created_at', [request()->date_from, request()->date_to]);
        //     })
        //     ->when(request()->username, function ($query) {
        //         $query->where('username', 'LIKE', '%'. request()->username .'%');
        //     })
        //     ->when(request()->remark, function ($query) {
        //         $query->where('remark', 'LIKE', '%'. request()->remark .'%');
        //     });
    }

    public function scopeGetWhiteListRequests($query, $params)
    {
        $resultData = DB::connection('ichips')->select("call sp_paginate_admin_whitelist_requests_datatable
        (
            :status,
            :date_start,
            :date_end,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'status' => isset($params->status) && $params->status !== '' ? $params->status : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips')->select("call sp_get_count_admin_whitelist_requests_datatable
        (
            :status,
            :date_start,
            :date_end
        )", [
            'status' => isset($params->status) && $params->status !== '' ? $params->status : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];

        // return $query->select($params['table_columns'])
        //     ->when(request()->date_from && request()->date_to, function ($query) {
        //         $query->whereBetween('created_at', [request()->date_from, request()->date_to]);
        //     })
        //     ->when(request()->status !== 'ALL', function ($query) {
        //         $query->whereStatus(request()->status);
        //     });
    }
}
