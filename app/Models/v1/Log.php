<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\Logs;
use App\Exceptions\RepositoryNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class Log extends Logs
{
    public $timestamps = false;

    /**
    * logs
    */
    const LOGS_TABLE = 'logs';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_consumer_username',
        'username',
        'user_type',
        'request_content',
        'response_content',
        'remark',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeGetIchipsLogs($query, $params)
    {
        $tableName = '';

        if ($params->date_from && $params->date_to) {
            $tableName = $params->end_point . '_log_' . date('Ymd', strtotime($params->date_from));
        }

        $query = DB::connection(self::LOGS_TABLE)
            ->table('information_schema.tables')
            ->select(['table_name'])
            ->where('table_schema', config('database.connections.logs.database'))
            ->where('table_name', 'LIKE', $params->end_point . '%')
            ->get()
            ->pluck('table_name');
            
        if ($query->count() == 0) {
            throw new RepositoryNotFoundException('No records found.', Response::HTTP_NOT_FOUND);
        }
        
        $resultData = DB::connection(self::LOGS_TABLE)->select("call sp_paginate_admin_ichips_logs_datatable
        (
            :user_type, 
            :table_name,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
            'table_name' => $tableName,
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection(self::LOGS_TABLE)->select("call sp_get_count_admin_ichips_logs_datatable
        (
            :user_type, 
            :table_name
        )", [
            'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
            'table_name' => $tableName,
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];

        // $tables = [];
        // $username = trim($param->username);
        // $user_type = trim($param->user_type);
        
        // if ($query->count() == 0) {
        //     throw new RepositoryNotFoundException('No records found.', Response::HTTP_NOT_FOUND);
        // }
        // foreach ($query as $key => $value) {
        //     $date = substr(substr($value, strripos($value, '_')), 1);
            
        //     if ($param->date_from && $param->date_to) {
        //         if (strtotime($date) >= strtotime($param->date_from) && strtotime($date) <= strtotime($param->date_to)) {
        //             array_push($tables, $value);
        //         }
        //     } else {
        //         array_push($tables, $value);
        //     }
        // }
        // $result = [];

        // if ($tables) {
            // $query = DB::connection(self::LOGS_TABLE)->table($tables[0]);
            // $this->addFilter($query);
            // return $query;
        // }

        // throw new RepositoryNotFoundException('No records found.', Response::HTTP_NOT_FOUND);
    }
    
    // private function addFilter(&$query)
    // {
    //     $username = trim(request()->username);
    //     $user_type = trim(request()->user_type);

    //     if ($username) {
    //         $query->whereUsername($username);
    //     }

    //     if ($user_type) {
    //         $query->whereUserType($user_type);
    //     }
    // }
}
