<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChipsAdmin;

class AdminRole extends IChipsAdmin
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_default',
        'role_group'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = true;
}
