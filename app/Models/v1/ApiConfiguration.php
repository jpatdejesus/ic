<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class ApiConfiguration extends IChips
{
    const FEATURE_WITHDRAW_VERIFICATION = 'withdraw_verification';

    const STATUS_ACTIVE = 1;
    
    const STATUS_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'feature_name',
        'status', // Computer IP
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
