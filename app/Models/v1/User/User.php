<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChips;
use App\Exceptions\ValidationResponseException;

class User extends IChips
{
    const STATUS = [
        'ENABLED',
        'DISABLED',
        'SUSPENDED',
    ];

    const SP_STATUS_SUCCESS = 'Success';
    const SP_STATUS_ERROR = 'Error';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname',
        'currency_id',
        'username',
        'balance',
        'point',
        'exp',
        'status',
        'last_login_time',
        'last_action_time',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'id' => 'id',
        'balance' => 'balance',
        'username' => 'username',
        'nickname' => 'nickname',
        'point' => 'point',
        'exp' => 'exp',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the currency of the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function currency()
    {
        return $this->hasOne(Currency::class, "id", "currency_id");
    }

    public function spUpdateBalance($operator, $amount, $user_id)
    {
        $result = DB::select("call sp_update_balance_MARS(:operator, :amount, :user_id)", [
            'operator' => $operator,
            'amount' => $amount,
            'user_id' => $user_id,
        ]);

        if ($result[0]->status == self::SP_STATUS_ERROR) {
            throw new ValidationResponseException(
                'Validation error!',
                ['amount' => [$result[0]->error_message]]
            );
        }

        return $result;
    }

    public function spUpdateBalanceByAdmin($operator, $amount, $user_id)
    {
        return DB::select("call sp_update_balance_by_admin_MARS(:operator, :amount, :user_id)", [
            'operator' => $operator,
            'amount' => $amount,
            'user_id' => $user_id,
        ]);
    }

    public function spVerifyWithdrawal($username, $userType)
    {
        return DB::select("call sp_withdrawVerify_MARS(:username, :usertype)", [
            'username' => $username,
            'usertype' => $userType,
        ]);
    }
}
