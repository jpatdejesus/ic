<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChips;
use App\Exceptions\ValidationResponseException;

class UserPHP extends IChips
{

    const SP_STATUS_SUCCESS = 'Success';
    const SP_STATUS_ERROR = 'Error';

    /**
     * @var string
     */
    protected $table = 'users_php';

    /**
     * @var string
     */
    protected $primaryKey = 'userid';

    /**
     * @var mixed
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'UserName',
        'TrueName',
        'userpass',
        'pre_idTrueName',
        'pre_idName',
        'pre_id',
        'pre_sequence',
        'telephone',
        'QQ',
        'Email',
        'WithdrawalPassword',
        'isuseable',
        'classid',
        'IsLogin',
        'IsOnline',
        'abc',
        'pltype',
        'islock',
        'remark',
        'sessionid',
        'updatetime',
        'moneysort',
        'curMoney1',
        'VideoBetLimitIDs',
        'RouletteBetLimitIDs',
        'SportsBetLimitIDs',
        'ElectronicGameBetLimitIDs',
        'LotteryGameBetLimitIDs',
        'ChipVideo',
        'ChipRoulette',
        'limits',
        'Operator',
        'AddTime',
        'AddressIP',
        'CredentialsType',
        'CredentialsNumber',
        'Memo',
        'CenterWallet',
        'UserType',
        'PlatformBetControl',
        'RegisterIP',
        'SourceInfo',
        'winPoint',
        'Question',
        'Answer',
        'Point',
        'Exp'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'userpass',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'id' => 'userid',
        'balance' => 'curMoney1',
        'username' => 'UserName',
        'nickname' => 'TrueName',
        'point' => 'Point',
        'exp' => 'Exp',
    ];

    /**
     * Status values.
     *
     * @param  $status
     * @return $status name
     */
    public function status($status = "")
    {
        switch ($this->isuseable) :
            case 0:
                $status = "SUSPENDED";
                break;
            case 2:
                $status = "DISABLED";
                break;
            default:
                $status = "ENABLED";
        endswitch;

        return $status;
    }

    public function spUpdateBalance($operator, $amount, $user_id)
    {
        $result = DB::select("call sp_update_balance_CASH(:operator, :amount, :user_id)", [
            'operator' => $operator,
            'amount' => $amount,
            'user_id' => $user_id,
        ]);

        if ($result[0]->status == self::SP_STATUS_ERROR) {
            throw new ValidationResponseException(
                'Validation error!',
                ['amount' => [$result[0]->error_message]]
            );
        }

        return $result;
    }

    public function spUpdateBalanceByAdmin($operator, $amount, $user_id)
    {
        return DB::select("call sp_update_balance_by_admin_CASH(:operator, :amount, :user_id)", [
            'operator' => $operator,
            'amount' => $amount,
            'user_id' => $user_id,
        ]);
    }
    
    public function spVerifyWithdrawal($username, $userType)
    {
        return DB::select("call sp_withdrawVerify_CASH(:username, :usertype)", [
            'username' => $username,
            'usertype' => $userType,
        ]);
    }
}
