<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class UserType extends IChips
{
    protected $table = 'user_types';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
     //protected $hidden = [];
}
