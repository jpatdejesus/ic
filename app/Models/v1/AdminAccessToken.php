<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChipsAdmin;

class AdminAccessToken extends IChipsAdmin
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'token',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'id' => 'id',
        'user_id' => 'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
