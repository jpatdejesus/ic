<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class UserChipLimitDefaults extends IChips
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_type_id',
        'currency_id',
        'min',
        'max',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
