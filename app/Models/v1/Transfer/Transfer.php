<?php

namespace App\Models\v1;

use App\BaseModels\v1\Datarepo;
use Illuminate\Support\Facades\DB;

class Transfer extends Datarepo
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transfer_id',
        'op_transfer_id',
        'username',
        'currency_code',
        'actions',
        'amount',
        'balance',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'transfer_id' => 'transfer_id',
        'username' => 'username',
    ];

    /**
     * @param $value
     */
    public function setUpdatedAt($value)
    {
        //Do-nothing.
    }

    public function getUpdatedAtColumn()
    {
        //Do-nothing
    }

    public function scopeFilterAllTransfers($query, $params)
    {
        /*return $query->select($selectColumns)
            ->when($params['transfer_code'], function ($query) use ($params) {
                $query->where('transfer_id', $params['transfer_code']);
            })
            ->when($params['start_date'] && $params['end_date'], function ($query) use ($params) {
                $query->whereBetween('created_at', [$params['start_date'], $params['end_date']]);
            });*/

        $resultData = DB::connection('datarepo')->select("call sp_get_all_transfers_history
        ( 
            :transfer_code,
            :date_start,
            :date_end,
            :table_alias,
            :start,
            :length
        )", [
            'transfer_code' => isset($params->transfer_code) && $params->transfer_code && $params->transfer_code !== '' ? $params->transfer_code : '',
            'date_start' => isset($params->start_date) && $params->start_date && $params->start_date !== '' ? $params->start_date : '',
            'date_end' => isset($params->end_date) && $params->end_date && $params->end_date !== '' ? $params->end_date : '',
            'table_alias' => $this->getTable(),
            'start' => $params->start,
            'length' => $params->limit
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_all_transfers_history
        (
            :transfer_code,
            :date_start,
            :date_end,
            :table_alias
        )", [
            'transfer_code' => isset($params->transfer_code) && $params->transfer_code && $params->transfer_code !== '' ? $params->transfer_code : '',
            'date_start' => isset($params->start_date) && $params->start_date && $params->start_date !== '' ? $params->start_date : '',
            'date_end' => isset($params->end_date) && $params->end_date && $params->end_date !== '' ? $params->end_date : '',
            'table_alias' => $this->getTable()
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }

    public function scopeFilterTransfersByUsername($query, $params)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_all_transfer_history_by_username
        ( 
            :username,
            :table_name,
            :start,
            :length
        )", [
            'username' => isset($params->username) && $params->username && $params->username !== '' ? $params->username : '',
            'table_name' => $this->getTable(),
            'start' => $params->start,
            'length' => $params->limit
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_all_transfer_history_by_username
        (
            :username,
            :table_name
        )", [
            'username' => isset($params->username) && $params->username && $params->username !== '' ? $params->username : '',
            'table_name' => $this->getTable(),
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }
}
