<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class AccessToken extends IChips
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_credential_id',
        'token',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'id' => 'id',
        'user_id' => 'role_credential_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
