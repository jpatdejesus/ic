<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class Currency extends IChips
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public $timestamps= true;
}
