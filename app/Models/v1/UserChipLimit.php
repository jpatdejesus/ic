<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;

class UserChipLimit extends IChips
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_type_id',
        'game_type_id',
        'min',
        'max',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
