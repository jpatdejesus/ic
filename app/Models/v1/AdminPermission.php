<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChipsAdmin;

class AdminPermission extends IChipsAdmin
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'parent_id',
        'description',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
