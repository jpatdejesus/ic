<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends IChips
{
    use SoftDeletes;

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_code',
        'game_name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @var string
     */
    protected $table = "games";

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'user_id',
    // ];
}
