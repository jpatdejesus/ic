<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChipsAdmin;

class AdminUpdateBalanceRequest extends IChipsAdmin
{
    /**
     * TYPE
     */
    const TYPE_ADD = 'ADD';
    const TYPE_DEDUCT = 'DEDUCT';

    /**
     * STATUS
     */
    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    const MAX_DEDUCT_AMOUNT = 1000000000;
    const MIN_ADD_AMOUNT = 1;
    const MIN_DEDUCT_AMOUNT = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'user_type',
        'amount',
        'reason',
        'type',
        'trans_type',
        'status',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return mixed
     */
    public function adminUser()
    {
        return $this->belongsTo(AdminUser::class, 'updated_by');
    }

    public function scopeFilterAllUpdateBalanceRequests($query, $params)
    {
        $resultData = DB::connection('ichips-admin')->select("call sp_paginate_admin_balance_requests_datatable
        (
            :user_type, 
            :status,
            :date_start,
            :date_end,
            :role_name,
            :start,
            :length,
            :created_by,
            :order_by,
            :order_sort
        )", [
            'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
            'status' => isset($params->searchParams['status']) && $params->searchParams['status'] !== '' ? $params->searchParams['status'] : '',
            'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
            'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            'role_name' => $params->role_name,
            'start' => $params->start,
            'length' => $params->limit,
            'created_by' => $params->id,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips-admin')->select("call sp_get_count_admin_balance_requests_datatable
        (
            :user_type, 
            :status,
            :date_start,
            :date_end,
            :role_name,
            :created_by
        )", [
            'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
            'status' => isset($params->searchParams['status']) && $params->searchParams['status'] !== '' ? $params->searchParams['status'] : '',
            'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
            'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            'role_name' => $params->role_name,
            'created_by' => $params->id,
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];

        // $builder = $query->select(
        //     'admin_update_balance_requests.*',
        //     DB::raw("CONCAT(admin_users.first_name, ' ',admin_users.last_name) as updated_by"),
        //     DB::raw("CONCAT(creator.first_name, ' ',creator.last_name) as created_by")
        // )
        // // ->join(
        // //     DB::raw(
        // //         "(" . $this->getRawUserTables() . ") users
        // //         ON
        // //             users.username = admin_update_balance_requests.username
        // //             AND
        // //             users.flag = admin_update_balance_requests.user_type"
        // //     ),
        // //     function ($query) {
        // //     }
        // // )
        // ->leftJoin('admin_users', 'admin_update_balance_requests.updated_by', '=', 'admin_users.id')
        // ->leftJoin('admin_users as creator', 'admin_update_balance_requests.created_by', '=', 'creator.id')
        // ->when(isset($params['user_type']) && $params['user_type'] !== '', function ($query) use ($params) {
        //     $query->where('admin_update_balance_requests.user_type', $params['user_type']);
        // })
        // ->when(isset($params['status']) && $params['status'] !== '', function ($query) use ($params) {
        //     $query->where('admin_update_balance_requests.status', $params['status']);
        // })
        // ->when((isset($params['date_start']) && $params['date_start'] !== '') && (isset($params['date_end']) && $params['date_end'] !== ''), function ($query) use ($params) {
        //     $query->whereBetween('admin_update_balance_requests.created_at', [$params['date_start'], $params['date_end']]);
        // });

        // if ($params['role_name'] === 'super_admin') {
        //     return $builder;
        // } else {
        //     return $builder->whereCreatedBy($params['id']);
        // }
    }

    /**
     * Generate raw users table for union all query
     */
    private function getRawUserTables()
    {
        $ichipsDatabase = config('database.connections.ichips.database');
        $queryArray = [];

        foreach (config('ichips.user_types') as $user_type => $user_type_entity_handler) {
            $model = app($user_type_entity_handler['users']['model']);

            array_push($queryArray, "SELECT ('" . $user_type . "' COLLATE utf8_unicode_ci) as flag,
                    " . $model->getTable() . "." . $model->getFieldName('balance') . " as balance,
                    " . $model->getTable() . "." . $model->getFieldName('username') . " as username
                    FROM " . $ichipsDatabase . "." . $model->getTable() . " AS " . $model->getTable());
        }

        return implode(' UNION ALL ', $queryArray);
    }
}
