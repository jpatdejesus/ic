<?php

namespace App\Models\v1;

use App\BaseModels\v1\Datarepo;
use Illuminate\Support\Facades\DB;

class CashflowPHP extends Datarepo
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'game_code',
        'trans_type',
        'betting_code',
        'trans_amount',
        'balance',
        'created_at',
        'updated_at',
        'path',
    ];

    /**
     * @var string
     */
    protected $table = "cashflow_php";

    public function scopePlayerCashflowByAdminInquiry($query, $params)
    {
        /*return $query->whereUsername($params['username'])
            ->whereBetween('created_at', [$params['date_start'], $params['date_end']]);*/

        $resultData = DB::connection('datarepo')->select("call sp_paginate_admin_player_inquiry_datatable
        (
            :username, 
            :user_id, 
            :date_start,
            :date_end,
            :table_alias,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'username' => isset($params->username) && $params->username !== '' && !is_numeric($params->username) ? $params->username : '',
            'user_id' => isset($params->username) && $params->username !== '' && is_numeric($params->username) ? $params->username : 0,
            'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
            'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            'table_alias' => $params->table_alias,
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_admin_player_inquiry_datatable
        (
            :username, 
            :user_id, 
            :date_start,
            :date_end,
            :table_alias
        )", [
            'username' => isset($params->username) && $params->username !== '' && !is_numeric($params->username) ? $params->username : '',
            'user_id' => isset($params->username) && $params->username !== '' && is_numeric($params->username) ? $params->username : 0,
            'date_start' => isset($params->searchParams['date_start']) && $params->searchParams['date_start'] !== '' ? $params->searchParams['date_start'] : '',
            'date_end' => isset($params->searchParams['date_end']) && $params->searchParams['date_end'] !== '' ? $params->searchParams['date_end'] : '',
            'table_alias' => $params->table_alias
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];
    }

    public function scopeChipFlowByUsername($query, $params)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_chip_flow_by_username_history
        ( 
            :username,
            :table_alias,
            :start,
            :length,
            :start_date,
            :end_date
        )", [
            'username' => $params->username,
            'table_alias' => $this->getTable(),
            'start' => $params->start,
            'length' => $params->limit,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_chip_flow_by_username_history
        (
            :username,
            :table_alias,
            :start_date,
            :end_date
        )", [
            'username' => $params->username,
            'table_alias' => $this->getTable(),
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }

    public function scopeAllChipFlow($query, $params)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_all_chip_flow_history
        ( 
            :table_alias,
            :start,
            :length
        )", [
            'table_alias' => $this->getTable(),
            'start' => $params->start,
            'length' => $params->limit,
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_all_chip_flow_history
        (
            :table_alias
        )", [
            'table_alias' => $this->getTable(),
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }
}
