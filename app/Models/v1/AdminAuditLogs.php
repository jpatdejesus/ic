<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChipsAdmin ;

class AdminAuditLogs extends IChipsAdmin
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'role',
        'event',
        'old_values',
        'new_values',
        'ip_address',
        'platform',
        'remarks',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeGetAdminAuditTrail($query, $params)
    {
        $requestEmail = '';
        $roleGroup = '';

        if (request()->user_data['role_group'] === "SUPER_ADMIN") {
            if (request()->email) {
                $requestEmail = request()->email;
            }
        } elseif (request()->user_data['role_group'] === "ADMIN") {
            if (request()->user_data) {
                $requestEmail = request()->user_data->email;
            }
            $query->where(function ($query) {
                $roleGroup = 'ADMIN';
            });
        } else {
            if (request()->user_data) {
                $requestEmail = request()->user_data->email;
            }
            $query->where(function ($query) {
                $roleGroup = 'AUDIT';
            });
        }

        $resultData = DB::connection('ichips-admin')->select("call sp_paginate_admin_audit_trails_datatable
        (
            :email, 
            :role_group,
            :date_start,
            :date_end,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'email' => $requestEmail,
            'role_group' => $roleGroup,
            'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
            'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips-admin')->select("call sp_get_count_admin_audit_trails_datatable
        (
            :email, 
            :role_group,
            :date_start,
            :date_end
        )", [
            'email' => $requestEmail,
            'role_group' => $roleGroup,
            'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
            'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];
        // $query->leftJoin('admin_roles', 'admin_audit_logs.role', '=', 'admin_roles.id');
        // if (request()->date_from && request()->date_to) {
        //     $query
        //         ->where('admin_audit_logs.created_at', '>=', request()->date_from)
        //         ->where('admin_audit_logs.created_at', '<=', request()->date_to);
        // }
        
        // if (request()->user_data['role_group'] === "SUPER_ADMIN") {
        //     if (request()->email) {
        //         $query->whereEmail(request()->email);
        //     }
        // } else if (request()->user_data['role_group'] === "ADMIN") {
        //     if (request()->user_data) {
        //         $query->whereEmail(request()->user_data->email);
        //     }
        //     $query->where(function ($query) {
        //         $query->where('admin_roles.role_group', '=', 'ADMIN');
        //     });
        // } else {
        //     if (request()->user_data) {
        //         $query->whereEmail(request()->user_data->email);
        //     }
        //     $query->where(function ($query) {
        //         $query->where('admin_roles.role_group', '=', 'AUDIT');
        //     });
        // }
        // return $query;
    }
}
