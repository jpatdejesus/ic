<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChipsAdmin;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class AdminUser extends IChipsAdmin implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'nickname',
        'profile_picture',
        'email',
        'password',
        'status',
        'login_attempts',
        'last_login_ip',
        'last_login_time',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $fieldMapper = [
        'username' => 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public $timestamps = true;

    public function adminUpdateBalanceRequest()
    {
        return $this->hasMany(AdminUpdateBalanceRequest::class);
    }

    public function scopeFilterAllUsers($query, $params)
    {
        $resultData = DB::connection('ichips-admin')->select("call sp_paginate_admin_users_datatable
        (
            :email, 
            :user_status,
            :admin_role_id,
            :user_id,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'email' => isset($params->searchParams['email']) && $params->searchParams['email'] !== '' ? $params->searchParams['email'] : '',
            'user_status' => isset($params->searchParams['status']) && $params->searchParams['status'] !== '' ? $params->searchParams['status'] : '',
            'admin_role_id' => isset($params->searchParams['role_id']) && $params->searchParams['role_id'] !== '' ? $params->searchParams['role_id'] : 0,
            'user_id' => $params->id,
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips-admin')->select("call sp_get_count_admin_users_datatable
        (
            :email, 
            :user_status,
            :admin_role_id,
            :user_id,
            :start,
            :length
        )", [
            'email' => isset($params->searchParams['email']) && $params->searchParams['email'] !== '' ? $params->searchParams['email'] : '',
            'user_status' => isset($params->searchParams['status']) && $params->searchParams['status'] !== '' ? $params->searchParams['status'] : '',
            'admin_role_id' => isset($params->searchParams['role_id']) && $params->searchParams['role_id'] !== '' ? $params->searchParams['role_id'] : 0,
            'user_id' => $params->id,
            'start' => $params->start,
            'length' => $params->limit,
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];
        // return $query->select($params['table_columns'])
        //     ->join('admin_user_roles', 'admin_user_roles.user_id', 'admin_users.id')
        //     ->join('admin_roles', 'admin_roles.id', 'admin_user_roles.role_id')
        //     ->when(isset($params['email']) && $params['email'] !== '', function ($query) use ($params) {
        //         $query->where('admin_users.email', 'LIKE', '%'. $params['email'] .'%');
        //     })
        //     ->when(isset($params['status']) && $params['status'] !== '', function ($query) use ($params) {
        //         $query->where('admin_users.status', $params['status']);
        //     })
        //     ->when(isset($params['role_id']) && $params['role_id'] !== '', function ($query) use ($params) {
        //         $query->where('admin_roles.id', $params['role_id']);
        //     })
        //     ->where('admin_users.id', '!=', $params['id'])
        //     ->where('admin_users.deleted_at', null);
    }
}
