<?php

namespace App\Models\v1;

use App\BaseModels\v1\Datarepo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Transaction extends Datarepo
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "bet_code",
        "bet_amount",
        "betting_code_bet",
        "betting_code_payout",
        "game_type",
        "game_id",
        "room_id",
        "room_number",
        "user_id",
        "username",
        "user_type",
        "bet_place",
        "balance_after_bet",
        "status",
        "effective_bet_amount",
        "shoehandnumber",
        "gameset_id",
        "gamename",
        "tablenumber",
        "balance",
        "win_loss",
        "result_id",
        "result",
        "bet_date",
        "super_six",
        "is_sidebet",
        "platform_code",
        "error_status",
        "withhold",
        "bet_device",
        "bet_browser",
        "second_order_time",
        "copy_time",
        "remark",
        "created_at",
        "updated_at",
    ];
    
    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilterTransactionByUsernameAndGameCode($query, $params, $historyParams)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_all_transaction_history_by_username
        ( 
            :username,
            :user_type,
            :game_code,
            :bet_code,
            :shoe_hand_number,
            :table_number,
            :date_start,
            :date_end,
            :start,
            :length
        )", [
            'username' => $params['username'],
            'user_type' => $params['user_type'],
            'game_code' => $params['game_code'],
            'bet_code' => isset($params['bet_code']) && $params['bet_code'] && $params['bet_code'] !== '' ? $params['bet_code'] : '',
            'shoe_hand_number' => isset($params['shoe_hand_number']) && $params['shoe_hand_number'] && $params['shoe_hand_number'] !== '' ? $params['shoe_hand_number'] : '',
            'table_number' => isset($params['table_number']) && $params['table_number'] && $params['table_number'] !== '' ? $params['table_number'] : '',
            'date_start' => isset($params['start_date']) && $params['start_date'] && $params['start_date'] !== '' ? $params['start_date'] : '',
            'date_end' => isset($params['end_date']) && $params['end_date'] && $params['end_date'] !== '' ? $params['end_date'] : '',
            'start' => $historyParams->start,
            'length' => $historyParams->limit
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_all_transaction_history_by_username
        ( 
            :username,
            :user_type,
            :game_code,
            :bet_code,
            :shoe_hand_number,
            :table_number,
            :date_start,
            :date_end
        )", [
            'username' => $params['username'],
            'user_type' => $params['user_type'],
            'game_code' => $params['game_code'],
            'bet_code' => isset($params['bet_code']) && $params['bet_code'] && $params['bet_code'] !== '' ? $params['bet_code'] : '',
            'shoe_hand_number' => isset($params['shoe_hand_number']) && $params['shoe_hand_number'] && $params['shoe_hand_number'] !== '' ? $params['shoe_hand_number'] : '',
            'table_number' => isset($params['table_number']) && $params['table_number'] && $params['table_number'] !== '' ? $params['table_number'] : '',
            'date_start' => isset($params['start_date']) && $params['start_date'] && $params['start_date'] !== '' ? $params['start_date'] : '',
            'date_end' => isset($params['end_date']) && $params['end_date'] && $params['end_date'] !== '' ? $params['end_date'] : '',
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
        // return $query->where(
        //     [
        //         'username' => $params['username'],
        //         'user_type' => $params['user_type'],
        //     ]
        // )->when($params['shoe_hand_number'], function ($query) use ($params) {
        //     $query->where('shoehandnumber', $params['shoe_hand_number']);
        // })->when($params['table_number'], function ($query) use ($params) {
        //     $query->where('tablenumber', $params['table_number']);
        // })->whereBetween('bet_date', [$params['start_date'], $params['end_date']]);
    }

    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilterAllTransactions($query, $params, $historyParams)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_all_transactions_history
        ( 
            :user_type,
            :game_code,
            :bet_code,
            :shoe_hand_number,
            :table_number,
            :date_start,
            :date_end,
            :start,
            :length
        )", [
            'user_type' => $params['user_type'],
            'game_code' => $params['game_code'],
            'bet_code' => isset($params['bet_code']) && $params['bet_code'] && $params['bet_code'] !== '' ? $params['bet_code'] : '',
            'shoe_hand_number' => isset($params['shoe_hand_number']) && $params['shoe_hand_number'] && $params['shoe_hand_number'] !== '' ? $params['shoe_hand_number'] : '',
            'table_number' => isset($params['table_number']) && $params['table_number'] && $params['table_number'] !== '' ? $params['table_number'] : '',
            'date_start' => isset($params['start_date']) && $params['start_date'] && $params['start_date'] !== '' ? $params['start_date'] : '',
            'date_end' => isset($params['end_date']) && $params['end_date'] && $params['end_date'] !== '' ? $params['end_date'] : '',
            'start' => $historyParams->start,
            'length' => $historyParams->limit
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_all_transactions_history
        ( 
            :user_type,
            :game_code,
            :bet_code,
            :shoe_hand_number,
            :table_number,
            :date_start,
            :date_end
        )", [
            'user_type' => $params['user_type'],
            'game_code' => $params['game_code'],
            'bet_code' => isset($params['bet_code']) && $params['bet_code'] && $params['bet_code'] !== '' ? $params['bet_code'] : '',
            'shoe_hand_number' => isset($params['shoe_hand_number']) && $params['shoe_hand_number'] && $params['shoe_hand_number'] !== '' ? $params['shoe_hand_number'] : '',
            'table_number' => isset($params['table_number']) && $params['table_number'] && $params['table_number'] !== '' ? $params['table_number'] : '',
            'date_start' => isset($params['start_date']) && $params['start_date'] && $params['start_date'] !== '' ? $params['start_date'] : '',
            'date_end' => isset($params['end_date']) && $params['end_date'] && $params['end_date'] !== '' ? $params['end_date'] : '',
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];

        // return $query->where(['user_type' => $params['user_type']])
        //     ->when($params['bet_code'], function ($query) use ($params) {
        //         $query->where('bet_code', $params['bet_code']);
        //     })
        //     ->when($params['shoe_hand_number'], function ($query) use ($params) {
        //         $query->where('shoehandnumber', $params['shoe_hand_number']);
        //     })
        //     ->when($params['table_number'], function ($query) use ($params) {
        //         $query->where('tablenumber', $params['table_number']);
        //     })
        //     ->when($params['start_date'] && $params['end_date'], function ($query) use ($params) {
        //         $query->whereBetween('created_at', [$params['start_date'], $params['end_date']]);
        //     });
    }

    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopePlayerTransactions($query, $params)
    {
        $params->game_aliases = '';
        
        if (in_array_r($params->game_name, config('ichips.game_name_aliases')) && $params->game_name !== '') {
            $params->is_in_array = 1;
            $params->game_aliases .= $params->game_name;
            foreach (config('ichips.game_name_aliases.'. $params->game_name) as $alias) {
                if ($alias !== $params->game_name) {
                    $params->game_aliases .= '|'. $alias;
                }
            }
        } else {
            $params->is_in_array = 0;
        }

        if (Str::startsWith($params->game_name, 'tw_')) {
            //Default Games
            $resultData = DB::connection('datarepo')->select("call sp_paginate_tw_admin_player_transaction_datatable
            (
                :username,
                :user_id,
                :game_name,
                :game_name_alias,
                :is_in_array,
                :bet_code,
                :user_type,
                :date_start,
                :date_end,
                :start,
                :length,
                :order_by,
                :order_sort
            )", [
                'username' => isset($params->userFilter) && $params->userFilter !== '' && !is_numeric($params->userFilter) ? $params->userFilter : '',
                'user_id' => isset($params->userFilter) && $params->userFilter !== '' && is_numeric($params->userFilter) ? $params->userFilter : 0,
                'game_name' => isset($params->game_name) && $params->game_name !== '' ? $params->game_name : '',
                'game_name_alias' => isset($params->game_aliases) && $params->game_aliases !== '' ? $params->game_aliases : '',
                'is_in_array' => $params->is_in_array,
                'bet_code' => isset($params->bet_code) && $params->bet_code !== '' ? $params->bet_code : '',
                'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
                'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
                'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
                'start' => $params->start,
                'length' => $params->limit,
                'order_by' => $params->sort['column'],
                'order_sort' => $params->sort['order_by']
            ]);

            $resultCount = DB::connection('datarepo')->select("call sp_get_count_tw_admin_player_transaction_datatable
            (
                :username,
                :user_id,
                :game_name,
                :game_name_alias,
                :is_in_array,
                :bet_code,
                :user_type,
                :date_start,
                :date_end
            )", [
                'username' => isset($params->userFilter) && $params->userFilter !== '' && !is_numeric($params->userFilter) ? $params->userFilter : '',
                'user_id' => isset($params->userFilter) && $params->userFilter !== '' && is_numeric($params->userFilter) ? $params->userFilter : 0,
                'game_name' => isset($params->game_name) && $params->game_name !== '' ? $params->game_name : '',
                'game_name_alias' => isset($params->game_aliases) && $params->game_aliases !== '' ? $params->game_aliases : '',
                'is_in_array' => $params->is_in_array,
                'bet_code' => isset($params->bet_code) && $params->bet_code !== '' ? $params->bet_code : '',
                'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
                'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
                'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            ]);
        } else {
            //Non-Taiwan Games
            $resultData = DB::connection('datarepo')->select("call sp_paginate_admin_player_transaction_datatable
            (
                :username,
                :user_id,
                :game_name,
                :game_name_alias,
                :is_in_array,
                :bet_code,
                :user_type,
                :date_start,
                :date_end,
                :start,
                :length,
                :order_by,
                :order_sort
            )", [
                'username' => isset($params->userFilter) && $params->userFilter !== '' && !is_numeric($params->userFilter) ? $params->userFilter : '',
                'user_id' => isset($params->userFilter) && $params->userFilter !== '' && is_numeric($params->userFilter) ? $params->userFilter : 0,
                'game_name' => isset($params->game_name) && $params->game_name !== '' ? $params->game_name : '',
                'game_name_alias' => isset($params->game_aliases) && $params->game_aliases !== '' ? $params->game_aliases : '',
                'is_in_array' => $params->is_in_array,
                'bet_code' => isset($params->bet_code) && $params->bet_code !== '' ? $params->bet_code : '',
                'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
                'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
                'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
                'start' => $params->start,
                'length' => $params->limit,
                'order_by' => $params->sort['column'],
                'order_sort' => $params->sort['order_by']
            ]);

            $resultCount = DB::connection('datarepo')->select("call sp_get_count_admin_player_transaction_datatable
            (
                :username,
                :user_id,
                :game_name,
                :game_name_alias,
                :is_in_array,
                :bet_code,
                :user_type,
                :date_start,
                :date_end
            )", [
                'username' => isset($params->userFilter) && $params->userFilter !== '' && !is_numeric($params->userFilter) ? $params->userFilter : '',
                'user_id' => isset($params->userFilter) && $params->userFilter !== '' && is_numeric($params->userFilter) ? $params->userFilter : 0,
                'game_name' => isset($params->game_name) && $params->game_name !== '' ? $params->game_name : '',
                'game_name_alias' => isset($params->game_aliases) && $params->game_aliases !== '' ? $params->game_aliases : '',
                'is_in_array' => $params->is_in_array,
                'bet_code' => isset($params->bet_code) && $params->bet_code !== '' ? $params->bet_code : '',
                'user_type' => isset($params->user_type) && $params->user_type !== '' ? $params->user_type : '',
                'date_start' => isset($params->date_start) && $params->date_start !== '' ? $params->date_start : '',
                'date_end' => isset($params->date_end) && $params->date_end !== '' ? $params->date_end : '',
            ]);
        }

        

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];

/*        $datarepoDB = config('database.connections.datarepo.database');
        $ichipsDB = config('database.connections.ichips.database');

        $gameTransactionsTable = $datarepoDB . '.' . $params['game_name'];

        $query = $query->select(
            $gameTransactionsTable . '_transactions.*',
            $ichipsDB . '.currencies.currency_code'
        )
            ->join(
                $ichipsDB . '.users',
                $ichipsDB . '.users.username',
                '=',
                $gameTransactionsTable . '_transactions' . '.username'
            )
            ->join(
                $ichipsDB . '.currencies',
                $ichipsDB . '.currencies.id',
                '=',
                $ichipsDB . '.users.currency_id'
            )
            ->where('user_type', $params['user_type'])
            ->when($params['bet_code'], function ($query) use ($params, $gameTransactionsTable) {
                $query->where($gameTransactionsTable . '_transactions.' .'bet_code', $params['bet_code']);
            })
            ->when($params['username'], function ($query) use ($params, $ichipsDB) {
                if (is_numeric($params['username'])) {
                    $query->where($ichipsDB . '.users.id', $params['username']);
                } else {
                    $query->where($ichipsDB . '.users.username', $params['username']);
                }
            })
            ->when($params['game_name'], function ($query) use ($params) {
                if (in_array_r($params['game_name'], config('ichips.game_name_aliases'))) {
                    $query->where(function ($query) use ($params) {
                        $query->where('gamename', 'LIKE', '%' . $params['game_name'] . '%');
                        foreach (config('ichips.game_name_aliases.'. $params['game_name']) as $alias) {
                            $query->orWhere('gamename', 'LIKE', '%' . $alias . '%');
                        }
                    });
                } else {
                    $query->where('gamename', 'LIKE', '%' . $params['game_name'] . '%');
                }
            })
            ->when($params['date_start'] && $params['date_end'], function ($query) use ($params, $gameTransactionsTable) {
                $query->whereBetween($gameTransactionsTable . '_transactions.' . 'created_at', [$params['date_start'], $params['date_end']]);
            });

        return $query;*/
    }

    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilterAllTableTransactionsByUsername($query, $params, $historyParams)
    {
        $queryData = function ($isRowCount = 0) use ($params, $historyParams) {
            $buildQuery = "";

            foreach ($params['games'] as $key => $value) {
                $gameTable = $value . "_transactions";
                if (!$isRowCount) {
                    $buildQuery .= "SELECT * FROM  `".$gameTable."` WHERE 1 AND `".$gameTable."`.`user_type` = " . "'" . $params['user_type'] . "'";
                } else {
                    $buildQuery .= "SELECT COUNT(*) AS row_count FROM  `".$gameTable."` WHERE 1 AND `".$gameTable."`.`user_type` = " . "'" . $params['user_type'] . "'";
                }

                if (isset($params['bet_code']) && $params['bet_code'] && $params['bet_code'] !== '') {
                    $buildQuery .= " AND `".$gameTable."`.`bet_code` = " . "'" . $params['bet_code'] . "'";
                }

                if (isset($params['shoe_hand_number']) && $params['shoe_hand_number'] && $params['shoe_hand_number'] !== '') {
                    $buildQuery .= " AND `".$gameTable."`.`shoehandnumber` = " . "'" . $params['shoe_hand_number'] . "'";
                }

                if (isset($params['table_number']) && $params['table_number'] && $params['table_number'] !== '') {
                    $buildQuery .= " AND `".$gameTable."`.`tablenumber` = " . "'" . $params['table_number'] . "'";
                }

                if ((isset($params['start_date']) && $params['start_date'] && $params['start_date'] !== '') && (isset($params['end_date']) && $params['end_date'] && $params['end_date'] !== '')) {
                    $buildQuery .= " AND `".$gameTable."`.`created_at` BETWEEN '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'";
                }
                
                $buildQuery .= " UNION ALL ";
            }
            
            $buildQuery = rtrim($buildQuery, ' UNION ALL ');

            if (!$isRowCount) {
                $buildQuery .= " ORDER BY " . $params['order_by'] . " " . $params['order_sort'];
                $buildQuery .= " LIMIT $historyParams->limit OFFSET $historyParams->start";
            }

            return $buildQuery;
        };

        $resultData = DB::connection('datarepo')->select(DB::raw($queryData()));
        $resultCount = DB::connection('datarepo')->select(DB::raw($queryData(1)));
        $resultDataCount = 0;

        foreach ($resultCount as $value) {
            $resultDataCount += $value->row_count;
        }

        return [
            'data' => $resultData,
            'count' => [0 => (object)['row_count' => $resultDataCount]]
        ];
    }
}
