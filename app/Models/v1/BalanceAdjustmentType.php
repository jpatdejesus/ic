<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChipsAdmin;

class BalanceAdjustmentType extends IChipsAdmin
{

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_name',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string
     */
    protected $table = "balance_adjustment_types";
}
