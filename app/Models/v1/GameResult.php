<?php

namespace App\Models\v1;

use App\BaseModels\v1\Datarepo;
use Illuminate\Support\Facades\DB;

class GameResult extends Datarepo
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'result',
        'shoehandnumber',
        'shoe_date',
        'table_no',
        'result_id',
        'values',
        "game_type",
        "game_id",
        "room_id",
        "room_number",
        "ante",
        "balance_limit",
        "game_result",
        "status",
        "start_date",
        "settle_date",
        "close_date",
        "copy_order_time",
        "platform_code",
    ];

    public $timestamps = false;

    public function scopeHistoryGameResult($query, $params)
    {
        $resultData = DB::connection('datarepo')->select("call sp_get_game_result_history
        ( 
            :game_name,
            :result_id,
            :start,
            :length,
            :start_date,
            :end_date
        )", [
            'game_name' => $params->game_name,
            'result_id' => request()->result_id,
            'start' => $params->start,
            'length' => $params->limit,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
        ]);

        $resultCount = DB::connection('datarepo')->select("call sp_get_count_game_result_history
        (
            :game_name,
            :result_id,
            :start_date,
            :end_date
        )", [
            'game_name' => $params->game_name,
            'result_id' => request()->result_id,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }
}
