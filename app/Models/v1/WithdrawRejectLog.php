<?php

namespace App\Models\v1;

use DB;
use App\BaseModels\v1\IChips;

class WithdrawRejectLog extends IChips
{
    const ACTION_DUPLICATE = 'DUPLICATE';
    
    const ACTION_REJECT = 'REJECT';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'user_type',
        'action', //DUPLICATE, REJECT
        'remark', // Computer IP
        'transfer_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeGetRejectedLog($query, $params)
    {
        $resultData = DB::connection('ichips')->select("call sp_paginate_admin_get_rejected_log_datatable
        (
            :username,
            :transfer_id,
            :remark,
            :date_start,
            :date_end,
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'username' => isset($params->username) && $params->username !== '' ? $params->username : '',
            'transfer_id' => isset($params->transfer_id) && $params->transfer_id !== '' ? $params->transfer_id : '',
            'remark' => isset($params->remark) && $params->remark !== '' ? $params->remark : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by'],
        ]);

        $resultCount = DB::connection('ichips')->select("call sp_get_count_admin_get_rejected_log_datatable
        (
            :username,
            :transfer_id,
            :remark,
            :date_start,
            :date_end
        )", [
            'username' => isset($params->username) && $params->username !== '' ? $params->username : '',
            'transfer_id' => isset($params->transfer_id) && $params->transfer_id !== '' ? $params->transfer_id : '',
            'remark' => isset($params->remark) && $params->remark !== '' ? $params->remark : '',
            'date_start' => isset($params->date_from) && $params->date_from !== '' ? $params->date_from : '',
            'date_end' => isset($params->date_to) && $params->date_to !== '' ? $params->date_to : '',
        ]);
        
        return [
            'data' => $resultData,
            'count' => $resultCount,
        ];
        // return $query->select($params['table_columns'])
        //     ->when(request()->date_from && request()->date_to, function ($query) use ($params) {
        //         $query->whereBetween('created_at', [request()->date_from, request()->date_to]);
        //     })
        //     ->when(request()->username, function ($query) {
        //         $query->where('username', 'LIKE', '%'. request()->username . '%');
        //     })
        //     ->when(request()->transfer_id, function ($query) {
        //         $query->where('transfer_id', 'LIKE', '%'. request()->transfer_id . '%');
        //     })
        //     ->when(request()->remark, function ($query) {
        //         $query->where('remark', 'LIKE', '%'. request()->remark . '%');
        //     });
    }
}
