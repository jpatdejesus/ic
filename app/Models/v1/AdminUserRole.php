<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChipsAdmin;

class AdminUserRole extends IChipsAdmin
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'role_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
