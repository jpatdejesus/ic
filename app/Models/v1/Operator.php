<?php

namespace App\Models\v1;

use App\BaseModels\v1\IChips;
use Illuminate\Support\Facades\DB;

class Operator extends IChips
{
    private const QUERY_TYPE_ID = 'ID';
    private const QUERY_TYPE_NAME = 'NAME';

    protected $primaryKey = 'UserId';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LoginName',
        'RealName',
        'Pwd',
        'StateId',
        'Remark',
        'LastLogin',
        'LastModify',
        'LastModifiedBy',
        'CheckSumUser',
        'ExpireTime',
        'ParentID',
        'Path',
        'Layer',
        'OccupyRate',
        'UserType',
        'VideoBetLimitIDs',
        'RouletteBetLimitIDs',
        'SportsBetLimitIDs',
        'ElectronicGameBetLimitIDs',
        'LotteryGameBetLimitIDs',
        'limits',
        'SessionID',
        'telephone',
        'QQ',
        'Email',
        'WithdrawalPassword',
        'CredentialsType',
        'CredentialsNumber',
        'SubParentID',
        'BankID',
        'BankName',
        'BankNumber',
        'BankProvince',
        'BankCity',
        'PromotionURL',
        'PromotionOtherURL',
        'Prefix',
        'SiteNumber',
        'userkey',
        'commendCode',
        'domain',
        'curMoney1',
        'isPay',
        'whkind',
        'xfkind',
        'Regdate',
        'platformbetcontrol'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'WithdrawalPassword',
        'Pwd'
    ];

    public $timestamps = false;

    public function scopeFilterOperator($query, $params)
    {
        if ((int)$params->searchParams['param'] &&
            is_numeric($params->searchParams['param']) &&
            $params->searchParams['param'] !== '') {
            $params->query_type = self::QUERY_TYPE_ID;
            /*return $query->select($params['table_columns'])
                ->where('operators.UserId', $params['param']);*/
        } else {
            $params->query_type = self::QUERY_TYPE_NAME;
            /*return $query->select($params['table_columns'])
                ->orWhere('operators.LoginName', 'LIKE', '%'. $params['param'] .'%')
                ->orWhere('operators.RealName', 'LIKE', '%'. $params['param'] .'%');*/
        }

        $resultData = DB::connection('ichips')->select("call sp_paginate_admin_operator_inquiry_datatable
        (
            :query_type, 
            :param, 
            :start,
            :length,
            :order_by,
            :order_sort
        )", [
            'query_type' => $params->query_type,
            'param' => isset($params->searchParams['param']) && $params->searchParams['param'] !== '' ? $params->searchParams['param'] : '',
            'start' => $params->start,
            'length' => $params->limit,
            'order_by' => $params->sort['column'],
            'order_sort' => $params->sort['order_by']
        ]);

        $resultCount = DB::connection('ichips')->select("call sp_get_count_admin_operator_inquiry_datatable
        (
            :query_type, 
            :param
        )", [
            'query_type' => $params->query_type,
            'param' => isset($params->searchParams['param']) && $params->searchParams['param'] !== '' ? $params->searchParams['param'] : ''
        ]);

        return [
            'data' => $resultData,
            'count' => $resultCount
        ];
    }
}
