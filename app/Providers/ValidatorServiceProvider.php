<?php

namespace App\Providers;

use App\Rules\V1\AdminNameRegexRule;
use App\Rules\V1\BetCodeRule;
use App\Rules\V1\CurrentPasswordValidationRule;
use App\Rules\V1\GameCodeRule;
use App\Rules\V1\NameRegexRule;
use App\Rules\V1\NewPasswordValidationRule;
use App\Rules\V1\NicknameRegexRule;
use App\Rules\V1\ResultRule;
use App\Rules\V1\RoleGroupRule;
use App\Rules\V1\UserStatusRule;
use App\Rules\V1\PasswordRegexRule;
use App\Rules\V1\UsernameRegexRule;
use App\Rules\V1\AdminRoleNameRegexRule;
use App\Rules\V1\MaxBalanceRule;
use App\Rules\V1\MoneyRegexRule;
use App\Rules\V1\ResultIdRule;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Validator::extend('username_regex', function ($attribute, $value, $parameters, $validator) {
            return (new UsernameRegexRule)->passes($attribute, $value);
        });
        Validator::extend('password_regex', function ($attribute, $value, $parameters, $validator) {
            return (new PasswordRegexRule())->passes($attribute, $value);
        });
        Validator::extend('admin_name_regex', function ($attribute, $value, $parameters, $validator) {
            return (new AdminNameRegexRule())->passes($attribute, $value);
        });
        Validator::extend('name_regex', function ($attribute, $value, $parameters, $validator) {
            return (new NameRegexRule())->passes($attribute, $value);
        });
        Validator::extend('nickname_regex', function ($attribute, $value, $parameters, $validator) {
            return (new NicknameRegexRule())->passes($attribute, $value);
        });
        Validator::extend('user_status', function ($attribute, $value, $parameters, $validator) {
            return (new UserStatusRule())->passes($attribute, $value);
        });
        Validator::extend('bet_code', function ($attribute, $value, $parameters, $validator) {
            return (new BetCodeRule())->passes($attribute, $value);
        });
        Validator::extend('game_code', function ($attribute, $value, $parameters, $validator) {
            return (new GameCodeRule())->passes($attribute, $value);
        });
        Validator::extend('result', function ($attribute, $value, $parameters, $validator) {
            return (new ResultRule())->passes($attribute, $value);
        });
        Validator::extend('role_group', function ($attribute, $value, $parameters, $validator) {
            return (new RoleGroupRule())->passes($attribute, $value);
        });
        Validator::extend('current_password', function ($attribute, $value, $parameters, $validator) {
            return (new CurrentPasswordValidationRule())->passes($attribute, $value);
        });
        Validator::extend('new_password', function ($attribute, $value, $parameters, $validator) {
            return (new NewPasswordValidationRule())->passes($attribute, $value);
        });
        Validator::extend('role_name_regex', function ($attribute, $value, $parameters, $validator) {
            return (new AdminRoleNameRegexRule())->passes($attribute, $value);
        });
        Validator::extend('max_balance', function ($attribute, $value, $parameters, $validator) {
            return (new MaxBalanceRule())->passes($attribute, $value);
        });
        Validator::replacer('max_balance', function ($message, $attribute, $rule, $parameters) {
            $rule = new MaxBalanceRule();

            return str_replace(':balance', $rule->getBalance(), $message);
        });
        Validator::extend('money', function ($attribute, $value, $parameters, $validator) {
            $rule = new MoneyRegexRule();
            $rule->setParameters($parameters);

            return $rule->passes($attribute, $value);
        });
        Validator::replacer('money', function ($message, $attribute, $rule, $parameters) {
            $rule = new MoneyRegexRule();
            $decimal = isset($parameters[0]) ? (int) $parameters[0] : $rule->getDefaultDecimal();
            $parameters[0] = $decimal;

            return str_replace([':decimal'], $parameters, $message);
        });
        Validator::extend('result_id', function ($attribute, $value, $parameters, $validator) {
            return (new ResultIdRule())->passes($attribute, $value);
        });
    }
}
