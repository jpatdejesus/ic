<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\UserType\EntityService;

class EntityServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\v1\UserRepositoryInterface', function () {
            $usersEntityService = new EntityService('users');

            return app(get_class($usersEntityService->getRepository()));
        });

        $this->app->bind('App\Repositories\v1\CashFlowRepositoryInterface', function () {
            $cashflowEntityService = new EntityService('cashflow');

            return app(get_class($cashflowEntityService->getRepository()));
        });

        $this->app->bind('App\Repositories\v1\TransferRepositoryInterface', function () {
            $transferEntityService = new EntityService('transfer');

            return app(get_class($transferEntityService->getRepository()));
        });
    }

    public function boot()
    {
    }
}
