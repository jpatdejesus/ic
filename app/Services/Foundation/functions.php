<?php

use Carbon\Carbon;

if (!function_exists('sanitize_url')) {
    /**
     * Sanitize URL. Add trailing slash.
     *
     */
    function sanitize_url($url, $slug = '')
    {
        return rtrim($url, '/') . '/' . ltrim($slug, '/');
    }
}

if (!function_exists('array_keys_exists')) {
    /**
     * compare if all keys exist
     */
    function array_keys_exists(array $keys, array $compare)
    {
        return !array_diff_key(array_flip($keys), $compare);
    }
}

if (!function_exists('check_production')) {
    /**
     * @return bool
     */
    function check_production()
    {
        if (config('app.env') === 'production' && config('app.debug') === false) {
            return true;
        }

        return false;
    }
}

if (!function_exists('request')) {
    /**
     * Get an instance of the current request or an input item from the request.
     *
     * @param  array|string  $key
     * @param  mixed   $default
     * @return \Illuminate\Http\Request|string|array
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        $value = app('request')->__get($key);

        return is_null($value) ? value($default) : $value;
    }
}

//https://laracasts.com/discuss/channels/lumen/lumen-get-route-parameters-value?page=1
if (!function_exists('route_parameter')) {
    /**
     * Get a given parameter from the route.
     *
     * @param $name
     * @param null $default
     * @return mixed
     */
    function route_parameter($name, $default = null)
    {
        $routeInfo = app('request')->route();

        return array_get($routeInfo[2], $name, $default);
    }
}

if (!function_exists('get_user_type_model')) {
    /**
     * Get model by user type
     *
     * @param $userType
     * @param $entity
     * @return mixed
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\UserType\UserTypeConfigException
     */
    function get_user_type_model($userType, $entity)
    {
        return App\Services\UserType\ModelFactory::make($userType, $entity);
    }
}

if (!function_exists('get_user_type_repository')) {
    /**
     * Get repository by user type
     *
     * @param $userType
     * @param $entity
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\UserType\UserTypeConfigException
     * @return mixed
     */
    function get_user_type_repository($userType, $entity)
    {
        return App\Services\UserType\RepositoryFactory::make($userType, $entity);
    }
}

if (!function_exists('get_app_type_model')) {
    /**
     * Get model by user type
     *
     * @param $appType
     * @param $entity
     * @return mixed
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\AppType\ConfigException
     * @throws \App\Services\AppType\ModelNotFoundException
     */
    function get_app_type_model($appType, $entity)
    {
        return App\Services\AppType\ModelFactory::make($appType, $entity);
    }
}

if (!function_exists('get_app_type_repository')) {
    /**
     * Get repository by user type
     *
     * @param $appType
     * @param $entity
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\AppType\ConfigException
     * @return mixed
     */
    function get_app_type_repository($appType, $entity)
    {
        return App\Services\AppType\RepositoryFactory::make($appType, $entity);
    }
}

if (!function_exists('get_role_management_app_type_model')) {
    /**
     * Get model by user type
     *
     * @param $appType
     * @param $entity
     * @return mixed
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\RoleManagement\ConfigException
     * @throws \App\Services\RoleManagement\ModelNotFoundException
     */
    function get_role_management_app_type_model($appType, $entity)
    {
        return App\Services\RoleManagement\ModelFactory::make($appType, $entity);
    }
}

if (!function_exists('get_role_management_app_type_repository')) {
    /**
     * Get repository by user type
     *
     * @param $appType
     * @param $entity
     * @throws App\Services\EntityResolver\EntityNotFoundException
     * @throws \App\Services\RoleManagement\ConfigException
     * @return mixed
     */
    function get_role_management_app_type_repository($appType, $entity)
    {
        return App\Services\RoleManagement\RepositoryFactory::make($appType, $entity);
    }
}

if (!function_exists('route_info')) {
    /**
     * Get a given parameter from the route.
     *
     * @param String $name
     * @param Null $default
     * @return String|Null
     */
    function route_info($name, $default = null)
    {
        $routeInfo = app('request')->route();

        return array_get($routeInfo[1], $name, $default);
    }
}

if (!function_exists('format_config_ip_whitelists')) {
    /**
     * @return bool
     */
    function format_config_ip_whitelists($ips)
    {
        return explode(',', $ips);
    }
}

if (!function_exists('is_ip_whitelisted')) {
    /**
     * @return bool
     */
    function is_ip_whitelisted()
    {
        // $ip = $_SERVER['REMOTE_ADDR'];
        $ip = get_client_ip_address();
        $whitelist = config('ichips.ip_whitelist');

        if (in_array($ip, $whitelist, true)) {
            return true;
        }

        //for wildcard
        foreach ($whitelist as $whitelistedIp) {
            $whitelistedIp = (string) $whitelistedIp;
            $wildcardPosition = strpos($whitelistedIp, "*");

            if ($wildcardPosition === false) {
                continue;
            }

            if (substr($ip, 0, $wildcardPosition) . "" === substr($whitelistedIp, 0, $wildcardPosition) . "") {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('get_client_ip_address')) {
    /**
     * @return mixed
     */
    function get_client_ip_address()
    {
        $ipaddress = '';

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }
}

if (!function_exists('generate_random_string')) {
    function generate_random_string($length = 8)
    {
        return substr(str_shuffle(str_repeat($x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._ ', ceil($length/strlen($x)))), 1, $length);
    }
}

if (!function_exists('pc_permute')) {
    function pc_permute($items, $perms = [])
    {
        $possibilities = '';
        if (empty($items)) {
            $possibilities = join('', $perms) . ",";
        } else {
            for ($i = count($items) - 1; $i >= 0; --$i) {
                $newitems = $items;
                $newperms = $perms;
                list($foo) = array_splice($newitems, $i, 1);
                array_unshift($newperms, $foo);
                $possibilities .= pc_permute($newitems, $newperms);
            }
        }
        return $possibilities;
    }
}

if (!function_exists('in_array_r')) {
    function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $key => $item) {
            if (($strict ? $key === $needle : $key == $needle) || (is_array($key) && in_array_r($needle, $key, $strict))) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('get_truncated_val')) {
    function get_truncated_val($number, $prec = 5)
    {
        $number = (string)$number;
        if (strpos($number, '.') === false) {
            $number .= '.00000';
        }

        $number_arr = explode('.', $number);

        $decimals = substr($number_arr[1], 0, $prec);
        if ($decimals === false) {
            $decimals = '0';
        }

        if (strlen($decimals) < $prec) {
            $decimals = str_pad($decimals, $prec, '0', STR_PAD_RIGHT);
        }

        return $number_arr[0] . '.' . $decimals;
    }
}

if (!function_exists('get_last_record_id')) {
    function get_last_record_id(...$args)
    {
        if ($args[1] > count($args[2]['data'])) {
            return 0;
        }
        return $args[0] + $args[1];
    }
}

if (!function_exists('to_utc_tz')) {
    function to_utc_tz($datetime)
    {
        return Carbon::createFromFormat(
            'Y-m-d H:i:s',
            $datetime,
            config('ichips.server_timezone')
        )->setTimezone(
            config('app.timezone')
        );
    }
}
