<?php

namespace App\Services\Log;

use Illuminate\Database\Eloquent\Model;

class LogService
{
    /**
     * Database connection to use for logging
     */
    const DATABASE_CONNECTION = 'logs';

    public $logModel;

    /**
     * Log service constructor
     *
     * @param string $logTableName
     * @param array $data
     */
    public function __construct(Model $logModel, string $logTableName)
    {
        $this->logModel = $logModel;
        
        $this->logModel->setTable($logTableName);
    }

    /**
     * Check first if route should have log then Call create log
     *
     * @param array $data
     * @return void
     */
    public function log(array $data)
    {
        $this->logModel->create($data);
    }
}
