<?php

namespace App\Services\Log;

use Carbon\Carbon;

class TableNameBuilder
{
    /**
     * Build log table name
     *
     * @param string $routeName
     * @return string
     */
    public static function build($routeName)
    {
        $date = Carbon::now()->format('Ymd');
        $routeName = self::snakeCase($routeName);
        $routeName = $routeName ?: 'route_has_no_name';
        $routeName = $routeName . '_log_'. $date;
        return $routeName;
    }

    public static function snakeCase($string)
    {
        $string = str_replace(' ', '', $string);
        return strtolower(
            preg_replace(
                ["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"],
                ["_$1", "_$1_$2"],
                lcfirst($string)
            )
        );
    }
}
