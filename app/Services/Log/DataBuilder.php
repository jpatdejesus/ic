<?php

namespace App\Services\Log;

class DataBuilder
{
    public $request;
    public $response;

    public function __construct($request, $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Build log data
     *
     * @return array
     */
    public function build()
    {
        return [
            'api_consumer_username' => $this->getApiConsumerUsername(),
            'username' => $this->getUsername(),
            'user_type' => $this->getUserType(),
            'request_content' => json_encode($this->request->all()),
            'response_content' => $this->response->content(),
            'remark' => '',
        ];
    }

    /**
     * Get username from request
     *
     * @return string
     */
    private function getUsername()
    {
        if (route_parameter('username')) {
            return route_parameter('username');
        }
        if ($this->request->has('username')) {
            return $this->request->username;
        }
        return "";
    }

    /**
     * Get user_type from request
     *
     * @return string
     */
    private function getUserType()
    {
        if (route_parameter('user_type')) {
            return route_parameter('user_type');
        }
        if ($this->request->has('user_type')) {
            return $this->request->user_type;
        }
        return "";
    }

    /**
     * Get api consumer username
     *
     * @return string
     */
    private function getApiConsumerUsername()
    {
        if (is_null($this->request) || is_null($this->request->user_data)) {
            return "";
        }
        return $this->request->user_data->username;
    }
}
