<?php

namespace App\Services\Log;

/**
 * Create log table runner
 */
class TableQueryBuilder
{
    const SQL_STR_TO_REPLACE = '{TABLE_NAME}';
    const SQL_FILE_PATH = 'sql/logs/2019_04_03_063122_create_logs_table.sql';

    /**
     * Table name to be created
     */
    protected $tableName;

    /**
     * Table query builder constructor
     *
     * @param string $tableName
     */
    public function __construct(string $tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * Build sql query
     *
     * @return string
     */
    public function build()
    {
        $sqlFilePath = $this->appendDatabaseAbsolutePath(self::SQL_FILE_PATH);

        $templateQuery = $this->getQuery($sqlFilePath);

        return $this->replaceTableName(self::SQL_STR_TO_REPLACE, $this->tableName, $templateQuery);
    }

    /**
     * Get query based on the provided filepath
     *
     * @param string $filePath

     * @return string|boolean
     */
    private function getQuery(string $filePath)
    {
        return file_get_contents($filePath);
    }

    /**
     * Replace template query table name
     *
     * @param string $search
     * @param string $replace
     * @param string $query
     * @return string|boolean
     */
    private function replaceTableName(string $search, string $replace, string $query)
    {
        return str_replace($search, $replace, $query);
    }

    /**
     * Append dabase folder absolute path to the sql file path
     *
     * @param string $filePath
     * @return string
     */
    protected function appendDatabaseAbsolutePath(string $filePath)
    {
        return database_path($filePath);
    }
}
