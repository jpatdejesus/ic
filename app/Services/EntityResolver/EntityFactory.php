<?php

namespace App\Services\EntityResolver;

use Illuminate\Support\Facades\Config;

/**
 *
 */
class EntityFactory
{
    public static function make(string $namespace)
    {
        if (class_exists($namespace)) {
            return new $namespace;
        }
        throw new EntityNotFoundException();
    }
}
