<?php

namespace App\Services\SqlRunner;

use Illuminate\Database\Connection;

class Runner implements Runnable
{

    /**
     * Database connection
     *
     * @var Connection
     */
    protected $dbConnection;

    /**
     * Sql file Runner constructor
     *
     * @param Connection $dbConnection
     */
    public function __construct(Connection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    /**
     * Run/execute sql file based on the sqlPath provided
     *
     * @param string $query
     * @return boolean
     */
    public function run(string $query)
    {
        try {
            if (!$query) {
                return false;
            }

            $this->dbConnection->beginTransaction();

            $this->dbConnection->unprepared($query);
            
            $this->dbConnection->commit();

            return true;
        } catch (\Exception $exception) {
            if ($this->dbConnection->transactionLevel() > 0) {
                $this->dbConnection->rollBack();
            }
            \Log::error($exception->getTraceAsString());
            return false;
        }
    }
}
