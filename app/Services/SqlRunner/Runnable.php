<?php

namespace App\Services\SqlRunner;

interface Runnable
{
    /**
     * Run sql file
     *
     * @return void
     */
    public function run(string $query);
}
