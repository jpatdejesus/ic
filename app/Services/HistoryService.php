<?php

namespace App\Services;

use App\Exceptions\ValidationResponseException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

class HistoryService
{
    const SUCCESS_MESSAGE = 'Success!';
    const PARAMETER_REQUIRED_ERROR = 'This parameter is required.';
    const PARAMETER_INVALID_ERROR = 'This parameter is invalid.';
    const PARAMETER_TYPE_ERROR = 'This parameter only accepts numeric values.';
    const PARAMETER_VALUE_ERROR = 'This parameter only accepts 25, 50, 100, 1000, 2000, 3000, 5000].';
    const PARAMETER_LENGTH_REQUIRED_ERROR = 'The length field is required';
    const PARAMETER_START_REQUIRED_ERROR = 'The start field is required';

    public $lengthChoices = [25, 50, 100, 1000, 2000, 3000, 5000];
    public $start = 1;
    public $start_date = '';
    public $length = 50;
    public $errors = [];
    public $message;

    public function validateParams()
    {
        $trace = debug_backtrace();
        if (isset($trace[2]) && isset($trace[2]['class'])) {
            $class = $trace[2]['class'];
            $class = str_replace('App\Http\v1\Client\Controllers\\', '', $class);
            $class = strtolower(str_replace('Controller', '', $class));
        }

        // Validate start parameter
        if (!is_null(request()->query('start'))) {
            if (!is_numeric(request()->query('start'))) {
                $this->errors['start'][] = self::PARAMETER_TYPE_ERROR;
            } else {
                if (request()->query('start') > 0) {
                    $this->start = (int) request()->query('start');
                }
            }
        } else {
            $this->errors['start'][] = self::PARAMETER_START_REQUIRED_ERROR;
        }

        // Validate length parameter
        if (!is_null(request()->query('length'))) {
            if (!is_numeric(request()->query('length'))) {
                $this->errors['length'][] = self::PARAMETER_TYPE_ERROR;
            } else {
                if (in_array(request()->query('length'), $this->lengthChoices)) {
                    $this->length = (int) request()->query('length');
                } else {
                    $this->errors['length'][] = self::PARAMETER_VALUE_ERROR;
                }
            }
        } else {
            $this->errors['length'][] = self::PARAMETER_LENGTH_REQUIRED_ERROR;
        }

        //validate result_id parameter
        if (!is_null(request()->result_id) && strlen(request()->result_id) > 50) {
            $this->errors['result_id'][] = 'The result_id field may not be greater than 50 characters.';
        }

        // //Validate start date parameter
        // if (request()->query('start_date') !== null) {
        //     if (request()->query('start_date') !== "" ) {
        //         if (!is_null(request()->query('start_date')) && !is_null(request()->query('end_date'))) {
        //             $this->start_date = request()->query('start_date');
        //         } else {
        //             $this->errors['end_date'][] = 'The end_date field is required.';
        //         }
        //     } else {
        //         $this->errors['start_date'][] = 'The start_date field is required.';
        //     }
        // }

        // //Validate end date parameter
        // if (request()->query('end_date') !== null) {
        //     if (request()->query('end_date') !== "" ) {
        //         if (!is_null(request()->query('end_date')) && !is_null(request()->query('start_date'))) {
        //             $this->end_date = request()->query('end_date');
        //         } else {
        //             $this->errors['start_date'][] = 'The start_date field is required.';
        //         }
        //     } else {
        //         $this->errors['end_date'][] = 'The end_date field is required.';
        //     }
        // }

        if (count($this->errors) > 0) {
            throw new ValidationResponseException(
                "Get {$class} history fail.",
                $this->errors
            );
        }
    }

    /*public function formatResponse(Builder $queryBuilder, $message = self::SUCCESS_MESSAGE)
    {
        $rowCount = $queryBuilder->get()->count();
        $queryBuilder = $this->paginate($queryBuilder);

        return response()->json([
            'message' => $message,
            'count_records' => $rowCount,
            'count_results' => $queryBuilder->get()->count(),
            'data' => $queryBuilder->get(),
        ], Response::HTTP_OK);
    }

    public function formatGameResultResponse(\App\Models\v1\GameResult $gameResult, $message = self::SUCCESS_MESSAGE)
    {
        $rowCount = $gameResult->get()->count();
        $gameResult = $this->paginate($gameResult);
        return response()->json([
            'message' => $message,
            'count_records' => $rowCount,
            'count_results' => $gameResult->get()->count(),
            'data' => $gameResult->get(),
        ], Response::HTTP_OK);
    }*/

    /*private function paginate(&$query)
    {
        $limit = $this->length;
        $start = $this->start;
        $end = $start == 1 ? $limit : ($start + $limit);

        $query = $query->whereBetween('id', [$start, $end]);

        return $query;
    }*/

    public function build($result)
    {
        $count = $result['count'][0]->row_count;

        return [
            'message' => self::SUCCESS_MESSAGE,
            'count_records' => $count ? $result['count'][0]->row_count : 0,
            'count_results' =>  $count ? count($result['data']) : 0,
            'last_record_id' => $count ? get_last_record_id($this->start, $this->length, $result) : 0,
            'data' => $count ? $result['data'] : [],
        ];
    }

    public function buildParams($message = self::SUCCESS_MESSAGE)
    {
        $this->validateParams();
        
        $limit = $this->length;
        $start = $this->start - 1;
        $end = $start == 1 ? $limit : ($start + $limit);

        $params = new \stdClass();
        $params->start = $start;
        $params->end = $end;
        $params->limit = $limit;
        $params->message = $message;

        return $params;
    }
}
