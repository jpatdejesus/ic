<?php
namespace App\Services;

use Illuminate\Http\Request;
use App\Services\UserType\EntityService;
use App\Exceptions\RepositoryNotFoundException;
use App\Exceptions\ValidationResponseException;
use App\Exceptions\RepositoryBadRequestException;
use Validator;

class CsvService
{
    /* MESSAGE_INVALID_FORMAT and MESSAGE_NO_FILE is for batch update */
    const MESSAGE_INVALID_FORMAT = 'Invalid format. Please check the file content.';
    
    const MESSAGE_NO_FILE = 'No uploaded file';

    const MESSAGE_PLAYER_NOT_FOUND = 'Player not found.';

    /**
     * @var mixed
     */
    protected $userRepository;

    /**
     * Set entityt services needed
     *
     */
    private function setEntityServices()
    {
        $userEntityService = new EntityService('users');

        $this->userRepository = $userEntityService->getRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function parseCsv(Request $request)
    {
        $this->setEntityServices();

        if ($request->file('file')) {
            $filename = $request->file('file')->getRealPath();
            $withHeader = $request['with_header'];
            $userType = $request['user_type'];
            $type = $request['type'];
            $batchData = array();

            /* Set header index */
            $username = 0;
            $amount = 1;
            $category = 2;
            $reason = 3;

            $handle = fopen($filename, "r");
            while (($data = fgetcsv($handle)) !== false) {
                 // Skip header row and set index for each header
                if ($withHeader == 'true') {
                    $withHeader = 'false';
                    $headerCollection = collect($data)->map(function ($name) {
                        return strtolower($name);
                    });
                    $username = $headerCollection->search('username');
                    $amount = $headerCollection->search('amount');
                    $category = $headerCollection->search('category');
                    $reason = $headerCollection->search('reason');
                    continue;
                }
                
                /* Check if user exist */
                if ($this->userRepository->isUserExist($data[$username])) {
                    $rowData = [
                        'username' => $data[$username],
                        'amount' => $data[$amount],
                        'category' => $data[$category],
                        'reason' => $data[$reason],
                        'user_type' => $userType,
                        'type' => $type
                    ];

                    $validator = Validator::make($rowData, [
                        'username' => 'required',
                        'amount' => 'required|money|numeric',
                        'category' => 'required',
                        'reason' => 'required|max:255',
                    ]);

                    if ($validator->fails()) {
                        throw new RepositoryBadRequestException(self::MESSAGE_INVALID_FORMAT);
                    }

                    array_push($batchData, $rowData);
                } else {
                    throw new RepositoryNotFoundException(self::MESSAGE_PLAYER_NOT_FOUND);
                }
            }
            fclose($handle);
           
            return $batchData;
        } else {
            throw new RepositoryNotFoundException(self::MESSAGE_NO_FILE);
        }
    }
}
