<?php

namespace App\Services\UserType;

use Illuminate\Support\Facades\Config;
use App\Services\EntityResolver\EntityFactory;

/**
 *
 */
class ModelFactory
{
    public static function make($userType, $entity)
    {
        $model = Config::get('ichips.user_types.' . $userType . '.' . $entity . '.model');

        if ($model) {
            return EntityFactory::make($model);
        }

        throw new ConfigException();
    }
}
