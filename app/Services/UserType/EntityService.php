<?php

namespace App\Services\UserType;

use App\Exceptions\RepositoryNotFoundException;

class EntityService
{
    const REPORT_MESSAGE = 'Repository not integrated.';

    private $entity;

    private $model;

    private $repository;

    /**
     * EntityService constructor.
     *
     * @param string $entity
     * @throws RepositoryNotFoundException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function __construct(string $entity)
    {
        $this->setEntity($entity);

        $this->model = get_user_type_model(request()->user_type, $this->getEntity());
        $this->repository = get_user_type_repository(request()->user_type, $this->getEntity());

        $this->report();
    }

    public function setEntity(string $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @throws RepositoryNotFoundException
     */
    protected function report() :void
    {
        if (!$this->repository) {
            throw new RepositoryNotFoundException(self::REPORT_MESSAGE);
        }
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getRepository()
    {
        return $this->repository;
    }
}
