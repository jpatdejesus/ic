<?php

namespace App\Services\UserType;

use Illuminate\Support\Facades\Config;
use App\Services\EntityResolver\EntityFactory;

/**
 *
 */
class RepositoryFactory
{
    public static function make($userType, $entity)
    {
        $repository = Config::get('ichips.user_types.' . $userType . '.' . $entity . '.repository');

        if ($repository) {
            return EntityFactory::make($repository);
        }
        
        return false;
    }
}
