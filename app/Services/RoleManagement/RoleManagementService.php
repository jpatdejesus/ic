<?php

namespace App\Services\RoleManagement;

use App\Models\v1\AdminRole;
use App\Models\v1\AdminUserRole;
use App\Services\AppType\AppTypeService;

class RoleManagementService
{

    public $response = true;

    protected $roles;
    protected $userRoles;

    public function __construct()
    {
        $rolesEntity = new RoleManagementAppTypeService('roles');
        $this->roles = $rolesEntity->getModel();

        $userRolesEntity = new RoleManagementAppTypeService('user_roles');
        $this->userRoles = $userRolesEntity->getModel();
    }

    /**
     * @return string
     *
     * Returns currently logged in user's role group
     */
    public function getUserRoleGroup()
    {
        return $this->getUserRoleAndRoleGroupDetails()->role_group;
    }

    /**
     * @return string
     *
     * Returns currently logged in user's role name
     */
    public function getUserRole()
    {
        return $this->getUserRoleAndRoleGroupDetails()->name;
    }

    /**
     * @param $roleGroup
     * @return object
     *
     * Returns currently logged in user's role name
     */
    public function getSelectedRoleGroupData($roleGroup)
    {
        return $this->roles->whereRoleGroup(strtoupper($roleGroup))->firstOrFail();
    }

    /**
     * @return array
     *
     *  Returns only allowed roles that has permission for CRUD for role management
     */
    public function getRestrictedRoleGroups()
    {
        $restrictedRoleGroup = [];
        $allowedRoles = ['super_admin', 'admin'];

        foreach (config('ichips.admin_default_roles_and_users') as $roleGroup => $roles) {
            foreach ($roles as $role => $defaultAccounts) {
                if (!in_array(strtolower($roleGroup), $allowedRoles)) {
                    array_push($restrictedRoleGroup, strtoupper($role));
                }
            }
        }

        return $restrictedRoleGroup;
    }

    /**
     * @return object
     *
     * @param $roleGroup
     * Returns roles by role group.
     */
    public function getRolesByRoleGroup($roleGroup = null)
    {
        $roles = new \stdClass();
        $listOfRoles = $this->roles->whereRoleGroup($roleGroup)->get();

        if (count($listOfRoles)) {
            foreach ($listOfRoles->toArray() as $key => $role) {
                $roles->{$role['name']} = $role['name'];
            }
        }

        return $roles;
    }
    
    /**
     * @return array
     *
     * @param $isCustomKeys
     * Returns default role define in ichips config file.
     */
    public function getRoleGroups($isCustomKeys = false)
    {
        $arrRoleGroup = [];
        $requestAppType = strtoupper(request()->request_app_type);

        if ($requestAppType === strtoupper(config('ichips.app_types.client'))) {
            $defaultRoleGroups = config('ichips.default_roles');
        } else {
            $defaultRoleGroups = config('ichips.admin_default_roles_and_users');
        }

        foreach ($defaultRoleGroups as $roleGroup => $roles) {
            if ($isCustomKeys) {
                $arrRoleGroup[strtolower($roleGroup)] = strtoupper($roleGroup);
            } else {
                $arrRoleGroup[] = strtoupper($roleGroup);
            }
        }

        return $arrRoleGroup;
    }

    /**
     * @return array|object
     *
     * Returns currently logged in user's role and role group details
     */
    public function getUserRoleAndRoleGroupDetails()
    {
        $userRoleData = AdminUserRole::whereUserId(request()->user_data->id)->firstOrFail();
        $roleData = AdminRole::whereId($userRoleData->role_id)->firstOrFail();

        return $roleData;
    }
}
