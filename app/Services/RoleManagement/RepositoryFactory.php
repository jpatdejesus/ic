<?php

namespace App\Services\RoleManagement;

use Illuminate\Support\Facades\Config;
use App\Services\EntityResolver\EntityFactory;

/**
 *
 */
class RepositoryFactory
{
    public static function make($appType, $entity)
    {
        $repository = Config::get('ichips.app_type_entities.' . $appType . '.' . $entity . '.repository');

        if ($repository) {
            return EntityFactory::make($repository);
        }
        
        return false;
    }
}
