<?php

namespace App\Services\RoleManagement;

use App\Exceptions\RepositoryNotFoundException;

class RoleManagementAppTypeService
{
    const REPORT_MESSAGE = 'Repository not integrated.';

    private $entity;

    private $model;

    private $repository;

    /**
     * EntityService constructor.
     *
     * @param  string $entity
     * @throws RepositoryNotFoundException
     * @throws \App\Services\EntityResolver\EntityNotFoundException
     */
    public function __construct(string $entity)
    {
        $requestAppType = request()->request_app_type;

        if (!$requestAppType) {
            $requestAppType = strtoupper(config('ichips.app_types.admin'));
        }

        $this->setEntity($entity);
        $this->model = get_role_management_app_type_model($requestAppType, $this->getEntity());
        $this->repository = get_role_management_app_type_repository($requestAppType, $this->getEntity());

        $this->report();
    }

    public function setEntity(string $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @throws RepositoryNotFoundException
     */
    protected function report() :void
    {
        if (!$this->repository) {
            throw new RepositoryNotFoundException(self::REPORT_MESSAGE);
        }
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getRepository()
    {
        return $this->repository;
    }
}
