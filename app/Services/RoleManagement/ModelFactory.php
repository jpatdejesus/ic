<?php

namespace App\Services\RoleManagement;

use Illuminate\Support\Facades\Config;
use App\Services\EntityResolver\EntityFactory;

/**
 *
 */
class ModelFactory
{
    public static function make($appType, $entity)
    {
        $model = Config::get('ichips.app_type_entities.' . $appType . '.' . $entity . '.model');

        if ($model) {
            return EntityFactory::make($model);
        }

        throw new ConfigException();
    }
}
