<?php

namespace App\Services\Datatable;

interface DataTableServiceInterface
{
    /**
     * Set query
     *
     * @return mixed
     */
    public function query();

    /**
     * Set table columns to be outputted
     *
     * @return array
     */
    public function tableColumns() : array;

    /**
     * Set filter columns
     *
     * @return array
     */
    public function searchColumns() : array;

    /**
     * Set sorting columns
     *
     * @return array
     */
    public function sortColumns() : array;

    /**
     * Set group by columns
     *
     * @return array
     */
    public function groupByColumns() : array;

    /**
     * Set datatable request
     *
     * @param null $request
     */
    public function setRequest($request);

    /**
     * Get datatable request
     *
     * @return array|Request|string
     */
    public function getRequest();

    /**
     * Set query condition flag
     *
     * @param $param
     */
    public function setQueryCondition($param);

    /**
     * Get query condition flag
     *
     * @return array|Request|string
     */
    public function getQueryCondition();

    /**
     * Set search parameter key for custom query
     *
     * @param $param
     */
    public function setCustomSearch($param);

    /**
     * Get search parameter key for custom query
     *
     * @return array|Request|string
     */
    public function getCustomSearch();

    /**
     * Getter for $start attribute
     *
     * @return int
     */
    public function getQueryStart() : int;

    /**
     * Getter for $length attribute
     *
     * @return int
     */
    public function getQueryLength() : int;

    /**
     * Setter for $sort attribute
     *
     * @return array
     */
    public function getQuerySort() : array;

    /**
     * DataTable response builder
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function build();
}
