<?php

namespace App\Services\Datatable;

use App\Exceptions\ValidationResponseException;
use Illuminate\Http\Request;

/**
 * Class DataTableService
 *
 * @package App\Services\Datatable
 * @author Ardee Ayes
 */
abstract class DataTableService implements DataTableServiceInterface
{
    const PARAMETER_REQUIRED_ERROR = 'This parameter is required.';
    const PARAMETER_INVALID_ERROR = 'This parameter is invalid.';
    const PARAMETER_NUMERIC_ERROR = 'This parameter only accepts numeric values.';
    const PARAMETER_ARRAY_ERROR = 'This parameter should be an array.';
    const PARAMETER_VALUE_ERROR = 'This parameter only accepts 25, 50, 100, 250, 500, 1000, 2000, 3000.';

    const ORDER_BY_ASC = 'ASC';
    const ORDER_BY_DESC = 'DESC';
    const VALID_ORDER_BY = [self::ORDER_BY_ASC, self::ORDER_BY_DESC];

    const CONDITION_EQUAL = '=';
    const CONDITION_LIKE = 'LIKE';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var array
     */
    private $lengthChoices = [25, 50, 100, 250, 500, 1000, 2000, 3000];

    /**
     * @var int
     */
    private $start = 0; //1

    /**
     * @var int
     */
    private $length = 25;

    /**
     * @var array
     */
    private $sort = [
        'column' => 'id',
        'order_by' => 'DESC'
    ];

    /**
     * @var array
     */
    private $search = [];

    /**
     * @var array
     */
    public $errors = [];

    /**
     * @var array
     */
    private $isAdvancedQuery = true;

    /**
     * @var array
     */
    private $customSearchParam = 'param';

    /**
     * Set query
     *
     * @return mixed
     */
    abstract public function query();

    /**
     * Set table columns to be outputted
     *
     * @return array
     */
    abstract public function tableColumns() : array;

    /**
     * Set filter columns
     *
     * @return array
     */
    abstract public function searchColumns() : array;

    /**
     * Set sorting columns
     *
     * @return array
     */
    abstract public function sortColumns() : array;

    /**
     * Set group by columns
     *
     * @return array
     */
    abstract public function groupByColumns() : array;

    /**
     * Set datatable request
     *
     * @param null $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * Get datatable request
     *
     * @return array|Request|string
     */
    public function getRequest()
    {
        if ($this->request === null) {
            $this->request = \request();
        }

        return $this->request;
    }

    /**
     * Set query condition flag
     *
     * @param $param
     */
    public function setQueryCondition($param)
    {
        $this->isAdvancedQuery = $param;
    }

    /**
     * Get query condition flag
     *
     * @return array|Request|string
     */
    public function getQueryCondition()
    {
        return $this->isAdvancedQuery;
    }

    /**
     * Set search parameter key for custom query
     *
     * @param $param
     */
    public function setCustomSearch($param)
    {
        $this->customSearchParam = $param;
    }

    /**
     * Get search parameter key for custom query
     *
     * @return array|Request|string
     */
    public function getCustomSearch()
    {
        return $this->customSearchParam;
    }

    private function validateParams()
    {
        // Validate start parameter
        if (!is_null($this->getRequest()->input('start'))) {
            if (!is_numeric($this->getRequest()->input('start'))) {
                $this->errors['start'][] = self::PARAMETER_NUMERIC_ERROR;
            }
        }

        // Validate length parameter
        if (!is_null($this->getRequest()->input('length'))) {
            if (!is_numeric($this->getRequest()->input('length'))) {
                $this->errors['length'][] = self::PARAMETER_NUMERIC_ERROR;
            } elseif (!in_array($this->getRequest()->input('length'), $this->lengthChoices)) {
                $this->errors['length'][] = self::PARAMETER_VALUE_ERROR;
            }
        }

        // Validate sort parameter
        if (!is_null($this->getRequest()->input('sort'))) {
            $sort = $this->getRequest()->input('sort');
            if (!is_array($sort)) {
                $this->errors['sort'][] = self::PARAMETER_ARRAY_ERROR;
            } elseif (!in_array($sort['order_by'], self::VALID_ORDER_BY)) {
                $this->errors['sort'][] = self::PARAMETER_INVALID_ERROR;
            } elseif (!in_array($sort['column'], array_keys($this->sortColumns()))) {
                $this->errors['sort'][] = self::PARAMETER_INVALID_ERROR;
            }
        }

        // Validate search parameter
        if (!is_null($this->getRequest()->input('search'))) {
            $search = $this->getRequest()->input('search');

            if (!is_array($search)) {
                $this->errors['search'][] = self::PARAMETER_ARRAY_ERROR;
            }

            if ($this->getQueryCondition()) {
                foreach ($search as $key => $value) {
                    if (!in_array($key, array_keys($this->searchColumns()))) {
                        $this->errors['search'][$key] = self::PARAMETER_INVALID_ERROR;
                    }
                }
            } else {
                if (!isset($search[$this->getCustomSearch()])) {
                    $this->errors['search'][$this->getCustomSearch()] = self::PARAMETER_INVALID_ERROR;
                }
            }
        }

        if (count($this->errors) > 0) {
            throw new ValidationResponseException(
                "Get datatable fail.",
                $this->errors
            );
        }
    }

    /**
     * Setter for $start attribute
     */
    private function setQueryStart()
    {
        if (!is_null($this->getRequest()->input('start')) && is_numeric($this->getRequest()->input('start'))) {
            $this->start = $this->getRequest()->input('start') == 1 ? 0 : $this->getRequest()->input('start');
        }
    }

    /**
     * Getter for $start attribute
     *
     * @return int
     */
    public function getQueryStart() : int
    {
        return $this->start;
    }

    /**
     * Setter for $length attribute
     */
    private function setQueryLength()
    {
        if (!is_null($this->getRequest()->input('length')) &&
            is_numeric($this->getRequest()->input('length')) &&
            in_array($this->getRequest()->input('length'), $this->lengthChoices)) {
            $this->length = $this->getRequest()->input('length');
        }
    }

    /**
     * Getter for $length attribute
     *
     * @return int
     */
    public function getQueryLength() : int
    {
        return $this->length;
    }

    /**
     * Setter for $sort attribute
     */
    private function setQuerySort()
    {
        if (!is_null($this->getRequest()->input('sort'))) {
            $this->sort = $this->getRequest()->input('sort');
        }
    }

    /**
     * Getter for $length attribute
     *
     * @return array
     */
    public function getQuerySearch()
    {
        return $this->search;
    }

    /**
     * Setter for $sort attribute
     */
    private function setQuerySearch()
    {
        if (!is_null($this->getRequest()->input('search'))) {
            $this->search = $this->getRequest()->input('search');
        }
    }

    /**
     * Setter for default $sort attribute
     *
     * @param string $column
     * @param string $orderBy
     */
    public function setDefaultSortValue(string $column, $orderBy = 'DESC')
    {
        $this->sort['column'] = $column;
        $this->sort['order_by'] = $orderBy;
    }

    /**
     * Setter for $sort attribute
     *
     * @return array
     */
    public function getQuerySort() : array
    {
        return $this->sort;
    }

    /**
     * DataTable response builder
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationResponseException
     */
    public function build()
    {
        return $this->buildData();

        // return response()->json([
        //     'count_records' => $response->count_records,
        //     'count_results' => $response->count_results,
        //     'data' => $response->data
        // ]);
    }

    /**
     * Data parameter builder
     *
     * @return array
     */
    private function buildData()
    {
        $result = $this->query();

        $count = $result['count'][0]->row_count;

        return [
            'count_records' => $count ? $result['count'][0]->row_count : 0,
            'count_results' =>  $count ? count($result['data']) : 0,
            'last_record_id' => $count ? get_last_record_id($this->getQueryStart(), $this->getQueryLength(), $result) : 0,
            'data' => $count ? $result['data'] : [],
        ];
    }

    /**
     * Data initialize
     *
     * @return \stdClass
     * @throws ValidationResponseException
     */
    public function getDataTableParams()
    {
        $this->validateParams();
        $this->setQueryStart();
        $this->setQueryLength();
        $this->setQuerySort();
        $this->setQuerySearch();

        $return = new \stdClass();
        $return->limit = $this->getQueryLength();
        $return->start = $this->getQueryStart(); //> 1 ? $this->getQueryStart() + 1 : 1
        $return->sort = $this->getQuerySort();
        $return->searchParams = $this->getQuerySearch();
        $return->tableColumns = $this->tableColumns();
        $return->sortColumns = $this->sortColumns();
        $return->groupByColumns = $this->groupByColumns();
        $return->sortColumn = $return->sortColumns[$return->sort['column']];
        $return->sortOrder = $return->sort['order_by'];

        return $return;

        /*$return = new \stdClass();
        $sort = $this->getQuerySort();
        $sortColumns = $this->sortColumns();

        if (count($this->getQuerySearch()) > 0) {
            $query = $this->withFilter($query);
        }

        //@todo
        if (!$query) { //IF QUERY IS FALSE, RETURN EMPTY RECORD!
            $return->count_records = 0;
            $return->count_results = 0;
            $return->data = [];
            return $return;
        }

        $return->count_records = $query->groupBy($this->groupByColumns())
            ->orderBy($sortColumns[$sort['column']], $sort['order_by'])
            ->get()
            ->count();

        $return->data = $query->skip($this->getQueryStart() - 1)
            ->take($this->getQueryLength())
            ->groupBy($this->groupByColumns())
            ->orderBy($sortColumns[$sort['column']], $sort['order_by'])
            ->get();
        $return->count_results = $return->data->count();

        return $return;*/
    }

    /**
     * Filter Setter
     *
     * @param $query
     * @return mixed
     */
    private function withFilter($query)
    {
        $searchColumns = $this->searchColumns();
        $search = $this->getQuerySearch();

        if ($this->getQueryCondition()) {
            foreach ($search as $key => $value) {
                if (in_array($key, array_keys($searchColumns))) {
                    $column = $searchColumns[$key];
                    if (strpos(strtolower($column), 'id') && (int)$value) {
                        $query = $query->where($column, self::CONDITION_EQUAL, $value);
                    } else {
                        $query = $query->where($column, self::CONDITION_LIKE, '%' . $value . '%');
                    }
                }
            }
        } else {
            $equalWheres = [];
            $likeWheres = [];

            foreach ($searchColumns as $column) {
                if (is_array($column) && count($column) === 2) {
                    list($columnName, $condition) = $column;

                    if ($condition === self::CONDITION_LIKE) {
                        $likeWheres[$columnName] = $search[$this->getCustomSearch()];
                    } else {
                        $equalWheres[$columnName] = $search[$this->getCustomSearch()];
                    }
                } else {
                    $query = $query->orWhere($column, self::CONDITION_LIKE, '%' . $search[$this->getCustomSearch()] . '%');
                }
            }

            if (count($equalWheres) > 0) {
                if ($query->where($equalWheres)->get()->count() > 0) {
                    $query = $query->where($equalWheres);
                }
            }

            if (count($likeWheres) > 0) {
                if ($query->where($equalWheres)->get()->count() === 0) {
                    foreach ($likeWheres as $key => $value) {
                        $query = $query->orWhere($key, self::CONDITION_LIKE, '%' . $value . '%');
                    }
                }
            }
        }

        return $query;
    }
}
