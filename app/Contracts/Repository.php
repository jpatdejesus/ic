<?php

namespace App\Contracts;

/**
 * Interface Repository
 *
 * @package App\Contracts
 */
interface Repository
{
    /**
     * @return mixed
     */
    public function model();

    /**
     * @param  $model
     * @return mixed
     * @throws RepositoryException
     */
    public function setModel($model);

    /**
     * @return mixed
     * @throws RepositoryException
     */
    public function makeModel();

    /**
     * @param  array $columns
     * @return mixed
     */
    public function all($columns = array('*'));

    /**
     * @param  $id
     * @param  array   $columns
     * @param  boolean $exclude
     * @return mixed
     */
    public function find($id, $columns = array('*'), $exclude = false);

    /**
     * @param  $attribute
     * @param  $value
     * @param  array     $columns
     * @param  boolean   $exclude
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'), $exclude = false);

    /**
     * @param  $attribute
     * @param  $value
     * @param  array     $columns
     * @return mixed
     */
    public function findLastBy($attribute, $value, $columns = array('*'), $exclude = false);

    /**
     * @param  $attribute
     * @param  $value
     * @param  array     $columns
     * @param  boolean   $exclude
     * @return mixed
     */
    public function findAllBy(
        $attribute,
        $value,
        $columns = array('*'),
        $exclude = false,
        $order_col = 'created_at',
        $order_setting = 'asc'
    );

    /**
     * Find a collection of models by the given query conditions.
     *
     * @param array $where
     * @param array $columns
     * @param array $group
     * @param bool  $or
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     *
     * SAMPLE QUERIES:
     */
    public function findWhere($where, $columns = ['*'], $group = [], $or = false);

    /**
     * @param  $join
     * @param  $model
     * @return mixed
     */
    public function buildJoin($join, $model);

    /**
     * @return mixed
     */
    public function getTable();

    /**
     * @param  array $params
     * @return mixed
     */
    public function createNew(array $params);

    /**
     * @param  int   $id
     * @param  array $params
     * @return mixed
     */
    public function update(int $id, array $params);

    /**
     * @param  int $id
     * @return mixed
     */
    public function delete(int $id);
}
