<?php

namespace App\Rules\V1;

use App\Repositories\v1\AdminUserRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;

/**
|--------------------------------------------------------------------------
| Validation rule for current password
|--------------------------------------------------------------------------
|
 * Class CurrentPasswordValidationRule
 * @package App\Rules\V1
 */
class CurrentPasswordValidationRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function passes($attribute, $value)
    {
        $hashedPassword = (new AdminUserRepository())->find(request()->user_data->id)->password;
        if (Hash::check($value, $hashedPassword)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
