<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;
use App\Repositories\v1\GameResultRepository;

/**
|--------------------------------------------------------------------------
| Validation rule for username
|--------------------------------------------------------------------------
| May consist of Upper case, lower case letter and Numbers.
| Special characters are not allowed.
|
 * Class UsernameRegexRule
 * @package App\Rules\V1
 */
class ResultIdRule implements Rule
{
    public $gameResultRepository;

    public function __construct()
    {
        $this->gameResultRepository = new GameResultRepository();
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        $this->gameResultRepository->setTable(request('game_code'));

        if (!$this->gameResultRepository->isTableExist(request('game_code'))) {
            return true;
        }

        if ($this->gameResultRepository->isResultIdUnique($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
    }
}
