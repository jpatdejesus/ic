<?php

namespace App\Rules\V1;

use App\Repositories\v1\GameResultRepository;
use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for result
|--------------------------------------------------------------------------
| May consist of Upper case, lower case letter and Numbers.
| Special characters are not allowed.
|
 * Class ResultRule
 * @package App\Rules\V1
 */
class ResultRule implements Rule
{
    public $gameResultRepository;

    public function __construct()
    {
        $this->gameResultRepository = new GameResultRepository();
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        $this->gameResultRepository->setTable(request('game_code'));

        if (!$this->gameResultRepository->isResultUnique($value)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
    }
}
