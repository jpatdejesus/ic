<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class MaxBalanceRule
 * @package App\Rules\V1
 */
class MaxBalanceRule implements Rule
{
    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        $balance = $this->getBalance();
        if ($value <= $balance) {
            return $balance;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }

    public function getBalance()
    {
        $username = request()->route('username');

        $user = get_user_type_model(request()->user_type, 'users')
            ->where('username', $username)
            ->first();

        return $user->getField('balance');
    }
}
