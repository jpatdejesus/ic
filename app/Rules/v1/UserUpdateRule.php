<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

class UserUpdateRule implements Rule
{
    /**
    * @var int
    */
    protected $params = 0;

    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (count($this->params) <=1) {
            return false;
        } else {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (count($this->params) <= 1) {
            return 'Atleast one parameter should filled.';
        } else {
            return 'Only one specific parameter should be filled.';
        }
    }
}
