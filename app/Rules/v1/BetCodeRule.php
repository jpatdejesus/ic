<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;
use App\Repositories\v1\Transaction\TransactionRepository;

/**
|--------------------------------------------------------------------------
| Validation rule for username
|--------------------------------------------------------------------------
| May consist of Upper case, lower case letter and Numbers.
| Special characters are not allowed.
|
 * Class UsernameRegexRule
 * @package App\Rules\V1
 */
class BetCodeRule implements Rule
{
    public $transactionRepository;

    public function __construct()
    {
        $this->transactionRepository = new TransactionRepository();
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        $this->transactionRepository->setTable(request('game_code'));

        if (!$this->transactionRepository->isTableExist(request('game_code'))) {
            return true;
        }

        if ($this->transactionRepository->isBetCodeUnique($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
    }
}
