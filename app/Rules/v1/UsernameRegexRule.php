<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for username
|--------------------------------------------------------------------------
| May consist of lower case letter and numbers and specific special characters(._)
|
 * Class UsernameRegexRule
 * @package App\Rules\V1
 */
class UsernameRegexRule implements Rule
{
    private $pattern = '/^[a-z0-9._]*$/';

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (preg_match($this->pattern, $value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
