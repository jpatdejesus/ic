<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for name
|--------------------------------------------------------------------------
| May consist of Upper case, lower case letter and specific special characters(.' -_)
|
 * Class NameRegexRule
 * @package App\Rules\V1
 */
class AdminNameRegexRule implements Rule
{
    private $pattern = '/^[\p{L}\p{N}\.\_ \'\-]+$/u';

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (preg_match($this->pattern, $value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
