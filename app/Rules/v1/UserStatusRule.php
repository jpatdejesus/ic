<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class UserStatusRule
 * @package App\Rules\V1
 */
class UserStatusRule implements Rule
{
    private $statuses;

    public function __construct()
    {
        $this->statuses = [
            config('ichips.user_statuses.enabled'),
            config('ichips.user_statuses.disabled'),
            config('ichips.user_statuses.suspended')
        ];
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (in_array($value, $this->statuses)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
