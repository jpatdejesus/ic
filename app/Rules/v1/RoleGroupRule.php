<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;
use App\Services\RoleManagement\RoleManagementService;

/**
 * Class RoleGroupRule
 * @package App\Rules\V1
 */
class RoleGroupRule implements Rule
{
    private $roleGroups;

    public function __construct()
    {
        $this->roleGroups = (new RoleManagementService())->getRoleGroups();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (in_array($value, $this->roleGroups)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
