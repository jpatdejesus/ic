<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for password
|--------------------------------------------------------------------------
| May consist of Upper case, lower case letter and Numbers.
| The following Special characters are allowed. @ # $ % ^ & ! \ *
|
 * Class PasswordRegexRule
 * @package App\Rules\V1
 */
class PasswordRegexRule implements Rule
{
    private $pattern = '/^(?=.*\d)(?=.*[@#$%^&!\*]?)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#$%^&!\*]{8,50}$/i';
    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (preg_match($this->pattern, $value)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
    }
}
