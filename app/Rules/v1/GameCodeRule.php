<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;
use App\Repositories\v1\TransactionRepository;

/**
|--------------------------------------------------------------------------
| Validation rule for game_code
|--------------------------------------------------------------------------
| Value should be in config config.default_games array
 * Class UsernameRegexRule
 * @package App\Rules\V1
 */
class GameCodeRule implements Rule
{
    public $transactionRepository;

    /**
     * Array of game code list
     *
     * @var array
     */
    public $gameCodeList;

    public function __construct()
    {
        $this->gameCodeList = collect(config('ichips.default_games'))->pluck('game_code')->toArray();
    }

    /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
    public function passes($attribute, $value)
    {
        if (in_array($value, $this->gameCodeList)) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
    }
}
