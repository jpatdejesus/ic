<?php

namespace App\Rules\V1;

use Illuminate\Contracts\Validation\Rule;

/**
|--------------------------------------------------------------------------
| Validation rule for money
|--------------------------------------------------------------------------
| Optional decimal and can set specific decimal places
|
 * Class MoneyRegexRule
 * @package App\Rules\V1
 */
class MoneyRegexRule implements Rule
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * @var string
     */
    private $parameters;

    /**
     * @var string
     */
    private $defaultDecimal = 5;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->setPattern();

        if (preg_match($this->getPattern(), $value)) {
            return true;
        }

        return false;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        //
        return 'The :attribute cannot be an empty string.';
    }
    
    private function setPattern()
    {
        $decimal = isset($this->getParameters()[0]) ? (int) $this->getParameters()[0] : $this->defaultDecimal;

        $this->pattern = '/^\d*(\.\d{1,'. $decimal .'})?$/';

        return $this;
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param string $parameters
     *
     * @return self
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultDecimal()
    {
        return $this->defaultDecimal;
    }
}
