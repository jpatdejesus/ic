<?php

namespace App\Repositories\v1;

use App\DataTables\v1\Admin\OperatorInquiryDataTable;
use App\Models\v1\Operator;

class OperatorRepository extends BaseRepository
{
    public function model()
    {
        return Operator::class;
    }

    /**
     * @return array
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getOperator($params)
    {
        return $this->model->filterOperator($params);
        /*$operatorInquiryDataTable = new OperatorInquiryDataTable();
        $operatorInquiryDataTable->setQueryCondition(false);
        $operatorInquiryDataTable->setDefaultSortValue('LastLogin');

        $dataTable = $operatorInquiryDataTable->initialize();
        $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        $params = $dataTable->searchParams;
        $params['table_columns'] = $dataTable->tableColumns;

        $result = [];
        $lastResultID = 0;

        if ($rowCount = $this->model->filterOperator($params)->count()) {
            $lastResultID = $this->model->filterOperator($params)->orderBy('UserId', 'DESC')->limit(1)->first()->UserId;

            $baseResult = $this->model->filterOperator($params)->whereBetween('UserId', [$dataTable->start, $end]);

            while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
                $end += ($dataTable->limit - $baseCount);

                $baseResult = $this->model->filterOperator($params)->whereBetween('UserId', [$dataTable->start, $end]);
            }

            $result = $this->model->filterOperator($params)->whereBetween('UserId', [$dataTable->start, $end])
                ->groupBy($dataTable->groupByColumns)
                ->get();

            $data= $this->model->filterOperator($params)->whereBetween('UserId', [$dataTable->start, $end])
                ->groupBy($dataTable->groupByColumns)
                ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
                ->get();
        }

        return [
            'count_records' => (!empty($result) ? $rowCount : 0),
            'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => (!empty($result) && $result->last() && $result->last()->UserId < $lastResultID ? $result->last()->UserId : 0),
            'data' => (!empty($data) ? $data : []),
        ];*/
    }
}
