<?php

namespace App\Repositories\v1;

use App\Models\v1\WithdrawRejectLog;

class WithdrawRejectLogRepository extends BaseRepository
{
    public function model()
    {
        return WithdrawRejectLog::class;
    }
}
