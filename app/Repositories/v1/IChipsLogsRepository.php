<?php

namespace App\Repositories\v1;

use DB;
use App\Models\v1\Log;
use App\DataTables\v1\Admin\AdminIChipsLogsDataTable;

class IChipsLogsRepository extends BaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed
     */
    public function model()
    {
        return Log::class;
    }
    /**
     *TODO::THIS IS TAPOSIN TO
     * @return mixed
     */
    public function getIChipsLogs($params)
    {
        $params->end_point = request()->end_point;
        $params->date_to = request()->date_to;
        $params->date_from = request()->date_from;
        $params->user_type = request()->user_type;

        return $this->model->getIchipsLogs($params);
        // $param = request();
        // $auditTrailsDataTable = new AdminIChipsLogsDataTable();
        // $dataTable = $auditTrailsDataTable->initialize();

        // $result = [];
        // $lastResultID = 0;
        
        // if ($rowCount = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
        //         $firstResultID = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->orderBy('id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->orderBy('id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
        //             ? $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - ($dataTable->limit - 1);

        //         $baseResult = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$end, $dataTable->start]);
        //         }

        //         $result = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->orderBy('id', 'DESC')->limit(1)->first()->id;

        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$dataTable->start, $end]);
        //         }

        //         $result = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $this->model->getIchipsLogs($param, $dataTable->tableColumns)->whereBetween('id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }
}
