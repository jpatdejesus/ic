<?php

namespace App\Repositories\v1;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\v1\WithdrawBlackList;
use App\Models\v1\WithdrawWhiteList;
use App\Models\v1\WithdrawRejectLog;
use App\Exceptions\ValidationResponseException;
use App\Services\RoleManagement\RoleManagementService;
use App\DataTables\v1\Admin\WithdrawWhiteListDataTable;
use App\DataTables\v1\Admin\WithdrawBlackListDataTable;
use App\DataTables\v1\Admin\WithdrawRejectLogsDataTable;

class WithdrawBlackAndWhiteListRepository extends BaseRepository
{
    const ADD_TO_WHITELIST_SUCCESS = 'User has been added to whitelist.';
    const ADD_TO_WHITELIST_FAILED = 'Failed adding user to whitelist!';
    const WHITELIST_ALREADY_EXISTS_MESSAGE = 'User already exist!';
    const UPDATE_WHITELIST_REQUEST_SUCCESS = 'Whitelist request has been updated.';
    const UPDATE_WHITELIST_REQUEST_FAILED = 'Whitelist request update failed!';

    public function model()
    {
        return WithdrawBlackList::class;
    }

    /**
     * Add user from blacklist to whitelist
     * @param $blackListId
     * @return mixed
     * @throws ValidationResponseException
     */
    public function addUserToWhitelist($blackListId)
    {
        $status = WithdrawWhiteList::STATUS['pending'];
        $userData = request()->user_data;
        $blackListData = (new WithdrawBlackList())->find($blackListId)->first();
        $isWhiteListExists = (new WithdrawWhiteList())
            ->whereUsername($blackListData['username'])
            ->whereUserType($blackListData['user_type'])
            ->whereNull('deleted_at')->first();

        if ($isWhiteListExists) {
            throw new ValidationResponseException(self::WHITELIST_ALREADY_EXISTS_MESSAGE, []);
        }

        $createParams = [
            'username' => $blackListData['username'],
            'user_type' => $blackListData['user_type'],
            'whitelisted_by' => $userData->email,
            'remark' => $blackListData['remark'],
        ];

        if (strtoupper($userData->role_group) === (new RoleManagementService())->getRoleGroups(true)['super_admin']) {
            $status = WithdrawWhiteList::STATUS['approved'];
            $createParams['expire_time'] = Carbon::now()->addHours(2)->toDateTimeString();
            $createParams['updated_by'] = $userData->email;
            $createParams['date_approved'] = Carbon::now()->toDateTimeString();
        }

        $createParams['status'] = $status;

        $this->deleteExpiredWhiteListItems();

        if ($response = (new WithdrawWhiteList())->create($createParams)) {
            $blackListData->delete();
            return $response->toArray();
        }

        return false;
    }

    /**
     * @return array
     * @throws ValidationResponseException
     */
    public function getBlackList($params)
    {
        return (new WithdrawBlackList())->getBlackLists($params);

        // $model = (new WithdrawBlackList());

        // $blackListDataTable = new WithdrawBlackListDataTable();
        // $dataTable = $blackListDataTable->initialize();

        // $result = [];
        // $params = [
        //     'table_columns' => $dataTable->tableColumns
        // ];
        // $lastResultID = 0;

        // if ($rowCount = $model->getBlackLists($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'withdraw_black_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_black_lists.created_at')) {
        //         $firstResultID = $model->getBlackLists($params)->orderBy('withdraw_black_lists.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $model->getBlackLists($params)->orderBy('withdraw_black_lists.id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = ($dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID) ?
        //             $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - $dataTable->limit;

        //         $baseResult = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$end, $dataTable->start]);
        //         }

        //         $result = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('withdraw_black_lists.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $model->getBlackLists($params)->orderBy('withdraw_black_lists.id', 'DESC')->limit(1)->first()->id;
        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$dataTable->start, $end]);
        //         }

        //         $result = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $model->getBlackLists($params)->whereBetween('withdraw_black_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'withdraw_black_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_black_lists.created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @return array
     * @throws ValidationResponseException
     */
    public function getWhiteList($params)
    {
        return (new WithdrawWhiteList())->getWhiteLists($params);

        // $model = (new WithdrawWhiteList());

        // $whiteListDataTable = new WithdrawWhiteListDataTable();
        // $dataTable = $whiteListDataTable->initialize();

        // $result = [];
        // $params = [
        //     'table_columns' => $dataTable->tableColumns
        // ];
        // $lastResultID = 0;

        // if ($rowCount = $model->getWhiteLists($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'withdraw_white_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_white_lists.created_at')) {
        //         $firstResultID = $model->getWhiteLists($params)->orderBy('withdraw_white_lists.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $model->getWhiteLists($params)->orderBy('withdraw_white_lists.id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = ($dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID) ?
        //             $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - $dataTable->limit;

        //         $baseResult = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start]);
        //         }

        //         $result = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('withdraw_white_lists.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $model->getWhiteLists($params)->orderBy('withdraw_white_lists.id', 'DESC')->limit(1)->first()->id;
        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end]);
        //         }

        //         $result = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $model->getWhiteLists($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'withdraw_white_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_white_lists.created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @return array
     * @throws ValidationResponseException
     */
    public function getWhiteListRequests($params)
    {
        return (new WithdrawWhiteList())->getWhiteListRequests($params);

        // $model = (new WithdrawWhiteList());

        // $whitelistDataTable = new WithdrawWhiteListDataTable();
        // $dataTable = $whitelistDataTable->initialize();

        // $result = [];
        // $params = [
        //     'table_columns' => $dataTable->tableColumns
        // ];
        // $lastResultID = 0;

        // if ($rowCount = $model->getWhiteListRequests($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'withdraw_white_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_white_lists.created_at')) {
        //         $firstResultID = $model->getWhiteListRequests($params)->orderBy('withdraw_white_lists.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $model->getWhiteListRequests($params)->orderBy('withdraw_white_lists.id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = ($dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID) ?
        //             $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - $dataTable->limit;

        //         $baseResult = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start]);
        //         }

        //         $result = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('withdraw_white_lists.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $model->getWhiteListRequests($params)->orderBy('withdraw_white_lists.id', 'DESC')->limit(1)->first()->id;
        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end]);
        //         }

        //         $result = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $model->getWhiteListRequests($params)->whereBetween('withdraw_white_lists.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'withdraw_white_lists.id' ||
        //             $dataTable->sortColumn === 'withdraw_white_lists.created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @param $columns
     * @return mixed
     * @throws
     */
    public function getRejectedLogs($params)
    {
        return (new WithdrawRejectLog())->getRejectedLog($params);

        // $model = (new WithdrawRejectLog());

        // $withdrawRejectLogsDataTable = new WithdrawRejectLogsDataTable();
        // $dataTable = $withdrawRejectLogsDataTable->initialize();

        // $result = [];
        // $params= [
        //     'table_columns' => $dataTable->tableColumns
        // ];

        // if ($rowCount = $model->getRejectedLog($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
        //         $firstResultID = $model->getRejectedLog($params)->orderBy('id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $model->getRejectedLog($params)->orderBy('id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = ($dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID) ?
        //             $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - $dataTable->limit;

        //         $baseResult = $model->getRejectedLog($params)->whereBetween('id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getRejectedLog($params)->whereBetween('id', [$end, $dataTable->start]);
        //         }

        //         $result = $model->getRejectedLog($params)->whereBetween('id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $model->getRejectedLog($params)->whereBetween('id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $model->getRejectedLog($params)->orderBy('id', 'DESC')->limit(1)->first()->id;
        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $model->getRejectedLog($params)->whereBetween('id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $model->getRejectedLog($params)->whereBetween('id', [$dataTable->start, $end]);
        //         }

        //         $result = $model->getRejectedLog($params)->whereBetween('id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $model->getRejectedLog($params)->whereBetween('id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @param $whiteListId
     * @param $status
     * @return mixed
     */
    public function approveOrRejectWhiteListRequest(int $whiteListId, string $status)
    {
        if (!$whiteListId || !$status) {
            return false;
        }

        $userData = request()->user_data;
        $whitelistRequestData = (new WithdrawWhiteList())->find($whiteListId);

        if ($whitelistRequestData) {
            if ($whitelistRequestData->status != WithdrawWhiteList::STATUS['pending']) {
                return false;
            }

            if ($status === WithdrawWhiteList::STATUS['approved']) {
                $whitelistRequestData->expire_time = Carbon::now()->addHours(2)->toDateTimeString();
                $whitelistRequestData->status = WithdrawWhiteList::STATUS['approved'];
                $whitelistRequestData->updated_by = $userData->email;
                $whitelistRequestData->date_approved = Carbon::now()->toDateTimeString();

                if ($whitelistRequestData->save()) {
                    return $whitelistRequestData->toArray();
                }

                return false;
            }

            if ($status === WithdrawWhiteList::STATUS['rejected']) {
                $whitelistRequestData->status = WithdrawWhiteList::STATUS['rejected'];
                $whitelistRequestData->updated_by = $userData->email;
                $whitelistRequestData->date_approved = Carbon::now()->toDateTimeString();

                if ($whitelistRequestData->save()) {
                    return $whitelistRequestData->toArray();
                }

                return false;
            }
        }

        return false;
    }

    /**
     * Deletes expired Whitelist Items
     */
    private function deleteExpiredWhiteListItems()
    {
        (new WithdrawWhiteList())
            ->where('expire_time', '<', Carbon::now())
            ->whereStatus(WithdrawWhiteList::STATUS['approved'])
            ->whereNull('deleted_at')
            ->each(
                function ($item) {
                    $item->delete();
                }
            );

        return true;
    }
}
