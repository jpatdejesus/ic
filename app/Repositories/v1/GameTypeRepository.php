<?php

namespace App\Repositories\v1;

use App\Models\v1\GameType;

class GameTypeRepository extends BaseRepository
{
    public function model()
    {
        return GameType::class;
    }
}
