<?php

namespace App\Repositories\v1;

use App\Models\v1\User;
use App\Models\v1\Cashflow;

class PayoutBalanceRepository extends BaseRepository
{

    protected $user;
    protected $cashflow;
    const PAYOUT = "PAYOUT";

    /**
     * Get user information.
     *
     * @param  $data
     * @return mixed
     */
    public function __construct()
    {
        $this->user = new User();
        $this->cashflow = new Cashflow();
    }

    /**
     * Payout method.
     *
     * @param  $data
     * @return mixed
     */
    public function payout($data = array(), $username = "")
    {
        $user = $this->user->where('username', $username)->first();
        $newBalance = (float)$user->balance  + (float)$data['amount'];
        $user->balance = $newBalance;
        if ($user->save()) {
            $this->cashflow->username = $user->username;
            $this->cashflow->trans_type = self::PAYOUT;
            $this->cashflow->betting_code = $data['betting_code'];
            $this->cashflow->trans_amount = (float)$data['amount'];
            $this->cashflow->balance = $newBalance;
            $this->cashflow->game_code = $data['game_code'];
            $this->cashflow->save();
            return ['code' => 0, 'current_balance' => $newBalance];
        }
    }
}
