<?php

namespace App\Repositories\v1;

use App\Exceptions\ValidationResponseException;
use App\Models\v1\Currency;
use App\Models\v1\Transfer;
use App\Http\v1\Client\Requests\DepositBalanceRequest;
use App\Http\v1\Client\Requests\WithdrawBalanceRequest;

class TransferRepository extends TransferBaseRepository
{
    public function model()
    {
        return Transfer::class;
    }

    /**
     * @inheritdoc
     */
    public function withdraw(WithdrawBalanceRequest $withdrawBalanceRequest, $user, $balance)
    {
        $this->model->amount = get_truncated_val($withdrawBalanceRequest->amount);
        $this->model->balance = $balance;
        $this->model->op_transfer_id = $withdrawBalanceRequest->op_transfer_id;
        $this->model->username = $user->getField('username');
        $this->model->currency_code = Currency::find($user->currency_id)->currency_code;
        $this->model->actions = self::WITHDRAW_ACTION;
        $this->model->save();

        if ($this->model->save()) {
            $this->model = $this->setTransferId($this->model);
            return $this->model;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function deposit(DepositBalanceRequest $depositBalanceRequest, $user, $balance)
    {
        $this->model->amount = get_truncated_val($depositBalanceRequest->amount);
        $this->model->balance = $balance;
        $this->model->op_transfer_id = $depositBalanceRequest->op_transfer_id;
        $this->model->username = $user->getField('username');
        $this->model->currency_code = Currency::find($user->currency_id)->currency_code;
        $this->model->actions = self::DEPOSIT_ACTION;

        if ($this->model->save()) {
            $this->model = $this->setTransferId($this->model);
            return $this->model;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getTransferHistoryByUsername($params, $historyParams)
    {
        $validParameterKeys = [
            'username',
            'user_type',
        ];

        foreach ($params as $key => $value) {
            if (!in_array($key, $validParameterKeys)) {
                throw new ValidationResponseException('Failed to get transfers with these parameters:' . print_r($params, true), []);
            }

            $historyParams->{$key} = $value;
        }

        return $this->model->filterTransfersByUsername($historyParams);

        // $select = [
        //     '`transfers.id`',
        //     '`transfers.transfer_id`',
        //     '`transfers.op_transfer_id`',
        //     '`transfers.username`',
        //     '`transfers.currency_code as currency`',
        //     '`transfers.actions`',
        //     '`transfers.amount`',
        //     '`transfers.balance`',
        //     '`transfers.created_at`',
        // ];

        // $result = [];
        
        // if ($rowCount = $this->model
        //     ->select($select)
        //     ->whereUsername($username)->count()
        // ) {
        //     $lastResultID = $this->model
        //         ->select($select)
        //         ->whereUsername($username)
        //         ->orderBy('transfers.id', 'DESC')
        //         ->limit(1)
        //         ->first()->id;

        //     $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

        //     $end = $historyParams->end;

        //     $baseResult = $this->model
        //         ->select($select)
        //         ->whereUsername($username)
        //         ->whereBetween('transfers.id', [$start, $end]);

        //     while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
        //         $end += ($historyParams->limit - $baseCount);

        //         $baseResult = $this->model
        //             ->select($select)
        //             ->whereUsername($username)
        //             ->whereBetween('transfers.id', [$start, $end]);
        //     }

        //     $result = $baseResult->get();
        // }

        // return [
        //     'message' => $historyParams->message,
        //     'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
        //     'data' => (!empty($result) ? $result : []),
        // ];
    }

    /**
     * @inheritdoc
     */
    public function getAllTransferHistory(array $params, $historyParams)
    {
        /*$selectColumns = [
            'transfers.id',
            'transfers.transfer_id',
            'transfers.op_transfer_id',
            'transfers.username',
            'transfers.currency_code as currency',
            'transfers.actions',
            'transfers.amount',
            'transfers.balance',
            'transfers.created_at',
        ];*/

        $validParameterKeys = [
            'transfer_code',
            'start_date',
            'end_date'
        ];

        foreach ($params as $key => $value) {
            if (!in_array($key, $validParameterKeys)) {
                throw new ValidationResponseException('Failed to get transfers with these parameters:' . print_r($params, true), []);
            }

            $historyParams->{$key} = $value;
        }

        return $this->model->filterAllTransfers($historyParams);

       /* $result = [];

        if ($rowCount = $this->model
            ->filterAllTransfers($params, $selectColumns)
            ->count()
        ) {
            $lastResultID =  $this->model
                ->filterAllTransfers($params, $selectColumns)
                ->orderBy('id', 'DESC')
                ->limit(1)
                ->first()->id;

            $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

            $end = $historyParams->end;

            $baseResult = $this->model
                ->filterAllTransfers($params, $selectColumns)
                ->whereBetween('id', [$start, $end]);

            while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
                $end += ($historyParams->limit - $baseCount);

                $baseResult = $this->model
                    ->filterAllTransfers($params, $selectColumns)
                    ->whereBetween('id', [$start, $end]);
            }

            $result = $baseResult->get();
        }

        return [
            'message' => $historyParams->message,
            'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
            'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
            'data' => (!empty($result) ? $result : []),
        ];*/
    }

    /**
     * @inheritdoc
     */
    public function validateTransferHistory($request)
    {
        $last24Hours = to_utc_tz(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::now())->setTimezone('Asia/Manila')->subDay()->toDateTimeString());
        
        return $this->model
            ->where('op_transfer_id', $request->transfer_id)
            ->where('actions', $request->transfer_type)
            ->where('amount', $request->transfer_amount)
            ->where('created_at', '>=', $last24Hours)
            ->get();
    }
}
