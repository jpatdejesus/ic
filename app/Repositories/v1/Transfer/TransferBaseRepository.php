<?php

namespace App\Repositories\v1;

abstract class TransferBaseRepository extends BaseRepository implements TransferRepositoryInterface
{
    /**
     * Withdraw Action
     */
    const WITHDRAW_ACTION = 'WITHDRAW';

    /**
     * Deposit Action
     */
    const DEPOSIT_ACTION = 'DEPOSIT';

    /**
     * Transfer Entity
     */
    const ENTITY = 'transfer';

    /**
     * Transfer ID Prefix
     * Sample Transfer ID format
     *  DRTRID-1-1553490402
     *  DRTRID-2-1553490509
     */
    const TRANSFER_ID_PREFIX = 'DRTRID'; //DATA REPO TRANSFER ID

    /**
     * @inheritdoc
     */
    public function verifyRecordByUsername(string $username)
    {
        return $this->model->whereUsername($username)->exists();
    }

    /**
     * @inheritdoc
     */
    public function setTransferId($model)
    {
        $transferId = self::TRANSFER_ID_PREFIX . '-' . $model->id . '-' . time();

        $model->transfer_id = $transferId;
        $model->save();

        return $model;
    }

    public function isDuplicate($op_transfer_id)
    {
        return $this->model->where('op_transfer_id', $op_transfer_id)->exists();
    }

    /**
     * Get all transfer history.
     *
     * @param $username
     * @return mixed
     */
    abstract public function getAllTransferHistory(array $params, $historyParams);
}
