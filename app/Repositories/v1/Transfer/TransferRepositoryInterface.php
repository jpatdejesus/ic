<?php

namespace App\Repositories\v1;

use App\Http\v1\Client\Requests\DepositBalanceRequest;
use App\Http\v1\Client\Requests\WithdrawBalanceRequest;

interface TransferRepositoryInterface
{
    /**
     * Verifying user record if it exist
     *
     * @param string $username
     * @return bool
     */
    public function verifyRecordByUsername(string $username);

    /**
     * Get all transfer history.
     *
     * @return mixed
     */
    public function getAllTransferHistory(array $params, $historyParams);

    /**
     * Get transfer history of user by username.
     *
     * @param $username
     * @return mixed
     */
    public function getTransferHistoryByUsername($username, $historyParams);

    /**
     * Save Withdraw transaction of the given user in transfer table
     *
     * @param $withdrawBalanceRequest
     * @param $user
     * @param $balance
     * @return mixed
     *
     */
    public function withdraw(WithdrawBalanceRequest $withdrawBalanceRequest, $user, $balance);

    /**
     * Save Deposit transaction of the given user in transfer table.
     *
     * @param $depositBalanceRequest
     * @param $user
     * @return mixed
     */
    public function deposit(DepositBalanceRequest $depositBalanceRequest, $user, $balance);

    /**
     * Set formatted transfer ID
     *
     * @param $model
     */
    public function setTransferId($model);
}
