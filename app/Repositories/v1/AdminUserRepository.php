<?php

namespace App\Repositories\v1;

use Carbon\Carbon;
use App\DataTables\v1\Admin\AdminUserDataTable;
use App\Exceptions\RepositoryInternalException;
use App\Models\v1\AdminRole;
use App\Models\v1\AdminUser;
use App\Models\v1\AdminUserRole;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AdminUserRepository extends BaseRepository
{
    const CREATE_SUCCESS = 'Add new user success!';
    const CREATE_FAILED = 'Add user fail.';
    const UPDATE_SUCCESS = 'Update user success.';
    const UPDATE_FAILED = 'Update user fail.';
    const ROLE_GROUP_SUPER_ADMIN = 'SUPER_ADMIN';
    const ROLE_GROUP_ADMIN = 'ADMIN';
    const ROLE_GROUP_AUDIT= 'AUDIT';
    const UPLOAD_PATH = 'public/uploads/v1/Admin/profile_pictures/';
    const GET_PROFILE_PICTURE_PATH = '/storage/uploads/v1/Admin/profile_pictures/';

    protected $validCreateAttributes =  [
        'first_name',
        'last_name',
        'nickname',
        'profile_picture',
        'email',
        'password',
        'status',
        'role'
    ];

    protected $validUpdateAttributes = [
        'first_name',
        'last_name',
        'nickname',
        'password',
        'status',
        'role'
    ];

    protected $validUpdateProfileAttributes = [
        'nickname',
        'password',
    ];

    protected $updateAutofillAttributes = [
        'admin_users.first_name',
        'admin_users.last_name',
        'admin_users.nickname',
        'admin_users.email',
        'admin_users.status',
        'admin_roles.name as role',
        'admin_roles.role_group as role_group',
        'admin_users.created_at'
    ];

    /**
     * @return mixed|string|void
     */
    public function model()
    {
        return AdminUser::class;
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getAllUsers($params)
    {
        $authUser = request()->user_data;

        // $adminUserDataTable = new AdminUserDataTable();
        // $dataTable = $adminUserDataTable->initialize();

        // $params = $dataTable->searchParams;
        // $params['id'] = $authUser->id;
        // $params['table_columns'] = $dataTable->tableColumns;
        // $params['start'] = $dataTable->start;
        // $params['length'] = $dataTable->limit;
        // $params['sort'] = $dataTable->sort;
        
        $params->id = $authUser->id;
        return $this->model->filterAllUsers($params);

        // dd($result);
        // $return = [
        //     'count_records' => $result['count'][0]->row_count,
        //     'count_results' =>  count($result['data']),
        //     'last_record_id' => get_last_record_id($dataTable, $result),
        //     'data' => $result['data'],
        // ];
        // $result = [];
        // $lastResultID = 0;

        // if ($rowCount = $this->model->filterAllUsers($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'admin_users.id' || $dataTable->sortColumn === 'admin_users.created_at')) {
        //         $firstResultID = $this->model->filterAllUsers($params)->orderBy('admin_users.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $this->model->filterAllUsers($params)->orderBy('admin_users.id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
        //             ? $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - ($dataTable->limit - 1);

        //         $baseResult = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$end, $dataTable->start]);
        //         }

        //         $result = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('admin_users.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $this->model->filterAllUsers($params)->orderBy('admin_users.id', 'DESC')->limit(1)->first()->id;

        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$dataTable->start, $end]);
        //         }

        //         $result = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $this->model->filterAllUsers($params)->whereBetween('admin_users.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'admin_users.id' || $dataTable->sortColumn === 'admin_users.created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUser($id = null)
    {
        if ($id === null) {
            $id = request()->user_data->id;
        }

        return $this->model->select($this->updateAutofillAttributes)
            ->leftJoin('admin_user_roles', 'admin_user_roles.user_id', 'admin_users.id')
            ->leftJoin('admin_roles', 'admin_roles.id', 'admin_user_roles.role_id')
            ->where('admin_users.id', $id)
            ->first();
    }

    public function createAdminUser($request)
    {
        $params = $request->only($this->validCreateAttributes);
        $params['email'] = strtolower($params['email']);
        $params['password'] = Hash::make($params['password']);

        $roleID = $params['role'];
        unset($params['role']);

        if (!$this->createNew($params)) {
            throw new RepositoryInternalException(
                'Failed to create user with these parameters:' . print_r($params, true)
            );
        }

        $adminUserID = $this->findBy('email', $params['email'])->id;

        $adminUserRole = new AdminUserRole();

        if (!$adminUserRole->create([
            'user_id' => $adminUserID,
            'role_id' => $roleID,
        ])) {
            throw new RepositoryInternalException(
                'Failed to create user with these parameters:' . print_r($params, true)
            );
        }

        return true;
    }

    public function updateAdminUser($request, $id)
    {
        $params = $request->only($this->validUpdateAttributes);
        $roleID = 0;

        if ($params['password'] !== '' && $params['password'] !== null) {
            $params['password'] = Hash::make($params['password']);
        } else {
            unset($params['password']);
        }

        $adminRole = AdminRole::find($params['role']);

        if ($adminRole && $adminRole->role_group === self::ROLE_GROUP_SUPER_ADMIN) {
            unset($params['role']);
        } else {
            $roleID = $params['role'];
            unset($params['role']);
        }

        $params['updated_at'] = Carbon::now();

        if (!$this->update($id, $params)) {
            $params['role'] = $roleID;

            throw new RepositoryInternalException(
                'Failed to update user with these parameters:' . print_r($params, true)
            );
        }

        if ($roleID !== 0) {
            $adminUserRole = (new AdminUserRole())->where('user_id', $id)->first();
            $adminUserRole->role_id = $roleID;

            if (!$adminUserRole->save()) {
                $params['role'] = $roleID;

                throw new RepositoryInternalException(
                    'Failed to update user with these parameters:' . print_r($params, true)
                );
            }
        }

        return true;
    }

    public function updateProfile($request)
    {
        $id = request()->user_data->id;

        $params = $request->only($this->validUpdateProfileAttributes);
        if ($params['password'] !== '' && $params['password'] !== null) {
            $params['password'] = Hash::make($params['password']);
        } else {
            unset($params['password']);
        }

        if (!$this->update($id, $params)) {
            throw new RepositoryInternalException(
                'Failed to update user profile with these parameters:' . print_r($params, true)
            );
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \App\Exceptions\RepositoryBadRequestException
     */
    public function isDisabled(int $id) : bool
    {
        $update = ['status' => config('ichips.user_statuses.disabled')];
        if ($user = $this->find($id)) {
            if ($this->update($id, $update)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $request
     * @return bool
     * @throws RepositoryInternalException
     * @throws \App\Exceptions\RepositoryBadRequestException
     */
    public function uploadAdminUserProfilePicture($request)
    {
        $authUser = request()->user_data;

        $extension = $request->file('profile_picture')->getClientOriginalExtension();

        $fileName = strtolower($authUser->first_name . '_' . $authUser->last_name . '_' . $authUser->id) . '.' . $extension;

        if (!file_exists(public_path('storage'))) {
            App::make('files')->link(
                storage_path('app/public'),
                public_path('storage')
            );
        }

        if (!$request->file('profile_picture')->storeAs(self::UPLOAD_PATH, $fileName)) {
            throw new RepositoryInternalException(
                'Failed to upload user profile picture.'
            );
        }

        if (!$this->update($authUser->id, ['profile_picture' => $fileName])) {
            throw new RepositoryInternalException(
                'Failed to upload user profile picture.'
            );
        }

        return true;
    }

    /**
     * @return array
     */
    public function getProfilePictureUrl()
    {
        $authUser = $this->find(request()->user_data->id);

        $schemeAndHttpHost = (env('APP_ENV') !== 'local') ? 'https://' . request()->getHttpHost() . '/' : 'http://' . request()->getHttpHost() . '/';
        
        if ($authUser->profile_picture !== null) {
            return [
                'profile_picture_url' => $schemeAndHttpHost . self::GET_PROFILE_PICTURE_PATH . $authUser->profile_picture . '?v=' . time()
            ];
        }
        
        return [
            'profile_picture_url' => $schemeAndHttpHost . '/images/' . config('ichips.default_pp'),
            'message' => 'No image found!'
        ];
    }
}
