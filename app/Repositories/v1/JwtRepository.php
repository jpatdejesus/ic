<?php

namespace App\Repositories\v1;

use Exception;
use App\Models\v1\RoleCredential;
use Firebase\JWT\ExpiredException;
use App\Services\AppType\AppTypeService;
use Illuminate\Support\Facades\DB;

class JwtRepository extends BaseRepository
{
    protected $user;
    protected $accessToken;
    protected $userIdColumnName;

    public function __construct()
    {
        parent::__construct();

        $userEntityService = new AppTypeService('user_credentials');
        $this->user = $userEntityService->getModel();

        $accessTokenEntityService = new AppTypeService('access_tokens');
        $this->accessToken = $accessTokenEntityService->getModel();

        $this->userIdColumnName = 'role_credential_id';

        if (request()->app_type === strtoupper(config('ichips.app_types.admin'))) {
            $this->userIdColumnName = 'user_id';
        }
    }

    public function model()
    {
        return RoleCredential::class;
    }

    /**
     * Validate token
     *
     * @param  $token
     * @return mixed
     */
    public function validateToken($token)
    {
        try {
            $credentials = $this->jwtDecodeToken($token);
            $user = $this->user;
            $selectColumns = [];

            if (request()->app_type == strtoupper(config('ichips.app_types.admin'))) {
                $usersTable = 'admin_users';
                $userRolesTable = 'admin_user_roles';
                $userRolesTableOn = $userRolesTable . '.user_id';
                $userId = $usersTable . '.id';

                $rolesTable = 'admin_roles';
                $rolesTableOn = $rolesTable . '.id';
                $roleId = $userRolesTable . '.role_id';

                $selectColumns[] = $usersTable . '.*';
                $selectColumns[] = $rolesTable . '.name AS role_name';
                $selectColumns[] = $rolesTable . '.role_group';
            } else {
                $usersTable = 'role_credentials';
                $userRolesTable = 'user_roles';
                $userRolesTableOn = $userRolesTable . '.role_id';
                $userId = $usersTable . '.id';

                $rolesTable = 'roles';
                $rolesTableOn = $rolesTable . '.id';
                $roleId = $userRolesTable . '.role_id';

                $selectColumns[] = $usersTable . '.*';
                $selectColumns[] = $rolesTable . '.name AS role_name';
                $selectColumns[] = $rolesTable . '.role_group';
            }

            $user = $user
                ->select($selectColumns)
                ->leftJoin($userRolesTable, $userRolesTableOn, $userId)
                ->leftJoin($rolesTable, $rolesTableOn, $roleId)
                ->where($userId, $credentials->sub)
                ->first();

            if ($user) {
                if ($access_token = $this->accessToken::where($this->userIdColumnName, $user->id)->first()) {
                    if ($access_token->token === $token) {
                        return $user;
                    }
                }
            }

            return false;
        } catch (ExpiredException $e) {
            return false;
        } catch (Exception $e) {
            return false;
        }
    }
}
