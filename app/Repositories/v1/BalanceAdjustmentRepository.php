<?php

namespace App\Repositories\v1;

use App\Models\v1\BalanceAdjustmentType;

class BalanceAdjustmentRepository extends BaseRepository
{
    public function model()
    {
        return BalanceAdjustmentType::class;
    }

    /**
     * @return mixed
     */
    public function getBalanceCategory()
    {
        return $this->model->select(['type_name'])->get()->toArray();
    }
}
