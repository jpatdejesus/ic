<?php

namespace App\Repositories\v1;

use App\Exceptions\ValidationResponseException;
use Carbon\Carbon;
use App\Models\v1\RoleCredential;
use Illuminate\Support\Facades\Hash;
use App\Services\AppType\AppTypeService;

class AuthRepository extends BaseRepository
{
    protected $user;
    protected $accessToken;
    protected $auditTrails;
    protected $userIdColumnName;
    protected $userNameOrEmail;

    public function __construct(
        AuditTrailsRepository $auditTrails
    ) {
        parent::__construct();

        $this->auditTrails = $auditTrails;

        $userEntityService = new AppTypeService('user_credentials');
        $this->user = $userEntityService->getModel();

        $accessTokenEntityService = new AppTypeService('access_tokens');
        $this->accessToken = $accessTokenEntityService->getModel();

        $this->userNameOrEmail = 'username';
        $this->userIdColumnName = 'role_credential_id';

        if (request()->app_type === strtoupper(config('ichips.app_types.admin'))) {
            $this->userNameOrEmail = 'email';
            $this->userIdColumnName = 'user_id';
        }
    }

    public function model()
    {
        return RoleCredential::class;
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param $credentials
     * @return mixed
     */
    public function token($credentials)
    {
        $user = $this->user->where($this->userNameOrEmail, $credentials[$this->userNameOrEmail])->first();
        $token = false;

        if ($user) {
            if (request()->app_type === strtoupper(config('ichips.app_types.admin')) &&
                $user->status !== config('ichips.user_statuses.enabled')) {
                throw new ValidationResponseException(
                    'Please contact your system administrator.',
                    ['status' => 'Credentials status is invalid.']
                );
            }
            if (Hash::check($credentials['password'], $user->password)) {
                $token = $this->generateToken($user);
            }
        }

        if (request()->app_type === strtoupper(config('ichips.app_types.admin'))) {
            if ($token) {
                $this->auditTrails->createAuditTrail($user, true);
            } elseif (!$token && $user) {
                $this->auditTrails->createAuditTrail($user, false);
            }
        }

        return $token;
    }

    /**
     * Refresh token.
     *
     * @param $request
     * @return mixed
     */
    public function refreshToken($request)
    {
        return $this->generateToken($request->user_data);
    }

    /**
     * Destroy token
     *
     * @param $request
     * @return mixed
     */
    public function destroyToken($request)
    {
        $userId = $request->user_data->id;

        if ($userId) {
            return $this->removeExpiredAndUnusedTokens($userId);
        }

        return false;
    }

    /**
     * Generates token using user_id.
     *
     * @param $user
     * @return mixed
     */
    private function generateToken($user)
    {
        if (!empty($user)) {
            $this->removeExpiredAndUnusedTokens($user->id);
            $token = $this->jwtCreateToken($user);
            $expiresAt = Carbon::createFromTimestamp($this->jwtDecodeToken($token)->exp)->format('Y-m-d h:i:s');

            // Create new access token
            $accessToken = new $this->accessToken();
            $accessToken->{$this->userIdColumnName} = $user->id;
            $accessToken->token = $token;
            $accessToken->expires_at = $expiresAt;
            $accessToken->save();

            if ($accessToken) {
                return $response = [
                    'token' => $token,
                    'expires_at' => $expiresAt
                ];
            }
        }

        return false;
    }

    /**
     * Remove expired or unused tokens.
     *
     * @param  $userID
     * @return mixed
     */
    private function removeExpiredAndUnusedTokens($userID)
    {
        $this->accessToken->where('expires_at', '<', Carbon::now())->each(
            function ($item) {
                $item->delete();
            }
        );

        if ($access_token = $this->accessToken->where($this->userIdColumnName, $userID)) {
            $access_token->delete();
        }

        return true;
    }
}
