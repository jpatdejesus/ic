<?php

namespace App\Repositories\v1;

use App\Models\v1\Role;

class RoleManagementRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }
}
