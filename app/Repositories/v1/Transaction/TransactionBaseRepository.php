<?php

namespace App\Repositories\v1\Transaction;

use App\Exceptions\RepositoryInternalException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use App\Repositories\v1\BaseRepository;

abstract class TransactionBaseRepository extends BaseRepository implements TransactionRepositoryInterface
{
    const TABLE_PREFIX = '_transactions';

    public $gameCode;

    public $repository;

    /**
     * Set dynamic table for Game Result model
     *
     * @param $gameCode
     */
    public function setTable($gameCode)
    {
        $this->gameCode = $gameCode;

        $this->model->setTable($this->gameCode . self::TABLE_PREFIX);
    }


    /**
     * Get all transaction history by params.
     *
     * @param array $params
     * @param mixed $historyParams
     * @return mixed
     */
    abstract public function getAllTransactions(array $params, $historyParams);

    /**
     * Get transaction history by username and game code.
     * @param string $username
     * @param $userType
     * @param string $gameCode
     * @return mixed
     */
    abstract public function getAllTransactionsByUsernameAndGameCode(array $params, $historyParams);

    /**
     * Create new transaction
     *
     * @param $gameCode
     * @param $data
     * @return mixed
     * @throws RepositoryInternalException
     */
    abstract public function createTransaction($gameCode, $data);

    /**
     * Transform data before calling the create function
     *
     * @param $data
     * @return mixed
     */
    abstract public function transformCreateTransactionData($data);

    /**
     * Create the database table if not exists
     *
     * @param $gameCode
     * @throws RepositoryInternalException
     */
    public function createTableIfNotExist($gameCode)
    {
        if ($this->isTableExist($gameCode)) {
            return;
        }

        if (!Artisan::call('transactions_table:create', ['--game_code' => $gameCode])) {
            throw new RepositoryInternalException('Could not create transaction table.');
        }
    }

    /**
     * Check if transaction table exists
     *
     * @param $gameCode
     * @return bool
     */
    public function isTableExist($gameCode)
    {
        return Schema::connection(parent::DATAREPO_DB)->hasTable($gameCode . self::TABLE_PREFIX);
    }

    /**
     * Check for bet_code uniqueness
     *
     * @param string $value
     * @return boolean
     */
    abstract public function isBetCodeUnique($value);
}
