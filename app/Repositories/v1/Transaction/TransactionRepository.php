<?php

namespace App\Repositories\v1\Transaction;

use App\Models\v1\Transaction;
use App\Exceptions\RepositoryInternalException;
use App\Exceptions\ValidationResponseException;
use Illuminate\Support\Str;

class TransactionRepository extends TransactionBaseRepository
{
    public $gameCode;

    public function model()
    {
        return Transaction::class;
    }

    /**
     * Get transaction history by user id and game code.
     *
     * @param array $params
     * @param $historyParams
     * @return array|mixed
     * @throws ValidationResponseException
     */
    public function getAllTransactionsByUsernameAndGameCode(array $params, $historyParams)
    {
        $validParameterKeys = [
            'username',
            'user_type',
            'game_code',
            'start_date',
            'end_date',
            'bet_code',
            'shoe_hand_number',
            'table_number'
        ];

        foreach ($params as $key => $value) {
            if (!in_array($key, $validParameterKeys)) {
                throw new ValidationResponseException('Failed to get transactions with these parameters:' . print_r($params, true), []);
            }
        }

        $this->setTable($params['game_code']);

        return $this->model->filterTransactionByUsernameAndGameCode($params, $historyParams);

        // $result = [];

        // if ($rowCount = $this->model
        //     ->filterTransactionByUsernameAndGameCode($params)
        //     ->count()
        // ) {
        //     $lastResultID = $this->model
        //         ->filterTransactionByUsernameAndGameCode($params)
        //         ->orderBy('id', 'DESC')
        //         ->limit(1)
        //         ->first()->id;

        //     $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;
        //     $end = $historyParams->end;

        //     $baseResult = $this->model
        //         ->filterTransactionByUsernameAndGameCode($params)
        //         ->whereBetween('id', [$start, $end]);

        //     while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
        //         $end += ($historyParams->limit - $baseCount);

        //         $baseResult = $this->model
        //             ->filterTransactionByUsernameAndGameCode($params)
        //             ->whereBetween('id', [$start, $end]);
        //     }

        //     $result = $baseResult->get();
        // }

        // return [
        //     'message' => $historyParams->message,
        //     'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
        //     'data' => (!empty($result) ? $result : []),
        // ];
    }

    /**
     * Get all transaction history by params.
     *
     * @param array $params
     * @param mixed $historyParams
     * @return mixed
     * @throws ValidationResponseException
     */
    public function getAllTransactions(array $params, $historyParams)
    {
        $validParameterKeys = [
            'user_type',
            'game_code',
            'start_date',
            'end_date',
            'bet_code',
            'shoe_hand_number',
            'table_number'
        ];

        foreach ($params as $key => $value) {
            if (!in_array($key, $validParameterKeys)) {
                throw new ValidationResponseException('Failed to get transactions with these parameters:' . print_r($params, true), []);
            }
        }

        $this->setTable($params['game_code']);

        return $this->model->filterAllTransactions($params, $historyParams);

        // $this->setTable($params['game_code']);

        // $result = [];

        // if ($rowCount = $this->model->filterAllTransactions($params)->count()) {
        //     $lastResultID = $this->model->filterAllTransactions($params)->orderBy('id', 'DESC')->limit(1)->first()->id;

        //     $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

        //     $end = $historyParams->end;

        //     $baseResult = $this->model->filterAllTransactions($params)->whereBetween('id', [$start, $end]);

        //     while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
        //         $end += ($historyParams->limit - $baseCount);

        //         $baseResult = $this->model->filterAllTransactions($params)->whereBetween('id', [$start, $end]);
        //     }

        //     $result = $baseResult->get();
        // }

        // return [
        //     'message' => $historyParams->message,
        //     'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
        //     'data' => (!empty($result) ? $result : []),
        // ];
    }

    /**
     * Create new transaction
     *
     * @param $gameCode
     * @param $data
     * @return mixed
     * @throws RepositoryInternalException
     */
    public function createTransaction($gameCode, $data)
    {
        $this->createTableIfNotExist($gameCode);
        $this->setTable($gameCode);
        $data['bet_amount'] = get_truncated_val($data['bet_amount']);

        if (!Str::startsWith($gameCode, 'tw_')) {
            $data = $this->transformCreateTransactionData($data);
        }

        if (isset($data['error_status']) && ($data['error_status'] == "" || $data['error_status'] == null)) {
            unset($data['error_status']);
        }

        return $this->model->create($data);
    }

    public function transformCreateTransactionData($data)
    {
        $data['super_six'] = ((boolean)$data['super_six']) ? 1:0;
        $data['is_sidebet'] = ((boolean)$data['is_sidebet']) ? 1:0;
        $data['balance'] = get_truncated_val($data['balance']);
        return $data;
    }

    /**
     * Check for bet_code uniqueness
     *
     * @param string $value
     * @return boolean
     */
    public function isBetCodeUnique($value)
    {
        $transaction = $this->model->where('bet_code', $value)->first();
        if ($transaction) {
            return false;
        }
        return true;
    }
    
    /**
     * Get transaction history by user id and game code.
     *
     * @param array $params
     * @param $historyParams
     * @return array|mixed
     * @throws ValidationResponseException
     */
    public function getAllTableTransactionsByUsername(array $params, $historyParams)
    {
        $validParameterKeys = [
            'username',
            'user_type',
            'start_date',
            'end_date',
            'bet_code',
            'shoe_hand_number',
            'table_number',
            'games',
            'order_by',
            'order_sort',
        ];

        foreach ($params as $key => $value) {
            if (!in_array($key, $validParameterKeys)) {
                throw new ValidationResponseException('Failed to get transactions with these parameters:' . print_r($params, true), []);
            }
        }

        foreach ($params['games'] as $value) {
            if (Str::startsWith($value, 'tw_') || !in_array($value, config('ichips.default_game_code'))) {
                throw new ValidationResponseException('Get transaction history failed.', ['games' => 'The games field value is invalid.']);
            }
        }

        return $this->model->filterAllTableTransactionsByUsername($params, $historyParams);
    }
}
