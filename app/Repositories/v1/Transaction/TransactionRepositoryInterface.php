<?php

namespace App\Repositories\v1\Transaction;

/**
 * TransactionRepositoryInterface
 *
 * @package App\Contracts
 */
interface TransactionRepositoryInterface
{
    /**
     * Set dynamic table for Game Result model
     *
     * @param $gameCode
     */
    public function setTable($gameCode);

    
    /**
     * Get all transaction history by params.
     *
     * @param array $params
     * @param mixed $historyParams
     * @return mixed
     */
    public function getAllTransactions(array $params, $historyParams);

    /**
     * Get transaction history by username and game code.
     * @param string $username
     * @param $userType
     * @param string $gameCode
     * @return mixed
     */
    public function getAllTransactionsByUsernameAndGameCode(array $params, $historyParams);

    /**
     * Create new transaction
     *
     * @param $gameCode
     * @param $data
     * @return mixed
     * @throws RepositoryInternalException
     */
    public function createTransaction($gameCode, $data);

    /**
     * Transform data before calling the create function
     *
     * @param $data
     * @return mixed
     */
    public function transformCreateTransactionData($data);

    /**
     * Create the database table if not exists
     *
     * @param $gameCode
     * @throws RepositoryInternalException
     */
    public function createTableIfNotExist($gameCode);

    /**
     * Check if transaction table exists
     *
     * @param $gameCode
     * @return bool
     */
    public function isTableExist($gameCode);

    /**
     * Check for bet_code uniqueness
     *
     * @param string $value
     * @return boolean
     */
    public function isBetCodeUnique($value);
}
