<?php

namespace App\Repositories\v1;

use Illuminate\Support\Facades\Log;
use App\Models\v1\AdminUpdateBalanceRequest;
use App\DataTables\v1\Admin\UpdateBalanceRequestDataTable;

class AdminUpdateBalanceRequestRepository extends BaseRepository
{
    /**
     * @return mixed|string|void
     */
    public function model()
    {
        return AdminUpdateBalanceRequest::class;
    }

    /**
     * @return mixed
     * @throws \App\Exceptions\ValidationResponseException
     */
    public function getAllUpdateBalanceRequests($params)
    {
        $authUser = request()->user_data;
        
        $params->id = $authUser->id;
        $params->role_name = $authUser->role_name;
        $params->user_type = request()->user_type;
        $params->date_start = request()->date_start;
        $params->date_end = request()->date_end;
        return $this->model->filterAllUpdateBalanceRequests($params);


        // $updateBalanceRequestDataTable = new UpdateBalanceRequestDataTable();
        // $dataTable = $updateBalanceRequestDataTable->initialize();
        // $params = $dataTable->searchParams;
        // $params['id'] = $authUser->id;
        // $params['column'] = $dataTable->sortColumn;
        // $params['order_by'] = $dataTable->sortOrder;
        // $params['table_columns'] = $dataTable->tableColumns;
        // $params['date_start'] = $updateBalanceRequestDataTable->getRequest()->date_start;
        // $params['date_end'] = $updateBalanceRequestDataTable->getRequest()->date_end;
        // $params['user_type'] = $updateBalanceRequestDataTable->getRequest()->user_type;
        // $params['role_name'] = $authUser->role_name;

        // $result = [];
        // $lastResultID = 0;

        // if ($rowCount = $this->model->filterAllUpdateBalanceRequests($params)->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'admin_update_balance_requests.created_at')) {
        //         $firstResultID = $this->model->filterAllUpdateBalanceRequests($params)->orderBy('admin_update_balance_requests.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $this->model->filterAllUpdateBalanceRequests($params)->orderBy('admin_update_balance_requests.id', 'ASC')->limit(1)->first()->id;
        //         $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
        //             ? $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - ($dataTable->limit - 1);

        //         $baseResult = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$end, $dataTable->start]);
        //         }

        //         $result = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('admin_update_balance_requests.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $this->model->filterAllUpdateBalanceRequests($params)->orderBy('admin_update_balance_requests.id', 'DESC')->limit(1)->first()->id;

        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$dataTable->start, $end]);
        //         }

        //         $result = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $this->model->filterAllUpdateBalanceRequests($params)->whereBetween('admin_update_balance_requests.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'admin_update_balance_requests.created_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }

    /**
     * @param $batch_data
     * @return mixed
     */
    public function batchAdminUpdateBalanceRequest($batch_data)
    {
        foreach ($batch_data as $key => $value) {
            $data = [
                'username' => $value['username'],
                'user_type' => $value['user_type'],
                'amount' => $value['amount'],
                'trans_type' => $value['category'],
                'reason' => $value['reason'],
                'type' => $value['type'],
            ];

            $data['status'] = AdminUpdateBalanceRequest::STATUS_APPROVED;
            $data['created_by'] = request()->user_data->id;
            $data['updated_by'] = request()->user_data->id;

            AdminUpdateBalanceRequest::create($data);
        }
    }
}
