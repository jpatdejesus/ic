<?php

namespace App\Repositories\v1;

use App\Models\v1\AdminUserRole;
use App\Models\v1\AdminAuditLogs;
use Illuminate\Support\Facades\Log;
use App\DataTables\v1\Admin\AuditTrailsDataTable;

class AuditTrailsRepository extends BaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed
     */
    public function model()
    {
        return AdminAuditLogs::class;
    }

    /**
     * @param $userData
     * @param $isSuccess
     * @return mixed
     */
    public function createAuditTrail($userData, $isSuccess = false)
    {
        $roleDetails = AdminUserRole::where('user_id', $userData->id)->first();
        $remarks = 'Login Failed!';

        if ($isSuccess) {
            $remarks = 'Login Success!';
        }

        $this->model::create([
            'email' => $userData->email,
            'role' => $roleDetails['role_id'],
            'event' => 'login',
            'old_values' => null,
            'new_values' => null,
            'ip_address' => request()->ip(),
            'platform' => request()->header('User-Agent'),
            'remarks' => $remarks,
        ]);

        return true;
    }

    /**
     *
     * @return mixed
     * @throws
     */
    //array $columns
    public function getAuditTrails($params)
    {
        return $this->model->getAdminAuditTrail($params);
        // $auditTrailsDataTable = new AuditTrailsDataTable();
        // $dataTable = $auditTrailsDataTable->initialize();
        // $tableColumns = $dataTable->tableColumns;
        // $result = [];
        // $lastResultID = 0;

        // if ($rowCount = $this->model->select($tableColumns)->getAuditTrails()->count()) {
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'admin_audit_logs.id' || $dataTable->sortColumn === 'admin_audit_logs.created_at' || $dataTable->sortColumn === 'admin_audit_logs.updated_at')) {
        //         $firstResultID = $this->model->select($tableColumns)->getAuditTrails()->orderBy('admin_audit_logs.id', 'DESC')->limit(1)->first()->id;

        //         $lastResultID = $this->model->select($tableColumns)->getAuditTrails()->orderBy('admin_audit_logs.id', 'ASC')->limit(1)->first()->id;

        //         $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
        //             ? $firstResultID : $dataTable->start - 2;

        //         $end = $dataTable->start - ($dataTable->limit - 1);

        //         $baseResult = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$end, $dataTable->start]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
        //             $end -= ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$end, $dataTable->start]);
        //         }

        //         $result = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy('admin_audit_logs.id', $dataTable->sortOrder)
        //             ->get();

        //         $data = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$end, $dataTable->start])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     } else {
        //         $lastResultID = $this->model->select($tableColumns)->getAuditTrails()->orderBy('admin_audit_logs.id', 'DESC')->limit(1)->first()->id;

        //         $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

        //         $baseResult = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$dataTable->start, $end]);
        //         while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
        //             $end += ($dataTable->limit - $baseCount);

        //             $baseResult = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$dataTable->start, $end]);
        //         }

        //         $result = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->get();

        //         $data = $this->model->select($tableColumns)->getAuditTrails()->whereBetween('admin_audit_logs.id', [$dataTable->start, $end])
        //             ->groupBy($dataTable->groupByColumns)
        //             ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
        //             ->get();
        //     }
        // }

        // $return = [
        //     'count_records' => (!empty($result) ? $rowCount : 0),
        //     'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
        //     'last_record_id' => 0,
        //     'data' => (!empty($data) ? $data : []),
        // ];

        // if (!empty($result) && $result->last()) {
        //     $lastResult = $result->last()->id;
        //     if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'admin_audit_logs.id' || $dataTable->sortColumn === 'admin_audit_logs.created_at' || $dataTable->sortColumn === 'admin_audit_logs.updated_at')) {
        //         if ($lastResult > $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     } else {
        //         if ($lastResult < $lastResultID) {
        //             $return['last_record_id'] = $lastResult;
        //         }
        //     }
        // }

        // return $return;
    }
}
