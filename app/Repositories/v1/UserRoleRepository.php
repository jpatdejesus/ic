<?php

namespace App\Repositories\v1;

use App\Models\v1\UserRole;

class UserRoleRepository extends BaseRepository
{
    public function model()
    {
        return UserRole::class;
    }
}
