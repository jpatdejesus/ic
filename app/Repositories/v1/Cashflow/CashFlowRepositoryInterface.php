<?php

namespace App\Repositories\v1;

use App\Models\v1\AdminUpdateBalanceRequest;
use App\Http\v1\Client\Requests\BetBalanceRequest;
use App\Http\v1\Client\Requests\DepositBalanceRequest;
use App\Http\v1\Client\Requests\WithdrawBalanceRequest;

interface CashFlowRepositoryInterface
{
    /**
     * Balance ajustment modification by admin
     *
     * @param request
     * @param $user
     * @param $balance
     * @param $transfer
     *
     * @return mixed
     */
    public function modifyByAdmin(AdminUpdateBalanceRequest $updateBalance, $user, $balance);

    /**
     * @param WithdrawBalanceRequest $withdrawBalanceRequest
     * @param $user
     * @param $balance
     * @param $transfer
     *
     * @return mixed
     */
    public function withdraw(WithdrawBalanceRequest $withdrawBalanceRequest, $user, $balance, $transfer);

    /**
     * @param BetBalanceRequest $betBalanceRequest
     * @param $user
     * @param $balance
     *
     * @return mixed
     */
    public function bet(BetBalanceRequest $betBalanceRequest, $user, $balance);

    /**
     * @param DepositBalanceRequest $depositBalanceRequest
     * @param $user
     * @param $balance
     * @param $transfer
     *
     * @return mixed
     */
    public function isDeposit(DepositBalanceRequest $depositBalanceRequest, $user, $balance, $transfer);

    /**
     * @param $user
     * @param $historyParams
     * @return mixed
     */
    public function getChipFlow($user, $historyParams);

    /**
     * Batch Balance ajustment modification by admin
     *
     * @param request
     * @param $batchUpdateBalance
     *
     * @return mixed
     */
    public function batchModifyByAdmin($balances);
}
