<?php

namespace App\Repositories\v1;

use App\Models\v1\Cashflow;

class CashFlowRepository extends CashFlowBaseRepository
{
    public function model()
    {
        return Cashflow::class;
    }
}
