<?php

namespace App\Repositories\v1;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\v1\AdminUpdateBalanceRequest;
use App\Http\v1\Client\Requests\BetBalanceRequest;
use App\DataTables\v1\Admin\PlayerCashflowDataTable;
use App\Http\v1\Client\Requests\PayoutBalanceRequest;
use App\Http\v1\Client\Requests\DepositBalanceRequest;
use App\Http\v1\Client\Requests\WithdrawBalanceRequest;

abstract class CashFlowBaseRepository extends BaseRepository implements CashFlowRepositoryInterface
{
    /**
     * Modfify transaction type
     */
    const MODIFY_ADD_TRANS_TYPE = 'MODIFY_ADD';

    /**
     * Modfify transaction type
     */
    const MODIFY_DEDUCT_TRANS_TYPE = 'MODIFY_DEDUCT';
    /**
     * Withdraw transaction type
     */
    const WITHDRAW_TRANS_TYPE = 'WITHDRAW';

    /**
     * Deposit transaction type
     */
    const DEPOSIT_TRANS_TYPE = 'DEPOSIT';

    /**
     * Bet transaction type
     */
    const BET_TRANS_TYPE = 'BET';

    /**
     * Payout transaction type
     */
    const PAYOUT_TRANS_TYPE = 'PAYOUT';

    /**
     * @inheritdoc
     */
    public function modifyByAdmin(AdminUpdateBalanceRequest $updateBalance, $user, $balance)
    {
        $trans_type = ($updateBalance->type == AdminUpdateBalanceRequest::TYPE_ADD ? self::MODIFY_ADD_TRANS_TYPE : self::MODIFY_DEDUCT_TRANS_TYPE);
        $this->model->username = $user->getField('username');
        $this->model->betting_code = $updateBalance->reason; // For verification
        $this->model->game_code = $updateBalance->trans_type;
        $this->model->trans_amount = get_truncated_val($updateBalance->amount);
        $this->model->trans_type = $trans_type;
        $this->model->balance = get_truncated_val($balance);
        $this->model->save();

        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function batchModifyByAdmin($balances)
    {
        foreach ($balances as $key => $value) {
            $trans_type = ($value['type'] == AdminUpdateBalanceRequest::TYPE_ADD ? self::MODIFY_ADD_TRANS_TYPE : self::MODIFY_DEDUCT_TRANS_TYPE);
            $data[] = [
                'username' => $key,
                'betting_code' => $value['reason'],
                'game_code' => $value['trans_type'],
                'trans_amount' => get_truncated_val($value['amount']),
                'trans_type' => $trans_type,
                'balance' => get_truncated_val($value[0]->afterBalance),
            ];
        }
        $this->model->insert($data);

        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function withdraw(WithdrawBalanceRequest $withdrawBalanceRequest, $user, $balance, $transfer)
    {
        $this->model->username = $user->getField('username');
        $this->model->betting_code = $transfer->transfer_id;
        $this->model->trans_amount = get_truncated_val($withdrawBalanceRequest->amount);
        $this->model->trans_type = self::WITHDRAW_TRANS_TYPE;
        $this->model->balance = get_truncated_val($balance);
        $this->model->save();

        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function bet(BetBalanceRequest $betBalanceRequest, $user, $balance)
    {
        \Log::info('cashflow balance: ' . $balance);
        $this->model->username = $user->getField('username');
        $this->model->game_code = $betBalanceRequest->game_code;
        $this->model->betting_code = $betBalanceRequest->betting_code;
        $this->model->trans_amount = get_truncated_val($betBalanceRequest->amount);
        $this->model->trans_type = self::BET_TRANS_TYPE;
        $this->model->balance = get_truncated_val($balance);
        $this->model->save();

        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function betBulk(array $betBulkData, $username, $afterBalance)
    {
        $cashFlowData = [];

        foreach ($betBulkData as $key => $betData) {
            $cashFlowData[] = [
                'username' => $username,
                'game_code' => $betData['game_code'],
                'betting_code' => $betData['betting_code'],
                'trans_amount' => get_truncated_val($betData['amount']),
                'trans_type' => self::BET_TRANS_TYPE,
                'balance' => get_truncated_val($afterBalance),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        $this->model->insert($cashFlowData);

        return $this->model;
    }

    /**
     * @param array $bettingCodes
     * @return mixed
     */
    public function getCashFlowByBulkBettingCode(array $bettingCodes)
    {
        return $this->model->whereIn('betting_code', $bettingCodes);
    }

    /**
     * @inheritdoc
     */
    public function payout(PayoutBalanceRequest $payoutBalanceRequest, $user)
    {
        return DB::transaction(function () use ($payoutBalanceRequest, $user) {

            $userBalances = $user->spUpdateBalance('+', get_truncated_val($payoutBalanceRequest['amount']), $user->getField('id'));
            $newBalance = $userBalances[0]->afterBalance;

            $this->model->username = $user->getField('username');
            $this->model->trans_type = self::PAYOUT_TRANS_TYPE;
            $this->model->betting_code = $payoutBalanceRequest['betting_code'];
            $this->model->trans_amount = get_truncated_val((float) $payoutBalanceRequest['amount']);
            $this->model->balance = get_truncated_val($newBalance);
            $this->model->game_code = $payoutBalanceRequest['game_code'];
            $this->model->save();

            return ['code' => 0, 'current_balance' => get_truncated_val((float) $newBalance)];
        });
    }

    /**
     * @inheritdoc
     */
    public function payoutBulk(array $payoutBulkData, $username, $afterBalance)
    {
        $cashFlowData = [];

        foreach ($payoutBulkData as $key => $payoutData) {
            $cashFlowData[] = [
                'username' => $username,
                'game_code' => $payoutData['game_code'],
                'betting_code' => $payoutData['betting_code'],
                'trans_amount' => get_truncated_val($payoutData['amount']),
                'trans_type' => self::PAYOUT_TRANS_TYPE,
                'balance' => get_truncated_val($afterBalance),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        $this->model->insert($cashFlowData);

        return $this->model;
    }

    /**
     * @inheritdoc
     */
    public function isDeposit(DepositBalanceRequest $depositBalanceRequest, $user, $balance, $transfer)
    {
        $this->model->username = $user->getField('username');
        $this->model->betting_code = $transfer->transfer_id;
        $this->model->trans_amount = get_truncated_val($depositBalanceRequest->amount);
        $this->model->trans_type = self::DEPOSIT_TRANS_TYPE;
        $this->model->balance = $balance;

        if ($this->model->save()) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getChipFlow($user, $historyParams)
    {
        $historyParams->username = $user->getField('username');

        return $this->model->chipFlowByUsername($historyParams);

        /*$result = [];

        if ($rowCount = $this->model
            ->whereUsername($user->getField('username'))
            ->count()
        ) {
            $lastResultID = $this->model
                ->whereUsername($user->getField('username'))
                ->orderBy('id', 'DESC')
                ->limit(1)
                ->first()->id;

            $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

            $end = $historyParams->end;

            $baseResult = $this->model
                ->whereUsername($user->getField('username'))
                ->whereBetween('id', [$start, $end]);

            while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
                $end += ($historyParams->limit - $baseCount);

                $baseResult = $this->model
                    ->whereUsername($user->getField('username'))
                    ->whereBetween('id', [$start, $end]);
            }

            $result = $baseResult->get();
        }

        return [
            'message' => $historyParams->message,
            'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
            'count_results' => (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
            'data' => (!empty($result) ? $result : []),
        ];*/
    }

    /**
     * @inheritdoc
     */
    public function getPlayerCashflowByAdminInquiry($username, $request, $params)
    {
        $authUser = request()->user_data;

        $params->id = $authUser->id;
        $params->user_type = $request->user_type;
        $params->username = $username;
        $params->date_start = $request->date_start;
        $params->date_end = $request->date_end;

        return $this->model->playerCashflowByAdminInquiry($params);

        /*
        $params = $dataTable->searchParams;
        $params['id'] = request()->user_data;
        $params['user_type'] = $request->user_type;
        $params['username'] = $username;
        $params['date_start'] = $request->date_start;
        $params['date_end'] = $request->date_end;
        $params['column'] = $dataTable->sortColumn;
        $params['order_by'] = $dataTable->sortOrder;
        $params['table_columns'] = $dataTable->tableColumns;

        $result = [];
        $lastResultID = 0;

        if ($rowCount = $this->model->playerCashflowByAdminInquiry($params)->count()) {
            if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
                $firstResultID = $this->model->playerCashflowByAdminInquiry($params)->orderBy('id', 'DESC')->limit(1)->first()->id;

                $lastResultID = $this->model->playerCashflowByAdminInquiry($params)->orderBy('id', 'ASC')->limit(1)->first()->id;

                $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
                    ? $firstResultID : $dataTable->start - 2;

                $end = $dataTable->start - $dataTable->limit;

                $baseResult = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$end, $dataTable->start]);
                while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
                    $end -= ($dataTable->limit - $baseCount);

                    $baseResult = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$end, $dataTable->start]);
                }

                $result = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$end, $dataTable->start])
                    ->groupBy($dataTable->groupByColumns)
                    ->orderBy('id', $dataTable->sortOrder)
                    ->get();

                $data = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$end, $dataTable->start])
                    ->groupBy($dataTable->groupByColumns)
                    ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
                    ->get();
            } else {
                $lastResultID = $this->model->playerCashflowByAdminInquiry($params)->orderBy('id', 'DESC')->limit(1)->first()->id;

                $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

                $baseResult = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$dataTable->start, $end]);
                while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
                    $end += ($dataTable->limit - $baseCount);

                    $baseResult = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$dataTable->start, $end]);
                }

                $result = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$dataTable->start, $end])
                    ->groupBy($dataTable->groupByColumns)
                    ->get();

                $data = $this->model->playerCashflowByAdminInquiry($params)->whereBetween('id', [$dataTable->start, $end])
                    ->groupBy($dataTable->groupByColumns)
                    ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
                    ->get();
            }
        }

        $return = [
            'count_records' => (!empty($result) ? $rowCount : 0),
            'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => 0,
            'data' => (!empty($data) ? $data : []),
        ];

        if (!empty($result) && $result->last()) {
            $lastResult = $result->last()->id;
            if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
                if ($lastResult > $lastResultID) {
                    $return['last_record_id'] = $lastResult;
                }
            } else {
                if ($lastResult < $lastResultID) {
                    $return['last_record_id'] = $lastResult;
                }
            }
        }

        return $return;*/
    }

    /**
     * @inheritdoc
     */
    public function getAllChipFlow($historyParams)
    {
        return $this->model->allChipFlow($historyParams);

        /*$result = [];

        if ($rowCount = $this->model->count()) {
            $lastResultID = $this->model->orderBy('id', 'DESC')->limit(1)->first()->id;

            $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

            $end = $historyParams->end;

            $baseResult = $this->model
                ->whereBetween('id', [$start, $end]);

            while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
                $end += ($historyParams->limit - $baseCount);

                $baseResult = $this->model->whereBetween('id', [$start, $end]);
            }

            $result = $baseResult->get();
        }

        return [
            'message' => $historyParams->message,
            'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
            'count_results' => (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
            'data' => (!empty($result) ? $result : []),
        ];*/
    }
}
