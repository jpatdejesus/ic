<?php

namespace App\Repositories\v1;

use App\Models\v1\CashflowPHP;

class CashFlowPHPRepository extends CashFlowBaseRepository
{
    public function model()
    {
        return CashflowPHP::class;
    }
}
