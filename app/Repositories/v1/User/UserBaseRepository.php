<?php

namespace App\Repositories\v1;

use App\Models\v1\AdminUpdateBalanceRequest;

abstract class UserBaseRepository extends BaseRepository implements UserRepositoryInterface
{
    const CREATE_SUCCESS = 'Add new user success!';
    const CREATE_FAILED = 'Add user fail.';
    const UPDATE_SUCCESS = 'Success!';
    const UPDATE_FAILED = 'Update user fail.';
    const METHOD_ADD = '+';
    const METHOD_SUBTRACT = '-';

    private $createUserAttributes;

    public $repository;

    /**
     * @param $attributes
     */
    public function setCreateAttributes(array $attributes) : void
    {
        $this->createUserAttributes = $attributes;
    }

    /**
     * @return array
     */
    public function getCreateAttributes() : array
    {
        return $this->createUserAttributes;
    }

    /**
     * Add or deduct user point or exp
     *
     * @param $data
     * @return array
     */
    protected function addOrDeduct($user, $data)
    {
        if ($data['method'] === self::METHOD_ADD) {
            if (!$this->setPointOrExpIfNull($user, $data)) {
                $user->increment($data['column'], $data['value']);
            }
        }

        if ($data['method'] === self::METHOD_SUBTRACT) {
            if (!$this->setPointOrExpIfNull($user, $data)) {
                $user->decrement($data['column'], $data['value']);
            }
        }
        
        return $this->getFormattedPointExpReturnValue($user, $data);
    }

    /**
     * Set Point/Exp if current value is null
     *
     * @param [type] $user
     * @param [type] $data
     * @return boolean
     */
    private function setPointOrExpIfNull($user, $data)
    {
        if (is_null($user[$data['column']])) {
            $user[$data['column']] = $data['value'];
            $user->save();
            return true;
        }
        return false;
    }

    /**
     * Get formatted point/exp return value
     *
     * @param [type] $user
     * @param [type] $data
     * @return decimal
     */
    private function getFormattedPointExpReturnValue($user, $data)
    {
        $newBalance = $user[$data['column']];
        $newBalance = number_format($newBalance, 5, '.', '');
        return $newBalance;
    }

    /**
     * @param $userRequest
     * @return array
     */
    abstract public function createUser($userRequest);

    /**
     * @param string $username
     * @return bool
     */
    abstract public function isUserExist(string $username) : bool;

    /**
     * @param mixed $param
     * @return mixed
     */
    abstract public function getUserInformation($param);

    /**
     * @param $request
     * @param $username
     * @return mixed
     */
    abstract public function updateUser($request, $username);

    /**
     * @param string $username
     * @return bool
     */
    abstract public function isDisabled(string $username) : bool;

    /**
     * @param $username
     * @return mixed
     */
    abstract public function getUserBalance($username);

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    abstract public function search($request, $query);
    
    /**
     * @inhertidoc
     */
    public function updatePlayerBalanceByAdmin(string $username, $type, $amount)
    {
        $user = $this->getUserByUsername($username);

        switch ($type) {
            case AdminUpdateBalanceRequest::TYPE_ADD:
                return $user->spUpdateBalanceByAdmin('+', get_truncated_val($amount), $user->getField('id'));
            case AdminUpdateBalanceRequest::TYPE_DEDUCT:
                return $user->spUpdateBalanceByAdmin('-', get_truncated_val($amount), $user->getField('id'));
        }
    }

    /**
     * @inhertidoc
     */
    public function batchUpdatePlayerBalanceByAdmin($batch_data)
    {
        foreach ($batch_data as $key => $value) {
            $user = $this->getUserByUsername($value['username']);

            $balances[$value['username']] = $user->spUpdateBalanceByAdmin('+', get_truncated_val($value['amount']), $user->getField('id'));
            $balances[$value['username']]['amount'] = $value['amount'];
            $balances[$value['username']]['type'] = $value['type'];
            $balances[$value['username']]['reason'] = $value['reason'];
            $balances[$value['username']]['trans_type'] = $value['category'];
        }

        return $balances;
    }
}
