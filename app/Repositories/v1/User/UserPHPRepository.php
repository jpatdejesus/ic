<?php

namespace App\Repositories\v1;

use App\Exceptions\RepositoryInternalException;
use App\Models\v1\UserPHP;
use Illuminate\Http\Request;
use App\Exceptions\RepositoryBadRequestException;

class UserPHPRepository extends UserBaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed|string|void
     */
    public function model()
    {
        return UserPHP::class;
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function getUserByUsername(string $username)
    {
        return $this->model->where('UserName', $username)
            ->firstOrFail();
    }

    /**
     * @param $userRequest
     */
    public function createUser($userRequest)
    {
        return false;
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function isUserExist(string $username): bool
    {
        return $this->model->where(['UserName' => $username])->exists();
    }

    /**
     * @param mixed $param
     * @return mixed
     */
    public function getUserInformation($param)
    {
        $select = [
            'users_php.userid as id',
            'users_php.moneysort as currency',
            'users_php.UserName as username',
            'users_php.TrueName as nickname',
            'users_php.curMoney1',
            'users_php.Point as point',
            'users_php.Exp as exp',
            'users_php.isuseable as status',
            'users_php.last_login',
        ];

        $query = $this->model
            ->select($select);

        if ((int)$param && is_numeric($param)) {
            return $query->where('UserId', $param);
        }

        return $query->where('UserName', $param);
    }

    /**
     * @inheritdoc
     */
    public function getPlayerInfoByAdminInquiry(string $userFilter)
    {
        $select = [
            'users_php.userid as id',
            'users_php.UserName as username',
            'users_php.curMoney1 as balance',
            'users_php.Point as point',
            'users_php.Exp as exp',
        ];

        $query = $this->model
            ->select($select)
            ->where('userid', $userFilter)
            ->orWhere('UserName', $userFilter);

        if ((int)$userFilter && is_numeric($userFilter)) {
            return $query->where('userid', $userFilter);
        }

        return $query->where('UserName', $userFilter);
    }

    
    /**
     * @inheritdoc
     */
    public function getPlayerForUpdateBalance(string $userFilter)
    {
        $select = [
            'users_php.userid as id',
            'users_php.UserName as username',
            'users_php.curMoney1 as balance',
            'moneysort as currency',
        ];

        $query = $this->model
            ->select($select);

        if ((int)$userFilter && is_numeric($userFilter)) {
            return $query->where('userid', $userFilter);
        }

        return $query->where('UserName', $userFilter);
    }

    /**
     * @param $request
     * @param $username
     * @return array|bool|mixed
     * @throws RepositoryInternalException
     */
    public function updateUser($request, $username)
    {
        $user = $this->getUserByUsername($username);
        if ($user) {
            $user->TrueName = $request->get('nickname');
            $user->islock = $request->get('status');
            $user->save();

            return $response = [
                'status' => 'Success!',
            ];
        } else {
            throw new RepositoryInternalException('User not found.');
        }
        return false;
    }

    /**
     * @param string $username
     * @return bool
     * @throws RepositoryBadRequestException
     */
    public function isDisabled(string $username): bool
    {
        $update = ['isuseable' => 2];
        if ($user = $this->findBy('UserName', $username)) {
            if ($this->update($user->getField('id'), $update)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getUserBalance($username)
    {
        $select = [
            'users_php.curMoney1',
        ];

        $userBalance = $this->model
            ->select($select)
            ->where(['UserName' => $username])
            ->first()
            ->toArray();

        return ['curMoney1' => get_truncated_val($userBalance['curMoney1'])];
    }

    /**
     * @param  string $query
     * @return mixed
     */
    public function search($request, $query)
    {
        $columns = array(
            "userid",
            "UserName",
            "TrueName",
            "isuseable",
            "moneysort",
            "curMoney1",
            "point",
            "exp",
            "last_login",
        );
        $user = array();
        if ($request['search'] == 1) {
            // user by user_id
            $user = $this->model->select($columns)->find($query);
        } else {
            // search by username
            $user = $this->model->where('UserName', $query)->select($columns)->first();
        }

        return !empty($user) ? [
            'user_id' => $user->userid,
            'username' => $user->UserName,
            'status' => $user->status(),
            'nickname' => $user->TrueName,
            'currency' => $user->moneysort,
            'balance' => $user->curMoney1,
            'point' => $user->point,
            'exp' => $user->exp,
            'last_login_time' => $user->last_login_time,
        ] : null;
    }

    /**
     * Update user point
     *
     * @param $request
     * @param string $username
     * @return array
     */
    public function updatePoint(Request $request, string $username)
    {
        $user = $this->getUserByUsername($username);
        $data = [
            'username' => $username,
            'method' => $request->user_points_method,
            'value' => $request->user_points_value,
            'column' => 'Point',
        ];
        $newPoint = $this->addOrDeduct($user, $data);
        return ['point' => $newPoint];
    }

    /**
     * Update user exp
     *
     * @param $request
     * @param string $username
     * @return array
     */
    public function updateExp(Request $request, string $username)
    {
        $user = $this->getUserByUsername($username);
        $data = [
            'username' => $username,
            'method' => $request->user_exp_method,
            'value' => $request->user_exp_value,
            'column' => 'Exp',
        ];
        $newExp = $this->addOrDeduct($user, $data);
        return ['exp' => $newExp];
    }
}
