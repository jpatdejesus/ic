<?php

namespace App\Repositories\v1;

use App\Models\v1\User;
use Illuminate\Http\Request;
use App\Exceptions\RepositoryInternalException;
use App\Exceptions\ValidationResponseException;
use App\Exceptions\RepositoryBadRequestException;

class UserRepository extends UserBaseRepository
{
    /**
     * Set Model for the Repository
     *
     * @return mixed|string|void
     */
    public function model()
    {
        $this->setCreateAttributes([
            'currency',
            'username',
            'nickname',
            'status',
            'user_type',
            'chip_limits'
        ]);

        return User::class;
    }

    public function getUserByUsername(string $username)
    {
        return $this->model->where('username', $username)
            ->firstOrFail();
    }

    /**
     * @param $userRequest
     * @return array
     * @throws RepositoryInternalException
     * @throws ValidationResponseException
     */
    public function createUser($userRequest)
    {
        $params = $userRequest->only($this->getCreateAttributes());
        if (isset($params['currency'])) {
            $params['currency_id'] = (new CurrencyRepository())->findBy('currency_code', $params['currency'])->id;
            unset($params['currency']);
        }

        if (isset($params['user_type'])) {
            $userTypeID = (new UserTypeRepository())->findBy('user_type_name', $params['user_type'])->id;
            unset($params['user_type']);
        }

        $chipLimits = [];
        if (isset($params['chip_limits'])) {
            $chipLimits = json_decode($params['chip_limits'], true);
            unset($params['chip_limits']);
            if (count($chipLimits) > 0) {
                (new GameRepository())->validateGameCodes($chipLimits);
            }
        }

        if (!$this->createNew($params)) {
            throw new RepositoryInternalException(
                'Failed to Create User with these parameters:' . print_r($params, true)
            );
        }

        $user = $this->findBy('username', $params['username']);

        if (count($chipLimits) > 0) {
            $userChipLimitRepository = new UserChipLimitRepository();

            foreach ($chipLimits as $chipLimit) {
                $userChipLimitsParam = [
                    'user_id' => $user->id,
                    'user_type_id' => $userTypeID,
                    'game_type_id' => (new GameRepository())->findBy('game_code', key($chipLimit))->game_type_id,
                    'min' => $chipLimit[key($chipLimit)]['min'],
                    'max' => $chipLimit[key($chipLimit)]['max']
                ];

                $userChipLimit = $userChipLimitRepository->model
                    ->where([
                        'user_id' => $userChipLimitsParam['user_id'],
                        'user_type_id' => $userChipLimitsParam['user_type_id'],
                        'game_type_id' => $userChipLimitsParam['game_type_id'],
                    ])->first();

                if ($userChipLimit) {
                    $userChipLimit->update([
                        'min' => $userChipLimitsParam['min'],
                        'max' => $userChipLimitsParam['max']
                    ]);
                } else {
                    $userChipLimitRepository->createNew($userChipLimitsParam);
                }
            }
        }

        return ['user_id' => $user->id];
    }

    /**
     * Verifying user record if it exist by username
     *
     * @param  int $id
     * @return bool
     * @throws RepositoryBadRequestException
     */
    public function isUserExist(string $username) : bool
    {
        return $this->model->whereUsername($username)->exists();
    }

    /**
     * @param mixed $param
     * @return mixed
     */
    public function getUserInformation($param)
    {
        $select = [
            'users.id as id',
            'currencies.currency_code as currency',
            'users.username',
            'users.nickname',
            'users.balance',
            'users.point',
            'users.exp',
            'users.status',
            'users.last_login_time as last_login',
        ];

        $query = $this->model
            ->select($select)
            ->join('currencies', 'currencies.id', 'users.currency_id');

        if ((int)$param && is_numeric($param)) {
            return $query->where('users.id', $param);
        }

        return $query->where('users.username', $param);
    }

    /**
     * @inheritdoc
     */
    public function getPlayerInfoByAdminInquiry(string $userFilter)
    {
        $select = [
            'users.id as id',
            'users.username',
            'users.balance',
            'users.point',
            'users.exp',
        ];

        $query = $this->model
            ->select($select);

        if ((int)$userFilter && is_numeric($userFilter)) {
            return $query->where('users.id', $userFilter);
        }

        return $query->where('users.username', $userFilter);
    }

    /**
     * @inheritdoc
     */
    public function getPlayerForUpdateBalance(string $userFilter)
    {
        $select = [
            'users.id as id',
            'users.username',
            'users.balance',
            'currencies.currency_code as currency',
        ];

        $query = $this->model
            ->select($select)
            ->join('currencies', 'currencies.id', 'users.currency_id');

        if ((int)$userFilter && is_numeric($userFilter)) {
            return $query->where('users.id', $userFilter);
        }

        return $query->where('users.username', $userFilter);
    }

    public function updateUser($request, $username)
    {
        $user = $this->getUserByUsername($username);
        if ($user) {
            if (!is_null($request->get('nickname'))) {
                $user->nickname = $request->get('nickname');
            }
            $user->status = $request->get('status');
            $user->save();

            return $response = [
                'status' => 'Success!'
            ];
        } else {
            throw new RepositoryInternalException('User not found.');
        }
        return false;
    }

    /**
     * @param  string $username
     * @return bool
     * @throws RepositoryBadRequestException
     */
    public function isDisabled(string $username) : bool
    {
        $update = ['status' => config('ichips.user_statuses.disabled')];
        if ($user = $this->findBy('username', $username)) {
            if ($this->update($user->id, $update)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getUserBalance($username)
    {
        $select = [
            'users.balance'
        ];

        $userBalance = $this->model
            ->select($select)
            ->where(['username' => $username])
            ->first()
            ->toArray();

        return ['balance' => get_truncated_val($userBalance['balance'])];
    }

    /**
     * @param  string $query
     * @return mixed
     */
    public function search($request, $query)
    {
        $columns = array(
                    "id",
                    "username",
                    "nickname",
                    "status",
                    "currency_id",
                    "balance",
                    "point",
                    "exp",
                    // "last_login_time"
                );
        $user = array();
        if ($request['search'] == 1) {
            // user by user_id
            $user = $this->model->select($columns)->find($query);
        } else {
            // search by username
            $user = $this->model->where('username', $query)->select($columns)->first();
        }

        return !empty($user) ? [
            'user_id' => $user->id,
            'username' => $user->username,
            'status' => $user->status,
            'nickname' => $user->nickname,
            'currency' => $user->currency->currency_code,
            'balance' => $user->balance,
            'point' => $user->point,
            'exp' => $user->exp,
            // 'last_login_time' => $user->last_login_time
        ] : null;
    }

    /**
     * Update user point
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function updatePoint(Request $request, string $username)
    {
        $user = $this->getUserByUsername($username);
        $data = [
            'username' => $username,
            'method' => $request->user_points_method,
            'value' => $request->user_points_value,
            'column' => 'point',
        ];
        $newPoint = $this->addOrDeduct($user, $data);
        return ['point' => $newPoint];
    }

    /**
     * Update user exp
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function updateExp(Request $request, string $username)
    {
        $user = $this->getUserByUsername($username);
        $data = [
            'username' => $username,
            'method' => $request->user_exp_method,
            'value' => $request->user_exp_value,
            'column' => 'exp',
        ];
        $newExp = $this->addOrDeduct($user, $data);
        return ['exp' => $newExp];
    }
}
