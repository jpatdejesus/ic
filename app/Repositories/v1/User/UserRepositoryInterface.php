<?php

namespace App\Repositories\v1;

use App\Exceptions\RepositoryInternalException;
use App\Exceptions\ValidationResponseException;
use Illuminate\Http\Request;

/**
 * UserRepositoryInterface
 *
 * @package App\Contracts
 */
interface UserRepositoryInterface
{
    /**
     * @param $username
     * @return mixed
     */
    public function getUserByUsername(string $username);

    /**
     * @param $userRequest
     * @return array
     * @throws RepositoryInternalException
     * @throws ValidationResponseException
     */
    public function createUser($userRequest);

    /**
     * @param string $username
     * @return bool
     */
    public function isUserExist(string $username) : bool;

    /**
     * @param mixed $param
     * @return mixed
     */
    public function getUserInformation($param);

    /**
     * Get player for update balance
     *
     * @param string $userFilter
     * @return mixed
     */
    public function getPlayerForUpdateBalance(string $userFilter);

    /**
     * Get player's information by admin inquiry
     *
     * @param string $userFilter
     * @return mixed
     */
    public function getPlayerInfoByAdminInquiry(string $userFilter);

    /**
     * @param $request
     * @param $username
     * @return mixed
     */
    public function updateUser($request, $username);

    /**
     * @param string $username
     * @return bool
     */
    public function isDisabled(string $username) : bool;

    /**
     * @param $username
     * @return mixed
     */
    public function getUserBalance($username);

    /**
     * @param $request
     * @param $query
     * @return mixed
     */
    public function search($request, $query);

    /**
     * Update user point
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function updatePoint(Request $request, string $username);

    /**
     * Update user exp
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function updateExp(Request $request, string $username);

    /**
     * Update user balance by admin
     *
     * @param Request $request
     * @param string $username
     * @return array
     */
    public function updatePlayerBalanceByAdmin(string $username, $type, $amount);

    /**
     * Batch Update user balance by admin
     *
     * @param Request $request
     * @return array
     */
    public function batchUpdatePlayerBalanceByAdmin($batch_data);
}
