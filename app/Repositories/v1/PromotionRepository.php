<?php

namespace App\Repositories\v1;

use DB;
use App\Models\v1\Transaction;
use App\Http\v1\Client\Requests\FilterPromotionRequest;
use Illuminate\Support\Facades\Log;

class PromotionRepository extends BaseRepository
{
    private const DB_DATAREPO = 'datarepo';
    private const DB_DATAREPO_CONNECTION = 'datarepo';
    private const TRANSACTION_TABLE_SUFFIX = '_transactions';
    private const GAME_RESULTS_TABLE_SUFFIX = '_game_results';
    private const DRAW_TYPE_COMBINATION_3 = 'COMBINATION_THREE_NUMBERS';
    private const DRAW_TYPE_COMBINATION_2 = 'COMBINATION_TWO_NUMBERS';
    private const DRAW_TYPE_GAME_RESULT_PAIR = 'PAIR';
    private const DRAW_TYPE_FIX_NUMBERS = 'FIXED';
    private const DRAW_TYPE_GAME_RESULT = 'GAME_RESULT';
    private const PLAYER_PAIR_DB_VALUE = 'player_pair';
    private const BANKER_PAIR_DB_VALUE = 'banker_pair';
    private const PLAYER_CARD_TYPE = 'playerCards';
    private const BANKER_CARD_TYPE = 'bankerCards';
    private const TWO_CARDS = '2-Cards';
    private const THREE_CARDS = '3-Cards';
    private const PAIR_VALUE_ALL = 'ALL';

    /**
     * FilterPromotionRequest $filterPromotionRequest
     */protected $filterPromotionRequest;

    /**
     * Set Model for the Repository
     *
     * @return mixed
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * @param FilterPromotionRequest $filterPromotionRequest
     * @return mixed
     */
    public function filterGameTransactions()
    {
        $query = DB::connection(self::DB_DATAREPO_CONNECTION)
            ->table('information_schema.tables')
            ->select(['TABLE_NAME'])
            ->where('TABLE_SCHEMA', config('database.connections.datarepo.database'))
            ->where(function ($query) {
                $tables = ['baccarat_game_results', 'baccarat_transactions'];
                $drawType = $this->getFilterPromotionRequest()->draw_type;

                if ($drawType !== self::DRAW_TYPE_GAME_RESULT_PAIR && $drawType !== self::DRAW_TYPE_GAME_RESULT) {
                    $tables = array_merge($tables, ['dragontiger_game_results', 'dragontiger_transactions']);
                }
                $query->whereIn('TABLE_NAME', $tables);
            })
            ->pluck('TABLE_NAME');

        $tables = [];
        $gameResultTables = [];

        foreach ($query as $key => $value) {
            if (strpos($value, self::TRANSACTION_TABLE_SUFFIX)) {
                array_push($tables, substr($value, 0, strpos($value, self::TRANSACTION_TABLE_SUFFIX)));
            }
        }

        if ($tables) {
            $transactionTable = $tables[0] . self::TRANSACTION_TABLE_SUFFIX;
            $gameResultTable = $tables[0] . self::GAME_RESULTS_TABLE_SUFFIX;
            $query = $this->unionTablesQuery($query, $transactionTable, $gameResultTable);

            $this->addFilter($query, $tables[0]);

            for ($i = 1; $i < count($tables); $i++) {
                $table = $tables[$i] . self::TRANSACTION_TABLE_SUFFIX;
                $game = $tables[$i] . self::GAME_RESULTS_TABLE_SUFFIX;
                $unionQuery = $this->unionTablesQuery($query, $table, $game);

                $this->addFilter($unionQuery, $tables[$i]);

                $query->unionAll($unionQuery);
            }

            $this->overWriteQueryFilters($query);

            // return $query->toSql();
            //$query->orderBy('id', 'ASC');
            //$queriedValues = [];

            //$query->chunk(3000, function ($filteredResults) use (&$queriedValues) {
            //    foreach ($filteredResults as $filteredResult) {
            //        $queriedValues[] = $filteredResult;
            //    }
            //});
            
            return $query->get();
            //return $queriedValues;
        }
    }

    /**
     * @param $drawType
     * @param $query
     */
    public function overWriteQueryFilters(&$query)
    {
        $filterPromotionRequest = $this->getFilterPromotionRequest();

        if ($filterPromotionRequest->draw_type === self::DRAW_TYPE_GAME_RESULT) {
            $drawTypeValue = $filterPromotionRequest->draw_type_value;
            $queryCondition = 'whereRaw';

            $query = DB::connection(self::DB_DATAREPO_CONNECTION)
                ->query()
                ->fromSub($query, 'rt');

            $query->whereRaw(
                " RIGHT(sum_of_list(" . $this->filterGameResult('rt', self::PLAYER_CARD_TYPE) . "), 1)" .
                $drawTypeValue['bet_value']['player']['condition'] . "'" .
                $drawTypeValue['bet_value']['player']['value'] . "' " . $filterPromotionRequest->draw_type_condition .
                " RIGHT(sum_of_list(" . $this->filterGameResult('rt', self::BANKER_CARD_TYPE) . "), 1)" .
                $drawTypeValue['bet_value']['banker']['condition'] . "'" .
                $drawTypeValue['bet_value']['banker']['value'] . "'"
            );

            if (!isset($drawTypeValue['card_value']['condition']) && count($drawTypeValue['card_value']['cards']) === 1) {
                if (in_array(self::TWO_CARDS, $drawTypeValue['card_value']['cards'])) {
                    $charLength = "1";
                } else {
                    $charLength = "2";
                }

                $query->whereRaw(
                    " count_char_occurence(',', TRIM(TRAILING ',' FROM " . $this->filterGameResult('rt', self::PLAYER_CARD_TYPE) . ")) = {$charLength} AND ".
                    " count_char_occurence(',', TRIM(TRAILING ',".'"}'."' FROM " . $this->filterGameResult('rt', self::BANKER_CARD_TYPE) . ")) = {$charLength}"
                );
            } else {
                $query->whereRaw(
                    " count_char_occurence(',', TRIM(TRAILING ',' FROM " . $this->filterGameResult('rt', self::PLAYER_CARD_TYPE) . ")) IN (1, 2) ".
                    $drawTypeValue['card_value']['condition'] .
                    " count_char_occurence(',', TRIM(TRAILING ',".'"}'."' FROM " . $this->filterGameResult('rt', self::BANKER_CARD_TYPE) . ")) IN (1, 2)"
                );
            }
        }
    }

    /**
     * @param $query
     * @param $transactionTable
     * @param $gameResultTable
     */
    private function unionTablesQuery(&$query, $transactionTable, $gameResultTable)
    {
        return DB::connection(self::DB_DATAREPO_CONNECTION)
            ->table($transactionTable)
            ->select(
                $transactionTable . '.*',
                $gameResultTable . '.values',
                DB::raw($gameResultTable . ".id AS 'game_result_id'"),
                DB::raw("'" . $gameResultTable . "' AS 'tablename'")
            )
            ->leftJoin($gameResultTable, DB::raw('BINARY ' . $transactionTable . '.result_id'), '=', DB::raw('BINARY ' . $gameResultTable . '.result_id'));
    }

    /**
     * @param $query
     * @param $table
     */
    private function addFilter(&$query, $table)
    {
        $filterPromotionRequest = $this->getFilterPromotionRequest();

        $transactionTable = $table . self::TRANSACTION_TABLE_SUFFIX;
        $gameResultTable = $table . self::GAME_RESULTS_TABLE_SUFFIX;

        $start = trim($filterPromotionRequest->from);
        $end = trim($filterPromotionRequest->to);
        $betAmountCondition = trim($filterPromotionRequest->bet_amount_condition);
        $betAmount = $filterPromotionRequest->bet_amount;
        $tableList = json_decode($filterPromotionRequest->tables);
        $drawType = $filterPromotionRequest->draw_type;
        $drawTypeValue = $filterPromotionRequest->draw_type_value;
        $drawTypeCondition = $filterPromotionRequest->draw_type_condition;

        $query->whereBetween($transactionTable . '.bet_date', [$start, $end])
            ->where($transactionTable . '.bet_amount', $betAmountCondition, (float) $betAmount)
            ->whereIn($transactionTable . '.tablenumber', $tableList);

        if ($drawType === self::DRAW_TYPE_COMBINATION_3) {
            $possibleCombinations = pc_permute($drawTypeValue);
            $permutations = array_unique(explode(',', rtrim($possibleCombinations, ',')));

            $query->where(function ($query) use ($transactionTable, $permutations) {
                foreach ($permutations as $permutation) {
                    $query->orWhere($transactionTable . '.bet_code', 'REGEXP', $permutation . '$');
                }
            });
        }

        if ($drawType === self::DRAW_TYPE_COMBINATION_2) {
            $query->where($transactionTable . '.bet_code', 'REGEXP', $drawTypeValue . '$');
        }

        if ($drawType === self::DRAW_TYPE_GAME_RESULT_PAIR) {
            $resultPairCondition = 'where';

            list($playerDrawTypeValue, $bankerDrawTypeValue) = $drawTypeValue;

            if ((!empty($playerDrawTypeValue) && $playerDrawTypeValue['active']) && (!empty($bankerDrawTypeValue) && $bankerDrawTypeValue['active'])) {
                if ($drawTypeCondition == 'OR') {
                    $resultPairCondition = 'orWhere';
                }
            }

            if (!empty($playerDrawTypeValue) && $playerDrawTypeValue['active']) {
                $query->where(function ($query) use ($gameResultTable, $playerDrawTypeValue) {
                    $query->where($gameResultTable . '.result', 'LIKE', '%' . self::PLAYER_PAIR_DB_VALUE . '%');
                    if ($playerDrawTypeValue['value'] !== self::PAIR_VALUE_ALL) {
                        $query->whereRaw($this->filterPair($gameResultTable, self::PLAYER_CARD_TYPE, $playerDrawTypeValue['value']));
                    }
                });
            }

            if (!empty($bankerDrawTypeValue) && $bankerDrawTypeValue['active']) {
                $query->{$resultPairCondition}(function ($query) use ($gameResultTable, $bankerDrawTypeValue) {
                    $query->where($gameResultTable . '.result', 'LIKE', '%' . self::BANKER_PAIR_DB_VALUE . '%');
                    if ($bankerDrawTypeValue['value'] !== self::PAIR_VALUE_ALL) {
                        $query->whereRaw($this->filterPair($gameResultTable, self::BANKER_CARD_TYPE, $bankerDrawTypeValue['value']));
                    }
                });
            }
        }

        if ($drawType === self::DRAW_TYPE_FIX_NUMBERS) {
            $query->where($transactionTable . '.bet_code', 'REGEXP', $drawTypeValue . '$');
        }
    }

    /**
     * @param $table
     * @param $key
     * @param $value
     */
    public function filterPair($table, $type, $value)
    {
        return DB::raw("CONVERT(ROUND (
                                    (
                                        CHAR_LENGTH(json_extract_c(" . $table . ".values,'" . $type . "'))
                                        - CHAR_LENGTH( REPLACE ( json_extract_c(" . $table . ".values,'" . $type . "'), '" . $value . "', '') )
                                    ) / CHAR_LENGTH('" . $value . "')), SIGNED) >= 2");
    }

    /**
     * @param $table
     * @param $type
     * @return mixed
     */
    public function filterGameResult($table, $type)
    {
        $replace = [
            'C' => "''",
            'S' => "''",
            'D' => "''",
            'H' => "''",
            'A' => "'1'",
            'K' => "'0'",
            'Q' => "'0'",
            'J' => "'0'",
        ];

        $resultReplacer = "json_extract_c(" . $table . ".values, '" . $type . "')";

        foreach ($replace as $key => $value) {
            $resultReplacer = "REPLACE(" . $resultReplacer . ", '" . $key . "'," . $value . ")";
        }

        return $resultReplacer;
    }

    /**
     * @return mixed
     */
    public function getFilterPromotionRequest()
    {
        return $this->filterPromotionRequest;
    }

    /**
     * @param mixed $filterPromotionRequest
     *
     * @return self
     */
    public function setFilterPromotionRequest($filterPromotionRequest)
    {
        $this->filterPromotionRequest = $filterPromotionRequest;

        return $this;
    }
}
