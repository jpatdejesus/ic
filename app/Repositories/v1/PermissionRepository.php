<?php

namespace App\Repositories\v1;

use App\Models\v1\Permission;

class PermissionRepository extends BaseRepository
{
    public function model()
    {
        return Permission::class;
    }
}
