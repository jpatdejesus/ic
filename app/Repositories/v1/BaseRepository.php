<?php

namespace App\Repositories\v1;

use App\Repositories\Repository;

class BaseRepository extends Repository
{
    const DATAREPO_DB = 'datarepo';
    const ICHIPS_DB = 'ichips';
    const ICHIPS_ADMIN_DB = 'ichips-admin';
    const LOGS_DB = 'logs';

    public function model()
    {
        //return User::class;
    }

    /**
     * Create a new token.
     *
     * @param  $user
     * @return string
     */
    protected function jwtCreateToken($user)
    {

        $payload = [
            'iss' => "lumen-jwt",
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + config('jwt.lifetime') * 60
        ];

        return $this->jwt::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Decode token.
     *
     * @param  $token
     * @return string
     */
    protected function jwtDecodeToken($token)
    {
        return $this->jwt->decode($token, env('JWT_SECRET'), ['HS256']);
    }
}
