<?php

namespace App\Repositories\v1;

use App\Models\v1\User;
use Illuminate\Http\Request;

class UpdateUserPointOrExpRepository extends BaseRepository
{
    const UPDATE_USER_NOT_FOUND = 'User not found.';
    const METHOD_ADD = '+';
    const METHOD_SUBTRACT = '-';

    /**
     * Set Model for the Repository
     *
     * @return mixed|string|void
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Get User
     *
     * @param String $username
     * @return void
     */
    public function getUser($username)
    {

        if (!$user = $this->findBy('username', $username)) {
            return false;
        }
        return $user;
    }

    /**
     * Update user point
     *
     * @param Request $request
     * @param String $username
     * @return Array
     */
    public function updatePoint(Request $request, $username)
    {
        $data = [
            'username' => $username,
            'method' => $request->user_points_method,
            'value' => $request->user_points_value,
            'column' => 'point',
        ];
        return $this->addOrDeduct($data);
    }

    /**
     * Update user exp
     *
     * @param Request $request
     * @param String $username
     * @return Array
     */
    public function updateExp(Request $request, $username)
    {
        $data = [
            'username' => $username,
            'method' => $request->user_exp_method,
            'value' => $request->user_exp_value,
            'column' => 'exp',
        ];
        return $this->addOrDeduct($data);
    }

    /**
     * Add or deduct user point or exp
     *
     * @param Array $data
     * @return Array
     */
    private function addOrDeduct($data)
    {
        if (!$user = $this->getUser($data['username'])) {
            return [];
        }

        if ($data['method'] === self::METHOD_ADD) {
            $user->increment($data['column'], $data['value']);
        }

        if ($data['method'] === self::METHOD_SUBTRACT) {
             $user->decrement($data['column'], $data['value']);
        }
        
        return $this->getFormattedReturnValue($user, $data);
    }

    private function getFormattedReturnValue($user, $data)
    {
        $newBalance = $user[$data['column']];
        $newBalance = number_format($newBalance, 5, '.', '');
        return [ $data['column'] => $newBalance ];
    }
}
