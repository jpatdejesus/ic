<?php

namespace App\Repositories\v1;

use App\Models\v1\UserType;

class UserTypeRepository extends BaseRepository
{
    public function model()
    {
        return UserType::class;
    }
}
