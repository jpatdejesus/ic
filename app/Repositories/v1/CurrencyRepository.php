<?php

namespace App\Repositories\v1;

use App\Models\v1\Currency;
use App\Exceptions\CurrencyNotFoundException;

class CurrencyRepository extends BaseRepository
{
    public function model()
    {
        return Currency::class;
    }

    /**
     * @param $currencyCode
     * @return mixed
     */
    public function getCurrencyByCurrencyCode($currencyCode)
    {
        return $this->model->where('currency_code', $currencyCode);
    }

    /**
     * @param $code
     * @param $amount
     * @return mixed
     */
    public function getRMBConversion($code, $amount = 1)
    {
        $CNYCurrency = [
            'CNY',
            'RMB',
        ];

        if (in_array($code, $CNYCurrency)) {
            return number_format($amount, 5, '.', '');
        }

        $currency = $this->model->whereCurrencyCode($code)->first();

        if (!$currency) {
            throw new CurrencyNotFoundException('Currency code (' . $code . ') of this user not found!');
        }

        $conversion = $currency->conversion_by_rmb * $amount;

        return number_format($conversion, 5, '.', '');
    }
}
