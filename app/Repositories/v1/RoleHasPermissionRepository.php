<?php

namespace App\Repositories\v1;

use App\Models\v1\RoleHasPermission;

class RoleHasPermissionRepository extends BaseRepository
{
    public function model()
    {
        return RoleHasPermission::class;
    }
}
