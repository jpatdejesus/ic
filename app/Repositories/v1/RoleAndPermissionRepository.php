<?php

namespace App\Repositories\v1;

use App\Models\v1\AdminRole;
use App\Exceptions\ValidationResponseException;
use App\Services\RoleManagement\RoleManagementService;
use App\Services\RoleManagement\RoleManagementAppTypeService;
use \Illuminate\Support\Facades\Route;

class RoleAndPermissionRepository extends BaseRepository
{
    const MODULE = "module";
    const PROTECTED_MODULES = [
        'module_role_management',
        'module_update_balance',
        'module_profile_management'
    ];
    const VALIDATION_ERROR_MESSAGES = [
        'ADD_NEW_ROLE_SUCCESS' => 'New role has been added successfully!',
        'ADD_NEW_ROLE_ERROR' => 'Error adding new role!',
        'GET_ROLE_ERROR' => 'Error getting roles!',
        'PERMISSION_NOT_ALLOWED_ERROR' => 'Request not permitted!',
        'ROLE_NAME_ALREADY_EXISTS_ERROR' => 'Role name already exists!',
        'DELETE_ROLE_SUCCESS' => 'Role has been deleted successfully!',
        'UPDATE_ROLE_SUCCESS' => 'Role has been updated successfully!',
        'DELETE_ROLE_FAILED' => 'Delete role failed!',
        'UPDATE_ROLE_FAILED' => 'Update role failed!',
        'CANNOT_DELETE_DEFAULT_ROLE' => 'Cannot delete default role!',
        'CANNOT_UPDATE_DEFAULT_ROLE' => 'Cannot update default role!',
        'ROLE_DOES_NOT_EXIST' => 'Role does not exist!',
        'INVALID_ROLE_GROUP' => 'The selected role group is invalid!',
        'SOMETHING_WENT_WRONG_ERROR' => 'Something went wrong. Please try again later!',
        'DUPLICATE_ROLE_ERROR' => 'Error duplicating role!',
        'DUPLICATE_NON_EXISTENT_ROLE_ERROR' => 'Trying to duplicate non-existent role!',
        'DUPLICATE_ROLE_AND_PERMISSION_SUCCESS' => 'Role and Permissions duplicated successfully!',
        'UPDATE_ROLE_AND_PERMISSION_SUCCESS' => 'Role Permissions has been updated successfully!',
    ];

    protected $role;
    protected $permission;
    protected $roleHasPermission;
    protected $roleManagementService;

    public $response = [];
    public $rawData = null;

    public $listRoutes = [];
    public $parentObj = [];
    public $listOfSubParent = [];
    public $roleGroups;

    public function __construct(
        RoleManagementService $roleManagementService
    ) {
        parent::__construct();

        $rolesEntity = new RoleManagementAppTypeService('roles');
        $this->role = $rolesEntity->getModel();

        $roleHasPermissionsEntity = new RoleManagementAppTypeService('role_has_permissions');
        $this->roleHasPermission = $roleHasPermissionsEntity->getModel();

        $permissionsEntity = new RoleManagementAppTypeService('permissions');
        $this->permission = $permissionsEntity->getModel();

        $this->roleManagementService = $roleManagementService;
        $this->roleGroups = $this->roleManagementService->getRoleGroups(true);

        if (in_array($this->roleManagementService->getUserRoleGroup(), $this->roleManagementService->getRestrictedRoleGroups())) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['PERMISSION_NOT_ALLOWED_ERROR'], []);
        }
    }

    public function model()
    {
        return AdminRole::class;
    }

    public function buildResponse($message = null, $data = [])
    {
        $this->response['message'] = $message;
        $this->response['data'] = $data;
    }

    /**
     * @param $params
     * @return array|bool
     */
    public function getRoles($params)
    {
        $userRoleGroup = $this->roleManagementService->getUserRoleGroup();
        $selectedRoleGroupData = $this->roleManagementService->getSelectedRoleGroupData($params['role_group']);

        if ($userRoleGroup === $this->roleGroups['super_admin']) {
            $this->rawData = $this->role->select('id', 'name')
                ->where(function ($query) use ($selectedRoleGroupData) {
                    $query->orWhere('master_role_id', $selectedRoleGroupData->id);
                    $query->orWhere('master_role_id', null);
                })
                ->where('role_group', $params['role_group'])
                ->get();

            if (count($this->rawData)) {
                $data = [];

                foreach ($this->rawData as $key => $value) {
                    $data[$key] = [
                        'id' => $value->id,
                        'name' => ucwords(str_replace('_', ' ', $value->name))
                    ];
                }

                $this->buildResponse('List of role', $data);
            } else {
                $this->buildResponse('Empty roles');
            }

            return $this->response;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getRoleGroups()
    {
        $roleGroups = $this->roleManagementService->getRoleGroups();
        $hiddenRoleGroups = ['SUPER_ADMIN'];

        if (count($roleGroups)) {
            if (request()->request_app_type === strtoupper(config('ichips.app_types.admin'))) {
                $data = [];
                foreach ($roleGroups as $key => $val) {
                    if (!in_array($val, $hiddenRoleGroups)) {
                        array_push($data, $val);
                    }
                }
                $this->buildResponse('List of role groups', $data);
            } else {
                $this->buildResponse('List of role groups', $roleGroups);
            }
        }

        return $this->response;
    }

    /**
     * @param $params
     * @return array|bool
     * @throws ValidationResponseException
     */
    public function addRole($params)
    {
        $data = [];
        $defaultPermissionIds = [];
        $userRoleGroup = $this->roleManagementService->getUserRoleGroup();
        $defaultPermissions = [
            'module_profile_management' => [],
            'module_role_management' => [
                'GET::Admin_Get_User_Role_and_Its_Permissions',
                'GET::Admin_Roles_By_Access_Level'
            ],
        ];

        if (request()->request_app_type === strtoupper(config('ichips.app_types.admin'))) {
            foreach ($defaultPermissions as $module => $permission_codes) {
                if (count($permission_codes) <= 0) {
                    $moduleId = $this->permission->select('id')->whereCode($module)->firstOrFail()->id;

                    array_push($defaultPermissionIds, $moduleId);

                    $modulePermissions = $this->permission->select('id')->whereParentId($moduleId)->get();

                    if (count($modulePermissions)) {
                        foreach ($modulePermissions->toArray() as $key => $value) {
                            array_push($defaultPermissionIds, $value['id']);
                        }
                    }
                } else {
                    $moduleId = $this->permission->select('id')->whereCode($module)->firstOrFail()->id;

                    array_push($defaultPermissionIds, $moduleId);

                    foreach ($permission_codes as $key => $permission_code) {
                        $modulePermissions = $this->permission->select('id')->whereCode($permission_code)->get();

                        if (count($modulePermissions)) {
                            foreach ($modulePermissions->toArray() as $key => $value) {
                                array_push($defaultPermissionIds, $value['id']);
                            }
                        }
                    }
                }
            }
        }

        if (!$userRoleGroup === $this->roleGroups['super_admin']) {
            return false;
        }

        $roleObj = $this->role->where('name', $params['role_name'])
            ->where('role_group', $params['role_group'])
            ->get();

        if (count($roleObj)) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['ROLE_NAME_ALREADY_EXISTS_ERROR'], []);
        }

        $roleObj = new $this->role;
        $roleObj->name = $params['role_name'];
        $roleObj->role_group = $params['role_group'];
        $roleObj->is_default = 0;
        $roleObj->master_role_id = null;

        if ($roleObj->save()) {
            $this->rawData = $this->role->where('name', $params['role_name'])->get();

            foreach ($this->rawData as $key => $value) {
                $data[$key] = [
                    'id' => $value->id,
                    'name' => ucwords(str_replace('_', ' ', $value->name))
                ];
            }

            if (count($defaultPermissionIds)) {
                foreach ($defaultPermissionIds as $key => $value) {
                    $this->roleHasPermission->create(['permission_id' => $value, 'role_id' => $roleObj->id]);
                }
            }

            $this->buildResponse(self::VALIDATION_ERROR_MESSAGES['ADD_NEW_ROLE_SUCCESS'], $data);
            return $this->response;
        }

        return false;
    }

    /**
     * @param $params
     * @return mixed
     * @throws ValidationResponseException
     */
    public function deleteRole($params)
    {
        $roleDetailsQuery = $this->role->where('id', $params['role_id'])
            ->where('role_group', $params['role_group']);

        if (!count($roleDetails = $roleDetailsQuery->get())) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['ROLE_DOES_NOT_EXIST'], []);
        }

        if ($roleDetails[0]->is_default) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['CANNOT_DELETE_DEFAULT_ROLE'], []);
        }

        if (!$roleDetailsQuery->delete()) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['DELETE_ROLE_FAILED'], []);
        }

        $this->buildResponse(self::VALIDATION_ERROR_MESSAGES['DELETE_ROLE_SUCCESS']);
        return $this->response;
    }

    /**
     * @param $params
     * @return mixed
     * @throws ValidationResponseException
     */
    public function updateRole($params)
    {
        $roleDetailsQuery = $this->role->where('id', $params['role_id']);

        if (!count($roleDetails = $roleDetailsQuery->get())) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['ROLE_DOES_NOT_EXIST'], []);
        }

        if ($roleDetails[0]->is_default) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['CANNOT_UPDATE_DEFAULT_ROLE'], []);
        }

        $roleDetails[0]->name = $params['role_name'];
        $roleDetails[0]->role_group = $params['role_group'];

        if (!$roleDetails[0]->save()) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['UPDATE_ROLE_FAILED'], []);
        }

        $this->buildResponse(self::VALIDATION_ERROR_MESSAGES['UPDATE_ROLE_SUCCESS'], ['name' => $roleDetails[0]->name]);
        return $this->response;
    }

    /**
     * @param $params
     * @return array
     * @throws ValidationResponseException
     */
    public function duplicateRoleAndPermissions($params)
    {
        $roleObj = $this->role->where('id', $params['role_id'])
            ->where('role_group', $params['role_group'])
            ->get();

        $nameExist = $this->role->where('name', $params['role_name'])
            ->where('role_group', $params['role_group'])
            ->get();

        if (!count($roleObj)) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['DUPLICATE_NON_EXISTENT_ROLE_ERROR'], []);
        }

        if (count($nameExist)) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['ROLE_NAME_ALREADY_EXISTS_ERROR'], []);
        }

        $role = new $this->role;
        $role->name = $params['role_name'];
        $role->role_group = $roleObj[0]->role_group;
        $role->is_default = 0;

        if (!$role->save()) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['SOMETHING_WENT_WRONG_ERROR'], []);
        }

        $permission_list = $this->roleHasPermission->where('role_id', $params['role_id'])->get();

        foreach ($permission_list as $key => $permissions) {
            $list = [
                'permission_id' => $permissions->permission_id,
                'role_id' => $role->id,
                'action' => $permissions->action
            ];
            $this->roleHasPermission->create($list);
        }

        $this->buildResponse(self::VALIDATION_ERROR_MESSAGES['DUPLICATE_ROLE_AND_PERMISSION_SUCCESS']);
        return $this->response;
    }

    /**
     * @param $params
     * @return array
     */
    public function getRoleAndPermissions($params)
    {
        $roleAndPermissions = [];
        $roles = $this->role->whereRoleGroup($params['role_group'])->get()->toArray();
        $groupedRoutes = $this->getGroupedPermissions();

        foreach ($roles as $key => $roleDetails) {
            $roleData = [
                'id' => $roleDetails['id'],
                'role_name' => $roleDetails['name'],
                'is_default' => (int)$roleDetails['is_default'],
                'role_group' => $roleDetails['role_group'],
                'permissions' => []
            ];

            foreach ($groupedRoutes as $key => $module) {
                $moduleData = [
                    'id' => $module['id'],
                    'module_name' => $module['module_name'],
                    'permissions' => [],
                ];

                $isModuleExists = $this->roleHasPermission
                    ->where('permission_id', $module['id'])
                    ->where('role_id', $roleDetails['id'])
                    ->count();

                if (!$isModuleExists) {
                    continue;
                }

                foreach ($module['permissions'] as $permissionKey => $permission) {
                    $permissionData = [
                        'id' => $permission['id'],
                        'name' => $permission['name'],
                        'code' => $permission['code'],
                        'description' => $permission['description'],
                    ];
                    $isRoleAndPermissionExists = $this->roleHasPermission
                        ->where('permission_id', $permission['id'])
                        ->where('role_id', $roleDetails['id'])
                        ->get();

                    if (count($isRoleAndPermissionExists)) {
                        $permissionData['is_active'] = 1;
                    } else {
                        $permissionData['is_active'] = 0;
                    }
                    array_push($moduleData['permissions'], $permissionData);
                }
                array_push($roleData['permissions'], $moduleData);
            }
            array_push($roleAndPermissions, $roleData);
        }

        $this->buildResponse('List of role and permission', $roleAndPermissions);
        return $this->response;
    }

    /**
     * @param $params
     * @return array
     * @throws ValidationResponseException
     */
    public function addOrUpdateRolePermissions($params)
    {
        $permissions = $params['permissions'];
        $roleDetails = $this->role->whereId($params['role_id'])->first();
        $ignoredPermissionIds = [];

        if (!$roleDetails) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['ROLE_DOES_NOT_EXIST'], []);
        }

        if ((int)$roleDetails->is_default) {
            throw new ValidationResponseException(self::VALIDATION_ERROR_MESSAGES['CANNOT_UPDATE_DEFAULT_ROLE'], []);
        }

        $moduleRoleManagement = $this->permission->select('id')->whereIn('code', self::PROTECTED_MODULES)->get();

        foreach ($moduleRoleManagement->toArray() as $key => $module) {
            array_push($ignoredPermissionIds, $module['id']);

            $roleManagementPermissions = $this->permission->select('id')->whereParentId($module['id'])->get()->toArray();

            foreach ($roleManagementPermissions as $key => $permission) {
                array_push($ignoredPermissionIds, $permission['id']);
            }
        }

        if (request()->request_app_type === strtoupper(config('ichips.app_types.admin'))) {
            $this->roleHasPermission->whereNotIn('permission_id', $ignoredPermissionIds)->where('role_id', $params['role_id'])->delete();
        } else {
            $this->roleHasPermission->where('role_id', $params['role_id'])->delete();
        }

        if (isset($permissions)) {
            foreach ($permissions as $key => $permission_id) {
                $this->roleHasPermission->create(['permission_id' => $permission_id, 'role_id' => $params['role_id']]);
                if (request()->request_app_type == strtoupper(config('ichips.app_types.admin'))) {
                    $permissionChildren = $this->permission->whereLinkedRouteId($permission_id)->get();
                    if (count($permissionChildren)) {
                        foreach ($permissionChildren->toArray() as $key => $value) {
                            $this->roleHasPermission->create(['permission_id' => $value['id'], 'role_id' => $params['role_id']]);
                        }
                    }
                }
            }
        }

        $this->buildResponse(self::VALIDATION_ERROR_MESSAGES['UPDATE_ROLE_AND_PERMISSION_SUCCESS']);
        return $this->response;
    }

    /**
     * @return array
     */
    public function getUserRoleAndPermissions()
    {
        $modulesAndPermissions = [];
        $userDetails = request()->user_data;
        $roleDetails = $this->role->whereName($userDetails['role_name'])->firstOrFail();
        $groupedRoutes = $this->getGroupedPermissions(true);

        $modules = [];
        foreach ($groupedRoutes as $key => $module) {
            array_push($modules, $module);
            $permissions = [];

            foreach ($module['permissions'] as $permissionkey => $permission) {
                $isRoleAndPermissionExists = $this->roleHasPermission
                    ->where('permission_id', $permission['id'])
                    ->where('role_id', $roleDetails->id)
                    ->get();

                if (count($isRoleAndPermissionExists)) {
                    array_push($permissions, $permission);
                }
            }

            $modules[$key]['permissions'] = $permissions;
        }

        foreach ($modules as $key => $value) {
            if (count($value['permissions'])) {
                array_push($modulesAndPermissions, $value);
            }
        }

        $this->buildResponse('List of role and permission', $modulesAndPermissions);
        return $this->response;
    }

    /**
     * @return array
     */
    public function getRolesByAccessLevel()
    {
        $roles = $this->role->select(['id', 'name'])->get();
        if (count($roles)) {
            $roles = $roles->toArray();
        }
        $this->buildResponse('Roles by access level', $roles);

        return $this->response;
    }

    /**
     * @params $displayAll
     * @return array
     */
    public function getGroupedPermissions($displayAll = false)
    {
        $returnData = [];
        $selectFilter = ['id', 'name', 'code', 'description'];
        $protectedModules = self::PROTECTED_MODULES;

        if (request()->request_app_type == strtoupper(config('ichips.app_types.client'))) {
            $permissionModules = $this->permission
                ->whereParentId(null)
                ->get();
        } else {
            if ($displayAll) {
                $protectedModules = [];
            }
            $permissionModules = $this->permission
                ->where('parent_id', null)
                ->whereNotIn('code', $protectedModules)
                ->get();
        }

        if (count($permissionModules)) {
            $modules = $permissionModules->toArray();

            foreach ($modules as $key => $value) {
                $data = [
                    'id' => $value['id'],
                    'module_name' => $value['name'],
                ];

                if (request()->request_app_type == strtoupper(config('ichips.app_types.admin'))) {
                    $permissions = $this->permission
                        ->select($selectFilter)
                        ->whereParentId($value['id'])
                        ->whereLinkedRouteId(null)
                        ->get();
                } else {
                    $permissions = $this->permission
                        ->select($selectFilter)
                        ->whereParentId($value['id'])
                        ->get();
                }

                if (count($permissions)) {
                    $data['permissions'] = $permissions->toArray();
                }

                array_push($returnData, $data);
            }
        }

        return $returnData;
    }
}
