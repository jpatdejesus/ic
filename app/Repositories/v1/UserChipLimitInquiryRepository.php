<?php

namespace App\Repositories\v1;

use App\Exceptions\RepositoryNotFoundException;
use App\Models\v1\Currency;
use App\Models\v1\Game;
use App\Services\UserType\EntityService;
use Illuminate\Support\Facades\DB;

class UserChipLimitInquiryRepository extends BaseRepository
{
    /**
     * @var mixed
     */
    protected $userRepository;

    public function __construct()
    {
        parent::__construct();

        $entityService = new EntityService('users');
        $this->userRepository = $entityService->getRepository();
    }

    public function model()
    {
        return Game::class;
    }

    /**
     * Get User Chip Limit.
     *
     * @param $request
     * @return array
     * @throws RepositoryNotFoundException
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function getChipLimit($request)
    {
        $params = $request->only(['param', 'user_type']);
        $userTypeID = (new UserTypeRepository())->findBy('user_type_name', $params['user_type'])->id;
        $user = $this->userRepository->getUserInformation($params['param'])->first();

        if ($userTypeID && $user) {
            $columns = [
                'games.game_name',
                DB::raw('COALESCE(user_chip_limits.min, user_chip_limit_defaults.min) as min'),
                DB::raw('COALESCE(user_chip_limits.max, user_chip_limit_defaults.max) as max'),
            ];

            $user->currency = (new Currency())->where('currency_code', $user->currency)->first()->id;

            $data = $this->model
                ->select($columns)
                ->leftJoin('user_chip_limits', function ($join) use ($user, $userTypeID) {
                    $join->on('user_chip_limits.game_type_id', '=', 'games.game_type_id')
                        ->where([
                            'user_chip_limits.user_id' => $user->id,
                            'user_chip_limits.user_type_id' => $userTypeID,
                        ]);
                })
                ->leftJoin('user_chip_limit_defaults', function ($join) {
                    $join->on('user_chip_limit_defaults.game_type_id', '=', 'games.game_type_id');
                })
                ->where('user_chip_limit_defaults.currency_id', '=', $user->currency)
                ->get()
                ->toArray();

            if (count($data) === 0) {
                $columns = [
                    'games.game_name',
                    DB::raw('user_chip_limit_defaults.min'),
                    DB::raw('user_chip_limit_defaults.max'),
                ];

                $data = $this->model
                    ->select($columns)
                    ->leftJoin('user_chip_limit_defaults', function ($join) {
                        $join->on('user_chip_limit_defaults.game_type_id', '=', 'games.game_type_id');
                    })
                    ->whereNull('user_chip_limit_defaults.currency_id')
                    ->get()
                    ->toArray();
            }

            return [
                'username' => $user->username,
                'data' => $data
            ];
        } else {
            throw new RepositoryNotFoundException('User not found!');
        }
    }
}
