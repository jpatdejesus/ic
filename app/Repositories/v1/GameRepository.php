<?php

namespace App\Repositories\v1;

use App\Exceptions\ValidationResponseException;
use App\Models\v1\Game;

class GameRepository extends BaseRepository
{
    public function model()
    {
        return Game::class;
    }

    public function getGameCodes()
    {
        return $this->all(['game_code'])->pluck(['game_code']);
    }

    /**
     * @param string $gameCode
     * @return bool
     */
    public function validateGameCode(string $gameCode) : bool
    {
        if (in_array($gameCode, $this->getGameCodes()->toArray())) {
            return true;
        }

        return false;
    }

    public function validateGameCodes(array $chipLimits)
    {
        $errors = [];
        foreach ($chipLimits as $chipLimit) {
            if (!$game = $this->findBy('game_code', key($chipLimit))) {
                $errors[] = "The selected game code " . "'" . key($chipLimit) . "'" . " is invalid.";
            }

            if ($chipLimit[key($chipLimit)]['min'] >= $chipLimit[key($chipLimit)]['max']) {
                $errors[] = "The selected game code " . "'" . key($chipLimit) . "'" . " min amount should be less than the max amount.";
            }
        }

        if (count($errors) > 0) {
            throw new ValidationResponseException(
                'Validation error!',
                ['chip_limits' => $errors]
            );
        }
    }
}
