<?php

namespace App\Repositories\v1;

use App\Models\v1\RoleCredential;

class RoleCredentialRepository extends BaseRepository
{
    public function model()
    {
        return RoleCredential::class;
    }
}
