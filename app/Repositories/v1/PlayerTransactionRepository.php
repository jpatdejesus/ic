<?php

namespace App\Repositories\v1;

use DB;
use App\Models\v1\Transaction;

class PlayerTransactionRepository extends BaseRepository
{
    /**
     * @var mixed
     */
    protected $username;

    /**
     * Set Model for the Repository
     *
     * @return mixed
     */
    public function model()
    {
        return Transaction::class;
    }

    /**
     * @return array
     */
    public function getAllAdminTransaction($params)
    {
        $query = DB::connection('datarepo')
            ->table('information_schema.TABLES')
            ->select(['TABLE_NAME'])
            ->where('TABLE_SCHEMA', config('database.connections.datarepo.database'))
            ->pluck('TABLE_NAME');

        if (request()->game_name) {
            $tableName = request()->game_name . '_transactions';
            if (in_array($tableName, $query->toArray())) {
                $this->model->setTable($tableName);
            }
        }

        return $this->model->playerTransactions($params);

        /*$this->setUsername($username);
        $datarepo = config('database.connections.datarepo.database');

        $model = new Transaction;

        $playerTransactionDataTable = new PlayerTransactionDataTable();
        $dataTable = $playerTransactionDataTable->initialize();
        $params = $dataTable->searchParams;
        $params['column'] = $dataTable->sortColumn;
        $params['order_by'] = $dataTable->sortOrder;
        $params['table_columns'] = $dataTable->tableColumns;
        $params['date_start'] = $playerTransactionDataTable->getRequest()->date_start;
        $params['date_end'] = $playerTransactionDataTable->getRequest()->date_end;
        $params['user_type'] = $playerTransactionDataTable->getRequest()->user_type;
        $params['bet_code'] = $playerTransactionDataTable->getRequest()->bet_code;
        $params['shoe_hand_number'] = $playerTransactionDataTable->getRequest()->shoe_hand_number;
        $params['game_name'] = $playerTransactionDataTable->getRequest()->game_name;
        $params['table_number'] = $playerTransactionDataTable->getRequest()->table_number;
        $params['username'] = $username;

        $query = DB::connection('datarepo')
            ->table('information_schema.TABLES')
            ->select(['TABLE_NAME'])
            ->where('TABLE_SCHEMA', config('database.connections.datarepo.database'))
            ->pluck('TABLE_NAME');

        if (request()->game_name) {
            $tableName = request()->game_name . '_transactions';
            if (in_array($tableName, $query->toArray())) {
                $model->setTable($tableName);
            }
        }
        $result = [];
        $lastResultID = 0;

        if ($rowCount = $model->playerTransactions($params)->count()) {
            if ($dataTable->sortOrder === 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at' || $dataTable->sortColumn === 'bet_date')) {
                $firstResultID = $model->playerTransactions($params)->orderBy($datarepo . '.' . $tableName . '.id', 'DESC')->limit(1)->first()->id;

                $lastResultID = $model->playerTransactions($params)->orderBy($datarepo . '.' . $tableName . '.id', 'ASC')->limit(1)->first()->id;

                $dataTable->start = $dataTable->start == 0 || $dataTable->start == 1 || $dataTable->start == $firstResultID
                ? $firstResultID : $dataTable->start - 2;

                $end = $dataTable->start - ($dataTable->limit - 1);

                $baseResult = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$end, $dataTable->start]);
                while (($baseCount = $baseResult->count()) < $dataTable->limit && $end >= $lastResultID) {
                    $end -= ($dataTable->limit - $baseCount);

                    $baseResult = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$end, $dataTable->start]);
                }

                $result = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$end, $dataTable->start])
                    ->groupBy($dataTable->groupByColumns)
                    ->get();

                $data = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$end, $dataTable->start])
                    ->groupBy($dataTable->groupByColumns)
                    ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
                    ->get();
            } else {
                $lastResultID = $model->playerTransactions($params)->orderBy($datarepo . '.' . $tableName . '.id', 'DESC')->limit(1)->first()->id;
                $end = $dataTable->start == 1 ? $dataTable->limit : ($dataTable->start + $dataTable->limit) - 1;

                $baseResult = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$dataTable->start, $end]);
                while (($baseCount = $baseResult->count()) < $dataTable->limit && $end <= $lastResultID) {
                    $end += ($dataTable->limit - $baseCount);

                    $baseResult = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$dataTable->start, $end]);
                }

                $result = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$dataTable->start, $end])
                    ->groupBy($dataTable->groupByColumns)
                    ->get();

                $data = $model->playerTransactions($params)->whereBetween($datarepo . '.' . $tableName . '.id', [$dataTable->start, $end])
                    ->groupBy($dataTable->groupByColumns)
                    ->orderBy($dataTable->sortColumn, $dataTable->sortOrder)
                    ->get();
            }
        }

        $return = [
            'count_records' => (!empty($result) ? $rowCount : 0),
            'count_results' => (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => 0,
            'data' => (!empty($data) ? $data : []),
        ];

        if (!empty($result) && $result->last()) {
            $lastResult = $result->last()->id;
            if ($dataTable->sortOrder == 'DESC' && ($dataTable->sortColumn === 'id' || $dataTable->sortColumn === 'created_at')) {
                if ($lastResult > $lastResultID) {
                    $return['last_record_id'] = $lastResult;
                }
            } else {
                if ($lastResult < $lastResultID) {
                    $return['last_record_id'] = $lastResult;
                }
            }
        }

        return $return;*/
    }

    /**
     * @param $query
     */
    private function addFilter(&$query)
    {
        $start = trim(request()->date_start);
        $end = trim(request()->date_end);
        $username = $this->getUsername();

        $query->whereBetween('created_at', [$start, $end])
            ->where('gamename', 'LIKE', '%' . request()->game_name . '%')
            ->where('user_type', request()->user_type)
            ->where('shoehandnumber', request()->shoe_hand_number)
            ->where('tablenumber', request()->table_number)
            ->where('bet_code', 'LIKE', '%' . request()->bet_code . '%');

        if ($username) {
            $query->where('username', $username);
        }
    }
}
