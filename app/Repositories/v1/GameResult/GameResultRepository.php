<?php

namespace App\Repositories\v1;

use App\Models\v1\GameResult;
use App\Exceptions\ValidationResponseException;

class GameResultRepository extends GameResultBaseRepository
{
    public $gameCode;

    public function model()
    {
        return GameResult::class;
    }

    /**
     * @param $gameCode
     * @param $data
     * @return mixed
     */
    public function createGameResult($gameCode, $data)
    {
        $this->setTable($gameCode);

        $nullables = ['settle_date', 'close_date', 'copy_order_time'];

        foreach ($nullables as $nullable) {
            if ((isset($data[$nullable]) && $data[$nullable] == '')) {
                unset($data[$nullable]);
            }
        }

        return $this->model->create($data);
    }

    /**
     * Get first result
     *
     * @param string $gameCode
     * @return GameResult
     */
    public function first($gameCode)
    {
        $this->setTable($gameCode);

        return $this->model->first();
    }

    /**
     * Check for result uniqueness
     *
     * @param string $value
     * @return boolean
     */
    public function isResultUnique($value)
    {
        $transaction = $this->model->where('result', $value)->first();
        if ($transaction) {
            return false;
        }
        return true;
    }

    /**
     * @param $gameCode
     * @param $historyParams
     * @return array
     */
    public function getGameResult($gameCode, $historyParams)
    {
        $historyParams->game_name = $gameCode;

        return $this->model->historyGameResult($historyParams);

        /*$result = [];

        if ($rowCount = $this->model->count()) {
            $lastResultID = $this->model->orderBy('id', 'DESC')->limit(1)->first()->id;

            $start = $historyParams->start == 1 ? 1 : $historyParams->start + 1;

            $end = $historyParams->end;

            $baseResult = $this->model->whereBetween('id', [$start, $end]);

            while (($baseCount = $baseResult->count()) < $historyParams->limit && $end <= $lastResultID) {
                $end += ($historyParams->limit - $baseCount);

                $baseResult = $this->model->whereBetween('id', [$start, $end]);
            }

            $result = $baseResult->get();
        }

        return [
            'message' => $historyParams->message,
            'count_records' => (!empty($result) && $result->count() ? $rowCount : 0),
            'count_results' =>  (!empty($result) && $result->count() ? $result->count() : 0),
            'last_record_id' => (!empty($result) && $result->last() && $result->last()->id < $lastResultID ? $result->last()->id : 0),
            'data' => (!empty($result) ? $result : []),
        ];*/
    }

     /**
     * Check for result_id uniqueness
     *
     * @param string $value
     * @return boolean
     */
    public function isResultIdUnique($value)
    {
        $transaction = $this->model->where('result_id', $value)->first();
        if ($transaction) {
            return false;
        }
        return true;
    }
}
