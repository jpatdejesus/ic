<?php

namespace App\Repositories\v1;

use Illuminate\Support\Facades\Schema;

abstract class GameResultBaseRepository extends BaseRepository implements GameResultRepositoryInterface
{
    const TABLE_PREFIX = '_game_results';

    public $repository;

    /**
     * Set dynamic table for Game Result model
     *
     * @param $gameCode
     */
    public function setTable($gameCode)
    {
        $this->gameCode = $gameCode;

        $this->model->setTable($this->gameCode . self::TABLE_PREFIX);
    }

    /**
     * @param $gameCode
     * @param $data
     * @return mixed
     */
    abstract public function createGameResult($gameCode, $data);

    /**
     * Check if Database table exists
     *
     * @param $gameCode
     * @return bool
     */
    public function isTableExist($gameCode)
    {
        return Schema::connection(parent::DATAREPO_DB)->hasTable($gameCode . self::TABLE_PREFIX);
    }

    /**
     * Check for result uniqueness
     *
     * @param string $value
     * @return boolean
     */
    abstract public function isResultUnique($value);

    /**
     * @param $gameCode
     * @param $historyParams
     * @return mixed
     */
    abstract public function getGameResult($gameCode, $historyParams);
}
