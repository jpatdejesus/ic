<?php

namespace App\Repositories\v1;

/**
 * GameResultRepositoryInterface
 *
 * @package App\Contracts
 */
interface GameResultRepositoryInterface
{
    /**
     * Set dynamic table for Game Result model
     *
     * @param $gameCode
     */
    public function setTable($gameCode);

    /**
     * @param $gameCode
     * @param $data
     * @return mixed
     */
    public function createGameResult($gameCode, $data);

    /**
     * Check if Database table exists
     *
     * @param $gameCode
     * @return bool
     */
    public function isTableExist($gameCode);

    /**
     * Check for result uniqueness
     *
     * @param string $value
     * @return boolean
     */
    public function isResultUnique($value);

    /**
     * @param $gameCode
     * @param $historyParams
     * @return mixed
     */
    public function getGameResult($gameCode, $historyParams);
}
