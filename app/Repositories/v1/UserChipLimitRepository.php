<?php

namespace App\Repositories\v1;

use App\Models\v1\Game;
use App\Models\v1\UserType;
use App\Models\v1\UserChipLimit;
use App\Models\v1\UserChipLimitDefaults;
use App\Services\UserType\EntityService;

class UserChipLimitRepository extends BaseRepository
{
    protected $user;
    protected $userRepository;

    public function __construct()
    {
        parent::__construct();
        
        $entityService = new EntityService('users');
        $this->user = $entityService->getModel();
        $this->userRepository = $entityService->getRepository();
    }

    public function model()
    {
        return UserChipLimit::class;
    }

    /**
     * Update or Create User Chip Limit.
     *
     * @param $requestData
     * @param $username
     *
     * @return mixed
     */
    public function updateOrCreateChipLimit($requestData, $username)
    {
        $user = $this->userRepository->getUserInformation($username)->firstOrFail();
        $game = Game::whereGameCode($requestData->game_code)->firstOrFail();
        $userType = UserType::whereUserTypeName($requestData->user_type)->firstOrFail();

        $userChipLimit = $this->model->whereUserId($user->id)
            ->whereUserTypeId($userType->id)
            ->whereGameTypeId($game->game_type_id)
            ->first();

        if ($userChipLimit) {
            $userChipLimit->min = $requestData->min;
            $userChipLimit->max = $requestData->max;
            $userChipLimit->save();
            return true;
        }

        $userChipLimit = new UserChipLimit();
        $userChipLimit->user_id = $user->id;
        $userChipLimit->user_type_id = $userType->id;
        $userChipLimit->game_type_id = $game->game_type_id;
        $userChipLimit->min = $requestData->min;
        $userChipLimit->max = $requestData->max;
        $userChipLimit->save();
        return true;
    }

    /**
     * Get chip limit of user by currency id, game type and user.
     *
     * @param $gameCode
     * @param $userTypeName
     * @param $username
     * @return array
     * @throws \App\Exceptions\RepositoryInternalException
     */
    public function getChipLimit($gameCode, $userTypeName, $username): array
    {
        // $user = $this->user->where($this->user->getFieldName('username'), $username)->firstOrFail();
        $user = $this->userRepository->getUserInformation($username)->firstOrFail();
        $game = Game::whereGameCode($gameCode)->firstOrFail();

        $userChipLimit = $this->getChipLimitByUserGameTypeAndCurrency($user, $userTypeName, $game->game_type_id);

        if ($userChipLimit) {
            return $userChipLimit;
        }

        $currency = (new CurrencyRepository)->getCurrencyByCurrencyCode($user->currency)->first()->id ?? null;

        $defaultUserChipLimit = $this->getDefaultChipLimitByGameTypeAndCurrency($currency, $game->game_type_id);

        return $defaultUserChipLimit;
    }

    /**
     * Get chip limit by user and gameType
     *
     * @param $user
     * @param $userTypeName
     * @param $gameTypeId
     *
     * @return mixed
     */
    public function getChipLimitByUserGameTypeAndCurrency($user, $userTypeName, $gameTypeId)
    {
        $userType = UserType::whereUserTypeName($userTypeName)->firstOrFail();
        $userChipLimit = $this->model->whereUserId($user->id)
            ->whereUserTypeId($userType->id)
            ->whereGameTypeId($gameTypeId)
            ->first();

        if ($userChipLimit) {
            return [
                'min' => $userChipLimit->min,
                'max' => $userChipLimit->max,
            ];
        }

        return false;
    }

    /**
     * Get default chip limit by currency and gameType
     *
     * @param $currencyId
     * @param $gameTypeId
     *
     * @return mixed
     */
    public function getDefaultChipLimitByGameTypeAndCurrency($currencyId, $gameTypeId)
    {
        $userChipLimitDefaults = UserChipLimitDefaults::where('game_type_id', $gameTypeId)
            ->where('currency_id', $currencyId)
            ->first();

        if (!$userChipLimitDefaults) {
            $userChipLimitDefaults = UserChipLimitDefaults::where('game_type_id', $gameTypeId)
                ->whereNull('currency_id')
                ->first();
        }

        return [
            'min' => $userChipLimitDefaults->min,
            'max' => $userChipLimitDefaults->max,
        ];
    }
}
