<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;

class KeyGenerateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'key:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Set the application key";

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $keyword1 = 'APP_KEY=';
        $keyword2 = 'JWT_SECRET=';

        $key1 = $keyword1 . $this->getRandomKey() . PHP_EOL;
        $key2 = $keyword2 . $this->getRandomKey() . PHP_EOL;

        $path = base_path() . '/.env';

        if (file_exists($path)) {
            $lines = file($path);
            foreach ($lines as $line) {
                if (strpos($line, $keyword1) !== false) {
                    file_put_contents(
                        $path,
                        str_replace($line, $key1, file_get_contents($path))
                    );
                }
                if (strpos($line, $keyword2) !== false) {
                    file_put_contents(
                        $path,
                        str_replace($line, $key2, file_get_contents($path))
                    );
                }
            }
        }

        $this->info("Application key [$key1] set successfully.");
        $this->info("Auth key [$key2] set successfully.");
    }

    /**
     * Generate a random key for the application.
     *
     * @return string
     */
    protected function getRandomKey()
    {
        return Str::random(32);
    }
}
