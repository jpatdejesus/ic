<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class PermissionInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed new permission and give it to default role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(PHP_EOL . 'Cache clearing...' . PHP_EOL);
        $this->call('cache:clear');
        $this->info(PHP_EOL . 'Seeding CLIENT data...' . PHP_EOL);
        $this->info(PHP_EOL . 'Seeding new permissions...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'PermissionTableSeeder']);
        $this->info(PHP_EOL . 'Seeding new roles...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'RoleTableSeeder']);
        $this->info(PHP_EOL . 'Seeding default permission for default roles...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'RoleHasPermissionTableSeeder']);
        $this->info(PHP_EOL . 'Seeding ADMIN data...' . PHP_EOL);
        $this->info(PHP_EOL . 'Seeding new admin permissions...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'AdminPermissionTableSeeder']);
        $this->info(PHP_EOL . 'Seeding new admin roles...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'AdminRoleTableSeeder']);
        $this->info(PHP_EOL . 'Seeding default admin permission for default roles...' . PHP_EOL);
        $this->call('db:seed', ['--class' => 'AdminRoleHasPermissionTableSeeder']);
    }
}
