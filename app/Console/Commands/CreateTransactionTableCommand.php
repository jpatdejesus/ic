<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;

class CreateTransactionTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions_table:create {--game_code=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute create transaction SQL files with option --game_code={file_name}.' .
        'Table name will be named {game_code}_transactions.';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */

    public $dbConnectionName = 'datarepo';

    /**
     * Game code to be used as table prefix
     *
     * @var String
     */
    public $gameCode;

    /**
     * Sql file path
     *
     * @var String
     */
    public $filePath;

    

    public function handle()
    {
        $dbConnectionName = 'datarepo';

        $this->filePath = database_path('sql/datarepo/2019_03_12_051146_create_transactions_table.sql');

        if (!$this->option('game_code')) {
            $this->warn('game_code option is required. Add --game_code="GAME_CODE_HERE" option.');
            return false;
        }

        if (!file_exists($this->filePath)) {
            $errorMessage = 'Error: Cannot find create_transactions_table.sql.';
            $this->error($errorMessage);
            Log::error($errorMessage);
            return false;
        }

        try {
            $this->info('Executing Create Transaction Table SQL file/s...' . PHP_EOL);

            $this->gameCode = $this->option('game_code');
            
            DB::connection($dbConnectionName);
            DB::connection($dbConnectionName)->beginTransaction();
            DB::connection($dbConnectionName)->statement('SET FOREIGN_KEY_CHECKS = 0'); //turn off referential integrity
            
            $this->updateCreateTransactionSqlFile();

            DB::connection($dbConnectionName)->unprepared(file_get_contents($this->filePath));

            $this->revertCreateTransactionSqlFile();

            DB::connection($dbConnectionName)->statement('SET FOREIGN_KEY_CHECKS = 1'); //turn referential integrity back on
            DB::connection($dbConnectionName)->commit();

            $this->info('Transaction table successfully created...');
        } catch (\Exception $exception) {
            if (DB::transactionLevel() > 0) {
                DB::rollBack();
            }

            $this->error($exception->getMessage());

            Log::error($exception->getTraceAsString());

            return false;
        }
    }

    private function updateCreateTransactionSqlFile()
    {
        file_put_contents(
            $this->filePath,
            str_replace('{GAME_CODE}', $this->gameCode, file_get_contents($this->filePath))
        );
    }

    private function revertCreateTransactionSqlFile()
    {
        file_put_contents(
            $this->filePath,
            str_replace($this->gameCode, '{GAME_CODE}', file_get_contents($this->filePath))
        );
    }
}
