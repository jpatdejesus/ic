<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;

class BuildDatabaseCommand extends Command
{
    const ACTION_CREATE = 'create';
    const ACTION_ALTER = 'alter';
    const ACTION_DROP = 'drop';
    const ACTION_FUNCTION = 'function';
    const ACTION_PROCEDURE = 'procedure';
    const INSERT_DATA = 'insert';
    const QUERY_DISABLE_CONSTRAINT = 'SET FOREIGN_KEY_CHECKS = 0';
    const QUERY_ENABLE_CONSTRAINT = 'SET FOREIGN_KEY_CHECKS = 1';
    const PREFIX_GAME_CODE = '{GAME_CODE}';
    const MIGRATIONS_TABLE = '2019_03_06_000000_create_migrations_table.sql';
    const CREATE_TRANSACTIONS_TABLE = 'create_transactions_table';
    const ALTER_TRANSACTIONS_TABLE = 'alter_transactions_table';
    const BUILD_CONFIG = 'build.config';
    const TABLES_CONFIG = 'tables.config';
    const CONFIG_FILES = [
        self::BUILD_CONFIG,
        self::TABLES_CONFIG
    ];
    const CREATE_LOGS_TABLE = '2019_04_03_063122_create_logs_table.sql';

    private $validActions = [self::ACTION_CREATE, self::ACTION_ALTER, self::ACTION_DROP, self::ACTION_FUNCTION, self::ACTION_PROCEDURE, self::INSERT_DATA];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:db {--file=} {--refresh} {database?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute SQL files, add options --file={file_name} to execute specific sql file or ' .
    '--refresh (!production env) to rebuild the entire database but will remove all records in each tables';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle()
    {
        if ($this->option('refresh')) {
            if (config('app.env') === 'production') {
                $this->error("Command with '{--refresh}' parameter is not available in production.");

                return false;
            }

            if (!$this->option('no-interaction')) {
                $confirmation = $this->ask(
                    'This will truncate all of your existing records, ' . PHP_EOL
                    . 'Do you wish to continue? (y/N)'
                );

                if (strtolower($confirmation) !== 'y') {
                    $this->info('Database builder has been cancelled.');

                    return false;
                }
            }

            foreach (config('database.connections') as $databaseName => $connections) {
                DB::connection($databaseName)->beginTransaction();
                $tables = [];

                if ($this->isTableExist($databaseName, 'migrations')) {
                    $tables = DB::connection($databaseName)
                        ->table('migrations')
                        ->select(['table_name'])
                        ->where(['action' => 'create'])
                        ->pluck('table_name');
                }

                if (file_exists(database_path(self::TABLES_CONFIG))) {
                    $tables = file(database_path(self::TABLES_CONFIG));
                }

                if (count($tables) > 0) {
                    foreach ($tables as $table) {
                        DB::connection($databaseName)->statement(self::QUERY_DISABLE_CONSTRAINT);
                        DB::connection($databaseName)->statement("DROP TABLE IF EXISTS {$table}");
                        DB::connection($databaseName)->statement(self::QUERY_ENABLE_CONSTRAINT);
                    }
                }

                DB::connection($databaseName)->statement("DROP TABLE IF EXISTS migrations");
                DB::connection($databaseName)->commit();
            }

            if (file_exists(database_path(self::BUILD_CONFIG))) {
                unlink(database_path(self::BUILD_CONFIG));
            }
            if (file_exists(database_path(self::TABLES_CONFIG))) {
                unlink(database_path(self::TABLES_CONFIG));
            }

            $this->comment(PHP_EOL . "If no errors showed up, all tables were dropped" . PHP_EOL);
        }

        try {
            $this->info('Executing SQL file/s...' . PHP_EOL);

            foreach (self::CONFIG_FILES as $config) {
                if (!$this->createConfigFile($config)) {
                    $this->error('Make file "' . $config . '" permission denied.');

                    return false;
                }
            }

            if (!$this->option('file')) {
                foreach (glob(database_path('sql/*')) as $database) {
                    $database = str_replace(database_path('sql/'), '', $database);

                    DB::connection($database)->beginTransaction();

                    foreach (glob(database_path("sql/{$database}/*.sql")) as $file) {
                        if ($this->isCreateLogTable($file)) {
                            continue;
                        }

                        $fileName = str_replace(database_path("sql/{$database}/"), '', $file);

                        if ($this->isFileExecuted($fileName) &&
                            (strpos($fileName, 'function') == false
                                || strpos($fileName, 'procedure') !== false)) {
                            continue;
                        }

                        if (!$this->isFileNotForTable($fileName)) {
                            $this->writeConfig($fileName);
                        }

                        $action = str_replace('_table.sql', '', substr($fileName, 18));
                        $action = (explode('_', $action))[0];

                        if (!in_array($action, $this->validActions)) {
                            $this->error("Invalid action '{$action}' for file -> {$fileName}.");
                            continue;
                        }

                        if (!$this->isTransactionsTable($fileName)) {
                            $this->info("Executing SQL file -> {$fileName} in {$database} database...");
                        }

                        $action = str_replace('_table.sql', '', substr($fileName, 18));
                        $action = (explode('_', $action))[0];

                        if (!in_array($action, $this->validActions)) {
                            $this->error("Invalid action '{$action}' for file -> {$fileName}.");
                            continue;
                        }

                        switch ($action) {
                            case self::ACTION_CREATE:
                                $tableName = str_replace('_table.sql', '', substr($fileName, 25));
                                break;
                            case self::ACTION_ALTER:
                                $tableName = str_replace('_table.sql', '', substr($fileName, 24));
                                break;
                            case self::ACTION_DROP:
                                $tableName = str_replace('_table.sql', '', substr($fileName, 23));
                                break;
                            case self::ACTION_FUNCTION:
                                $tableName = str_replace('_table.sql', '', substr($fileName, 27));
                                break;
                            case self::ACTION_PROCEDURE:
                                $tableName = str_replace('_table.sql', '', substr($fileName, 28));
                                break;
                        }

                        DB::connection($database)->statement(self::QUERY_DISABLE_CONSTRAINT);

                        if ($this->isTransactionsTable($fileName)) {
                            $gameCodes = collect(config('ichips.default_games'))->pluck(['game_code']);

                            foreach ($gameCodes as $gameCode) {
                                $migration = str_replace("{$action}_transactions", "{$action}_{$gameCode}_transactions", $fileName);

                                $table = $gameCode . '_' . $tableName;

                                if (!$this->isTableExist($database, $table) && $action === self::ACTION_ALTER) {
                                    continue;
                                }

                                $this->info("Executing SQL file -> {$migration} in {$database} database...");

                                file_put_contents(
                                    $file,
                                    str_replace(self::PREFIX_GAME_CODE, $gameCode, file_get_contents($file))
                                );

                                DB::connection($database)->unprepared(file_get_contents($file));

                                if ($action === self::ACTION_CREATE) {
                                    $this->writeConfig($table, true);
                                } elseif ($action === self::ACTION_DROP) {
                                    $this->writeConfig($table, true, true);
                                }

                                file_put_contents(
                                    $file,
                                    str_replace($gameCode, self::PREFIX_GAME_CODE, file_get_contents($file))
                                );

                                $this->info('SQL file -> ' . $migration . ' successfully executed.');
                            }
                        } else {
                            DB::connection($database)->unprepared(file_get_contents($file));

                            if ($action === self::ACTION_ALTER) {
                                if (!$this->isTableExist($database, $tableName)) {
                                    continue;
                                }
                            }

                            if (!$this->isFileNotForTable($fileName) && $action === self::ACTION_CREATE) {
                                $this->writeConfig($tableName, true);
                            } elseif ($action === self::ACTION_DROP) {
                                $this->writeConfig($tableName, true, true);
                            }
                        }

                        DB::connection($database)->statement(self::QUERY_ENABLE_CONSTRAINT);
                        DB::connection($database)->commit();

                        if (!$this->isTransactionsTable($fileName)) {
                            $this->info('SQL file -> ' . $fileName . ' successfully executed.');
                        }
                    }
                }
            } else {
                if (!$this->argument('database')) {
                    $this->error('Database is not set.');

                    return false;
                }

                $database = $this->argument('database');
                $file = database_path('sql/') . rtrim($database, '/') . '/' . $this->option('file');
                $fileName = $this->option('file');

                if (!$this->isFileExecuted($fileName)) {
                    $this->writeConfig($fileName);

                    $tableName = str_replace('_table.sql', '', substr($fileName, 25));

                    if (!File::exists($file)) {
                        throw new \Exception("SQL file {$file} does not exist.");
                    }

                    $this->info("Executing SQL file -> {$fileName} in {$database} database...");

                    DB::connection($database);
                    DB::connection($database)->beginTransaction();
                    DB::connection($database)->statement(self::QUERY_DISABLE_CONSTRAINT);
                    DB::connection($database)->unprepared(file_get_contents($file));
                    DB::connection($database)->statement(self::QUERY_ENABLE_CONSTRAINT);
                    DB::connection($database)->commit();

                    if (!$this->isFileNotForTable($fileName)) {
                        $this->writeConfig($tableName, true);
                    }
                }
                $this->info('SQL file -> ' . $fileName . ' successfully executed!');
            }
            $this->info(PHP_EOL . 'SQL file/s successfully executed!');
            $this->comment(PHP_EOL . "If no sql files has been executed, it means your database is already up to date");

            return true;
        } catch (\Exception $exception) {
            if (DB::transactionLevel() > 0) {
                DB::rollBack();
            }

            $this->error($exception->getMessage());

            Log::error($exception->getTraceAsString());

            return false;
        }
    }

    private function isFileNotForTable($fileName)
    {
        if (strpos($fileName, 'function') !== false || strpos($fileName, 'procedure') !== false) {
            return true;
        }

        return false;
    }

    private function isTransactionsTable($fileName)
    {
        if (strpos($fileName, self::ALTER_TRANSACTIONS_TABLE) !== false || strpos($fileName, self::CREATE_TRANSACTIONS_TABLE) !== false) {
            return true;
        }

        return false;
    }

    private function isFileExecuted(string $fileName)
    {
        $contents = file_get_contents(database_path(self::BUILD_CONFIG));

        $pattern = preg_quote($fileName, '/');
        $pattern = "/^.*$pattern.*\$/m";

        if (preg_match_all($pattern, $contents, $matches)) {
            return true;
        } else {
            return false;
        }
    }

    private function createConfigFile($config)
    {
        if (!file_exists(database_path($config))) {
            if (!fopen(database_path($config), 'w')) {
                return false;
            }
        }
        return true;
    }

    private function writeConfig($writeContent, $isTableConfig = false, $removeAction = false)
    {
        $config = self::BUILD_CONFIG;

        if ($isTableConfig) {
            $config = self::TABLES_CONFIG;
        }

        $lines = file(database_path($config));

        if (sizeof($lines) === 0) {
            file_put_contents(database_path($config), $writeContent);
        } else {
            $contents = file_get_contents(database_path($config));

            if (!$removeAction) {
                $contents .= PHP_EOL . $writeContent;
            } else {
                $contents = str_replace($writeContent . PHP_EOL, '', $contents);
            }

            file_put_contents(database_path($config), $contents);
        }
    }

    private function isTableExist($database, $table)
    {
        $result = DB::connection($database)
            ->table('information_schema.tables')
            ->select(['table_name'])
            ->where(['table_schema' => $database, 'table_name' => $table])
            ->get();

        if (count($result) > 0) {
            return true;
        }
        return false;
    }

    private function isCreateLogTable($file)
    {
        return (strpos($file, self::CREATE_LOGS_TABLE) !== false);
    }
}
