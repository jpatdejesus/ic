<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class MakeSqlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sql {file_name} {database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create sql file with parameters {file_name} {database}';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle()
    {
        if (!$this->argument('file_name')) {
            $this->error('SQL file name is not set.');
            return false;
        }

        if (!$this->argument('database')) {
            $this->error('Database is not set.');
            return false;
        }
        $database = $this->argument('database');

        if (!File::exists(database_path('sql/'))) {
            mkdir(database_path('sql/'));
        }

        $databasePath = database_path('sql/'. $database);
        if (!File::exists($databasePath)) {
            mkdir($databasePath);
        }

        $date = Carbon::now()->format('Y_m_d_His_');
        $fileName = $date . $this->argument('file_name') . '.sql';
        $file = $databasePath . '/' . $fileName;
        $file = fopen($file, 'w');
        fclose($file);

        $this->info("Database '{$database}' added SQL file '{$fileName}' successfully.");

        return true;
    }
}
