<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\KeyGenerateCommand::class,
        Commands\BuildDatabaseCommand::class,
        Commands\IChipsInstallCommand::class,
        Commands\MakeSqlCommand::class,
        Commands\MakeTraitCommand::class,
        Commands\CreateTransactionTableCommand::class,
        Commands\PermissionInstallCommand::class,
        Commands\StorageLinkCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
