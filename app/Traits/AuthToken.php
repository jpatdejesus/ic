<?php

namespace App\Traits;

use App\Models\v1\AdminUser;
use App\Models\v1\RoleCredential;

//TODO::should be move to tests folder so that this trait can be use for test cases only
trait AuthToken
{
    /**
     * Generate token
     *
     * @param $user
     * @param string $appType
     * @return mixed
     */
    public function loginAs($user, $appType = 'CLIENT')
    {
        if ($appType === strtoupper(config('ichips.app_types.client'))) {
            $args = $user->only(['username', 'password']);
            $response = $this->put('v1/auth/token', $args);
        } else {
            $args = $user->only(['email', 'password']);
            $response = $this->put('v1/admin/auth/token', $args);
        }

        return json_decode($response->response->getContent())->data;
    }

    /**
     * @param string $appType
     * @return array
     */
    public function login($appType = 'CLIENT')
    {
        $user = RoleCredential::where('username', 'ichips')->first();

        if ($appType === strtoupper(config('ichips.app_types.admin'))) {
            $email = config('ichips.admin_default_roles_and_users.SUPER_ADMIN.super_admin');
            $user = AdminUser::where('email', $email)->first();
        }

        $user->password = 'password123!';
        return ['token' => $this->loginAs($user)->token];
    }
}
