<?php

namespace App\Traits;

use App\Models\v1\AdminUser;

trait AdminAuthToken
{

    /**
     * Generate token
     *
     * @param $user
     * @return mixed
     */
    public function loginAs($user)
    {
        $args = $user->only(['email', 'password']);

        $response = $this->put('v1/admin/auth/token', $args);

        return json_decode($response->response->getContent())->data;
    }

    public function login()
    {
        $user = AdminUser::where('email', 'superadmin@nomail.com')->first();
        $user->password = 'password123!';
        return ['token' => $this->loginAs($user)->token];
    }
}
