<?php

namespace App\BaseModels\v1;

use App\BaseModels\v1\BaseModel;

class IChipsAdmin extends BaseModel
{
    protected $connection = 'ichips-admin';
}
